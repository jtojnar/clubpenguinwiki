{{MinigameInfobox
|name= Pizzatron 3000
|image= File:Pizzatron3000 Logo.PNG
|caption= The Game Logo.
|players= 1
|room= [[Pizza Parlor]]
|date= February 26, 2007
|stamps= Yes (10)
}}
{{cquote2|I hope I don't have to eat all these pizzas. Otherwise I'll be stuffed as my [[Stamp Book]]!|A Pizza Chef from The [[Club Penguin Times]].}}
'''Pizzatron 3000''' is a minigame that is played in the [[Pizza Parlor]]. It was created by ''[[Club Penguin]]''<nowiki>'</nowiki>s inventor and scientist, [[Gary the Gadget Guy]] and released on February 26, 2007. An alternative level of this game is hidden at the start of the game and is called [[Candytron 3000]] (Cookietron 3000 in [[Club Penguin (app)|Club Penguin app]]), substituting pizza ingredients with candy. [[Stamp]]s were released for the game on April 26, 2011.

When playing, the player must look at a sign on which the current order is displayed. Then, he/she must make the correct type of [[pizza]]. The correct type of [[pizza]] must exactly match the type of sauce and number of toppings shown on the sign. Every correct pizza is worth five coins. If the player makes a mistake, the incorrectly made pizza will not sell, but he or she will have a chance to remake that pizza. If you incorrectly make a pizza, no coins are subtracted from your score. After making five correct pizzas in a row, a bonus tip is given. Tips start at ten coins per pizza and increase by five additional [[coin]]s for every five correct pizzas in a row. If the player makes five mistakes or forty pizzas in total (including incorrect pizzas), the game is over, and the [[coin]]s are given to the player.

== Candytron 3000 ==
''See main article, [[Candytron 3000]].''

To go to the secret level, you will have to click on the lever at the right side of the Pizzatron 3000 on the starting screen. Then, click Start. It will not show as Candytron 3000 on the starting page or the instructions page.

{{Tip|You earn more coins with Candytron 3000, but it is harder.}}

==Stamps==
====Easy====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Food Fiasco stamp]]||Make a mess of the kitchen with 3 wrong pizzas.
|-
|[[Just Dessert stamp]]||Play Pizzatron in Candy Mode.
|}

====Medium====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Chef's Hat stamp]]||Make 20 pizzas without any mistake.
|-
|[[Spice Sea stamp]]||Make 3 hot sauce and shrimp pizzas to order.
|-
|[[Cocoa Beans stamp]]||Make 3 jellybean and chocolate pizzas to order.
|}

====Hard====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Fiery Squids stamp]]||Make 3 hot sauce and squid pizzas to order.
|-
|[[Candy Land stamp]]||Make 3 pink sauce and marshmallow pizzas to order.
|-
|[[Pizza Chef stamp]]||Make 30 pizzas without any mistakes.
|}

====Extreme====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Pizza Master stamp]]||Make 40 pizzas without any mistake.
|-
|[[Dessert Chef stamp]]||Make 40 candy pizzas without any mistake.
|}

==Trivia==
*At the [[Halloween Parties|Halloween Party 2007]], the candy version of Pizzatron 3000, [[Candytron 3000]], was released.
*When trying to find your buddy who is playing Pizzatron 3000, it will show as "(Name) is making pizzas".
*The music during the minigame is called "Extra Anchovies" in the Igloo Music list.
*The highest amount of coins possible, without making any mistakes, is 1085 coins; in [[Candytron 3000]], the maximum amount is 1285 coins.
*Candytron 3000 was mentioned in the [[Club Penguin Times]] years after its release.

==Gallery==
===Starting Screen===
<gallery>
File:PizzatronNewStartScreen.PNG|The starting screen.
File:Candytronlever.png|Where the lever is to play [[Candytron 3000]].
File:StartupPizzatron.jpg|The old starting screen.
File:Pizzatron3000.PNG|Where the lever was to play Candytron 3000 on the old sarting screen.
File:Pizzatron3000ClubPenguinAppStart.png|The starting screen on the [[Club Penguin (app)|Club Penguin]] app.
</gallery>
===Gameplay===
<gallery>
Image:Pizzatron.PNG|Gameplay of Pizzatron 3000.
File:Pizzatron3000ClubPenguinApp.png|Gameplay of Pizzatron 3000 on the Club Penguin app.
File:Candytron 3000.jpg|Gameplay of Candytron 3000
</gallery>
===Other===
<gallery>
Image:Tron.PNG|When you try to find your buddy when he/she is playing this game.
Image:Gary pizza.PNG|Gary after the conclusion of his invention, the Pizzatron 3000.
Image:Perfect Score!.jpg|A perfect score in the standard mode.
File:Secret.jpg|A penguin pulling the secret lever.
</gallery>

==SWFs==
*[http://archives.clubpenguinwiki.info/static/images/archives/d/df/Music106.swf Music]

{{SWFArchives}}

== See also ==
*[[Hot Sauce]]
*[[Candytron 3000]]
*[[Pizza]]
*[[List of Gary's Inventions]]
*[[Gary]]
*[[Pizza Parlour]]

{{Games}}

[[Category:Games]]
[[Category:Job Games]]
[[Category:Gary's inventions]]
