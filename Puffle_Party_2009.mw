{{Archive}}
{{PartyInfoBox
|name = Puffle Party 2009
|image = File:Puffle-party.png
|imagesize= 200px
|caption = The logo of the party.
|membersonly = No
|when = February 20–24, 2009
|freeitems = [[Puffle Bandana]]
|wherehappening = [[Club Penguin Island|Everywhere]]
|famouspenguins = None
|previous = [[Winter Fiesta 2009]]
|next = [[St. Patrick's Day Party 2009]]
}}

The '''Puffle Party 2009''' was the first annual celebration of the [[puffle]]s in ''[[Club Penguin]]''. Many of the rooms throughout the [[Club Penguin Island|island]] had been decorated for this puffle celebration. Some of the highlights of the party are [[Puffle Feeding Area]], the ball pit in the [[Forest]], and the fact that each colored puffle had its own fort at [[Snow Forts]]. Whenever you walked a certain color puffle, the balloons in areas such as the [[Beach]] would change to that puffle's color, and so would the sign outside the [[Pet Shop]].

==Free Items==
One free item was made available for this party.
<gallery>
File:Puffle_Bandana.png|link=Puffle Bandana|'''[[Puffle Bandana]]'''<br/>[[Cove]]
</gallery>

==Domains==
Each color puffle had its own room dedicated to them, or in other words, their domain. Their domains were:
{|border="1" class="wikitable" 
!'''Puffle'''!!'''Room'''
|-
|bgcolor="666666"|'''[[Black Puffle|<span style="color:#FFFFFF">Black</span>]]'''||[[Cave]]
|-
|bgcolor="3bdcff"|'''[[Blue Puffle|<span style="color:#000000">Blue</span>]]'''||[[Forest]]
|-
|bgcolor="3fcc00"|'''[[Green Puffle|<span style="color:#000000">Green</span>]]'''||[[Beacon]]
|-
|bgcolor="feccff"|'''[[Pink Puffle|<span style="color:#000000">Pink</span>]]'''||[[Iceberg]]
|-
|bgcolor="a85fd1"|'''[[Purple Puffle|<span style="color:#FFFFFF">Purple</span>]]'''||[[Night Club]]
|-
|bgcolor="ec4244"|'''[[Red Puffle|<span style="color:#000000;">Red</span>]]'''||[[Cove]]
|-
|bgcolor="ffff00"|'''[[Yellow Puffle|<span style="color:#000000;">Yellow</span>]]'''||[[Lighthouse]]
|}

==Gallery==
=== Party ===
<gallery widths="180">
File:PuffleParty09Beach.png|The [[Beach]]
File:PuffleParty09Beacon.png|The [[Beacon]]
File:PuffleParty09Cave.png|The [[Cave]] (open windows)
File:PuffleParty09Cave2.png|The [[Cave]] (closed windows)
File:PuffleParty09Cove.png|The [[Cove]]
Image:Dock_Puffle.jpg|The [[Dock]]
File:PuffleParty09Forest.png|The [[Forest]]
Image:Iceberg_Puffle.jpg|The [[Iceberg]]
File:PuffleParty09Lighthouse.png|The [[Lighthouse]]
File:PuffleParty09Dance.png|The [[Night Club]]
Image:Puffle_feeding.jpg|The [[Puffle Feeding Area]]
Image:Pp2009petshop.png|The [[Pet Shop]]
Image:Pp2009plaza.png|The [[Plaza]]
File:PuffleParty09Forts.png|The [[Snow Forts]]
Image:Pp2009town.png|The [[Town]]
</gallery>

==Trivia==
*All the puffles at the Snow Forts threw snowballs without the use of hands or arms. 
*A new subspecies of puffle was first seen in this party. Every 30 minutes (XX:00 and XX:30) a [[White Puffle|white puffle]] would reveal itself in the [[Dojo Courtyard]] bushes and every 30 minutes (XX:15 and XX:45) it would appear at the [[Ski Hill]] for a few moments.
*Before the party started, ''Club Penguin'' made a short documentary on puffles.

{{SWFArchives}}

==See also==
*[[Puffle Parties]]
*[[Puffle]]
*[[Parties]]
*[[White Puffle]]

{{Party}}

[[Category:Events]]
[[Category:Parties]]
[[Category:Puffles]]
[[Category:Pets]]
[[Category:Parties of 2009]]
[[pt:Festa dos Puffles 2009]]
