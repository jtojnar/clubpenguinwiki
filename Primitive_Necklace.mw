{{ItemInfobox
|name= Primitive Necklace
|image= File:PrimitiveNecklace.png
|available= No
|type= Neck Item
|member= Yes
|party= None
|cost= 100 coins
|found= [[Costume Trunk]]<br>[[Penguin Style]]
|id= 190 
|unlock= No
}}
The '''Primitive Necklace''' is a neck item in ''[[Club Penguin]]''. It cost 100 coins and could be found in the [[Costume Trunk]] catalog. It is available when the play ''[[Penguins that Time Forgot]]'' and ''[[The Vikings That Time Forgot]]'' are at the [[Stage]]. This item is part of Criteroo's costume, along with the [[Zebra Cave Toga]] and [[The Fern Fuzz]]. In August 2012 ''Penguin Style'', it goes along with [[The Flip]] and [[Green Surf Shorts]].

In January 2013, it went with the [[Zebra Cave Toga]] and the [[Big Brow]] in the Costume Trunk.

==History==
The Primitive Necklace is a common item.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan=7|Costume Trunk||June 13, 2008||July 11, 2008
|-
|September 10, 2009||October 8, 2009
|-
|January 27, 2011||February 24, 2011
|-
|February 8, 2012||March 15, 2012
|-
|January 3, 2013||February 13, 2013
|-
|January 15, 2014||February 12, 2014
|-
|July 2, 2014||{{Available|Items}}
|-
|Penguin Style||August 1, 2012||November 1, 2012
|}

==Gallery==
<gallery>
File:PrimitiveNecklace1.PNG|The Primitive Necklace in-game.
File:PrimitiveNecklace2.PNG|The Primitive Necklace on a [[player card]].
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Colar Primitivo
|spanish= Collar primitivo
|french= Le Collier Primitif
|german= Urwald-Kette
|russian= Первобытное ожерелье
}}

==Appearances==
*A [[Stamp Mascots|stamp mascot]] wears this.
*One of the penguins in the [[Happy Birthday postcard]] wears this.
*On the [[Stamp Book postcard]].
==SWFs==
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/190.swf Primitive Necklace (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/190.swf Primitive Necklace (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/190.swf Primitive Necklace (paper)]

[[Category:Items]]
[[Category:Clothing]]
[[Category:Neck Items]]
[[Category:Clothes released in 2008]]
[[Category:Secret Items]]
