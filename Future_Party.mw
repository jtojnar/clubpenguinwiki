{{Archive}}
{{PartyInfobox 
|name = Future Party
|image = File:FuturePartyLogo.PNG
|imagesize = 200px
|caption = Future Party logo
|membersonly = No
|when = May 21, 2014 - June 5, 2014
|freeitems = ''[[Future Party#Free Items|See list]]''
|wherehappening = [[Snow Forts]] and Future [[Club Penguin Island]]
|catalog= [[Future Catalog]]
|famouspenguins = [[Gary]]
|previous= [[Puffle Party 2014]]
|next= [[Penguin Cup]]
}}
The '''Future Party''' was a party on ''[[Club Penguin]]''. It started on May 21, 2014 and ended on June 4, 2014.

==History==
The party was confirmed on April 3 in the membership page <ref>[https://secure.clubpenguin.com/membership/login.php?0 "Gear Up for the Future - Members can get ready for out-of-this-world adventures... "]</ref>. [[Polo Field]] mentioned it during the Field Friday <ref>http://pt.clubpenguinwiki.info/static/images/cpwpt/e/e9/Confirma%C3%A7%C3%A3o_de_Spike_Futuro.png</ref>. [[Megg]] revealed two sneak peeks of the party on the [[What's New Blog]] on April 23<ref>http://www.clubpenguin.com/blog/2014/04/may-party-sneak-peek</ref>. A sneak peek showed how the robots are and other showed a possible new minigame that will be available at the party. On May 7, Polo Field revealed more two sneak peeks of the party on the [[What's New Blog]]<ref>http://www.clubpenguin.com/blog/2014/05/new-mini-game-future-party</ref>.

On May 12, the episode #20 of The Spoiler Alert showed a sneak peek of the [[Snow Forts]] during the construction. Two days later, on May 14, Polo Field revealed one sneak peek of the party on his Twitter <ref>https://twitter.com/polofield/status/466702405648650242/photo/1</ref>. On May 20, the episode #21 of The Spoiler Alert showed some rooms and the items of the party. On the same day, Megg posted on her Twitter account an image of a holographic Fluffy<ref>https://twitter.com/clubmegg/status/468831374460190721</ref>. On May 23, during the party, Polo Field posted on the What's New Blog a video about the [[Protobot]] on the island<ref>http://www.clubpenguin.com/blog/2014/05/video-future-party-short-protobot-returns</ref>.

===In-game development===
On May 1, [[Jet Pack Guy]] said on the issue #445 of the [[Club Penguin Times]] that the Protobot may be active again. On May 8, Gary published an article on the issue #446 of the Club Penguin Times saying that he returned from the future and he met Gary 3000 there. He also said that he saw some robots and these robots were piloted by the Extra-Planetary Federation. On May 15, on the issue #447 of the newspaper, Gary said that Protobot was spotted by Gary 3000 in the future. According to him, in Gary 3000's time, Protobot has rebuilt himself and now he's a huge robot that launches meteors. If Gary 3000 doesn't give in to his demands, he's going to sink the island with them. Gary also said that we must go to the future to destroy the meteors because the Extra-Planetary Federation is busy helping the planet Upzar II. On the same day, a closed time portal appeared on the Snow Forts. On May 22, the time portal was opened and penguins were able to travel to the year of 4014.

==Features==
If a penguin was using the Space Cadet Jetpack (or the [[robos]], for member penguins) and pressed D, it would go to  [[Space|space]]. All players were able to use the respective items and destroy meteors or microbots to win meteor points. Gary also visited the island and gave players the chance to get [[Gary 3000 Giveaway|his background]].
==Free items==
A total of eighteen free items were made available for this party. Items marked with a badge ([[File:Memberbadge.png|20px]]) indicate that the item can only be obtained by members and items marked with a Meteor Point ([[File:MeteorPoints.PNG|20px]]) indicate that the item can only be obtained with [[Meteor Points]].
<gallery>
File:SpaceCadetJetpack.PNG|link=Space Cadet Jetpack|'''[[Space Cadet Jetpack]]'''<br/>[[Future Catalog]]
File:ProtobotHelmet.PNG|link=Protobot Helmet|'''[[Protobot Helmet]]'''<br/>After defeating [[Protobot]]
File:Gary3000GiveawayPlayerCard.png|link=Gary 3000 Giveaway|'''[[Gary 3000 Giveaway]]'''<br/>Meet [[Gary]]
File:ProtobotCostume.PNG|link=Protobot Costume|[[File:Memberbadge.png|20px|Members only]] '''[[Protobot Costume]]'''<br/>After defeating [[Protobot]]
File:FinalFrontierHelmet.PNG|link=Final Frontier Helmet|[[File:MeteorPoints.PNG|20px|Requires 100 Meteor Points.]] '''[[Final Frontier Helmet]]'''<br/>[[Future Catalog]]
File:AsteroidHead.PNG|link=Asteroid Head|[[File:MeteorPoints.PNG|20px|Requires 150 Meteor Points.]] '''[[Asteroid Head]]'''<br/>[[Future Catalog]]
File:ZaryaxVIHat.PNG|link=Zaryax VI Hat|[[File:MeteorPoints.PNG|20px|Requires 150 Meteor Points.]] '''[[Zaryax VI Hat]]'''<br/>[[Future Catalog]]
File:BubbleRayGun.PNG|link=Bubble Ray Gun|[[File:MeteorPoints.PNG|20px|Requires 100 Meteor Points.]] '''[[Bubble Ray Gun]]'''<br/>[[Future Catalog]]
File:CyborgHat.PNG|link=Cyborg Hat|[[File:MeteorPoints.PNG|20px|Requires 100 Meteor Points.]] '''[[Cyborg Hat]]'''<br/>[[Future Catalog]]
File:TheTransmitter.PNG|link=The Transmitter|[[File:MeteorPoints.PNG|20px|Requires 100 Meteor Points.]] '''[[The Transmitter]]'''<br/>[[Future Catalog]]
File:CyberneticArm.PNG|link=Cybernetic Arm|[[File:MeteorPoints.PNG|20px|Requires 150 Meteor Points.]] '''[[Cybernetic Arm]]'''<br/>[[Future Catalog]]
File:PlanetZetaCostume.PNG|link=Planet Zeta Costume|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 350 Meteor Points.]] '''[[Planet Zeta Costume]]'''<br/>[[Future Catalog]]
File:FinalFrontierSuit.PNG|link=Final Frontier Suit|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 350 Meteor Points.]] '''[[Final Frontier Suit]]'''<br/>[[Future Catalog]]
File:AsteroidCostume.PNG|link=Asteroid Costume|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 500 Meteor Points.]] '''[[Asteroid Costume]]'''<br/>[[Future Catalog]]
File:UFOCostume.PNG|link=UFO Costume|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 850 Meteor Points.]] '''[[Planet Zeta Costume]]'''<br/>[[Future Catalog]]
File:StarOutCostume.PNG|link=Star Out Costume|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 250 Meteor Points.]] '''[[Star Out Costume]]'''<br/>[[Future Catalog]]
File:PlanetCostume.PNG|link=Planet Costume|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 300 Meteor Points.]] '''[[Planet Costume]]'''<br/>[[Future Catalog]]
File:WeComeinPeaceCostume.PNG|link=We Come in Peace Costume|[[File:Memberbadge.png|20px|Members only]] [[File:MeteorPoints.PNG|20px|Requires 350 Meteor Points.]] '''[[We Come in Peace Costume]]'''<br/>[[Future Catalog]]
</gallery>

==Gallery==
===Party Pictures===
<gallery widths="180">
File:FuturePartyParty7.PNG|The [[Beach (Future Party)|Beach]]
File:FuturePartyParty3.PNG|The [[Blast Off Bistro]]
File:FuturePartyParty4.PNG|The [[Dance Dome]]
File:FuturePartyParty1.PNG|The [[Future Forts]]
File:FuturePartyParty2.PNG|The [[Future Town]]
File:FuturePartyParty6.PNG|The [[Interstellar Zoo]]
File:FuturePartyParty5.PNG|The [[Robo Shop]]
File:FuturePartyForts.PNG|The [[Snow Forts]]
File:FuturePartyParty9.PNG|The [[Space (Future Party)|Space]]
File:FuturePartyParty8.PNG|The [[Space Academy]]
File:FuturePartyParty11.PNG|The [[Space Break]]
</gallery>

===Sneak Peeks===
<gallery widths=180>
File:FuturePartySneakPeek1.PNG|A sneak peek posted on the [[What's New Blog]] of the robots.
File:FuturePartySneakPeek2.PNG|A sneak peek posted on the What's New Blog of the new minigame.
File:FuturePartySneakPeek3.PNG|A sneak peek posted on the What's New Blog the new minigame.
File:FuturePartySneakPeek4.PNG|A sneak peek posted on the What's New Blog the new minigame.
File:FuturePartySneakPeek5.PNG|A sneak peek of the construction of the party revealed during an episode of The Spoiler Alert.
File:FuturePartySneakPeek6.PNG|A sneak peek posted on Polo Field's Twitter account.
File:FuturePartySneakPeek7.PNG|A sneak peek of the party revealed during an episode of The Spoiler Alert.
File:FuturePartySneakPeek8.PNG|A sneak peek of the robots revealed during an episode of The Spoiler Alert.
File:FuturePartySneakPeek9.PNG|A sneak peek of the catalog revealed during an episode of The Spoiler Alert.
File:FuturePartySneakPeek10.PNG|Another sneak peek of the party revealed during an episode of The Spoiler Alert.
File:FuturePartySneakPeek11.PNG|Another sneak peek of the party revealed during an episode of The Spoiler Alert.
File:FuturePartySneakPeek12.PNG|A sneak peek posted on Megg's Twitter account.
</gallery>

===Login and Exit Screens===
<gallery widths=180>
File:FuturePartyLoginScreen1.PNG|The first [[Login Screen]].
File:FuturePartyExitScreen1.PNG|The first [[Exit Screen]].
</gallery>

===Construction===
<gallery widths=180>
File:FuturePartyPreForts.PNG|The [[Snow Forts]]
</gallery>

===Miscellaneous===
<gallery widths=180>
File:FuturePartyHomepage1.PNG|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:FuturePartyFacebookHeader.PNG|The Club Penguin's Facebook Header.
File:MapFutureParty.PNG|The map of the future island.
</gallery>

==Videos==
<youtube>C7xdxnCr6jg</youtube>

==Names in other languages==
{{OtherLanguage
|portuguese= Festa do Futuro
|spanish= Viaje al futuro
|french= Fête du Futur
|german= Zukunfts-Party
|russian= Праздник Будущего
}}

==References==
{{reflist}}
{{SWFArchives}}

{{Party}}
[[Category:Parties of 2014]]
[[pt:Festa do Futuro]]
