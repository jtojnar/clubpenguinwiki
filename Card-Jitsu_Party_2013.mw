{{Archive}}
{{PartyInfoBox
|name=Card-Jitsu Party 2013
|image= File:CardJitsuParty2013Village.PNG
|imagesize = 250px
|caption= The [[Ski Village]] during the party.
|membersonly= No
|when= May 22, 2013 - June 5, 2013
|freeitems= '''''Non-Member'''''<br />[[Fire Headband]], [[Snow Headband]], [[Water Headband]], [[Sensei's Bonsai Giveaway]]
----
'''''Member'''''<br />[[Fire Training Plates]], [[Snow Training Plates]], [[Water Training Plates]]
|famouspenguins = [[Sensei]]
|catalog= [[Card-Jitsu Party Catalog]]
|wherehappening= [[Club Penguin Island]]
|previous=[[Marvel Super Hero Takeover 2013]]
|next= [[Monsters University Takeover]]
}}The '''Card-Jitsu Party 2013''' was a party in ''[[Club Penguin]]''. It was the second [[Card-Jitsu Party]]. It started on May 22, 2013 and ended on June 5, 2013. It was celebrating the release of [[Card-Jitsu Snow]] along with the [[Snow Dojo]], and it was confirmed through the [[Club Penguin Media Summit]].<ref>http://clubpenguinmemories.com/2013/04/club-penguin-card-jitsu-snow-party-sneak-peeks/</ref>

==Development==
===History===
The Card-Jitsu Party 2013 was first confirmed through the Club Penguin Media Summit, featuring 3 rooms for the party. The Snow Dojo, Ski Hill and Ski Village. On Twitter, Spike Hike showed 2 images of Sensei in a blizzard. On May 1<sup>st</sup> an [[Exit Screen]] and [[Login Screen]] were created to advertise the party. In the [[What's New Blog]] an image of the [[Dojo Courtyard]] in a blizzard was featured in a blog post. On May 7, [[Polo Field]] announced that the [[Card-Jitsu Fire]] and [[Card-Jitsu Water|Water]] would be available for all players permanently.<ref>http://www.clubpenguin.com/blog/2013/05/card-jitsu-special-announcement - "Starting this week, Card-Jitsu Fire and Card-Jitsu Water will be open to everyone." - Polo Field.</ref> On May 15, 2013, snow drifts piled up in [[Dojo Courtyard]] and also in [[Ninja Hideout]] due to the storm that had occurred. During May 17–20, a four-parts sneak peek-series was posted on Club Penguin's YouTube channel, entitled "Polo and Daffo's Card-Jitsu Snow Quest".

==Free items==
A total of seven free items were made available for this party. Items marked with a badge ([[File:Memberbadge.png|20px]]) indicate that the item can only be obtained by members.
<gallery>
File:FireHeadband.PNG|link=Fire Headband|'''[[Fire Headband]]'''<br/>[[Card-Jitsu Party Catalog]]
File:SnowHeadband.PNG|link=Snow Headband|'''[[Snow Headband]]'''<br/>[[Card-Jitsu Party Catalog]]
File:WaterHeadband.PNG|link=Water Headband|'''[[Water Headband]]'''<br/>[[Card-Jitsu Party Catalog]]
File:Sensei'sBonsaiGiveawayIcon.PNG|link=Sensei's Bonsai Giveaway|'''[[Sensei's Bonsai Giveaway]]'''<br/>Meet [[Sensei]]
File:FireTrainingPlates.PNG|link=Fire Training Plates|[[File:Memberbadge.png|20px|Members only]] '''[[Fire Training Plates]]'''<br/>[[Card-Jitsu Party Catalog]]
File:SnowTrainingPlates.PNG|link=Snow Training Plates|[[File:Memberbadge.png|20px|Members only]] '''[[Snow Training Plates]]'''<br/>[[Card-Jitsu Party Catalog]]
File:WaterTrainingPlates.PNG|link=Water Training Plates|[[File:Memberbadge.png|20px|Members only]] '''[[Water Training Plates]]'''<br/>[[Card-Jitsu Party Catalog]]
</gallery>

==Gallery==
===Sneak Peeks===
<gallery>
File:Cjspartysnowdojo.jpg|The [[Snow Dojo]].
File:Cj-snow-ski-hill.jpg|The [[Ski Hill]].
File:Cj-snow-ski-village.jpg|The [[Ski Village]].
File:Snowdojofinal.png|The Snow Dojo.
File:Snowpartymountain.png|The Ski Hill.
File:Snowparty.png|The Ski Village.
File:CardJitsuSnowPartySensei.PNG|"Snow is Coming"
File:CardJitsuSnowPartySensei2.PNG|"Snow is Coming"
File:CardJitsuSnowPartyDojoExt.jpg|The [[Dojo Courtyard]].
File:CardJitsuSnowCourt.PNG|Another sneak peek of the [[Dojo Courtyard]].
File:CardJitsuSnowSenseiDrawing.jpg|A drawing of [[Sensei]].
File:CardJitsuSnowPartyDojoExt2.jpg|A section of the [[Dojo Courtyard]].
File:CardJitsuSnowPartyDojoExt3.jpg|Another sneak peek of the [[Dojo Courtyard]].
File:CardJitsuSnowForts.PNG|The [[Snow Forts]].
File:CardJitsuSnowPizza.PNG|The [[Pizza Parlor]].
File:CardJitsuSnowDojo.PNG|The [[Dojo]].
File:CardJitsuSnowParty1.PNG|The [[Ninja Headquarters]].
File:CardJitsuSnowParty2.PNG|The [[Ski Hill]].
</gallery>

===Login and Exit Screen===
<gallery widths=180>
File:CardJitsuSnowLoginScreen.PNG|The first [[Login Screen]].
File:CardJitsuSnowLoginScreen2.PNG|The second [[Login Screen]].
File:CardJitsuSnowLoginScreen3.PNG|The third [[Login Screen]].
File:CardJitsuSnowExitScreen.PNG|The first [[Exit Screen]].
File:CardJitsuSnowExitScreen2.PNG|The second [[Exit Screen]].
</gallery>

===Rooms===
====Construction====
<gallery widths=180>
File:CardJitsuParty2013ConsDojoExt.PNG|The [[Dojo Courtyard]] (Week 1)
File:CardJitsuParty2013ConsDojoHide.PNG|The [[Ninja Hideout]] (Week 1)
File:CardJitsuParty2013ConsDojoExt2.PNG|The [[Dojo Courtyard]] (Week 2)
File:CardJitsuParty2013ConsDojoHide2.PNG|The [[Ninja Hideout]] (Week 2)
File:CardJitsuParty2013ConsDojo.PNG|The [[Dojo]]
</gallery>

===Party Pictures===
<gallery widths=180>
File:CardJitsuParty2013Beach.PNG|The [[Beach]]
File:CardJitsuParty2013Coffee.PNG|The [[Coffee Shop]]
File:CardJitsuParty2013Cove.PNG|The [[Cove]]
File:CardJitsuParty2013Dock.PNG|The [[Dock]]
File:Dojo2013.PNG|The [[Dojo]]
File:DojoExt2013.PNG|The [[Dojo Exterior]]
File:CardJitsuParty2013Forest.PNG|The [[Forest]]
File:CardJitsuParty2013Party1.PNG|The [[Ninja Headquarters]]
File:CardJitsuParty2013Pizza.PNG|The [[Pizza Parlor]]
File:CardJitsuParty2013Plaza.PNG|The [[Plaza]]
File:CardJitsuParty2013Mtn.PNG|The [[Ski Hill]]
File:CardJitsuParty2013Lodge.PNG|The [[Ski Lodge]]
File:CardJitsuParty2013Village.PNG|The [[Ski Village]]
File:SnowDojo.PNG|The [[Snow Dojo]]
File:CardJitsuParty2013Forts.PNG|The [[Snow Forts]]
File:CardJitsuParty2013Rink.PNG|The [[Stadium]]
File:CardJitsuParty2013Town.PNG|The [[Town]]
</gallery>

===Miscellaneous===
<gallery widths=180>
File:CardJitsuSnowHomepage.JPG|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:CardJitsuSnowHomepage2.JPG|Club Penguin's homepage (Slide 2).
File:CardJitsuSnowHomepage3.JPG|Club Penguin's homepage (Slide 3).
</gallery>

==Videos==
<youtube>j3zy9bGa9aw#!</youtube>
<youtube>WF1CwIyMp-I#!</youtube>
<youtube>7iJuPeQPXPs#!</youtube>
<youtube>XyMh0JdZJtg#!</youtube>
<youtube>1DGmexMYauY</youtube>

==Trivia==
*The Card-Jitsu battle at the [[Stadium]] and the [[Ninja Headquarters]] returned.
*The [[Dojo Courtyard]] and the [[Dojo]] were renovated. The [[Ninja Hideout]] was removed as a part of the renovation.
*It replaced the regular May [[Medieval Party]].
*This was the only Card-Jitsu event to take place in the month of May.

{{SWFArchives}}

==References==
{{reflist}}
{{NinjaInfo}}
{{Party}}
[[Category:Parties of 2013]][[pt:Festa Desafio Ninja Neve]]
