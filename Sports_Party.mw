{{FAOTW}}
{{Archive}}
{{Stub}}
{{PartyInfoBox
|name = Sports Party
|image = File:SportsDay2006-IceRink.png
|imagesize = 200px
|caption = The Ice Rink during Sports Party.
|membersonly = No
|when = August 11–21, 2006 {{fact}}
|freeitems = [[Red Face Paint]], [[Blue Face Paint]], [[Ice Skates]]
|famouspenguins = None
|wherehappening = [[Club Penguin Island|Everywhere]]
|previous= [[Wild West Party]]
|next= [[Lighthouse Party]]
}}
''This article is about the Sports Party from 2006. If you are looking for a different sporting event, please go [[Sports Party (disambiguation)|here]].''

The '''Sports Party''', as it's name suggests, was a sport-themed [[party]] that took place in August 2006. It centralized around the sports available in ''[[Club Penguin]]'' which at that time was mainly [[Ice Hockey]]. Despite not having most elements that modern parties would have, it was very popular thanks to the refurbishment of the [[Ice Rink]]. At the time, this party led to a boost of purchases in the [[Sports Shop]] (rather than the Gift Shop). 

==History==
{{expansion}}

==Features and Decorations==
===Ice Rink===
The [[Ice Rink]] was the center of attention as it served three purposes. Those who bought the face paint cheered on in the stands. Meanwhile, many member penguins bought sport items to play in the Ice Rink. Most notably, scoreboards were added onto both sides of the pitch, coloured for each team. Often, the room became full and many penguins battled out for the puck. Makeshift matches took place, and each time the puck enter the net, the score was added onto the scoreboards. Usually, the team with the highest score won. The blue and red teams distinguished themselves from each other using the face paint or corresponding colored clothing.
====Bugs====
The Ice Rink was also home to many bugs, especially the malfunctioning scoreboards. Even after tons of goals, the scoreboards never added on an extra number or even displayed an incorrect answer. The puck was also found outside of the playing field, and therefore out of reach of the penguins. This fueled many protests to fix it, as Ice Hockey was unplayable for long amounts of times. The scoreboards were eventually removed, and were promised to return after the issues were fixed{{Citation needed}}. None returned however, despite the introduction of the [[Soccer Pitch]] in 2008.

==Free items==
{|class="wikitable sortable"
! scope="col"| Image
! scope="col"| Item
! scope="col"| Location
! scope="col"| Members only?
|-
|[[File:Red Face Paint 2.PNG|45px]]
|[[Red Face Paint]]
|[[Coffee Shop]]
|No
|-
|[[File:Blue Face Paint.PNG|45px]]
|[[Blue Face Paint]]
|[[Pizza Parlor]]
|No
|-
|[[File:IceSkates.png|45px]]
|[[Ice Skates]]
|[[Snow Forts]]
|No
|}

The Red Face Paint and the Blue Face Paint were both released at the beginning of the party so players could support the [[Red Team]] or the [[Blue Team]]. The [[Ice Skates]] were released sometime after the party was extended on August 14. 

==Gallery==
<gallery>
File:SportsDay2006-Town.jpg|The [[Town]]
File:SportsDay2006-Plaza.jpg|Part of the [[Plaza]]
File:SportsDay2006-SnowForts.jpg|[[Snow Forts]]
File:SportsDay2006-IceRink.png|[[Ice Rink]]
File:SportsDay2006-Dock.gif|The [[Dock]]
File:SportsDay2006-Beach.jpg|The [[Beach]]
File:SportsDay2006-SkiHill.jpg|The [[Ski Hill]]
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Festa do Esporte
|spanish= Fiesta de Deportes
|french= Fête du Sport
|german= Sportparty
}}

{{SWFArchives}}

==See also==
*[[Ice Hockey]]
*[[Ice Rink]]
*[[Penguin Games]]
*[[Stadium Games]]

{{Party}}

[[Category:Parties]]
[[Category:Parties of 2006]]
[[pt:Festa do Esporte]]
