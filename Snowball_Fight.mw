[[Image:Whoa.png|thumb|350px|The Nachos vs. the Marines (CPM) and others in a non-scheduled war.]]A '''Snowball Fight''' is a large, usually very organized, [[snowball]] battle between two or more opposing parties or teams. During a snowball fight, [[penguin]]s take sides with the group of their choosing, usually by changing [[color]]s and/or [[clothing]] to the corresponding team, and throw snowballs at the opposing groups. These fights usually happen in the [[Dojo]], the [[Snow Forts]], the [[Iceberg]], or the [[Cove]]. However, the [[Dock]] and other various rooms have been used for Snowball Fights.

==Variations and Gimmicks==
*'''Hostage'''
Where one side owns a hostage, prince, or princess (different words for the same thing), and the other side has to rescue them, either by moving up to them, and heading back to base, or by saying a password, "spell". Usually these hostage(s) can move, but pretend to be trapped.

*'''Points'''
This is where a small group of [[penguin]]s fight in a typical snowball fight. Each time a player gets hit, the opposing team gains a point. After a specific, desired amount of time, the game finishes and the [[player]]s add up their points tally. The team with the highest points wins the game.

*'''Capture The Flag'''
The [[penguin]] must move to a predetermined location, and come back to another location.

*'''Small/Unorganized/Jr. Game'''
Jr. snow practice is usually thrown together at the last minute, or practice by one army.

*'''War'''
Two sides. Unlimited snowballs.
The two teams constantly throw snowballs at each other until one team is completely empty or really outnumbered.

*'''Spies/Sabotage'''
"Spies", or agents, will change color/clothing etc., and infiltrate the enemy's side.

*'''Army War'''
Same as war, but with 15 or more -a-side.

*'''Dodgeball'''
A snowball fight but with dodge balls. Used during the plays [[Team Blue's Rally 2]] and [[Team Blue vs. Team Red]].
*"'''War Face'''"
Apparently a tactic for "winning" a snowball fight, it involves members of one group displaying angry [[emote]]s in rapid succession, covering up their opponents' speech.

*"'''Joke Bomb'''"
Named by a group called the "Nachos", the Joke Bomb serves an identical purpose to the War Face. By pressing the J key, penguins begin reciting the Club Penguin jokes much to the laughter and somewhat annoyance of their opponent.

*"'''Text Flooding'''"
Traditionally, the group would scream the name of their "army" or some other random phrase such as "badger" or "spam", done for the same goal as the "Joke Bomb" and "War Face".

==Rapid Fire==
The "T" shortcut button can be used, in conjunction with clicking, to rapid fire. This rapid fire only happens on '''your''' computer, so you will not win by doing it. The snowballs come in a regular delay time, however the delay time is long. There is also a glitch: when you have got a few snowballs queued up, and you move away from your spot, the snowballs come from the ground.

==See also==
*[[Snowball]]

[[Category:Games]]
