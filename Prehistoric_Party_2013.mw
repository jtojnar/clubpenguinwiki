{{Archive}}
{{PartyInfobox
|name= Prehistoric Party 2013
|image= File:PhPLogo.png
|imagesize=190px
|caption = Prehistoric Party 2013 logo
|membersonly= No
|when= January 17, 2013 - January 29, 2013
|freeitems= '''''Non-Member'''''<br />[[T Rex Hoodie]], [[Gary's Prehistoric Giveaway]]<br />
----
'''''Member'''''<br/>[[Lava Flow]], [[Waterfall (furniture)|Waterfall]], [[Volcano (furniture)|Volcano]], [[Ancient Tree]], [[Age of Dinosaurs]]
|catalog = [[Prehistoric Catalog]]
|famouspenguins = [[Gary]]
|wherehappening= [[Snow Forts]] and Prehistoric [[Club Penguin Island]]
|previous= [[Holiday Party 2012]]
|next= [[Hollywood Party]]
}}

The '''Prehistoric Party 2013''' was a party in ''[[Club Penguin]]''. It was the first annual [[Prehistoric Party]]. It started on January 17, 2013 and ended on January 29, 2013. Players were able to use the [[Time Trekker]] to go to the past and members were able to transform into dinosaurs. It was the first party of 2013.

==History==
The Prehistoric Party 2013 was confirmed in issue 12 of [[Club Penguin Magazine]]. However, the first hints about it were at the [[Halloween Party 2012]]; when you looked in the telescope at Gariwald's mansion, you were able to see the Time Trekker. On December 27, 2012, Polo Field released a hint of upcoming events in January 2013. On December 28, he officially confirmed the party by releasing a party sneak peek. On the same day, the [[Exit Screen]] was also changed to advertise the party. As seen in a sneak peek provided by [[Spike Hike]] on his personal Twitter account, [[Membership|members]] were be able to transform into dinosaurs.

== Free items ==
{|class="wikitable sortable"
! scope="col"| Image
! scope="col"| Item
! scope="col"| Location
! scope="col"| Members only?
|-
|[[File:TRexHoodie.PNG|45px]]
|[[T Rex Hoodie]]
|Obtained by finding all the dinosaur eggs in [[Dino Dig]]
|No
|-
|[[File:Gary'sPrehistoricGiveawayIcon.PNG|45px]]
|[[Gary's Prehistoric Giveaway]]
|Obtained by finding [[Gary]]
|No
|-
|[[File:LavaFlow.PNG|45px]]
|[[Lava Flow]]
|Dinosaur Checklist
|Yes
|-
|[[File:WaterfallFurniture.PNG|45px]]
|[[Waterfall (furniture)|Waterfall]]
|Dinosaur Checklist
|Yes
|-
|[[File:VolcanoFurniture.PNG|45px]]
|[[Volcano (furniture)|Volcano]]
|Dinosaur Checklist
|Yes
|-
|[[File:AncientTree.PNG|45px]]
|[[Ancient Tree]]
|Dinosaur Checklist
|Yes
|-
|[[File:AgeofDinosaurs.PNG|45px]]
|[[Age of Dinosaurs]]
|Dinosaur Checklist
|Yes
|}

==Transformations==
During this party, [[Membership|members]] could transform into three types of [[Dinosaur|dinosaurs]]: Pteranodon, Triceratops and Tyrannosaurus.

{|class="wikitable sortable"
! scope="col"| Artwork
! scope="col"| In-game
! scope="col"| Name
|-
|[[File:PteranodonGreen.PNG|100px|link=|center]]
|[[File:PteranodonSprite.PNG|160px|link=|center]]
|Pteranodon
|-
|[[File:TriceratopsPurple.PNG|100px|link=|center]]
|[[File:TriceratopsSprite.PNG|100px|link=|center]]
|Triceratops
|-
|[[File:TyrannosaurusRed.PNG|100px|link=|center]]
|[[File:TyrannosaurusSprite.PNG|80px|link=|center]]
|Tyrannosaurus
|}

==Gallery==
===Sneak Peeks===
<gallery>
File:PrehistoricPartySneakPeek2013.png|The [[Stony Town]].
File:PrehistoricPartySneakPeek1.PNG|The [[Tree Place]].
File:PrehistoricPartySneakPeek2.PNG|The [[Tyranno Town‎]].
File:PrehistoricPartySneakPeek3.PNG|The [[Time Trekker (Party Room)|Time Trekker]].
File:PrehistoricPartySneakPeek4.PNG|The transformations on Player Cards.
File:PrehistoricPartySneakPeek5.PNG|The [[Scary Ice]].
File:PrehistoricPartySneakPeek6.PNG|Another sneak peek of the Time Trekker.
File:PrehistoricPartySneakPeek7.PNG|A Dinosaur's ability to throw puke snowballs.
File:PrehistoricPartySneakPeek8.PNG|The [[Prehistoric Catalog]].
File:PrehistoricPartySneakPeek9.PNG|The Map.
</gallery>

=== Miscellaneous ===
<gallery>
File:IconPrehistoricParty2013.JPG|YouTube icon.
File:TwitterPrehistoricParty2013.JPG|Twitter background.
File:YouTubePrehistoricParty2013.JPG|YouTube background.
</gallery>

===Login and Exit Screen===
<gallery>
File:PrehistoricPartyLogin1.PNG|The first [[Login Screen]]
File:PrehistoricPartyLogin2.PNG|The second [[Login Screen]]
File:PrehistoricPartyLogin3.PNG|The third [[Login Screen]]
File:PreHistoricPartyLogout1.PNG|The first [[Exit Screen]], featuring the three main dinosaurs.
File:PreHistoricPartyLogout2.JPG|The second [[Exit Screen]], featuring the [[Lava Flow]] igloo item.
File:PreHistoricPartyLogout3.PNG|The third [[Exit Screen]], featuring the [[Waterfall (furniture)|Waterfall]] igloo item.
File:PreHistoricPartyLogout4.JPG|The fourth [[Exit Screen]], featuring the [[Volcano (furniture)|Volcano]] igloo item.
File:PrehistoricPartyExit5.PNG|The fifth [[Exit Screen]], featuring the [[Ancient Tree]] igloo item.
File:PrehistoricPartyExit6.PNG|The sixth [[Exit Screen]], featuring the [[Age of Dinosaurs]] igloo location.
</gallery>

===Construction===
<gallery>
File:PrehistoricPartyConstParty1.PNG|The [[Time Trekker|Bonus Room]]
File:PrehistoricPartyConstForts.PNG|The [[Snow Forts]]
</gallery>

===Party Pictures===
<gallery>
File:PrehistoricParty2013Party6.PNG|The [[Dino Nests]]
File:PrehistoricParty2013Party10.PNG|The [[Hunting Spot]]
File:PrehistoricParty2013Party11.PNG|The [[Ptero Town]]
File:PrehistoricParty2013Forts.PNG|The [[Snow Forts]]
File:PrehistoricParty2013Party12.PNG|The [[Scary Ice]]
File:PrehistoricParty2013Party3.PNG|The [[Stony Town]]
File:PrehistoricParty2013Party1.PNG|The [[Time Trekker (Party Room)|Time Trekker]]
File:PrehistoricParty2013Party2.PNG|The [[Tree Place]]
File:PrehistoricParty2013Party8.PNG|The [[Tricera Town]]
File:PrehistoricParty2013Party9.PNG|The [[Tyranno Town‎]]
File:PrehistoricParty2013Party5.PNG|The [[Water Place]]
File:PrehistoricParty2013Party7.PNG|The [[Yuck Swamp]]
File:PrehistoricParty2013Party4.PNG|The [[Yum Yum]]
</gallery>

==Videos==
<youtube>joQ3nUZA75s</youtube>
<youtube>qAeyn8LNNhA</youtube>

==Trivia==
*This is the first party to not be an expedition in January since 2009, as [[Spike Hike]] confirmed it is not an expedition.
*This is the second [[party]] in a row to have three different transformation options and special [[party]] [[emotes]], and the third party overall.
*[[Penguin|Penguins]] are able to excavate Dino eggs with the [[Dino Dig]] game.
*You can travel back to the normal Club Penguin via the [[Map]] or the [[Time Trekker]], and vice-versa.
*This is the first party in which normal Club Penguin isn't decorated, if we are not counting the [[Snow Forts]] with the Time Trekker.

{{SWFArchives}}

==See also==
*[[Gary]]
*[[Time Trekker 3000]]

{{Party}}
[[Category:Parties of 2013]]
[[pt:Viagem Pré-Histórica]]
