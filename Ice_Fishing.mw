{{MinigameInfobox
|name= Ice Fishing
|image= Image:IceFishing Logo.PNG
|caption= The Game Logo.
|players= 1
|room= [[Ski Lodge]]
|date= March 10, 2006
|stamps=Yes (10)
}}

:''If you were looking for the Ice Fishing mini game on [[Elite Penguin Force]], click [[Ice Fishing (DS Version)|here]].''

'''Ice Fishing''' is a game played outside the [[Ski Lodge]], accessed by clicking on the door that says "Go Fishing" or [[My Penguin]] app. The objective of the game is to catch as many [[fish]] as possible before the game ends. The [[penguin]] (which is your player) fishes through a hole in the ice. Underneath the hole lie [[fish]], but also other potentially dangerous obstacles such as [[crab]]s, jellyfish and sharks. The maximum number of yellow [[fish]] the player can catch is about 68. The [[Penguin|player]] starts out with 3 [[worm]]s for bait and can catch bonus bait throughout the game. The player can catch [[Grey Fish]] if you have the [[Flashing Lure Fishing Rod]], but they are harder to catch. Since  April 27, 2011, you can earn stamps for the game.


== Objects ==
'''There are many objects that float past in the water.'''

* [[Fluffy the Fish|Yellow fish]]
**What the player is supposed to catch.

* [[Can of Worms]]
**These give the player one extra worm.

* [[Puffle]]
**Pink [[Puffle]]s with a mask and snorkel swim past occasionally with a banner. The banner often warns the player of something to come. The reason it is a pink one is because pink [[Puffles]] can swim very well.

* [[Mullet]]
**The [[mullet]] is the big, red, [[fish]] at the end of the game. You can see a stuffed version of it hanging on the wall in the [[Ski Lodge]]. You can catch it by getting a smaller fish and when it goes across the screen have the fish. It will try to eat it. You will get a 100 coin bonus if it is caught.

* [[Barrel]]
**[[Barrel]]s knock [[fish]] off the hook.

* Boot
**Boots knock [[fish]] off the hook.

* [[Jellyfish]]
**[[Jellyfish]] can shock the line and take a worm away. They also take away a [[fish]] if there is one on the hook.

* [[Shark]]
**[[Sharks]] can bite both the worm and [[fish]] off the hook.

* [[Crab]]
**[[Crab]]s can crawl along on the underside of the ice and cut the player's line.

*[[Grey Fish]]
**[[Grey Fish]] are another version of the [[Fluffy the Fish|Yellow fish]]. They can only be caught by [[member]]s with the [[Flashing Lure Fishing Rod]], which is bought via [[Game Upgrades]]. The player must be wearing it when they enter the game. They aren't very easy to catch, because they quickly dart away from the [[Worm|bait]]. As a result, they are worth twice as many coins as yellow fish.

==Stamps==
====Easy====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Snack Attack stamp]]||Feed a fish to a shark.
|}

====Medium====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Shock King stamp]]||Get 3 shocks from jellyfish and finish the game.
|-
|[[Fishtastic stamp]]||Catch 15 fish without any mistake.
|}

====Hard====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Worm Win stamp‎]]||Finish the game without losing a worm.
|-
|[[Crab Cuts stamp‎]]||Have 3 crabs cut your line and finish the game.
|-
|[[Afishionado stamp]]||Catch 45 fish without any mistake.
|-
|[[Gray Goodies stamp]] <small>''(members only)''</small>||Catch 15 gray fish.
|-
|[[Prize Mullet stamp]]||Capture Mullet.
|-
|[[Fly Fisher stamp]]||Catch 63 fish in under 5 minutes.
|}

====Extreme====
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Ace Angler stamp]] <small>''(members only)''</small>||Hook 15 gray fish and Mullet with no worm lost.
|}


== Secrets ==
*At the last part of the game, there will be a giant red [[Mullet|fish]]. It is the [[mullet]]. Use a [[Fluffy the Fish|yellow fish]] or a [[Grey Fish|grey fish]] as bait for the [[mullet]] to lure it. Then, you will earn 100 [[coins]] as a reward for catching it.

*If you buy a Flashing Lure Fishing Rod from the [[Game Upgrades]] catalog and then play Ice Fishing wearing it, you will see that you are catching [[Grey Fish|gray fish]] in addition to yellow fish. Each [[Grey Fish|gray fish]] will get you double the amount of [[coin]]s than a yellow [[fish]] will, also, there will be more yellow fish when the mullet is coming.

== Tips ==
*Near the first part of the game, you should try catching the [[Grey Fish]] instead of the yellow fish, but you will have to wait for the fish to swim by. If you are near the end, try waiting for a while. If you think the [[Grey Fish]] will not swim by, just catch the yellow fish and the mullet.

*To catch a grey fish easily, you should place the worm on the opposite side of where the fish is swimming. For example, if the fish is swimming down, place the worm up, and the fish has been programmed to move to the opposite side. 

== Trivia ==
*The maximum number of fish a player can catch on the [[Club Penguin (app)|Club Penguin]] app is 60. Once this is achieved, the game is over.


== Gallery ==
<gallery> 
File:IceFishingStart.png|Start screen on the Club Penguin app.
Image:Ice Fishing.PNG|Gameplay of Ice Fishing on [[clubpenguin.com]].
File:IceFishingGameplay.png|Gameplay of Ice Fishing on the Club Penguin app.
Image:Mullet.jpg|A [[mullet]].
Image:CanOfWorms.PNG|Can of worms.
Image:Crab.JPG|The Crab in Ice Fishing.
File:67 lrg-1024.jpg|the Ice Fishing wallpaper.
</gallery>

==Name In Other Languages==
{{OtherLanguage
|portuguese= Pescaria no Gelo
|spanish= Glaglasticot
|french= Pesca en el Hielo
|german= Eisfischen
|russian= Зимняя рыбалка
}}

==SWFs==
*[http://archives.clubpenguinwiki.info/static/images/archives/a/a0/Music103.swf Music]

{{SWFArchives}}

== See also ==
*[[Mullet]]
*[[Worms]]
*[[Grey Fish]]
*[[Clubpenguinus Crabus]]


{{games}}

[[Category:Games]]
