{{MinigameInfobox
|name = Astro Barrier
|image = File:AstroBarrierLogo.PNG
|caption = The logo of Astro Barrier.
|players= 1
|room = [[Dance Lounge]]
|date = October 2005
|stamps = Yes (12)
}}

'''Astro Barrier''' is a ''[[Club Penguin]]'' mini-game that is located in the [[Dance Lounge]], next to [[Thin Ice]] and is styled around a classic arcade game. It was originally playable by all players. However, on July 26, all players were able to play up to the 5th level before it became members-only. This was because of the release of the [[Stamps]].

==Secrets==
[[File:ABCP2.PNG|thumb|The start screen.]]
{{Spoiler}}
*It is possible to skip levels right after clicking start. On your keyboard press 1 to go to level 10, 2 to go to 20, or 3 to go to 30.
*The secret levels in Astro Barrier are accessible before level 11 starts. Wait 30 seconds a when the screen is showing information about the orange switch, and shoot the blue ship that appears. Then after the 10 Secret Levels, you will skip levels to level 16.
*The Expert Levels are accessible before level 31. Just like the first secret levels wait 25 seconds and shoot to make the turret shoot the blue ship. Then after the 10 Expert Secret Levels, you can shoot five targets with a number of 500. That means you got 2500 extra points. Then the game is won.
*You can earn more points to addition to your [[coins]] by shooting at targets and torrents when they are in the Instruction Pages.
*Before proceeding on to another level, a loading box will appear for about 4 seconds, this box isn't a real loading box, so by pressing enter, it will disappear and you will progress to the next level.

{{Spoiler2}}

==Controls==
Press the left and right arrow keys to move your ship. Pressing the Space Bar will shoot, but the player has limited bullets, ranging from three to eight.

==Trivia==
*Other than [[Paint By Letters]] this is the only single player game without music.


[[File:Astro ID not ready.png|thumb|An error.]]
==Stamps==
===Astro Barrier===
====Easy====
{|border="1" class="wikitable" 
!'''Name''' !! Description
|-
|[[Astro5 stamp]]|| Finish 5 levels.
|-
|[[Astro5 Max stamp]]|| Finish 5 levels without missing a shot.
|}

====Medium====
{|border="1" class="wikitable" 
!'''Name''' !! Description
|-
|[[Astro40 stamp]] <small>''(members only)''</small>|| Finish 1-40 levels.
|-
|[[Ship Blast stamp]] <small>''(members only)''</small>|| Use a Blaster to shoot a ship.
|-
|[[1-up Blast stamp]] <small>''(members only)''</small>|| Use a Blaster to shoot a 1-up.
|-
|[[Astro Secret stamp]] <small>''(members only)''</small>|| Complete all Secret Levels.
|-
|[[Astro10 Max stamp]] <small>''(members only)''</small>|| Complete levels 1-10 without missing a shot.
|-
|[[Astro20 Max stamp]] <small>''(members only)''</small>|| Complete levels 1-20 without missing a shot.
|}

====Hard====
{|border="1" class="wikitable" 
!'''Name''' !! Description
|-
|[[Astro Expert stamp]] <small>''(members only)''</small>|| Complete the Expert Levels.
|-
|[[Astro30 Max stamp]]  <small>''(members only)''</small> || Complete levels 1-30 without missing a shot.
|-
|[[Astro 1-up stamp]] <small>''(members only)''</small>|| Collect 8 1-ups.
|}

====Extreme====
{|border="1" class="wikitable" 
!'''Name''' !! Description
|-
|[[Astro Master stamp]] <small>''(members only)''</small>|| Complete 25 levels + Secret and Expert.
|}

==Names in other languages==
{{OtherLanguage
|portuguese= Patrulha Espacial
|spanish= Barra Espacial
|french= Astro-Torpille
|german= Astrowache
|russian= Космо-барьер
}}

==See also==
*[[Thinicetrobarrier]]
*[[Thin Ice]]
*[[Bits & Bolts]]
*[[Astro Barrier Ship Pin]]
*[[Astro Barrier T-Shirt]]

{{Games}}
