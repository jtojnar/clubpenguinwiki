{{CharacterInfobox
|name= Rookie
|image= File:Rookie2014.PNG
|imagesize= 170px
|fullname= Rookie
|species= [[Penguin]]
|position= [[Secret Agent]], Comm Lead, [[The Director]] (future)<ref>http://www.clubpenguinwiki.info/wiki/File:Felipe79030_Rookie_The_Director.png</ref>
|appeared= [[Secret Missions]], sometimes in the [[Newspaper]]
|color= [[Green]]<br>[[Dark Green]] (only when waddling around)
|clothes= [[Rookies Hat|Hat]]<small><sup>[[Rookie's Hat Oct 2013|1]]</sup></small><br>[[Rookies Sunglasses|Sunglasses]]<br>[[Rookies Shirt|Normal Shirt]]<br>[[Rookie's Halloween Shirt|Halloween Shirt]]<br>[[Rookies Cardboard Box|Cardboard Box]] (sometimes)
|related= Unknown
|friends= [[Gary the Gadget Guy|Gary]], [[Jet Pack Guy]], [[Dot]], [[Aunt Arctic|The Director]]
|walk= Yes
}}<center>{{cquote2|This place should be my igloo!|Rookie talking about [[A Silly Place]].}}</center>
{{Seeideas}}
'''Rookie''' is the public relations officer for the [[Penguin Secret Agency|PSA]] and a novice [[Secret Agent]]. He is also the communication lead, just as [[Gary]] is the tech lead. He is a green [[penguin]] wearing a [[red propeller cap]] and [[red sunglasses]] in PSA Secret Missions but also wearing a [[Red Hawaiian Shirt]] and the [[Rookies Cardboard Box|Box Costume]] online. He appears in [[Secret Missions]] #3, #7, #10, and #11. His first appearance is in the mission 'Case of the Missing Coins', where he is examining the vault. Like most people who are Rookies in real-life activities, Rookie makes mistakes that more experienced [[Secret Agent|agents]] wouldn't. He seems to be very dumbfounded sometimes. He is also an [[Elitist]].  Rookie has been a [[mascot]] at certain parties since the [[April Fools' Party 2011]]; and meeting him gets you the [[Rookie's Giveaway Background]] and the [[Rookie stamp]]. Rookie can wear the [[Red Hawaiian Shirt]] and the [[Rookies Cardboard Box|Box Costume]] at the same time (because he has a special box costume that is a neck item), he can also fly with the [[Rookies Hat|Red Propeller Cap]] while wearing those items. 

[[File:LM96Rookie3.PNG|thumb|If you look at this picture closely, you'll see he is wearing the shirt and Box Costume at the same time.]]

==Poem==
Rookie made up a poem for the [[Club Penguin Times]] during April Fools' Day 2010:

{{cquote2|Quack, whack, thwack, ack!
Why can't I be on the right track?

Who's the penguin with the Propeller hat?

It's me, the Rookie,

And that is that!|Rookie}}

This poem, however, makes [[Twee]] laugh her head off, but displeases the other Elite Agents.

==Appearances==
===Online===
He appears as a famous meetable character online. You get [[Rookie stamp|a stamp]] for meeting him.

====Missions====
[[Secret Missions#Case of the Missing Coins|Mission 3]]: Helps on Missing Coins case. He guards the vault in the [[Gift Shop]], but accidentally closes it and forgets the combination. Later he watches after the [[coin]]s that were on the ceiling.

[[Secret Missions#Clockwork Repairs|Mission 7]]: Posts signs around the [[Town]] to help find the missing Prime Gear.

[[Secret Missions#Waddle Squad|Mission 10]]: Is a member of the Waddle Squad. He accidentally tells Herbert about the golden [[Puffle]] at the [[Night Club]], but that was deemed fine because the [[puffle]] was a trap. Then he prepares the [[Electromagnets|Electromagnet 3000]] in the [[Gift Shop]] but can not power it due to forgetting to bring the Solar Panel 3000. After capturing [[Herbert]], Rookie gives him his [[Spy Phone]] and allows [[Herbert]] to teleport away and escape, only to leave behind a packet of seeds.

[[The Veggie Villain|Mission 11]]: Is sent to investigate the disappearance of several items in the [[Gift Shop]]. He also wonders if wigs can be part of their uniforms. Afterward, he helps you stop [[Herbert|Herbert's]] video transmission. He later appears in the [[HQ]] during the popcorn-bomb crisis.

Also, in the 7<sup>th</sup>, 10<sup>th</sup> and 11<sup>th</sup> missions, if you move your cursor towards his hat, the propeller will spin.

====System Defender====
In [[System Defender]] Rookie appears as a [[penguin]] informing you about robots, but often has to be corrected by [[Jet Pack Guy]].

===In Video Games===

In ''[[Club Penguin: Elite Penguin Force]]'' for Nintendo DS, Rookie gets trapped behind a river in the [[Wilderness]]. Rookie appears in ''[[Club Penguin: Elite Penguin Force: Herbert's Revenge]]'' in multiple missions, including downloadable missions.

Rookie appears in ''[[Club Penguin: Game Day!]]'' for Nintendo Wii as a cameo in the Fast Freeze Easy level.

===In the [[Future Party|future]]===
In the future, [[Rookie]] will not be the Comm Lead anymore. He will improve his skills and become the most important penguin in the [[Extra-Planetary Federation]]. He will replace [[Aunt Arctic]] and become [[The Director]]<ref>http://www.clubpenguinwiki.info/wiki/File:Felipe79030_Rookie_The_Director.png</ref>.

===Club Penguin Magazine===
He makes frequent appearances in the Club Penguin Magazine and is a recurring character in the comics.

==Glitches==
*During the [[Underwater Expedition]], there was a bug that showed Rookie as an Aqua [[penguin]] in the [[Player Card|player card]].

==Knowledge of Other Characters==
*[[Rockhopper]]: Rockhopper is the greatest captain
*[[Ultimate Proto-Bot 10,000]]: Eek! Protobot?! WHERE?!

==Gallery==
<gallery widths="180">
File:RookieInGame.PNG|Rookie in-game.
File:RookieCard.PNG|Rookie's player card.
File:RookieAqua.PNG|The bug that showed Rookie using the color [[Aqua]] during the [[Underwater Expedition]].
File:RookieBuddyListIcon.PNG|Rookie's 1st Buddy List icon.
File:RookieBuddyListIcon3.PNG|Rookie's 3rd Buddy List icon.
File:RookieBuddyListIcon4.PNG|Rookie's 4th Buddy List icon.
File:Rookie sig.png|Rookie's signature.
File:Rookie Stamp.PNG|Rookie's [[Stamp]].
</gallery>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Artwork</big></center>
<div class="mw-collapsible-content">
<gallery>
Rookie.PNG|Rookie from PSA [[Secret Mission]] 3: Case of the Missing Coins.
Rookie Happy.PNG|Rookie seen again in Case of the Missing Coins.
Rookie paper.png|Rookie in another secret mission.
File:Rookie mission 10.png|Rookie during secret mission 10: Waddle Squad.
File:RookiePSAIcon.png|Rookie's icon in the [[Spy Phone]] during [[PSA Secret Missions]].
File:Rookie Icon.png|Rookie's Icon in [[System Defender]] and [[EPF Spy Phone]] Messages. (Note: the eyebrows)
File:RookieTwelthFish.png|Rookie reading The Twelfth Fish script.
File:RookieUEAnnouc.PNG|Rookie in the [[Underwater Expedition]] announcement in [[The Club Penguin Times]].
File:RookieCPTimes327Artwork.PNG|Rookie trying to prevent the island's flooding.
File:Rookie hawaiin shirt.PNG|Rookie tensed.
File:RookieLook!.PNG|Rookie looking at island sinking.
File:RookieDucky.png|Rookie preparing for [[April Fools' Party 2012|April Fools' Party]]
File:Rookie blue.PNG|Rookie in the April Fools' Day issue of [[The Club Penguin Times]].
File:RookieGSR.PNG|Rookie in [[The Club Penguin Times]].
File:RookieHawaiianShirt.PNG|Rookie's pose in his first background.
File:RookieSilly.PNG|Rookie being silly for [[April Fools' Party]].
File:RookieTongue.PNG|Rookie sticking his tongue out.
File:RookieSlide.PNG|Rookie Sliding.
File:RookieExcited.PNG|Rookie excited.
File:RookieUniversityOpening.PNG|Rookie in the [[What's New Blog]] in German, during the opening of the [[University]]
File:Rookie-Issue389.PNG|Rookie worried.
File:Rookie-Issue412.PNG|Rookie jumping with pencils.
File:AskRookie2013.gif|Ask Rookie (2013)
File:RookieHalloween2013.PNG|Rookie during the [[Halloween Party 2013]]
File:CPTimes415-Rookie.PNG|Rookie with a Halloween Book
File:CPTimes415-2-Rookie.PNG|Rookie scared
File:CPTimes416-Rookie.PNG|Rookie turning into a werewolf
File:CPTimes455-Rookie.PNG|Rookie on the Music Cruise.
</gallery>
</div></div>

==Trivia==
*Stated by himself in Mission 11, he had to take the [[Secret Agent]] Test 44 times.
*In certain issues of [[The Club Penguin Times]], Rookie takes over for [[Aunt Arctic]] in the "Ask [[Aunt Arctic]]" column due to an upcoming event. So far, these are the [[April Fools Party 2010]], [[April Fools' Party 2011]], and [[The Fair 2012]].
*Rookie can understand and speak a bit of crab language, as he can almost understand [[Klutzy|Klutzy's]] clicking in ''System Defender''.
*He has a Blue Hawaiian Shirt and a pair of [[Blue Sunglasses|blue sunglasses]], as seen in the [[Club Penguin Times]].<sup>''[[Club Penguin Wiki:Sourcing|[which issue?]]]''</sup>
*Despite his color being green, he was seen wearing dark green at the [[April Fools' Party 2011]].
*As well as Rookie being the Public Relations Officer of the EPF, he's the Comm Lead too. That was his status when he talked to you after finishing the [[Field-Op]].
*Rookie has crashed the [[Hydro Hopper]] boat before, as stated in Club Penguin Times issue 360.
*Rookie has a wooden puffle, as revealed in WaddleOn: Sidekick Search
*According to him, he has two pets, a rock and a rubber duck.<ref>http://www.clubpenguinwiki.info/wiki/File:Rookie_Cream_Soda_Dimension_Rookie_me.png</ref>
*Rookie glued his hat on his head as he revealed in the [[Club Penguin Magazine]] comics. However, in a [[Club Penguin Times]] issue, Rookie is seen playing in [[The Fair 2014]] rides and the hat is going out of his head.
*Along with you and [[Gary]], Rookie was trapped inside of the [[PSA]] HQ by [[Herbert]] in Mission #11.
*In the [[Future Party|future]], it is rumored that he will replace [[Aunt Arctic]] and become '''The Director of the [[Extra-Planetary Federation]]'''.<ref>http://www.clubpenguinwiki.info/wiki/File:Felipe79030_Rookie_The_Director.png</ref>

==Names in other languages==
{{OtherLanguage
|portuguese= Rookie
|spanish= Rookie
|french= Rookie
|german= Rookie
|russian= Новичков
}}


==Sources and References==
{{reflist}}


{{RookieSightings}}
{{EPF}}
{{Characters}}
[[Category:People]][[Category:PSA]][[Category:Elite Agents]][[Category:DS]][[Category:Penguins]][[Category:Famous Penguins]][[Category:Characters]][[pt:Rookie]]
