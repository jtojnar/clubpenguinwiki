{{Archive}}
{{PartyInfobox
|name = Halloween Party 2014
|image= File:HalloweenPartyLogo.PNG
|imagesize = 200px
|caption = Halloween Party 2014 logo.
|membersonly = No
|when = October 22, 2014 - November 4, 2014
|freeitems = ''[[Halloween Party 2014#Free Items|See list]]''
|wherehappening = [[Club Penguin Island]]
|famouspenguins = [[Gary]]
|previous=[[9th Anniversary Party]]
|next=[[Pirate Party 2014]]
}}The '''Halloween Party 2014''' was a party on ''[[Club Penguin]]''. It started on October 22, 2014 and ended on November 4, 2014. This Halloween Party was also the tenth annual [[Halloween Party]].

==History==
===Development===
The party was confirmed on July 29, 2014, in a blog post from the [[What's New Blog]].<ref>[http://www.clubpenguin.com/blog/2014/07/you-decide-halloween-costumes What's New Blog - You Decide: Halloween Costumes]</ref> Players were asked to choose which of the 3 costumes (Clown, Pirate and Dragon) they want to return in the [[October 2014 Penguin Style|October 2014]] ''[[Penguin Style]]'' catalog. The winner was revealed on September 30, 2014: the "Dragon" ''([[Blue Dragon Costume|costume]], [[Blue Dragon Feet|feet]])''.<ref>[http://www.clubpenguin.com/blog/2014/09/you-decide-halloween-costume-results What's New Blog - You Decide: Halloween Costume Results]</ref> On the same day, [[Megg]] posted a traditional sneak peek with 3 random photos of the party on the blog.<ref>[http://www.clubpenguin.com/blog/2014/09/official-october-party-announcement What's New Blog - Official October Party Announcement]</ref>

On October 1, advertisements on the homepage, the login and exit screens revealed the [[Ghost Puffle]]s. One day later, on October 2, the membership page showed that members can adopt them during the party. It was also revealed that members can find haunted items in the [[Puffle Hotel]].

===In-game===
On October 16, [[Gary]] revealed on the [[Club Penguin Times]] #469 that his great uncle [[Gariwald]] will be visiting the island during the party. On the same day, the construction started in the Puffle Hotel.

==Free Items==
A total of fifteen free items were made available for this party. Items marked with a badge ([[File:Memberbadge.png|20px]]) indicate that the item can only be obtained by member players.

<gallery>
File:GhostlySuitcase.PNG|link=Ghostly Suitcase|'''[[Ghostly Suitcase]]'''<br/>Halloween Party 2014 interface 
File:MagicHat.PNG|link=Magic Hat|'''[[Magic Hat]]'''<br/>Halloween Party 2014 interface
File:BellhopHat.PNG|link=Bellhop Hat|'''[[Bellhop Hat]]'''<br/>After defeating [[Skip]]
File:LittleJack.PNG|link=Little Jack|'''[[Little Jack]]'''<br/>Halloween Party 2014 interface
File:CreepyCandle.PNG|link=Creepy Candle|'''[[Creepy Candle]]'''<br/>Halloween Party 2014 interface
File:EyeofNewtGumballs.PNG|link=Eye of Newt Gumballs‎|'''[[Eye of Newt Gumballs]]'''<br/>Halloween Party 2014 interface
File:Inspector'sMagnifyingGlass.PNG|link=Inspector's Magnifying Glass|'''[[Inspector's Magnifying Glass]]'''<br/>Halloween Party 2014 interface
File:Gary'sExplosiveGiveawayIcon.PNG|link=Gary's Explosive Giveaway|'''[[Gary's Explosive Giveaway]]'''<br/>Meet [[Gary]]
File:ShadowWings.PNG|link=Shadow Wings|[[File:Memberbadge.png|20px|Members only]] '''[[Shadow Wings]]'''<br/>Halloween Party 2014 interface
File:GhostPuffleCostume.PNG|link=Ghost Puffle Costume|[[File:Memberbadge.png|20px|Members only]] '''[[Ghost Puffle Costume]]'''<br/>Halloween Party 2014 interface
File:GhostPuffle.png|link=Ghost Puffle|[[File:Memberbadge.png|20px|Members only]] '''[[Ghost Puffle]]'''<br/>After defeating [[Skip]]
File:SkeletonMask.PNG|link=Skeleton Mask|[[File:Memberbadge.png|20px|Members only]] '''[[Skeleton Mask]]'''<br/>Halloween Party 2014 interface
File:SpectralCostume.PNG|link=Spectral Costume|[[File:Memberbadge.png|20px|Members only]] '''[[Spectral Costume]]'''<br/>Halloween Party 2014 interface
File:GhostVial‎.PNG|link=Ghost Vial|[[File:Memberbadge.png|20px|Members only]] '''[[Ghost Vial]]'''<br/>Halloween Party 2014 interface
File:WebHoodie.PNG|link=Web Hoodie|[[File:Memberbadge.png|20px|Members only]] '''[[Web Hoodie]]'''<br/>Halloween Party 2014 interface
</gallery>

== Gallery ==
===Party Pictures===
<gallery widths="180">
File:HalloweenParty2014Beach.PNG|The [[Beach]]
File:HalloweenParty2014Beacon.PNG|The [[Beacon]]
File:HalloweenParty2014Book.PNG|The [[Book Room]]
File:HalloweenParty2014Cave.PNG|The [[Cave]]
File:HalloweenParty2014Shop.PNG|The [[Clothes Shop]]
File:HalloweenParty2014CloudForest.PNG|The [[Cloud Forest]]
File:HalloweenParty2014Cove.PNG|The [[Cove]]
File:HalloweenParty2014Dock.PNG|The [[Dock]]
File:HalloweenParty2014Dojo.PNG|The [[Dojo]]
File:HalloweenParty2014DojoExt.PNG|The [[Dojo Courtyard]]
File:HalloweenParty2014DojoFire.PNG|The [[Fire Dojo]]
File:HalloweenParty2014Forest.PNG|The [[Forest]]
File:HalloweenParty2014Berg.PNG|The [[Iceberg]]
File:HalloweenParty2014Light.PNG|The [[Lighthouse]]
File:HalloweenParty2014Attic.PNG|The [[Lodge Attic]]
File:HalloweenParty2014Shack.PNG|The [[Mine Shack]]
File:HalloweenParty2014Pet.PNG|The [[Pet Shop]]
File:HalloweenParty2014Pizza.PNG|The [[Pizza Parlor]]
File:HalloweenParty2014Plaza.PNG|The [[Plaza]]
File:HalloweenParty2014HotelLobby.PNG|The [[Puffle Hotel Lobby]]
File:HalloweenParty2014HotelSpa.PNG|The [[Puffle Hotel Spa]]
File:HalloweenParty2014HotelRoof.PNG|The [[Puffle Hotel Roof]]
File:HalloweenParty2014Park.PNG|The [[Puffle Park]]
File:HalloweenParty2014School.PNG|The [[School]]
File:HalloweenParty2014Skatepark.PNG|The [[Skatepark]]
File:HalloweenParty2014Mtn.PNG|The [[Ski Hill]]
File:HalloweenParty2014Lodge.PNG|The [[Ski Lodge]]
File:HalloweenParty2014Village.PNG|The [[Ski Village]]
File:HalloweenParty2014DojoSnow.PNG|The [[Snow Dojo]]
File:HalloweenParty2014Forts.PNG|The [[Snow Forts]]
File:HalloweenParty2014Rink.PNG|The [[Stadium]]
File:HalloweenParty2014Town.PNG|The [[Town]]
</gallery>

===Party Rooms===
<gallery widths=180>
File:HalloweenParty2014Party9.PNG|The [[Puffle Hotel Balcony]]
File:HalloweenParty2014Party3.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party12.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party13.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party14.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party15.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party16.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party17.PNG|The [[Puffle Hotel Ballroom]]
File:HalloweenParty2014Party1.PNG|The [[Puffle Hotel Basement]]
File:HalloweenParty2014Party5.PNG|The [[Puffle Hotel Deluxe Suite]]
File:HalloweenParty2014Party4.PNG|The [[Puffle Hotel Dining Room]]
File:HalloweenParty2014Party7.PNG|The [[Puffle Hotel Library]]
File:HalloweenParty2014Party8.PNG|The [[Puffle Hotel Luxury Suite]]
File:HalloweenParty2014Party6.PNG|The [[Puffle Hotel Pool]]
File:HalloweenParty2014Party2.PNG|The [[Puffle Hotel Sitting Room]]
File:HalloweenParty2014Party10.PNG|The [[Puffle Hotel Storage]]
File:HalloweenParty2014Party11.PNG|The [[Puffle Hotel Thirteenth Floor]]
</gallery>

===Sneak Peeks===
<gallery widths="180">
File:HalloweenParty2014SP.jpeg|The poll options from the What's New Blog (The Dragon Costume won).
File:HalloweenParty2014SP1.PNG|A sneak peek on the What's New Blog.
File:HalloweenParty2014SP3.PNG|Sneak peeks on the Membership page.
</gallery>

===Login and Exit Screens===
<gallery widths=180>
File:HalloweenParty2014LoginScreen1.PNG|The first [[Login Screen]].
File:HalloweenParty2014ExitScreen1.jpg|The first [[Exit Screen]].
</gallery>

===Miscellaneous===
<gallery widths=180>
File:HalloweenParty2014Advert.jpg|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:HalloweenParty2014HomepageBG.jpg|[[Club Penguin (website)|Club Penguin's homepage]] (Background).
File:MapHalloweenParty2014.PNG|The map of the island during the party.
</gallery>

===Pre-Party===
<gallery widths=180>
File:HalloweenParty2014PreHotelLobby.PNG|The [[Puffle Hotel Lobby]]
File:HalloweenParty2014PreHotelRoof.PNG|The [[Puffle Hotel Roof]]
File:HalloweenParty2014PreHotelSpa.PNG|The [[Puffle Hotel Spa]]
</gallery>

==Videos==
<youtube>qWQaJOU1WBQ</youtube>

==Sources and References==
{{reflist}}
{{SWFArchives}}

{{Party}}
[[Category:Parties of 2014]]

[[pt:Festa de Halloween 2014]]
