{{Tocright}}
This is a '''List of Natural Disasters in Club Penguin'''. There has been 15 known disasters on [[Club Penguin]]. Those 15 disasters were:
* A '''blizzard'''
* Seven '''storms'''
* An '''avalanche'''
* An '''earthquake'''
* Two '''snow storms'''
* A '''flood'''
* An '''Inclination'''
*Two '''Rockslides'''
*A '''meteor strike'''

[[Penguins]] sometimes made best use of them by [[parties|partying]] to hide the fact that there was a natural disaster, or using them as part of a party, such as the [[Halloween Parties]].

== The Blizzard ==
''See main article: [[Blizzards]]''

The '''Blizzard''' happened during the Christmas [[Party]] 2006. The blizzard hit just before the Christmas party started in December 2006. The snow was so high that [[penguins]] had to dig tunnels to get around. This snow was stored in the [[Lodge Attic]], cooled by the [[AC 3000]], and later used during the Festival of Snow that was held February 2007. This storm also hit [[Rockhopper]] and [[Migrator|his ship]]. Note: Some of the pictures provided may look like the are from the new snow storm but this is because Club Penguin used old files for it like at the [[Dock]]. When the blizzard and the snow storm happened, the Dock was the same. Proof of this anomaly are at these sites, and also on the December and February page of the 06-07 year book along with most of the December section of [[Rockhopper's Journal]].

== The Storm ==
[[Image:Storm - Binoculars.PNG|right|thumb|130px|The Storm]]The Storm was seen on Club Penguin June 22, 2007. If someone went to the [[Cove]] and looked through the [[binoculars]] they could see the storm in a distance. During the Storm if one went to the [[Cave]] and looked out the windows they could see chunks of [[iceberg]]s. After the Storm a new level in [[Catchin' Waves]] called Survival was added to the game.

The level took place in the storm and you would need survival in the stormy fast Ice Berg filled waters. You would have to dodge the icebergs coming at you or you would lose a life. There were four more storms - Halloween 2008, Halloween 2009, Halloween 2010 and Halloween 2011.
{{clear}}

== The Avalanche ==
[[Image:Sled Runs Closed Sign.PNG|The Avalanche|right|thumb|111px]]The Avalanche happened sometime after [[Rockhopper]] left in July 2007. It was spotted at the [[Mine Shack]], the [[Iceberg]], and the [[Ski Village]]. When the Avalanche hit the [[Mountain]], the [[Ski Lift]] broke, so the Mountain was closed. This led to the [[Secret Mission]] #4: Avalanche Rescue. In the mission, the ski lift is missing a belt. It is stolen by Herbert. Shortly after the Avalanche had cleared the Mountain was re-opened and the Ski Lift was fixed.
{{clear}}

== The Earthquake ==
[[Image:Town Earthquake.PNG|right|thumb|300px|Earthquake in the Town]]
:''See main article: [[Earthquake]]''

The Earthquake mysteriously appeared on or around June 20, 2008. It disappeared on June 27. The earthquake damaged the [[Town]], [[Gift Shop]], [[Night Club]], [[Dance Lounge]], [[Coffee Shop]], and the [[Book Room]]. During the earthquake, those rooms had some cracks on the walls and grounds.<br>
This led to the [[Secret Missions|Secret Mission]] #8: ''Mysterious Tremors''. As a [[Secret Agent]] you must stop [[Herbert P. Bear]] from damaging the island. In this mission the [[Gift Shop]] is sinking and you must lift it up again. Herbert had created a tunnel and tried to damage Club Penguin on purpose. The earthquake therefore wasn't a "natural" disaster.
{{clear}}

==The Second Storm==
[[Image:Storm heading to Club Penguin.PNG|right|thumb|130px|The Storm heading to Club Penguin.]]
:''See main article: [[The Great Storm of 2008]]''

This is known to be the largest storm in Club Penguin History. It hit as soon as the Halloween Party started, and the most lightning could be seen from the [[Dojo]]. There was also something thought to be a myth before, that if you stand at the Dojo and look around the window everytime lightning flashes you can see a shadow, this was proven true. This storm left the island when the Halloween party ended but before it left it caused damage to the Dojo (probably because of all the lightning) but it gave us access to the roof. It brought a mystery [[penguin]] named '''??????''' (Now known as [[Sensei]]).

:[[Sensei]] now sits on a green pillow in the [[Dojo]] training players how to play [[Card-Jitsu]] or battling them.
{{clear}}

==Snow Storm==
[[Image:Attic.jpg|thumb|The Festival of Snow had been delayed.]]
On December 19, 2008, snow was seen from the [[Mountain]] and the [[Iceberg]]. The whole island of [[Club Penguin]] was covered in snow and the island looked the same way it did a long time ago in the Christmas Party of 2006 during a blizzard. The snow went into storage in the [[Lodge Attic]] when the 2008 [[Christmas Party 2008|Christmas Party]] ended.

It ended on December 29, 2008. It was used for the Snow Sculptures.
{{clear}}

==[[Fire Dojo|Volcano]]==
[[File:Volnew.JPG|thumb|An image of the Volcano during the [[Halloween Party 2009]]]]
A Volcano (Now known as the Fire Dojo) was seen in the Dojo Courtyard in Sepember 2009. Eventually, it got more powerful and it made a third Storm in the [[Dojo Courtyard]] and [[Ninja Hideout]]. Eventually, this spread everywhere during the [[Halloween Party 2009|Halloween Party]]. It has now been tamed by [[Sensei]] and led to the building of the [[Fire Dojo]].
{{clear}}

==Rockslide==
[[File:Rock Bear.PNG|thumb|150px|The old Rockslide.]]
[[File:Rockslide bigger.PNG|thumb|150px|The Rockslide on January 15, 2010.]]
A Rockslide was initially spotted at the [[Mine]], on the 8th of January 2010. On the updates of January 15, a big rock was seen above the Rockslide. If you hover your mouse over it, the big rock will fall, revealing a cave. However, you could enter it since January 22, 2010. It was the entrance to the [[Mine Cave]].
{{clear}}

== Flood ==
[[File:Flood.PNG|thumb|250px|The first flood of [[Club Penguin]].]]
In [[Club Penguin: Elite Penguin Force: Herbert's Revenge]], [[Herbert P. Bear]] caused a massive flood because of his invention, a giant magnifying glass. It made a hole near the [[Mine Shack]] and nearly caused the island to sink. Luckily, the island was saved by Herbert when he dropped a sculpture of him down on the hole, covering it. The water from the flood covered the west side of the island. Because of this, a water party was hosted so that no penguins got suspicious of what had happened. The statue that Herbert had dropped is in the online [[Club Penguin]].
{{clear}}

== Aqua Grabber Earthquake ==
During the third time of playing [[Aqua Grabber]]'s first level, the earth shakes and the [[Giant Squid]] appears. This "earthquake" is not a natural disaster but it is unknown how the squid created this disaster.
{{clear}}

== The Great Storm of 2010 ==
'''The Great Storm of 2010''' was a storm that hit the island on October 28, 2010. It was first confirmed in issue #261 of the [[Club Penguin Times]] by Gary. It lasted through the [[Halloween Party 2010]], and it started to rain for the first time ever on November 10, making puddles and putting out fires. It cleared up on November 15, 2010. 

== The Great Storm of 2011 ==
'''The Great Storm of 2011''' was a lightning storm that hit the island on October 20, 2011, when the [[Halloween Party 2011]] started.

==Inclination==
[[File:UnderwaterExpeditionBerg.PNG|thumb|250px|[[Club Penguin Island]] inclinated as viewed from the [[Iceberg]].]]
The inclination took place between January 26 and February 1, 2012. The entire island of Club Penguin was tilted because of a huge load of anvils that were placed by the [[Lighthouse]]. It caused many parts of the island to be submerged, including [[Toughest Mountain]], where [[Herbert P. Bear]] was hibernating thanks to the [[EPF]]. The Polar Bear presumably woke and escaped as the water flooded his hideout, and his current location is unknown. At first, many thought that all these events were [[Rookie]]'s fault, who had ordered the anvils placed at the [[Beach]] for the [[Underwater Expedition]]. However, it was later revealed that someone else had tripled the order of anvils, leaving some to suspect [[Klutzy]] the crab, who was not in EPF custody at that time.
{{clear}}

==The Meteor Strike==
[[File:MarvelSuperheroTakeoverConsDock.PNG|thumb|250px|The meteor in the [[Dock]].]]
The meteor was first spotted in the sky during the [[Medieval Party 2012]] through the [[Beacon Telescope]]. It crashed on the island on June 7, 2012. It transformed all penguins in super heroes or super villains.

== The Great Storm of 2012 ==
'''The Great Storm of 2012''' was a lightning storm that hit the island on October 17, 2012, when the [[Halloween Party 2012]] started.

==The Second Snow Storm==
[[File:CardJitsuParty2013ConsDojoExt2.PNG|thumb|250px|The [[Dojo Courtyard]] covered by snow.]]
The second snow storm was started in May 9, 2013 in the [[Dojo Courtyard]] and in the [[Ninja Hideout]]. After a week snowing, there was a avalanche that covered the Dojo and the Ninja Hideout. This event happened because of the debut of [[Card-Jitsu Snow]].
{{clear}}

== The Great Storm of 2013 ==
'''The Great Storm of 2013''' was a storm that hit the island on October 16, 2013, when the [[Halloween Party 2013]] started. It was only a lightning storm.

==The Second Rockslide==
[[File:SecondRockslideMine.PNG|thumb|250px|The Mine with the Rockslide.]]
[[File:SecondRockslideLake.PNG|thumb|250px|The Hidden Lake with the Rockslide.]]
The Second Rockslide occurred on November 7, 2013. A big tremor caused a rockslide and the Cave Mine was blocked. After a week, a new mine was discovered (the [[Gold Mine]]) and, in this mine, the [[Gold Puffle]] was discovered as well. 

== The Great Storm of 2014 ==
'''The Great Storm of 2014''' was a storm that hit the island on October 22, 2014, when the [[Halloween Party 2014]] started. It was only a lightning storm.

{{Event}}
[[Category:Lists]]
[[Category:Events]]
