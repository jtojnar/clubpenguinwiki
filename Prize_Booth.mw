{{FA}}
{{Archive}}
{{CatalogInfobox
|name = Prize Booth
|image = File:TheFair2015PrizeBooth-1.PNG
|imagesize = 200px
|caption = The Prize Booth
|date = [[Fall Fair 2008]]
|location = [[Plaza]] (2007)<br>[[Park Entrance]] (2014 and 2015)<br>
'''''Non-Member'''''<br/>[[Forest]]<br>(2008, 2009, 2010, 2011 and 2012)<br />  
----
'''''Member'''''<br/>[[Bonus Game Room|Arcade Circle]] (2008)<br>[[Great Puffle Circus Entrance]] (2009, 2010, 2011 and 2012)<br/>
|sell = Clothes, Backgronds, Pins, Igloos, Puffle Hats
|available = No
|updates= At [[The Fair]]
}}
[[File:October Prize booth.PNG|thumb|The Prize Booth at the [[Forest]] during the [[Fall Fair 2008]].]]

The '''Prize Booth''' was a special booth at [[The Fair]]. It contains prizes that require [[tickets]] from playing the special [[The Fair|Fair]] minigames to redeem. Since 2008, there have been two versions of the prize booth - one for all players and one for [[Membership|members-only]]. Since 2014, there was only one version of the prize booth.

==Appearances==
===Fall Fair 2007===
This booth was located at the [[Plaza]], for both [[non-member]]s and [[members]].

<gallery>
File:Candy Necklace.PNG|[[Candy Necklace]]<br>(700 tickets)
File:Circus Tent Pin.PNG|[[Circus Tent pin]]<br>(100 tickets)
File:PinkCottonCandy.png|[[Cotton Candy (item)|Cotton Candy]]<br>(600 tickets)
File:Feathered Tiara.PNG|[[Feathered Tiara]]<br>(1200 tickets)
File:LollipopItem.PNG|[[Lollipop]]<br>(1000 tickets)
File:PaddleBall.png|[[Paddle Ball]]<br>(1500 tickets)
File:TeddyBearBackgroundIcon.PNG|[[Teddy Bear background]] (400 tickets)
</gallery>

===Fall Fair 2008===
The prize booths were located at the [[Forest]] for [[non-members]] and the [[Arcade Circle]] for [[members]].

====Non-members====
<gallery>
File:Blue Cotton Candy.PNG|[[Blue Cotton Candy]]<br>(600 tickets)
File:CarouselBackgroundIcon.PNG|[[Carousel background]]<br>(400 tickets)
File:Feathered Tiara.PNG|[[Feathered Tiara]]<br>(1200 tickets)
File:GGS.png|[[Giant Green Sunglasses]] (1000 tickets)
File:LollipopItem.PNG|[[Lollipop]]<br>(1000 tickets)
File:PaddleBall.png|[[Paddle Ball]]<br>(1500 tickets)
File:Yellow Balloon Pin.PNG|[[Yellow Balloon pin]]<br>(100 tickets)
</gallery>

====Members====
<gallery>
File:CandyApple.png|[[Candy Apple]]<br>(800 tickets)
File:GiantYellowSunglasses.png|[[Giant Yellow Sunglasses]] (1000 tickets)
File:Bear.png|[[Teddy Bear (item)|Teddy Bear]]<br>(2000 tickets)
</gallery>

===The Fair 2009===
The prize booths were located at the [[Forest]] for non-members and The [[Great Puffle Circus Entrance]] for the members.

====Non-members====
<gallery>
File:Cosmic Star Hat.PNG|[[Cosmic Star Hat]]<br>(100 tickets)
File:Cotton Candy Pin.PNG|[[Cotton Candy pin]]<br>(50 tickets)
File:FairBeaconBackgroundIcon.PNG|[[Fair Beacon background]] (50 tickets)
File:Feathered Tiara.PNG|[[Feathered Tiara]]<br>(100 tickets)
File:Stripey Hat.PNG|[[Stripey Hat]]<br>(150 tickets)
</gallery>

====Members====
<gallery>
File:Cricus tent.png|[[Circus Tent]]<br>(700 tickets)
File:CurlyMustache.png|[[Curly Mustache]]<br>(60 tickets)
File:FairBackgroundIcon.PNG|[[Fair background]]<br>(50 tickets)
File:RingMasterHat.png|[[Ring Master Hat]]<br>(110 tickets)
File:RingMasterOutfit.png|[[Ring Master Outfit]]<br>(300 tickets)
File:SnackStand.PNG|[[Snack Stand]]<br>(300 tickets)
File:Bear.png|[[Teddy Bear (item)|Teddy Bear]]<br>(200 tickets)
</gallery>

===The Fair 2010===
The prize booths were located at the [[Forest]] for non-members and The [[Great Puffle Circus Entrance]] for the members.

====Non-members====
<gallery>
File:Balloon Flower Hat.PNG|[[Balloon Flower Hat]]<br>(110 tickets)
File:BlueFuzzyVikingHat.png|[[Blue Fuzzy Viking Hat]]<br>(125 tickets)
File:FairBeaconBackgroundIcon.PNG|[[Fair Beacon background]] (50 tickets)
File:GreenCosmicHat.png|[[Green Cosmic Hat]]<br>(100 tickets)
File:Popcorn Pin.PNG|[[Popcorn pin]]<br>(50 tickets)
File:Red and Yellow Stripey Hat.PNG|[[Red and Yellow Stripey Hat]] (150 tickets)
</gallery>

====Members====
<gallery>
File:ClownWig.png|[[Clown Hair]]<br>(200 tickets)
File:Clown Shoes.png|[[Clown Shoes]]<br>(150 tickets)
File:Csuit.png|[[Clown Suit]]<br>(300 tickets)
File:PinkCottonCandy.png|[[Cotton Candy (item)|Cotton Candy]]<br>(75 tickets)
File:GreenBalloon.png|[[Green Balloon (clothing)|Green Balloon]]<br>(80 tickets)
File:Ice Cream Cone.PNG|[[Ice Cream Cone]]<br>(75 tickets)
File:PaddleBall.png|[[Paddle Ball]]<br>(40 tickets)
File:Popcorn.png|[[Popcorn (item)|Popcorn]]<br>(75 tickets)
</gallery>

===The Fair 2011===
The prize booths were located at the [[Forest]] for non-members and The [[Great Puffle Circus Entrance]] for the members.

====Non-members====
<gallery>
File:BallPitBackgroundCard.PNG|[[Ball Pit background]]<br>(75 tickets)
File:PurpleBalloonPin.PNG|[[Purple Balloon pin]]<br>(75 tickets)
File:TheBalloonistNew.PNG|[[The Balloonist]]<br>(100 tickets)
File:TheJingleJangle.PNG|[[The Jingle Jangle]]<br>(200 tickets)
File:TheSundaeSurpriseNew.PNG|[[The Sundae Surprise]]<br>(200 tickets)
File:TheTrapezeArtistNew.PNG|[[The Trapeze Artist]]<br>(250 tickets)
</gallery>

====Members====
<gallery>
File:CherryBalloon.PNG|[[Cherry Balloon]]<br>(75 tickets)
File:EveryFlavorIceCream.PNG|[[Every Flavor Ice Cream]]<br>(75 tickets)
File:LemonBalloon.PNG|[[Lemon Balloon]]<br>(75 tickets)
File:MimeBeretNew.PNG|[[Mime Beret]]<br>(200 tickets)
File:MimeCostume.PNG|[[Mime Costume]]<br>(350 tickets)
File:MimeFacePaint.PNG|[[Mime Face Paint]]<br>(150 tickets)
File:SquidHug.PNG|[[Squid Hug]]<br>(300 tickets)
File:TheRedRacer.PNG|[[The Red Racer]]<br>(500 tickets)
File:Turtle.PNG|[[Turtle (item)|Turtle]]<br>(200 tickets)
File:Unicycle.PNG|[[Unicycle]]<br>(300 tickets)
</gallery>

===The Fair 2012===
The prize booths are located at the [[Forest]] for non-members and The [[Great Puffle Circus Entrance]] for the members.
====Non-members====
<gallery>
File:Balloon Flower Hat.PNG|[[Balloon Flower Hat]]<br>(200 tickets)
File:PinkCottonCandy.png|[[Cotton Candy (item)|Cotton Candy]]<br>(100 tickets)
File:FairBeaconBackgroundIcon.PNG|[[Fair Beacon Background]]<br>(100 tickets)
File:LollipopItem.PNG|[[Lollipop]]<br>(150 tickets)
File:PaddleBall.png|[[Paddle Ball]]<br>(100 tickets)
File:TheSundaeSurpriseNew.PNG|[[The Sundae Surprise]]<br>(300 tickets)
File:TheTaaDaaNew.PNG|[[The Taa Daa]]<br>(500 tickets)
</gallery>

====Members====
<gallery>
File:CandyApple.png|[[Candy Apple]]<br>(200 tickets)
File:CurlyMustache2012.PNG|[[Curly Mustache (2012 version)|Curly Mustache]]<br>(180 tickets)
File:EveryFlavorIceCream.PNG|[[Every Flavor Ice Cream]]<br>(150 tickets)
File:FairBackgroundIcon.PNG|[[Fair Background]]<br>(100 tickets)
File:Mischief_Maker_Costume.png|[[Mischief Maker Costume]]<br>(400 tickets)
File:Popcorn.png|[[Popcorn (item)|Popcorn]]<br>(150 tickets)
File:Bear.png|[[Teddy Bear (item)|Teddy Bear]]<br>(400 tickets)
File:TheMischiefMakerNew.PNG|[[The Mischief Maker]]<br>(700 tickets)
File:TheRedRacer.PNG|[[The Red Racer]]<br>(1000 tickets)
</gallery>

===The Fair 2014===
The prize booth is located at the [[Park Entrance]].

====Non-members====
<gallery>
File:CarnivalPartyHat.PNG|[[Carnival Party Hat]]<br>(1000 tickets)
File:PixelShades.PNG|[[Pixel Shades]]<br>(1500 tickets)
File:OldWestLasso.PNG|[[Old West Lasso]]<br>(700 tickets)
File:CarnivalJesterHat.PNG|[[Carnival Jester Hat]]<br>(1000 tickets)
File:BlueBalloonSword.PNG|[[Blue Balloon Sword]]<br>(1500 tickets)
File:SeaFishingRod.PNG|[[Sea Fishing Rod]]<br>(900 tickets)
File:NobleSteed.PNG|[[Noble Steed]]<br>(1500 tickets)
File:TwinkleTwinkleHat.PNG|[[Twinkle Twinkle Hat]]<br>(1000 tickets)
File:RetroJoystick.PNG|[[Retro Joystick]]<br>(500 tickets)
File:CoolDownHat.PNG|[[Cool Down Hat]]<br>(1000 tickets)
</gallery>

====Members====
<gallery>
File:PorkPieHat.PNG|[[Pork Pie Hat]]<br>(900 tickets)
File:CarnivalBarkerOutfit.PNG|[[Carnival Barker Outfit]]<br>(2000 tickets)
File:BlueWheeler.PNG|[[Blue Wheeler]]<br>(3500 tickets)
File:SheriffStetson.PNG|[[Sheriff Stetson]]<br>(4000 tickets)
File:CarnivalCane.PNG|[[Carnival Cane]]<br>(750 tickets)
File:WhitePatentShoes.PNG|[[White Patent Shoes]]<br>(900 tickets)
File:RoosterCostume.PNG|[[Rooster Costume]]<br>(2500 tickets)
File:RoosterFeet.PNG|[[Rooster Feet]]<br>(900 tickets)
File:GreenMotorbike.PNG|[[Green Motorbike]]<br>(3500 tickets)
File:FluffyStuffie.PNG|[[Fluffy Stuffie]]<br>(1500 tickets)
File:CowCostume.PNG|[[Cow Costume]]<br>(2000 tickets)
File:HorseCostume.PNG|[[Horse Costume]]<br>(2000 tickets)
File:QuasarHelmet.PNG|[[Quasar Helmet]]<br>(4000 tickets)
File:PolkaDotPirate.PNG|[[Polka Dot Pirate]]<br>(4000 tickets)
File:BlueMotorbike.PNG|[[Blue Motorbike]]<br>(3500 tickets)
File:TheRedRacer.PNG|[[The Red Racer]]<br>(3000 tickets)
File:HorseHooves.PNG|[[Horse Hooves]]<br>(900 tickets)
File:GreenWheeler.PNG|[[Green Wheeler]]<br>(3000 tickets)
</gallery>

===The Fair 2015===
The prize booth is located at the [[Park Entrance]].

====Non-members====
<gallery>
File:TheGammaGal.PNG|[[The Gamma Gal]]<br>(200 tickets)
File:TheShadowGuy.PNG|[[The Shadow Guy]]<br>(200 tickets)
File:BigGameCatch.PNG|[[Big Game Catch]]<br>(75 tickets)
File:JellyDonutPlush.PNG|[[Jelly Donut Plush]]<br>(75 tickets)
File:BucketHatNew.PNG|[[Bucket Hat]]<br>(75 tickets)
File:RainbowWingedHelm.PNG|[[Rainbow Winged Helm]]<br>(100 tickets)
File:RedPixelGlasses.PNG|[[Red Pixel Glasses]]<br>(100 tickets)
File:Popcorn.png|[[Popcorn (item)|Popcorn]]<br>(50 tickets)
File:Bell.png|[[Bell]]<br>(50 tickets)
File:TheHandyman.PNG|[[The Handyman]]<br>(100 tickets)
</gallery>

====Members====
<gallery>
File:ShadowGuyReboot.PNG|[[Shadow Guy Reboot]]<br>(300 tickets)
File:MonkeyStuffie.PNG|[[Monkey Stuffie]]<br>(150 tickets)
File:GammaGalReboot.PNG|[[Gamma Gal Reboot]]<br>(300 tickets)
File:AncientGreenDragon.PNG|[[Ancient Green Dragon]]<br>(350 tickets)
File:GoldVikingHelmetPuffle.PNG|[[Gold Viking Helmet (puffle hat)|Gold Viking Helmet]]<br>(400 tickets)
File:GreenButterflyWings.PNG|[[Green Butterfly Wings]]<br>(200 tickets)
File:QuasarHelmet.PNG|[[Quasar Helmet]]<br>(400 tickets)
File:BlueFluffyStuffie.PNG|[[Blue Fluffy Stuffie]]<br>(150 tickets)
File:StarfishCostume.PNG|[[Starfish Costume]]<br>(350 tickets)
File:RainbowFro.PNG|[[Rainbow Fro]]<br>(400 tickets)
</gallery>

==Gallery==
<gallery captionalign="left">
File:Fall-fair.jpg|The 2007 Prize Booth located in the Plaza.
File:Proyo.png|The first 2007 Prize Booth.
File:Fall-fair-prizes4.jpg|The updated 2007 Prize Booth.
File:Fallfairforest.png|The 2008 Prize Booth located in the Forest.
File:Update15.png|The first 2008 Prize Booth.
File:October Prize booth.PNG|The updated 2008 Prize Booth.
File:Arcadecircle.png|The 2008 [[Arcade Circle]] Booth located in the same name.
File:Fall-fair-member-ticket-booth.png|The 2008 Arcade Circle Booth.
File:Ff0914.png|The 2009 Prize Booth located in the Forest, once again.
File:Fall-fair-ticket-booth.png|The first 2009 Prize Booth.
File:New-fair-prizes1.png|The updated 2009 Prize Booth.
File:Ff0915.png|The 2009 [[Great Puffle Circus Entrance]] Booth located in the same name.
File:Prize-booth.png|The first 2009 Great Puffle Circus Entrance Booth.
File:New-fair-prizes2.png|The updated 2009 Great Puffle Circus Entrance Booth.
File:0cp.jpg|The 2010 Prize Booth located in the Forest, once again.
File:0cp1.jpg|The first 2010 Prize Booth.
File:TheFair2010_PrizeBooth.png|The updated 2010 Prize Booth.
File:2010prizeboothmembersion.png|The 2010 [[Great Puffle Circus Entrance]] Booth located in the same name, once again.
File:2010fairbtooth.png|The first 2010 Great Puffle Circus Entrance Booth.
File:TheFair2010_MembersPrizeBooth.png|The updated 2010 Great Puffle Circus Entrance Booth.
File:TheFair2011Forest.PNG|The 2011 Prize Booth located in the Forest, once again.
File:Fair11prizebooth1.png|The first 2011 Prize Booth.
File:TheFair2011_PrizeBooth.png|The updated 2011 Prize Booth.
File:TheFair2011GreatPuffleCircusEntrance.PNG|The 2011 [[Great Puffle Circus Entrance]] Booth located in the same name, once again.
File:Fair11prizeboothmember1.png|The first 2011 Great Puffle Circus Entrance Booth.
File:TheFair2011_MembersPrizeBooth.png|The updated 2011 Great Puffle Circus Entrance Booth.
File:TheFair2012Forest.PNG|The 2012 Prize Booth located in the Forest, once again.
File:TheFair2012PrizeBooth1.PNG|The first 2012 Prize Booth.
File:TheFair2012PrizeBooth2.PNG|The updated 2012 Prize Booth.
File:TheFair2012GreatPuffleCircusEntrance.PNG|The 2012 [[Great Puffle Circus Entrance]] Booth located in the same name, once again.
File:TheFair2012MemberPrizeBooth1.PNG|The first 2012 Great Puffle Circus Entrance Booth.
File:TheFair2012MemberPrizeBooth2.PNG|The updated 2012 Great Puffle Circus Entrance Booth.
File:TheFair2014PrizeBooth-1.PNG|The 2014 Booth first part.
File:TheFair2014PrizeBooth-2.PNG|The 2014 Booth second part (when it was closed).
File:TheFair2014Party1.PNG|The 2014 [[Park Entrance]] Booth located in the same name.
File:TheFair2015PrizeBooth-1.PNG|The 2015 Booth first part.
File:TheFair2015PrizeBooth-2.PNG|The 2015 Booth second part.
File:TheFair2015PrizeBooth-3.PNG|The 2015 Booth third part.
File:TheFair2015PrizeBooth-4.PNG|The 2015 Booth fourth part.
File:TheFair2015Party1.PNG|The 2015 [[Park Entrance]] Booth located in the same name.
</gallery>

==Trivia==
*The [[Feathered Tiara]] has been available at the Non-Members' Prize booth for three years in a row. (2007-2009)
*The [[Teddy Bear]] has been available at the Members' Prize booth for two years in a row.
*The [[Paddle Ball]] toy was available on the [[Fall Fair 2007]], [[Fall Fair 2008]], [[The Fair 2010]] and [[The Fair 2012]] events.
*The [[Forest]] has been the location for the Non-Members' Prize booth for four years in a row. (2008-2012)
*After the [[Fall Fair 2008]], the prizes became easier to win, since the prices were lowered. In [[The Fair 2014]], the prices were raised again.

==SWFs==
*[http://media1.clubpenguin.com/play/v2/content/local/en/catalogues/prizebooth.swf Prize Booth (Latest)]
*[http://media1.clubpenguin.com/play/v2/content/local/en/catalogues/prizeboothmember.swf Prize Booth (Latest - Member)]

[[Category:Catalogs]]
[[Category:Exclusive Party Catalogs]]
[[pt:Barraca de Prêmios]]
