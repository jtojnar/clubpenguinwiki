{{ItemInfobox
|name= Coffee Apron
|image= File:CoffeeApron.png
|available=Yes (Penguin Style)
No (Treasure Book)
|type= Body Item
|member= Yes
|party= None
|cost= 100 [[coins]]
|found= [[Penguin Style]]<br>[[Treasure Book]]
|id= 262<br>10262
|unlock= Yes (Series 1)
}}

The '''Coffee Apron''' is a body item in ''[[Club Penguin]]''. [[Members]] are able to buy this item for 100 [[coins]] in the [[Penguin Style]] catalog. It is part of the [[apron]] series. It could also be found in the Series 1 [[Treasure Book]], as well as in ''[[Club Penguin: Elite Penguin Force]]''. If you just wear the apron and [[dance]] then you will make [[coffee]].

It is a body item in game. However, in ''[[Club Penguin: Elite Penguin Force]]'', it is a neck item. You can also earn the [[Coffee Server stamp]] with this item.
==History==
This item was released in March 2006 and is now classified as a common item. It is one of the most well-known and popular items of Club Penguin history, and is mostly worn by penguins who have "jobs" at the [[Coffee Shop]]. 
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan=5|Penguin Style||March 3, 2006||February 1, 2008
|-
|March 7, 2008||April 2, 2008
|-
|January 7, 2011||February 4, 2011
|-
|January 31, 2013||March 7, 2013
|-
|June 30, 2015||{{Available|Items}}
|}

==Gallery==
<gallery captionalign="left">
File:CoffeeApron1.png|The Coffee Apron in-game.
File:CoffeeApron2.png|The Coffee Apron on a [[player card]].
File:Coffee Apron action ingame.PNG|After making coffee.
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Avental do Café
|spanish= Delantal de café
|french= Le Tablier Café
|german= Café-Schürze
|russian= Фартук "Кофе"
}}

==See also==
*[[Apron]]
*[[Coffee Server Stamp]]

==SWF==
===262===
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/262.swf Coffee Apron (icons)]
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/262.swf Coffee Apron (sprites)]
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/262.swf Coffee Apron (paper)]
===10262===
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10262.swf Coffee Apron (icons)]
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10262.swf Coffee Apron (sprites)]
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10262.swf Coffee Apron (paper)]

[[Category:Clothing]]
[[Category:Clothes released in 2006]]
[[Category:Body Items]]
[[Category:Treasure Book Items]]
[[pt:Avental do Café]]
