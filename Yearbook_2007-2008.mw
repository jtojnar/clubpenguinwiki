[[File:Yearbook 2007-2008.PNG|thumb|150px|The cover.]]
The '''Yearbook 2007-2008''' is a book in ''[[Club Penguin]]'' found in the [[Book Room]]'s [[library]]. It tells you the events that happened in Club Penguin from October 2007 up to September 2008. It was released on October 24, 2008, during the [[3rd Anniversary Party]]. It could be found on the table in the [[Book Room]] during all the anniversary parties.

==Content==

===October 2007===
We celebrated our [[2nd Anniversary Party|Second anniversary Party]] at the [[Coffee Shop]] - Complete with a giant cake!
It was a spooky time on the Island, when another eclipse darkened the sky for the [[Halloween Party 2007|Halloween Party]]. The spine-tingling film, [[Night Of The Living Sled (series)|Night of the Living Sled]] made us shudder at the [[Lighthouse]].
We started wearing [[Wigs]], and looked great with [[The Beekeeper|Beekeepers]] and [[The Spikester|Spikesters]]!

===November 2007===
The [[Plaza]] was renovated, and it became a cultural hotspot with the addition of [[The Stage]]. Actors and actresses were invited to star in our first ever play, [[Space Adventure|''Space Adventure!'']].

===December 2007===
During the [[Christmas Party 2007|holiday festivities]], we donated to [[Coins for Change]] and showed we care about the environement and kids around the world.

===January-March 2008===
What a busy start to the year! While we dance with our Sombreros at the [[Winter Fiesta 2008|Winter Fiesta-Val]], superheroes battled against a giant squid in [[Squidzoid vs Shadow Guy and Gamma Gal]]

Divers and Mermaids splashed around in snorkelling gear at our [[Sub-Marine Party 2008|Sub-Marine Party]]. Sporty penguins wore soccer shirts and brought gym kits to work out in their igloo!

===Save the Migrator===

[[The Migrator]], [[Rockhopper]]'s trusty ship, went up against an iceberg and sank. The captain and [[Yarr]] escaped safely and we started the [[Save The Migrator Project]].

Brave divers used the [[Aqua Grabber]] to salvage [[Rockhopper]]'s ship. As a thank you, he gave us the key to the [[Captain's Quarters]].

===April 2008===
We waddled around an artful-looking island and felt dizzy with the [[Forest]] Upside down for our [[April Fools' Party 2008|April Fool's Day Party]]. Penguins [[Easter Egg Hunt 2008|gathered eggs for Easter]] and got new jobs with the Rescue Squad.

===May 2008===
Hark! Brave Knights, Daring Princesses, Ferocious Dragons, and ye olde Blacksmiths did waddleth about the great [[Medieval Party 2008|Medieval Party]]. Penguins Built a secret [[Treetop Fort|Tree-House]] at the Forest, and across the land, good cheer and celebrations could be found.

===June 2008===
Water Balloons were tossed at the [[Water Party 2008|Water Party]], and at [[The Stage]], we travelled back in time with "[[The Penguins That Time Forgot]]." In town, an [[Earthquake Driller|unexpected]] [[List of natural disasters in Club Penguin#The Earthquake|earthquake]] damaged buildings, but quick thinking penguins soon cleaned up the mess.

===July 2008===
July was a busy month! We organised our inventories with improved [[Player Card]]s and sent Postcards with [[Penguin Mail]]. At the [[Music Jam 2008|Music Jam 08]], crowds danced, bands jammed, and penguins made music dance videos. We went wild with the [[Penguin Band]] when they rocked the 'berg and hung out with their [[Backstage]] fans.

===August 2008===
Fitness was the name of the game as we earned medals during the [[Penguin Games 2008|Penguin Games]]. We worked out in a thriathlon, played soccer at the [[Stadium|pitch]], and then relaxed in our new [[Ship Igloo|ship igloos]], Phew!

===September 2008===
We played new games, collected tickets and won new prizes at the [[Fall Fair 2008|Second Fall Fair]]. They were plenty special treats in store, including a colorful carousel and a tasty [[Candy Apple]].

==Gallery==
<gallery>
File:3rdClubPenguinPartyBookRoom.PNG|The Yearbook 2007-2008 during the [[3rd Anniversary Party]] (it is the book in the middle).
File:4thClubPenguinPartyBookRoom.PNG|The Yearbook 2007-2008 during the [[4th Anniversary Party]] (it is the third book in left to right).
File:5thClubPenguinPartyBookRoom.PNG|The Yearbook 2007-2008 during the [[5th Anniversary Party]] (it is the third book in left to right).
</gallery>

==Hidden Pins==
*[[Ping Pong Paddle Pin]]
*[[UFO Pin]]
*[[Holly Pin]]
*[[Book Pin]]
*[[Anchor Pin]]
*[[Pyramid Pin]]
*[[Goblet Pin]]
*[[Icecream Cone Pin]]
*[[Treble Clef Pin]]
*[[150th Newspaper Pin]]
*[[Magnifying Glass Pin]]

==Names in other languages==
{{OtherLanguage
|portuguese= Livro do Ano 2007 - 2008
|spanish= Anuario de Club Penguin 2007 - 2008
|french= Annales de Club Penguin 2007 - 2008
|german= Club Penguin Jahrbuch 2007 - 2008
|russian= Ежегодник "Клуба пингвинов" 2007-2008 гг.
}}

==SWF==
*[http://archives.clubpenguinwiki.info/static/images/archives/5/54/ENBooksYear0708.swf Yearbook 2007-2008]

{{Books}}

[[Category:Books]]
