{{ItemInfobox 
|name= Blue Face Paint
|image= File:Blue Face Paint.PNG
|available= No (Catalog and Party)<br>Yes (Unlockable)
|type= Face Item
|member= No (Parties)<br> Yes (Catalog)
|party= [[Sports Party]], [[Penguin Games]]
|cost= Free, 15 [[coin]]s
|found= [[Pizza Parlor]], [[Snow and Sports]], transfer from ''[[Club Penguin: Game Day!]]'', [[Treasure Book]], ''[[Penguin Style]]''
|id= 134 (Parties), 2027 (Catalog), 10134 (Unlockable)
|unlock= Yes (Series 10 or transfer from ''Club Penguin: Game Day!'')
}}The '''Blue Face Paint''' is a face item in ''[[Club Penguin]]''. Members were able to buy this item for 15 [[coins]]. When doing the wave [[action]] wearing nothing else but the face paint, the [[player]] will hold up a sign saying "GO BLUE!". Similar items are the [[Black Face Paint|Black]], [[Brown Face Paint|Brown]], [[Green Face Paint|Green]], [[Orange Face Paint|Orange]], [[Pink Face Paint|Pink]], [[Red Face Paint|Red]], [[White Face Paint|White]] and [[Yellow Face Paint]]s. In March 2012 Penguin Style, It goes along with [[The Frosting]] or [[The Blueberry]], [[Blue Feather Boa]] or [[Blue Cape]], [[I Heart My Blue Puffle T-Shirt]] and [[Blue Checkered Shoes]].

==History==
The Blue Face Paint is a common item. 
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Party'''!!'''Available from'''!!'''Available until'''
|-
|rowspan=2|None||[[Sports Party]]||August 11, 2006||August 21, 2006
|-
|[[Penguin Games]]||August 22, 2008||August 27, 2008
|-
|''[[Club Penguin: Game Day!]]''||rowspan=4|None||September 24, 2010||''Available''
|-
|[[Snow and Sports]]||September 24, 2010||January 13, 2011
|-
|[[Treasure Book]] - Series 10||September 27, 2010||October 4, 2011
|-
|''Penguin Style''||February 29, 2012||April 4, 2012
|}

==Gallery==
<gallery>
File:BlueFacePaint1.PNG|The Blue Face Paint in-game.
File:BlueFacePaint2.PNG|The Blue Face Paint on a player card.
File:Blue_face_paint.png|A [[penguin]] doing the wave [[action]] with the Blue Face Paint on causing it to hold up a sign that says '''<span style="color:blue">GO BLUE</span>'''.
File:Blue Paint A No No.png|The glitch with the paint in the [[Player Card]].
</gallery>

==Trivia==
*There is a glitch where unless you have unlocked the paint from [[Club Penguin: Game Day]], it only covers a little part of the beak on the player card instead of the whole face. In game it covers the whole face though.
*The item originally had the word "BLUE" on it. It was added back January 16, 2014.

==Names in other languages==
{{OtherLanguage
|portuguese= Tinta Facial Azul
|spanish= Pintura para la cara azul
|french= Le Maquillage Bleu
|german= Blaue Schminke
|russian= Синий грим для лица
}}

==SWF==
===134===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/134.swf Blue Face Paint (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/134.swf Blue Face Paint (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/134.swf Blue Face Paint (paper)]
===2027===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/2027.swf Blue Face Paint (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/2027.swf Blue Face Paint (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/2027.swf Blue Face Paint (paper)]
===10134===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10134.swf Blue Face Paint (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10134.swf Blue Face Paint (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10134.swf Blue Face Paint (paper)]

[[Category:Clothing]]
[[Category:Face Items]]
[[Category:Blue Team]]
[[Category:Free Items]]
[[Category:Clothes released in 2006]]
[[pt:Tinta Facial Azul]]
