{{ItemInfobox
|name= Blue Superhero Mask 
|image= File:BlueSuperheroMask.png
|available= Yes
|type= Face Item
|member= Yes
|party= None
|cost= 100 coins
|found= [[Costume Trunk]]
|id= 123<br>10123 (unlockable)
|unlock = Yes (Series 1, 2, 7, 8, 9, 12)
}}
The '''Blue Superhero Mask''' is a face item in ''[[Club Penguin]]''. [[Members]] are able to buy this item for 100 [[coin]]s in the [[Costume Trunk]] catalog. It was also unlockable for all penguins in the Series 1, 2, 7, 8, 9, and 12 ''[[Treasure Book]]s''. It is part of [[Shadow Guy]]'s costume, along with the [[Shadow Guy Costume]] and [[Blue Cape]]. It is similar to the [[Black Superhero Mask]] and [[Pink Superhero Mask]].

==History==
This item was released in January 2008 for the play [[Squidzoid vs. Shadow Guy and Gamma Gal]]. It is re-released in every encore of the play. It was released on October 19, 2011 for the play [[Night of the Living Sled: Live]].

==History==
The Blue Superhero Mask is a common item.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|''Penguin Style''||April 2006||???
|-
|rowspan=2|Costume Trunk||January 11, 2008||February 7, 2008
|-
|July 11, 2008||August 8, 2008
|-
|[[Treasure Book (Series 1)]]||October 24, 2008||October 4, 2011
|-
|[[Treasure Book (Series 2)]]||December 22, 2008||October 4, 2011
|-
|rowspan=2|Costume Trunk||January 9, 2009||February 13, 2009
|-
|October 8, 2009||November 13, 2009
|-
|[[Treasure Book (Series 7)]]||March 1, 2010||October 4, 2011
|-
|[[Treasure Book (Series 8)]]||April 10, 2010||October 4, 2011
|-
|[[Treasure Book (Series 9)]]||July 12, 2010||October 4, 2011
|-
|Costume Trunk||August 27, 2010||September 17, 2010
|-
|[[Treasure Book (Series 12)]]||February 7, 2011||October 4, 2011
|-
|rowspan=2|Costume Trunk||March 17, 2011||April 21, 2011
|-
|October 19, 2011||February 8, 2012
|-
|rowspan=2|Penguin Style||February 29, 2012||April 4, 2012
|-
|May 30, 2012||September 5, 2012
|-
|rowspan=9|Costume Trunk||July 5, 2012||July 18, 2012
|-
|April 3, 2013||April 24, 2013
|-
|February 12, 2014||March 19, 2014
|-
|June 4, 2014||June 18, 2014
|-
|July 2, 2014||September 17, 2014
|-
|October 1, 2014||December 17, 2014
|-
|December 30, 2014||February 18, 2015
|- 
|March 4, 2015||March 25, 2015
|-
|April 9, 2015||{{Available|Items}}
|}

==Appearances==
*[[Shadow Guy]] uses this item.
*On the [[Join Team Blue postcard]].

==Gallery==
<gallery>
File:BlueSuperheroMask1.PNG|The Blue Superhero Mask in-game.
File:BlueSuperheroMask3.PNG|The Blue Superhero Mask in-game in [[My Penguin]].
File:BlueSuperheroMask2.PNG|The Blue Superhero Mask on a player card.
File:Superheromask.png|The Blue Superhero Mask as seen in the [[Costume Trunk]].
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Máscara Azul de Super-Herói
|spanish= Antifaz de superhéroe azul
|french= Le Masque de Super Héros Bleu
|german= Blaue Superhelden-Maske
|russian= Синяя маска супергероя
}}

==SWF==
===123===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/123.swf Blue Superhero Mask (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/123.swf Blue Superhero Mask (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/123.swf Blue Superhero Mask (paper)]
===10123===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10123.swf Blue Superhero Mask (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10123.swf Blue Superhero Mask (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10123.swf Blue Superhero Mask (paper)]

[[Category:Clothing]]
[[Category:Face Items]]
[[Category:Stage]]
[[Category:Stage Items]]
[[Category:Treasure Book Items]]
[[pt:Máscara Azul de Super-Herói]]
