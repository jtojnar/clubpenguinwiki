{{Archive}}
{{PartyInfobox
|name= Rockhopper's Quest
|image= File:Rockhopper'sQuestlogo.PNG
|imagesize= 250px
|caption= The Rockhopper's Quest logo.
|membersonly= No
|when= February 24, 2012 - March 7, 2012
|famouspenguins = [[Rockhopper]]
|freeitems= '''''Non-Member'''''<br />[[Crew Cap]]<br>[[Life Vest]]<br />[[Rockhopper Background (ID 9075)|Rockhopper Background]]<br>[[Shipwreck Beacon pin]]
----
'''''Member'''''<br/>[[Viking Lord Armor]]<br>[[Viking Lord Helmet]]
|catalog= [[Trading Post Catalog]], [[Dinosaur Catalog]]
|wherehappening= [[Town]], [[Plaza]] [[Beach]], [[Dinosaur Island]], [[Shipwreck Island]] and [[Swashbuckler Trading Post]]
|previous=[[Underwater Expedition]]
|next=[[Puffle Party 2012]]
}}
The '''Rockhopper's Quest''' was a [[party]] in ''[[Club Penguin]]''. It started on February 24, 2012 and ended on March 7, 2012. On Dinosaur Island, there was a [[Dinosaur Catalog]]. Also, at the Swashbuckler Trading Post, there was a [[Trading Post Catalog]]. Halfway through the party, the [[Crew Cap]] was replaced by the [[Life Vest]] at the [[Beach]]. [[Rockhopper]] also made an appearance at the party.

==History==
It was first confirmed in an Australian Disney Magazine<ref> Yarrrrrrrr! How will you fare on Rockhopper's Quest this month?- Australian Disney Magazine</ref> and later confirmed on the membership page.<ref>http://www.clubpenguinwiki.info/static/images/www/7/78/Feb2012MemberPage1.PNG</ref> The mascot messages confirmed ahead of time that Rockhopper will make an appearance at this party. According with the same mascot messages script, that will be there islands to visit: [[Dinosaur Island]], [[Swashbuckler Trading Post]] and [[Shipwreck Island]]. On Shipwreck Island, there was the [[Halls of the Viking Lords|Hall of the Viking Lords]].

==Free items==
{|class="wikitable sortable"
! scope="col"| Image
! scope="col"| Item
! scope="col"| Location
! scope="col"| Members only?
|-
|[[File:CrewCap.png|45px]]
|[[Crew Cap]]
|[[Beach]] (Feb. 23-29)
|No
|-
|[[File:Life Vest.PNG|45px]]
|[[Life Vest]]
|[[Beach]] (Feb. 29-Mar. 7)
|No
|-
|[[File:ShipwreckBeaconPin.PNG|45px]]
|[[Shipwreck Beacon pin]]
|[[Shipwreck Island]]
|No
|-
|[[File:RockhopperBackground3rdCard.PNG|45px]]
|[[Rockhopper Background (ID 9075)|Rockhopper Background]]
|Obtained by meeting [[Rockhopper]]
|No
|-
|[[File:VikingLordHelmetNew.PNG|45px]]
|[[Viking Lord Helmet]]
|[[Viking Hall]]
|Yes
|-
|[[File:VikingLordArmor.PNG|45px]]
|[[Viking Lord Armor]]
|[[Viking Hall]]
|Yes
|}

== Gallery ==
===Sneak Peeks===
[[File:RH-Party.gif|thumb|A sneak peek posted in the What's New Blog.]]
<gallery>
File:Rhquest.png|The [[Pirate Ship]].
File:Screenshot2012-01-28at92722AM-1.png|Another part of the Migrator.
File:RQSneakPeek1.png|The [[Town]]
File:RQSneakPeek2.png|The [[Beach]]
File:RQSneakPeek3.png|Another part of the [[Beach]]
File:RQSneakPeek4.png|Another picture of the [[Pirate Ship]]
File:RQSneakPeek5.png|The [[Shipwreck Island]]
File:Rhmigratorquest2.png|Another picture (Shipwreck Island)
File:Rhquest3.png|Another part of the event.
File:Feb2012MemberPage1.PNG|The party mentioned in the Membership Page.
</gallery>

===General===
<gallery>
File:Rockhoppersquestloginscreen1.PNG|The first [[Login Screen]] for the party.
File:Rhquestscreen2.PNG|The second [[Login Screen]] for the party.
File:Rhquestscreen3.PNG|The third [[Login Screen]] for the party.
File:RHUpgrade.PNG|[[Rockhopper]] and [[Gary]] in the [[Migrator]] upgrade.
</gallery>

===Construction===
<gallery>
File:RQConstBeach.PNG|The [[Beach]] 
File:CP.comRQConstruction.png|[[Club Penguin (website)]]
</gallery>

===Party Pictures===
<gallery>
File:RQCPcom1.PNG|[[ClubPenguin.com]]
File:RockhoppersQuestBeach.PNG|The [[Beach]] (February 23 - 28)
File:RockhoppersQuestBeach1.PNG|The [[Beach]] (February 29 - March 6)
File:RockhoppersQuestDinosaurIsland.PNG|The [[Dinosaur Island]] 
File:RockhoppersQuestDock.PNG|The [[Dock]]
File:RockhoppersQuestMigrator1.PNG|The [[Migrator (Party Room)|Migrator]]
File:RockhoppersQuestPlaza.PNG|The [[Plaza]]
File:RockhoppersQuestShipwreckIsland.PNG|The [[Shipwreck Island]]
File:RockhoppersQuestForts.PNG|The [[Snow Forts]]
File:RockhoppersQuestTown.PNG|The [[Town]]
File:RockhoppersQuestTradingPost.PNG|The [[Trading Post]]
File:RockhoppersQuestVikingHall.PNG|The [[Viking Hall]]
</gallery>

===Notes===
<gallery>
File:QuestMap.PNG|The Quest Map.
File:CargoNote.PNG|The Cargo Note.
</gallery>

==Video==
<youtube>
eDhMFug25yo
</youtube>

==Banner==
[[File:RockhoppersQuest2012Banner.gif]]

==Names in other languages==
{{OtherLanguage
|portuguese= Cabotagem do Rockhopper
|spanish= Travesía Con Rockhopper
|french= La Quéte De Rockhopper
|german= Rockhoppers Reise
}}

==Trivia==
*The party was originally supposed to begin on February 23rd, 2012, but was pushed back a day because the CP staff needed more time to work on it. <ref>http://clubpenguinmemories.com/2012/02/reminder-club-penguin-is-updating-tonight-2/</ref>

{{SWFArchives}}

== Sources and References ==
{{reflist}}

{{Party}}

[[Category:Parties of 2012]]
[[pt:Cabotagem do Rockhopper]]
