{{CharacterInfobox
|name = Ultimate Proto-Bot 10,000
|image = File:ProtobotFutureParty2.PNG
|imagesize= 270px
|fullname = Ultimate Proto-Bot 10,000
|species = Robot
|position = Evil Robot
|appeared = [[Club Penguin: Elite Penguin Force]]<br>[[System Defender]]<br>EPF Messages<br>[[Agent Rookie's Secret Mission]]<br>[[Jetpack Boost]]<br>[[Future Party]]
|color = N/A
|clothes = N/A
|related = Unknown
|friends = [[Herbert]] (formerly), [[Test Robots]]
|walk = No
}}
The '''Ultimate Proto-Bot 10,000''', also known as the '''Protobot''', is a giant robot in ''[[Club Penguin: Elite Penguin Force]]'' that the [[Test Bots]] created using drawings from [[Gary the Gadget Guy|Gary's]] Notebook. They used items around [[Club Penguin Island]] such as [[The Stage]] Ticket Booth, the [[innertube]]s from the [[Dock]] and [[Ski Village]], the [[Aqua Grabber (machine)|Aqua Grabber]] cockpit bubble and red tubes, a [[mine]] cart, the [[Boiler Room]] boiler, two [[Night Club]] Speakers and other items from the test bots. The Ultimate Proto-Bot kidnaps [[Gary the Gadget Guy|Gary]] and the [[Elite Puffles]]. The player must stop the Ultimate Proto-Bot and rescue [[Gary the Gadget Guy|Gary]] and the [[Elite Puffles]]. In the video game, the Ultimate Proto-Bot is 3D, except for in certain sequences and in [[Jet Pack Adventure (DS)|Jet Pack Adventure]]. When you defeat the Robot, you will be promoted and you have finished the major missions. You must use the [[Elite Puffles]] during the battle. 

==History==
===Elite Penguin Force series (2008-2010)===
====[[Club Penguin: Elite Penguin Force]]====
Protobot was created in 2008 by the [[Test Robots]] after they went haywire. He kidnapped [[Gary]] and all of the [[Elite Puffles]], however the player (you) saves them. Protobot was left in ruins on the [[Tallest Mountain]]. However, it wasn't there during the [[Festival of Flight]].

====2010====
In [[Club Penguin: Elite Penguin Force: Herbert's Revenge|Herbert's Revenge]], Herbert helps the EPF by dropping his statue in the [[Mine Shack]], which plugs the hole and saves the island. In doing so, his hot air balloon went out of control, and presumably went to the Tallest Mountain. That is where Herbert re-built Protobot, and they became friends.

===Modern Days (2011-2013)=== 
In 2010, Herbert started rebuilding the Protobot and the [[Test Bots]]. As he was trying to rebuild it, [[G]] reported to us by the [[Field-Op|Field-Ops]] that many objects were being stolen. In 2011, Protobot attacked the EPF System Defender in Level 5. As Protobot and Herbert kept attacking the EPF with different schemes in early 2011, Herbert was getting fed up with Protobot and the Test Bots. During the [[Earth Day 2011]], Protobot had a plan that Herbert thought was "crazy", so he gave the EPF a hint on Protobot's location. The EPF stopped Protobot's plan and destroyed Wheel Bot, which caused Protobot to retreat. In May, Herbert did not like that Protobot wanted to destroy the [[EPF]]. So, he gave the EPF Protobot's location. During [[Field-Op]] 44 Protobot was severely damaged by the EPF's tactical H20 water cannons. He escaped, but he had a system failure in the [[Wilderness]] North of the [[Cove]], which caused him to "shut down". 

In issue 345 of the ''[[Club Penguin Times]]'' (released in May 2012), two hikers reported to find a strange contraption in the Wilderness above the Cove. The "contraption" was indeed a destroyed Protobot, as proved by the picture included with the newspaper article. In Issue #346 of the [[Club Penguin Times]], Protobot announced that the Purple Meteor has revived its circuits and he is trying to take over [[Club Penguin]]. He built "Destructobot", a giant robot that could be battled in the [[Downtown]] during the [[Marvel Superhero Takeover]]. He was the main villain of that party, and was seen around the island until Field-Op 92<ref>http://www.clubpenguinwiki.info/wiki/File:FieldOp92.PNG</ref>.

He also appeared in the book [[Agent Rookie's Secret Mission]], where he attacked the EPF and the reader and [[Rookie]] had to try and defeat him.

He returned again at the [[Marvel Super Hero Takeover 2013]], having pretty much the same role as last time. In issue 392 of the ''[[Club Penguin Times]]'', Protobot announced that the crystals restored his power and began an attack on [[Club Penguin Island|Pengtropolis]]. Ask UP10K also replaced Ask Aunt Arctic on the villain side of the page.

===The Big Return (4014)=== 
Protobot returned in May 2014 during the [[Future Party]]. Gary said that Gary 3000 has spotted him in the future, so Gary built a time portal at the [[Snow Forts]] to go to the future and stop Protobot.
In the future, Protobot is a huge robot that launches meteors<ref>http://archives.clubpenguinwiki.info/static/images/archives/b/b4/ENNews447FeatureMore.swf</ref>. He creates a portal from the sphere in his body and the meteors come from the portal on their way to hit the island. He is now a war robot and can shoot missiles from his shoulder and laser from his arms. He returned along with the [[Test Bots]] to steal [[Gary#Gary 3000|Gary 3000's]] secret space travel technology so he can control all the planets in the galaxy<ref>http://archives.clubpenguinwiki.info/static/images/archives/1/14/ENNews448AskAuntArcticBack.swf</ref>. However, 2014 penguins defeat both Protobot and the [[Test Bots]] joining the [[Extra-Planetary Federation]] and attacking them using the [[robos]]. When the Protobot is defeated, he short-circuits and is thrown to space, fading in the background.

==Trivia==
*In ''[[Club Penguin: Elite Penguin Force]]'', the Ultimate Proto-Bot is made with [[Wikipedia:Computer animation|computer animation]] unlike anything else in the game. This explains why it looks three-dimensional.
*When you defeat the Ultimate Proto-Bot in ''Club Penguin: Elite Penguin Force'', you always have to use the last [[Elite Puffle]] you rescued. [[Blast]] is the first, and [[Pop]] is the last.
*Instead of referring to [[Herbert]] by his real name, the Proto-Bot just calls him [[Polar Bear]]. This irritates him as proven in Field-Op 34 and an [[EPF Spy Phone]] message from March 27, 2011. However, in Field-Op 40, the Protobot called him Herbert. However, Herbert does this in return, calling him Ultimate Pea-brain 10,000, Proto-bumbler, and an overgrown toaster.
*In the [[Test Bots]] original plan, the vault door of the [[Gift Shop]] would also be a part of Protobot's body (probably a shield), but the [[Elite Penguin Force Agents]] stopped them.
*If the [[Test Bots]] had succeeded on stealing the vault door and making it Protobot's shield, it would be way harder to defeat the Protobot and he would be almost invencible.
*According to [[Jet Pack Guy]], the Protobot can control eletronics, causing them to misbehave<ref>http://archives.clubpenguinwiki.info/static/images/archives/0/0c/ENNews445SupportStory.swf</ref>.
*Oddly enough, in the EPF files<ref>http://archives.clubpenguinwiki.info/static/images/archives/0/0c/ENNews445SupportStory.swf</ref> there are photos<ref>http://www.clubpenguinwiki.info/wiki/File:Protobot_Operation_Blackout1.png</ref><ref>http://www.clubpenguinwiki.info/wiki/File:Protobot_Operation_Blackout2.png</ref> of the Protobot appearing in [[Operation: Blackout]] rooms, even though he was supposed to be destroyed at that time since [[Marvel Superhero Takeover 2012]] ended and [[Herbert]] was the main villain during the Blackout.
*When the Protobot hits you in the [[Future Party]], your body burns.
*According to [[Gary#Gary 3000|Gary 3000]], after stealing the [[Extra-Planetary Federation|EPF]] secret, Protobot would simply destroy our planet and then move on to the next<ref>http://archives.clubpenguinwiki.info/static/images/archives/1/14/ENNews448AskAuntArcticBack.swf</ref>, similar to [[wikipedia:Galactus|Galactus]].
*In the [[Future Party|future]], Protobot's body is not made of parts of Club Penguin anymore. Just his head still the same with the [[Aqua Grabber (machine)|Aqua Grabber]] cockpit broken in flame shape.

==Parts==
These are the parts the Ultimate Proto-Bot is made out of.
*[[Aqua Grabber (machine)|AquaGrabber]] Cockpit
*[[Aqua Grabber (machine)|AquaGrabber]] oxygen tubes
*[[Inner Tube|Inner Tubes]]
*[[Stage]] Ticket Booth
*[[Beacon]] Telescope
*[[Mine Cart]]
*[[Night Club]] Stereos
*[[Boiler Room]] boiler (on its back)
*[[Jet Pack]] rockets (only in the [[Club Penguin App]])

==Gallery==
===Artwork===
<gallery>
File:CardJitsu-ProtoBot10000.PNG|[[Card-Jitsu]]
File:Protobot2011.png|[[System Defender]]
File:ProtobotCPTimes245.PNG|Proto-Bot's head.
File:ProtoBotCPTimes246.PNG|Proto-Bot in [[Villain Lair]].
File:ProtoBotIssue392.PNG|Proto-Bot on the issue 392 of the [[Club Penguin Times]]
File:Protobot Flying.png|Proto-Bot flying using two jetpack rockets.
File:ProtobotFutureParty1.PNG|The Proto-Bot in the [[Future Party]]
File:Protobot appearing.png|Protobot appearing.
File:Protobot preparing Laser.png|Protobot preparing his laser.
File:Protobot preparing Missiles.png|Protobot preparing his missiles.
File:Protobot shooting Missiles.png|Protobot shooting his missiles.
File:Protobot creating Portal.png|Protobot creating the portal.
File:Protobot Portal.png|Protobot's portal.
File:Protobot fading.png|Protobot fading in the background after has been defeated.
</gallery>

===Other===
<gallery>
File:Giant Penguin Robot.png|The Proto-Bot seen in [[Club Penguin: Elite Penguin Force]].
File:Protobot Boiler.png|The boiler part of his body can be seen when he is on his back.
File:FieldOp26.png|[[Herbert]] and the [[Test Bots]] trying to steal the ticket booth to rebuild Protobot's body.
File:FieldOp27.png|The [[Wheel Bot]]'s track after stealing the booth.
File:FieldOp28.PNG|[[Herbert]] and the [[Test Bots]] trying to steal the speakers to rebuild Protobot's shoulder.
File:Field-Op 29.png|[[Herbert]] and the [[Test Bots]] trying to steal the [[Aqua Grabber (machine)|Aqua Grabber]] cockpit to rebuild Protobot's head.
File:Field-Op 30.png|[[Herbert]] and the [[Test Bots]] succeeding on stealing the [[mine cart]] and the [[inner tube|inner tubes]] to finally complete the Protobot.
File:FieldOp31.PNG|[[G]] finally figuring out that the Protobot was back. 
File:FieldOp32.PNG|[[Herbert]] admitting that was him who had rebuilt the Protobot.
File:FieldOp44.PNG|[[Herbert]] betraying the Proto-Bot and the [[Test Bots]].
File:FieldOp85.PNG|Protobot's come back during [[Marvel Superhero Takeover 2012]].
File:JetPackBoost-Protobot1.png|The Protobot attacking you in the [[Club Penguin App]] game [[Jetpack Boost]].
File:JetPackBoost-Protobot2.png|The Protobot returning to attack you again.
File:Protobot Case File.png|The Protobot's secret files.
File:Protobot Missiles Penguin On Fire.png|The penguin on fire when hit by Protobot's missiles.
File:Protobot Laser Penguin On Fire.png|The penguin on fire when hit by Protobot's laser.
File:Protobot Operation Blackout1.png|Photo of Protobot spotted in the [[Operation: Blackout]] revealed by [http://archives.clubpenguinwiki.info/static/images/archives/0/0c/ENNews445SupportStory.swf EPF Case File: Protobot].
File:Protobot Operation Blackout2.png|Another photo of Protobot being spotted in the [[Operation: Blackout|Blackout]].
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Protótipo de Destruição Total 10000
|spanish= ProtoBot de Última Generación 10000
|french= Proto-bot Ultra 10 000
|german= Ultimativer Protobot 10.000
|russian= Универсальный протобот
}}

==Sources and References ==
{{reflist}}
==See also==
*[[Club Penguin: Elite Penguin Force]]
*[[Test Bots]]

{{EPF}}
{{Characters}}

[[Category:Machines]]
[[Category:DS]]
[[Category:Villains]]
[[pt:Protótipo de Destruição Total 10000]]
