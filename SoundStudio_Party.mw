{{Archive}}
{{PartyInfobox
|name = SoundStudio Party
|image= File:SoundStudioPartyLogo.PNG
|imagesize = 200px
|caption = SoundStudio Party logo.
|membersonly = No
|when = February 18, 2015 - March 4, 2015
|freeitems = ''[[SoundStudio Party#Free Items|See list]]''
|famouspenguins = [[Cadence]]<br>[[Penguin Band]]
|wherehappening = [[Club Penguin Island]]
|previous= [[Star Wars Rebels Takeover]]
|next= [[Puffle Party 2015]]
}}The '''SoundStudio Party''' was a party in ''[[Club Penguin]]''. It started on February 18, 2015 and ended on March 4, 2015. It was a combination of the [[Music Jam]] and the [[Hollywood Party]]. <ref>http://www.clubpenguin.com/blog/2015/02/soundstudio-party-official-announcement-and-sneak-peek</ref>. This party celebrated the [[SoundStudio]] app that was released on mobile.

== History ==
The party was confirmed on January 15, 2015 by [[Polo Field]] on a meetup <ref>http://www.clubpenguinwiki.info/static/images/www/5/55/HollywoodMusicalPoloField1.jpg</ref>. A few days after, Polo Field confirmed in Twitter that [[Cadence]] song, ''[[You've Got This]]'', will premiere during this party <ref>http://www.clubpenguinwiki.info/static/images/www/0/02/HollywoodMusicalPoloField2.jpg</ref>. On January 20, [[Megg]] posted on the [[What's New Blog]] a concept ar of a room and confirmed that February would be the Music Month <ref>http://www.clubpenguin.com/blog/2015/01/february-music-month-club-penguin</ref>. On February 3, Megg posted on the [[What's New Blog]] the official announcement and the finished room that was presented on January 20 <ref>http://www.clubpenguin.com/blog/2015/02/soundstudio-party-official-announcement-and-sneak-peek</ref>.

==Free Items==
A total of eight free items were made available for this party. Items marked with a badge ([[File:Memberbadge.png|20px]]) indicate that the item can only be obtained by member players.
<gallery>
File:GlowBracelets.PNG|link=Glow Bracelets|'''[[Glow Bracelets]]'''<br/>After completing the music in the [[Dance Stage]].
File:RhinestoneSunglasses‎.PNG|link=Rhinestone Sunglasses‎|'''[[Rhinestone Sunglasses‎]]'''<br/>After completing the music in the [[Pop Stage]].
File:TheRocker-chic‎.PNG|link=The Rocker-chic‎|'''[[The Rocker-chic‎]]'''<br/>After completing the music in the [[Dubstep Stage]].
File:ThePunkhawk‎.PNG|link=The Punkhawk‎|'''[[The Punkhawk‎]]'''<br/>After completing the music in the [[Rock Stage]].
File:DanceCrewOutfit.PNG|link=Dance Crew Outfit|[[File:Memberbadge.png|20px|Members only]] '''[[Dance Crew Outfit]]'''<br/>After completing the music in the [[Dance Stage]].
File:PopStarHoodie.PNG|link=Pop Star Hoodie|[[File:Memberbadge.png|20px|Members only]] '''[[Pop Star Hoodie]]'''<br/>After completing the music in the [[Pop Stage]].
File:DubstepDJOutfit‎.PNG|link=Dubstep DJ Outfit‎|[[File:Memberbadge.png|20px|Members only]] '''[[Dubstep DJ Outfit‎]]'''<br/>After completing the music in the [[Dubstep Stage]].
File:Six-stringAxe.PNG|link=Six-string Axe|[[File:Memberbadge.png|20px|Members only]] '''[[Six-string Axe]]'''<br/>After completing the music in the [[Rock Stage]].
</gallery>

==Gallery==
===Party Pictures===
<gallery widths="180">
File:SoundStudioPartyCoffee.PNG|The [[Coffee Shop]]
File:SoundStudioPartyParty1.PNG|The [[Dance Stage]]
File:SoundStudioPartyDock.PNG|The [[Dock]]
File:SoundStudioPartyParty3.PNG|The [[Dubstep Stage]]
File:SoundStudioPartyForest.PNG|The [[Forest]]
File:SoundStudioPartyParty5.PNG|The [[Limo]]
File:SoundStudioPartyPartySolo1.PNG|The [[Play For DJ Cadence]]
File:SoundStudioPartyPartySolo2.PNG|The [[Play For DJ Cadence]]
File:SoundStudioPartyPartySolo3.PNG|The [[Play For DJ Cadence]]
File:SoundStudioPartyPartySolo4.PNG|The [[Play For DJ Cadence]]
File:SoundStudioPartyPlaza.PNG|The [[Plaza]]
File:SoundStudioPartyParty2.PNG|The [[Pop Stage]]
File:SoundStudioPartyParty4.PNG|The [[Rock Stage]]
File:SoundStudioPartyForts.PNG|The [[Snow Forts]]
File:SoundStudioPartyRink.PNG|The [[Stadium]]
File:SoundStudioPartyStage.PNG|The [[The Stage|Stage]]
File:SoundStudioPartyTown.PNG|The [[Town]]
</gallery>

===Construction===
<gallery widths="180">
File:SoundStudioPartyPreForts.PNG|The [[Snow Forts]]
</gallery>

===Sneak Peeks===
<gallery widths="180">
File:SoundStudioPartySP1.PNG|A sneak peek on the What's New Blog.
File:SoundStudioPartySP2.PNG|A sneak peek on the What's New Blog.
</gallery>

===Login Screens===
<gallery widths=180>
File:SoundStudioPartyLoginScreen1.PNG|The [[Login Screen]].
</gallery>

===Miscellaneous===
<gallery widths=180>
File:SoundStudioPartyHomepage1.JPG|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:SoundStudioFacebookHeader.PNG|Club Penguin's Facebook header.
File:MapSoundStudioParty.PNG|The [[map]] of the island during the party.
</gallery>

==Trivia==
*Although [[Cadence]] and the [[Penguin Band]] were supposed to waddle around during the party, they didn't because of a glitch. Despite Polo Field claiming the glitch would be fixed, it never was.

==Names in other languages==
{{OtherLanguage
|portuguese= Festa SoundStudio
|spanish= Fiesta SuperDJ
|french= Fête StudioMix
|german= Tonstudio Party
|russian= Праздник Студии звукозаписи
}}

==Sources and References==
{{reflist}}

{{SWFArchives}}

{{Party}}
[[Category:Parties of 2015]]
[[pt:Festa SoundStudio]]
