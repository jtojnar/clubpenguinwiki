{{ItemInfobox 
|name= Green Ball Cap
|image= File:GreenBallCap.png
|available= Yes
|type= Head Item
|member= No (starting with 2013)
|party= None
|cost= 200 [[Coins]]
|found= [[Snow and Sports]]<br>[[Penguin Style]]
|id= 405<br>1539
|unlock= No
}}

The '''Green Ball Cap''' (formerly the '''Ball Cap''') is a head [[item]] in [[Club Penguin]]. It costs 200 [[coin]]s, and it is available to all players. It is also available in the [[My Penguin]]'s Penguin Style. This item goes with the [[Green Baseball Uniform]] and the [[Baseball Glove]]. It was formerly a members-only item. Since the January 2013 [[Penguin Style]] catalog, it is classified as an item for everyone.

==History==
<!-- Removed from first sentence: "that first came out in 2005". Needs verification on first release date. -->
The Green Ball Cap was released for the first time in 2007 and is a common item.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan=2|Snow and Sports||August 31, 2007||April 25, 2008
|-
|February 25, 2010||May 27, 2010
|-
|rowspan=2|Penguin Style||February 6, 2009||May 1, 2009
|-
|January 3, 2013||rowspan=2|''Available''
|-
|Penguin Style ([[My Penguin]])||May 8, 2013
|}

==Appearances==
*[[Club Penguin Times]] Issue #35
*[[Billybob]] can usually be seen wearing this item. 
*On the cover of the September 2007 Snow and Sports catalog.
*In the video [[Best Seat in the House]].

==Gallery==
<gallery>
File:GreenBallCap1.PNG|The Green Ball Cap in-game.
File:GreenBallCap2.PNG|The Green Ball Cap on a player card.
File:GreenBallCap3.png|The non-member version of the Green Ball Cap on a player card.
File:CPTimes35Penguin5.PNG|A penguin wearing the Green Ball Cap along with a [[Whistle]].
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Boné Verde de Jogo
|spanish= Gorrita deportiva verde
|french= La Casquette Verte
|german= Grüne Baseball-Mütze
|russian= Зелёная бейсболка
}}

==SWF==
===405===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/405.swf Green Baseball Cap (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/405.swf Green Baseball Cap (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/405.swf Green Baseball Cap (paper)]
===1539===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/1539.swf Green Baseball Cap (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/1539.swf Green Baseball Cap (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/1539.swf Green Baseball Cap (paper)]

==See also==
*[[Ball Cap]]
*[[Billybob]]

[[Category:Clothing]]
[[Category:Head Items]]
[[Category:Green Team]]
[[Category:Clothes released in 2007]]
[[Category:Available Items]]
