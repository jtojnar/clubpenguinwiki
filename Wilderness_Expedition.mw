{{Archive}}
{{PartyInfobox
|name= Wilderness Expedition
|image= File:WildernessExpeditionLogo.png
|imagesize=190px
|caption= The Wilderness Expedition Logo
|membersonly= No
|when= January 17, 2011 - January 31, 2011 
|freeitems= '''''Non-Member'''''<br>[[Expedition Hat]]
----
'''''Member'''''<br>[[Brown Puffle]]<br>[[Brown Puffle House (2011 version)|Brown Puffle House]]
|famouspenguins = None
|catalog= [[Shore Catalog]]
|wherehappening= [[Dock]], [[Cove]], [[Town]], [[Plaza]], [[Glade]], [[Wilderness (room)|Wilderness]], [[Cliff]], [[Shore]], [[Bay]], [[Brown Puffle Cave]].
|previous= [[Holiday Party 2010]]
|next= [[Puffle Party 2011]]
}}
{{cquote2|We found brown puffles!|[[Happy77]]}}
The '''Wilderness Expedition''' was an activity that began on January 17th 2011 and ended on the 31st. It was confirmed in issue #273 of the [[Club Penguin Times]]. The dates were also mentioned in a login screen for the [[party]]. Everyone can obtain an [[Expedition Hat]] at the [[Glade]]. [[Brown Puffle]]s were first introduced here and could be adopted by [[members]] for free at the [[Brown Puffle Cave]]. There were two member items, a new [[Life Jacket (2011 version)|Life Jacket]], for sale at the [[Shore]] that cost 50 [[coin]]s and a free [[Brown Puffle House (2011 version)|Brown Puffle House]].

==Free items==
{|class="wikitable sortable"
! scope="col"| Image
! scope="col"| Item
! scope="col"| Location
! scope="col"| Members only?
|-
|[[File:Expedition hat.png|45px]]
|[[Expedition Hat]]
|[[Glade]]
|No
|-
|[[File:BROWNpuffle.png|45px]]
|[[Brown Puffle]]
|[[Brown Puffle Cave]]
|Yes
|-
|[[File:Brown Puffle House 2011.PNG|45px]]
|[[Brown Puffle House (ID 665)|Brown Puffle House]] <small>''(furniture)''</small>
|[[Brown Puffle Cave]]
|Yes
|}

== Party rooms ==
* [[Glade]]
* [[Wilderness (room)]]
* [[Cliff]]
* [[Shore]]
* [[Bay]]
* [[Brown Puffle Cave]]

==Mysterious notes==
In several rooms, there were several pieces of paper with notes on them:
*'''Glade:''' ''I wonder where these machine parts are from? I'll follow them.''
*'''Lost Room:''' ''It seems I've lost my way... I'll take one of these trails back to the start.''
*'''Cliff:''' ''Who or what built this weird machine? How do I switch it on?''
*'''Shore:'''
:''Notes''
:''- wear a life jacket - build the boat''
:''Must find out what's across this Bay.''
*'''Brown Puffle Cave:'''
:''What a find! I've been searching for a brown puffle for so long!''
:''Notes''
::''Are those safety goggles?''
::''Did it build the machine on the cliff?''
:''It looks like only one will follow me. Excited to study these creatures.''
:''Adopt a puffle '''or''' You have already adopted a brown puffle.''

It was revealed in "The Epic Official Guide to Club Penguin: Ultimate Edition" book that [[PH]] was the one who left all the notes behind. 

==Gallery==
===Sneak Peeks===
<gallery>
File:Join The Wilderness Expedition Logon Screen.PNG|The log in screen for the expedition. 
File:Explorers Planning expedition.PNG|An image from the [[Club Penguin Times]].
File:Join The Wilderness Expedition.PNG|Another image from the [[Club Penguin Times]].
File:Explorers Excited.PNG|An image from the [[Club Penguin Times]].
File:Roykoopa.png|Another login screen for the expedition.
</gallery>

===Party Pictures===
<gallery>
File:WildernessExpedition2011Bay.PNG|The [[Bay]]
File:Wilderness Expedition Brown Puffle Cave.PNG|The [[Brown Puffle Cave]]
File:Cliff expedition.PNG|The [[Cliff]]
File:WildernessExpeditionCove.PNG|The [[Cove]] when expedition is complete.
File:WE-Dock.png|The [[Dock]]
File:Glade.png|The [[Glade]]
File:WildernessExpeditionPlaza.PNG|The [[Plaza]]
File:WildernessExpedition2011Shore.PNG|The [[Shore]]
File:WildernessExpeditionTown.png|The [[Town]]
File:Wilderness Room 1.png|The [[Wilderness (room)|Wilderness]] - Room 1
File:Wilderness Room 2.png|The [[Wilderness (room)|Wilderness]] - Room 2
File:Wilderness Room 3.png|The [[Wilderness (room)|Wilderness]] - Room 3
File:Wilderness Room 4.png|The [[Wilderness (room)|Wilderness]] - Room 4
File:Wilderness room 5.png|The [[Wilderness (room)|Wilderness]] - Room 5
File:Wilderness room 6.png|The [[Wilderness (room)|Wilderness]] - Room 6
File:Wilderness room 7.png|The [[Wilderness (room)|Wilderness]] - Room 7
File:Wilderness lost room.png|The [[Wilderness (room)|Wilderness]] - Lost Room
</gallery>

===Mysterious notes===
<gallery>
File:Mysterious Notes glade.PNG|The notes at the Glade.
File:Mysterious Notes lost.PNG|The notes when you're lost.
File:Mysterious Notes cliff.PNG|The notes at the Cliff.
File:Mysterious Notes shore.PNG|The notes at the Shore.
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Expedição à Natureza
|spanish= Expedición a la naturaleza
|french= L'Expédition Nature
|german= Wildnis-Expedition
}}

{{SWFArchives}}

{{Party}}

[[Category:Events]]
[[Category:Parties of 2011]]
[[pt:Expedição à Natureza]]
