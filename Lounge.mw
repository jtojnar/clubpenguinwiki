{{Seeideas}}
{{RoomInfobox
|title= Lounge
|image= File:DL DL.png
|place= Above the [[Night Club]]
|open= October 24, 2005
|games= [[Astro Barrier]]<br> [[Thin Ice]]<br> [[Bits and Bolts]]
|roomid= 121
|tourguide= "{{TourLounge}}"
}}

The '''Lounge''' (formerly '''Dance Lounge''') is a room in ''[[Club Penguin]]''. It is the second floor of the [[Dance Club]]. It is a place where penguins can rest after spending time dancing in the [[Night Club]]. There are tables and arcade games called [[Thin Ice]], [[Astro Barrier]] and [[Bits & Bolts]] in the Lounge.

==Appearance==
The Lounge is a room often decorated for parties. Any [[penguin]] can play the arcade games [[Astro Barrier]], [[Bits & Bolts]] and [[Thin Ice]]. You can see the bottom floor (the [[Night Club]]) through a glass on the floor of this room. This room also houses the target-hitting game here, which has the background of the [[stage]] play [[Squidzoid vs. Shadow Guy and Gamma Gal]]. It also has [[Melmonst]], who appears as a target.

At one time [[Thin Ice]] was not a game there but it was later added in the form of a crate and then revealed to be a new game. The same music from the [[Night Club]] is heard in the lounge.

[[Bits & Bolts]] was released on March 28, along with the renovation of the Lounge.

==Pins==
{|border="1" class="wikitable" 
!'''Pin #'''!!'''Pin name'''!!'''Available from'''!!'''Available until'''
|-
|3||[[File:Plant Pin.PNG|30px]][[Plant pin]]||April 14, 2006||April 28, 2006
|-
|27||[[File:Shrimp Pin.PNG|30px]][[Shrimp pin]]||March 2, 2007||March 17, 2007
|-
|36||[[File:Water Droplet Pin.PNG|30px]][[Water Droplet pin]]||July 6, 2007||July 20, 2007
|-
|46||[[File:UFO Pin.PNG|30px]][[UFO pin]]||November 9, 2007||November 27, 2007
|-
|75||[[File:3rd Anniversary Cake Pin.PNG|30px]][[3rd Anniversary Cake pin]]||October 24, 2008||November 7, 2008
|-
|115||[[File:Buckle Boot Pin.PNG|30px]][[Boot pin]]||March 11, 2010||March 25, 2010
|-
|145||[[File:ConchShell.PNG|30px]][[Fire Extinguisher pin]]||January 27, 2011||February 10, 2011
|-
|258||[[File:PuffleMedicPin.png|30px]][[Puffle Medic pin]]||November 27, 2013||December 11, 2013
|-
|278||[[File:SpaceAcademyCrestpin.PNG|30px]][[Space Academy Crest pin]]||May 28, 2014||June 11, 2014
|}

==Minigames==
Three minigames are present in this room, and they are all arcade games. However, none of them appear in [[Club Penguin: Elite Penguin Force]].
*[[Astro Barrier]]
*[[Thin Ice]]
*[[Bits & Bolts]]

==Trivia==
*During [[Puffle Parties]] this room is where the [[Brown Puffle]] domain is.

==Gallery==
<gallery>
File:DanceLounge2006.png|The Lounge before the addition of [[Thin Ice]].
File:DL 3-17-11.png|The old Lounge.
File:DL DL.png| The new Lounge.
File:Music_Jam_Lounge.PNG|During the [[Music Jam 2008]].
File:Lounge09.png|Dance Lounge during the [[Halloween Party 2009]].
File:Christmas_Party_2009_Dance_Lounge.PNG|During the [[Holiday Party 2009]].
File:Dancelonge.png|The Dance Lounge during the 2009 [[Dance-A-Thon]] [[party]].
File:Ff097.png|During [[The Fair 2009]].
File:MedievalParty2010Dance_Lounge.PNG|During the [[Medieval Party 2010]].
File:MusicJam2010Dance_Lounge.PNG|During the [[Music Jam 2010]].
File:Holiday Party 2010 Dance Lounge.PNG|During the [[Holiday Party 2010]].
File:Puffle Party 2011 Dance Lounge.PNG|During the [[Puffle Party 2011]].
File:DanceLoungeSneakPeak.png|A sneak peek of the new Dance Lounge, as seen on the [[What's New Blog]].
File:Targetlounge.png|The target game.
File:MedievalParty2011DanceLounge.PNG|During the [[Medieval Party 2011]].
File:MusicJam2011 Lounge.PNG|During the [[Music Jam 2011]].
File:HalloweenParty2011-Lounge.PNG|During the [[Halloween Party 2011]].
File:HolidayParty11Lounge.png|During the [[Holiday Party 2011]].
File:PuffleParty2012Lounge.PNG|During the [[Puffle Party 2012]].
File:MedievalParty2012Lounge.PNG|During the [[Medieval Party 2012]].
File:UltimateJamLounge.PNG|During the [[Make Your Mark: Ultimate Jam]].
File:HalloweenParty2012Lounge.PNG|During the [[Halloween Party 2012]].
File:PuffleParty2013Lounge.PNG|During the [[Puffle Party 2013]].
</gallery>

==Names in Other Languages==
{{OtherLanguage
|portuguese = Área VIP
|spanish = Living
|french = Loft
|german= Lounge
|russian= Комната отдыха
}}

==SWF==
*[http://media1.clubpenguin.com/play/v2/content/global/rooms/lounge.swf Lounge]
*[http://media1.clubpenguin.com/play/v2/content/global/music/5.swf Old Music]
*[http://media1.clubpenguin.com/play/v2/content/global/music/665.swf New Music]

==See also==
*[[Night Club]]
*[[Astro Barrier]]
*[[Thin Ice]]
*[[Bits & Bolts]]

{{Geographic location
|Centre    = Lounge
|North     = 
|Northeast = 
|East      = [[Night Club]]
|Southeast = 
|South     = 
|Southwest = 
|West      = 
|Northwest = 
}}


{{Places}}

[[Category:Places]]
[[Category:Town]]

[[pt:Área VIP]]
