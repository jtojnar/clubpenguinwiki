{{Archive}}
{{PartyInfobox
|name = Star Wars Takeover
|image = File:StarWarsTakeoverLogo.PNG
|imagesize = 200px
|caption = Star Wars Takeover logo.
|membersonly = No
|when = July 24, 2013 - August 14, 2013
|freeitems = '''''Non-Member'''''<br />[[Stormtrooper Helmet]], [[Rebel Reward Pin]], [[Rebel Helmet]], [[Death Star Plans background]], [[X-wing Helmet]], [[Yavin 4 background]], [[Dark Side Giveaway]]
----
'''''Member'''''<br />[[Stormtrooper Costume]], [[Sandtrooper Pauldrons]], [[Rebel Boots]], [[Rebel Costume]], [[X-wing Pilot Costume]], [[Rebel Reward Medal]], [[Anakin's Lightsaber]], [[The Padawan]], [[Jedi Robes]], [[Jedi Cloak]], [[Black Jedi Robes]], [[Luke's Lightsaber]], [[Obi-Wan Cloak]], [[Darth Vader Helmet]], [[Darth Vader Costume]], [[Darth Vader's Lightsaber]]
|catalog = [[Star Wars Catalog]]
|famouspenguins = [[Herbert P. Bear|Darth Herbert]]
|wherehappening = [[Club Penguin Island]]<br>[[Tatooine]]<br>[[Death Star]]<br>[[Yavin 4]]
|previous= [[Monsters University Takeover]]
|next= [[Teen Beach Movie Summer Jam]]
}} 
The '''Star Wars Takeover''' was a party on ''[[Club Penguin]]''. It started on July 24, 2013 and ended on August 14, 2013. <ref>https://secure.clubpenguin.com/membership/login.php</ref> This was the third takeover party of the year after [[Marvel Super Hero Takeover 2013|Marvel Super Hero Takeover]] and [[Monsters University Takeover]]. [[Herbert]], [[Cadence]], [[Sensei]], and [[Jet Pack Guy]] played the roles of [[wikipedia:Darth Vader|Darth Vader]], [[wikipedia:Princess Leia|Princess Leia]], [[wikipedia:Obi-Wan Kenobi|Obi-Wan Kenobi]], and a Rebel pilot respectively. 

==History==
The first sign of a Star Wars party was in June 2013, when a space background appeared on [http://clubherbert.com/ clubherbert.com]. It wasn't the background that had anything to do with Star Wars, but the print at the bottom of the page that had "STAR WARS" in it. On June 20, 2013, [[Polo Field]] published on the What's New Blog a video that revealed the occurrence of the party in July.<ref>http://www.clubpenguin.com/blog/2013/06/special-july-2013-party-announcement</ref> On July 3, 2013, a trailer was released for the party that revealed the plot, as well as a few characters ([[Herbert]] as [[wikipedia:Darth Vader|Darth Vader]] and [[Cadence]] as [[wikipedia:Princess Leia|Princess Leia]]). On July 4, new login and logout screens were released, at the same time when on the [[Club Herbert (website)|Club Herbert]] site appeared the ''I ♥ Dark Side'' message. On July 11, the [[Club Penguin Times]] announced that the Rebel Alliance and the [[Millennium Falcon]] would be landing on the island on July 25, seeking help against the Imperial forces. From the same day, the [[Death Star]] could be seen at the [[Mountain]], [[Beach]], [[Dock]], [[Town]] [[Iceberg]], [[Ski Village]] and the telescope. A new commercial from Disney Channel, "Game On", revealed future costumes, rooms, characters, plot, catalog and the existence of four parallel worlds to Club Penguin. [http://movies.yahoo.com Yahoo Movies] revealed on July 11, 2013 that this would be the biggest party in the history of the game and it would come close to 18 decorated rooms, three mini games and 20 or more fantasy themes. On July 12th<ref>http://www.clubpenguin.com/blog/2013/07/stormtroopers-work-drawing-droids</ref>, 15th<ref>http://www.clubpenguin.com/blog/2013/07/stormtroopers-work-writing</ref>, 17th<ref>http://www.clubpenguin.com/blog/2013/07/stormtroopers-work-coffee-break</ref>, 23rd<ref>http://www.clubpenguin.com/blog/2013/07/stormtroopers-work-managing-projects</ref> and 24th <ref>http://www.clubpenguin.com/blog/2013/07/stormtroopers-work-music-break</ref>, Polo Field published some images on the What's New Blog in order for penguins to caption them. The Club Penguin Staff also watched ''A New Hope'' during production of the party to be inspired and make sure they were accurate.<ref>http://www.clubpenguinwiki.info/static/images/www/a/ad/CPTeamStudies.png</ref>

==Development==
The party focuses its main decorations on other worlds parallel to [[Club Penguin Island]], as [[Millennium Falcon]], [[Tatooine]], [[Death Star]] and [[Yavin 4]]. On the Island, the only decoration is at the [[Dock]], where the Millennium Falcon and a transmitting antenna are located. When a player enters the spacecraft, he quickly takes off into the sky and all the controls are activated. The Death Star can still be seen from the Dock, [[Town]], [[Ski Village]], [[Iceberg]], [[Ski Hill]] and [[Beach]]. The [[Star Wars Catalog]] is available at the [[Dock]] and all other rooms of the Star Wars universe. Costumes and other items are available for members and non-members.

A button located in the upper right of the screen provides shows a map with 5 worlds and the Jedi Master Journey, led by [[Sensei]] as Obi-Wan Kenobi, and giving players costumes when they win 30 duels progressively. On Tatooine there is a unique map that shows the three components of the planet rooms. In this same world you can play [[Defeat Stormtroopers]], an exclusive mini-game game where the player must throw snowballs at stormtroopers during 3 levels. Each level has a different difficulty and releases a different item.

The 2<sup>nd</sup> leg of the mission for the event, which is located on the Death Star, was released on August 1. [[Cadence]] as Princess Leia gives players the mission to infiltrate the Death Star and can thus enter the planet. There, they must go through the halls of the Death Star unlocking the terminal security through the exclusive mini-game [[Infiltrate the Death Star]]. When the entire route is completed, the player discovers the Secret Plans of Darth Herbert. The Secret Plans contain a technical weakness of the Death Star, which rebels would use to destroy it on August 8, when [[Yavin 4]] and a new game was be released.

==Factions==
{{expansion}}
As with many other [[Takeover|takeovers]], this party had temporary new factions (aka teams) for people to play on and dress up as. After the party these factions still existed, but not officially.
===Jedi===
During this party, members could become a Jedi by completing the Jedi Master Journey. To complete the Jedi Master Journey, you must duel other Jedi and win, at which time free items such as lightsabers and Jedi robes are also given to the player.

===Sith===
After members win 30 Jedi duels in the Jedi Master Journey, they receive Darth Vader's [[Darth Vader Helmet|helmet]], [[Darth Vader Costume|costume]], and [[Darth Vader's Lightsaber|lightsaber]], at which time they can choose to be a Sith rather than a Jedi by using the red lightsaber and donning some dark cloaks or a Sith's outfit (Darth Vader or Emperor Palpatine). 
===Rebels===
===Imperials===

==Free items==
{|class="wikitable sortable"
! scope="col"| Image
! scope="col"| Item
! scope="col"| Location
! scope="col"| Members only?
|-
|[[File:StormtrooperHelmet.PNG|45px]]
|[[Stormtrooper Helmet]]
|[[Defeat Stormtroopers]]
|No
|-
|[[File:RebelRewardPin.PNG|45px]]
|[[Rebel Reward Pin]]
|[[Infiltrate the Death Star]]
|No
|-
|[[File:RebelHelmet.PNG|45px]]
|[[Rebel Helmet]]
|[[Infiltrate the Death Star]]
|No
|-
|[[File:DeathStarPlansBackgroundIcon.PNG|45px]]
|[[Death Star Plans background]]
|[[Infiltrate the Death Star]]
|No
|-
|[[File:X-wingHelmet.PNG|45px]]
|[[X-wing Helmet]]
|[[Defeat TIE Fighters]]
|No
|-
|[[File:Yavin4BackgroundIcon.PNG|45px]]
|[[Yavin 4 background]]
|[[Defeat TIE Fighters]]
|No
|-
|[[File:DarkSideGiveawayIcon.PNG|45px]]
|[[Dark Side Giveaway]]
|Obtained by meeting [[Herbert P. Bear|Darth Herbert]]
|No
|-
|[[File:StormtrooperCostume.PNG|45px]]
|[[Stormtrooper Costume]]
|[[Defeat Stormtroopers]]
|Yes
|-
|[[File:SandtrooperPauldrons.PNG|45px]]
|[[Sandtrooper Pauldrons]]
|[[Defeat Stormtroopers]]
|Yes
|-
|[[File:RebelBoots.PNG|45px]]
|[[Rebel Boots]]
|[[Infiltrate the Death Star]]
|Yes
|-
|[[File:RebelCostume.PNG|45px]]
|[[Rebel Costume]]
|[[Infiltrate the Death Star]]
|Yes
|-
|[[File:X-wingPilotCostume.PNG|45px]]
|[[X-wing Pilot Costume]]
|[[Defeat TIE Fighters]]
|Yes
|-
|[[File:RebelRewardMedal.PNG|45px]]
|[[Rebel Reward Medal]]
|After destroy the [[Death Star]]
|Yes
|-
|[[File:Anakin'sLightsaber.PNG|45px]]
|[[Anakin's Lightsaber]]
|Jedi Master Journey 
|Yes
|-
|[[File:ThePadawan.PNG|45px]]
|[[The Padawan]]
|Jedi Master Journey 
|Yes
|-
|[[File:JediRobes.PNG|45px]]
|[[Jedi Robes]]
|Jedi Master Journey 
|Yes
|-
|[[File:JediCloak.PNG|45px]]
|[[Jedi Cloak]]
|Jedi Master Journey 
|Yes
|-
|[[File:Luke'sLightsaber.PNG|45px]]
|[[Luke's Lightsaber]]
|Jedi Master Journey 
|Yes
|-
|[[File:BlackJediRobes.PNG|45px]]
|[[Black Jedi Robes]]
|Jedi Master Journey 
|Yes
|-
|[[File:BlackJediCloak.PNG|45px]]
|[[Black Jedi Cloak]]
|Jedi Master Journey 
|Yes
|-
|[[File:Obi-WanCloak.PNG|45px]]
|[[Obi-Wan Cloak]]
|Obtained by winning 30 Jedi Duels.
|Yes
|-
|[[File:DarthVaderMaskIcon.PNG|45px]]
|[[Darth Vader Helmet]]
|Obtained by winning 30 Jedi Duels.
|Yes
|-
|[[File:DarthVaderCostumeIcon.PNG|45px]]
|[[Darth Vader Costume]]
|Obtained by winning 30 Jedi Duels.
|Yes
|-
|[[File:DarthVader'sLightsaber.PNG|45px]]
|[[Darth Vader's Lightsaber]]
|Obtained by winning 30 Jedi Duels.
|Yes
|}

==Gallery==
===Login and Exit Screen===
<gallery widths=180>
File:StarWarsTakeoverLoginScreen1.PNG|The first [[Login Screen]].
File:StarWarsTakeoverLoginScreen2.PNG|The second [[Login Screen]].
File:StarWarsTakeoverLoginScreen3.PNG|The third [[Login Screen]].
File:StarWarsTakeoverLoginScreen4.PNG|The fourth [[Login Screen]].
File:StarWarsTakeoverExitScreen.jpg|The first [[Exit Screen]].
File:StarWarsTakeoverExitScreen2.PNG|The second [[Exit Screen]].
File:StarWarsTakeoverExitScreen3.jpeg|The third [[Exit Screen]].
File:StarWarsTakeoverExitScreen4.PNG|The fourth [[Exit Screen]].
</gallery>

===Miscellaneous===
<gallery widths=180>
File:StarWarsTakeoverHomepage1.JPG|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:StarWarsTakeoverHomepage2.JPG|Club Penguin's homepage (Slide 2).
File:StarWarsTakeoverHomepage3.JPG|Club Penguin's homepage (Slide 3).
File:StarWarsTakeoverHomepage4.JPG|Club Penguin's homepage (Slide 4).
File:StarWarsTakeoverClubHerbert1.PNG|[http://ClubHerbert.com ClubHerbert.com] (June 19<sup>th</sup>)
File:StarWarsTakeoverClubHerbert2.JPG|ClubHerbert.com (July 3<sup>rd</sup>)
File:StarWarsTakeoverClubHerbert3.JPG|ClubHerbert.com (July 17<sup>th</sup>)
File:StarWarsDeathStar.png|'Death Star', seen from the [[Beach]], [[Ski Hill]], [[Ski Village]], [[Iceberg]] and telescope.
</gallery>

===Other===
<gallery widths=180>
File:StarWarsTakeoverFacebookIcon.JPG|Facebook's icon
File:StarWarsTakeoverFacebookCover.JPG|Facebook's cover
</gallery>

===Construction===
====Week 1====
<gallery widths=180>
File:StarWarsPreparationsBeach.PNG|The [[Beach]]
File:StarWarsPreparationsDock.PNG|The [[Dock]]
File:StarWarsPreparationsBerg.PNG|The [[Iceberg]]
File:StarWarsPreparationsSkiHill.PNG|The [[Ski Hill]]
File:StarWarsPreparationsVillage.PNG|[[Ski Village]]
File:StarWarsPreparationsTown.PNG|The [[Town]]
</gallery>

====Week 2====
<gallery widths=180>
File:StarWarsPreparations2Beach.PNG|The [[Beach]]
File:StarWarsPreparations2Dock.PNG|The [[Dock]]
File:StarWarsPreparations2Berg.PNG|The [[Iceberg]]
File:StarWarsPreparations2SkiHill.PNG|The [[Ski Hill]]
File:StarWarsPreparations2Village.PNG|[[Ski Village]]
File:StarWarsPreparations2Town.PNG|The [[Town]]
</gallery>

=== Party ===
====Before destruction of [[Death Star]]====
<gallery widths=180>
File:StarWarsTakeoverBeach.PNG|The [[Beach]]
File:StarWarsTakeoverDock.PNG|The [[Dock]]
File:StarWarsTakeoverBerg.PNG|The [[Iceberg]]
File:StarWarsTakeoverMtn.PNG|The [[Ski Hill]]
File:StarWarsTakeoverVillage.PNG|[[Ski Village]]
File:StarWarsTakeoverTown.PNG|The [[Town]]
</gallery>

====After destruction of [[Death Star]]====
<gallery widths=180>
File:StarWarsTakeoverBeach2.PNG|The [[Beach]]
File:StarWarsTakeoverDock2.PNG|The [[Dock]]
File:StarWarsTakeoverBerg2.PNG|The [[Iceberg]]
File:StarWarsTakeoverMtn2.PNG|The [[Ski Hill]]
File:StarWarsTakeoverTown2.PNG|The [[Town]]
</gallery>

===Party Rooms===
<gallery widths=180>
File:StarWarsTakoverParty11.PNG|The [[Bridge (Star Wars Takeover)|Bridge]]
File:StarWarsTakoverParty7.PNG|The [[Cantina]]
File:StarWarsTakoverParty5.PNG|The [[Desert]]
File:StarWarsTakoverParty15.PNG|The [[Detention Block]]
File:StarWarsTakoverParty8.PNG|The [[Docking Bay]]
File:StarWarsTakoverParty12.PNG|The [[Elevators]]
File:StarWarsTakoverParty4.PNG|The [[Lars Homestead]]
File:StarWarsTakoverParty10.PNG|The [[Meeting Room]]
File:StarWarsTakoverParty1.PNG|The [[Millennium Falcon]]
File:StarWarsTakoverParty6.PNG|The [[Mos Eisley]]
File:StarWarsTakoverParty17.PNG|The [[Rebel Base]]
File:StarWarsTakoverParty3.PNG|The [[Sand Crawler]]
File:StarWarsTakoverParty2.PNG|The [[Tatooine (room)|Tatooine]]
File:StarWarsTakoverParty9.PNG|The [[Throne Room]]
File:StarWarsTakoverParty14.PNG|The [[Tractor Beam]]
File:StarWarsTakoverParty13.PNG|The [[Trash Compactor]]
File:StarWarsTakoverParty16.PNG|The [[Yavin 4 (room)|Yavin 4]]
</gallery>

===After the Party===
<gallery widths=180>
File:StarWarsTakeoverClubHerbert4.PNG|ClubHerbert.com (August 15<sup>th</sup>)
</gallery>

==Videos==
<youtube>-Jx80ADzC_c</youtube>
<youtube>jIQGbPZdb-4</youtube>
<youtube>yAlshPatRFo</youtube>

==Trivia==
*Disney bought [[wikipedia:Lucasfilm|Lucasfilm]], the company behind the [[wikipedia:Star Wars|Star Wars]] franchise, in October 2012. <ref>http://www.bbc.co.uk/news/business-20146942</ref>, which is really why the party is held.
*It captures the events of the original (first made) movie: [[wikipedia:Star Wars Episode IV: A New Hope|Star Wars Episode IV: A New Hope]].
*It is the third party to have the suffix "takeover" , first two being the [[Marvel Super Hero Takeover]] and [[Monsters University Takeover]].
*[[Membership|Members]] can become Jedi Masters by dueling other penguins with [[Lightsaber]]s.
*[[Darth Herbert]] was meetable one week after the party, like [[Rookie]] at the [[April Fools' Party 2011]].
*This party had the most free items out of any Club Penguin party until the [[Frozen Fever Party]], beating the previous record-holder, the [[Hollywood Party]].

==Names in other languages==
{{OtherLanguage
|portuguese= Star Wars - A Invasão
|spanish= Star Wars - La Invasión
|french= Star Wars - La Fête
|german= Star Wars Party
}}

{{SWFArchives}}

==See also== 
*[[Star Wars Rebels Takeover]]

==References==
{{reflist}} 

{{Party}}

[[Category:Parties of 2013]]
[[pt:Star Wars - A Invasão]]
