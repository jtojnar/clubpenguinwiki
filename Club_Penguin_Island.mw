[[Image:Islands.jpg|right|thumb|170px|The [[Iceberg]], Club Penguin Island and the [[Three Little Islands]]]]
'''Club Penguin Island''' is the place where the whole game of ''[[Club Penguin]]'' takes place. [[Penguins]] are the main species of this island, along with [[puffles]]. 

== History ==
{{Expansion}}
The ancient history of '''Club Penguin Island''' is very unclear prior to 2005. [[Club Penguin]] has never given players a firm description of the island's ancient history, but there are some clues.

===Prehistoric Era===
During the Prehistoric Era, cavepenguins and [[dinosaur]]s roamed the island. Club Penguin's appearance in this era was very rocky and a lot of trees surrounded the area. A single volcano could be seen.

===Medieval Era===
Not much is known about the Medieval Era. It is stated in the [[Club Penguin Times]] Issue #341 that there were five kingdoms that lived in harmony. One day, a [[Scorn|terrible dragon]] arose from darkness, cursed the lands, and declared himself king. Before Scorn could destroy all five kingdoms, the kingdoms used the Thunder Blade to strike Scorn down. They banished him, but warned the penguins that he may return one day.

===Present/Modern Era===
:''See Also: [[Club Penguin Timeline]]''
The Present Era (sometimes known as the Modern Era) is the era that penguins live in today.

==Location==
{{Rumor}}
Club Penguin Island is probably placed somewhere near Antarctica. Some people claim it to be Penguin Island (just off King George's Island). [[Rockhopper Island]] is rumored to be Christmas Island, off the coast of Western Australia. This is unlikely as it would take months and months to go to Penguin Island from there, even in a [[Wikipedia:Clipper ship|clipper ship]] such as the [[Migrator]].<br>

According to the Brazilian kids magazine ''Revista Recreio'', the Club Penguin Island is located somewhere between Canada and the North Pole.{{Fact}}

===Factors that point to Penguin Island===
*Penguin Island has one of the highest [[penguin]] populations in the world.
*The [[Iceberg]] on the [[Map]] of Club Penguin Island resembles an iceberg that has been floating off the coast of Penguin Island for years.
*The [[Mountain]] in Club Penguin Island is rumored to be [[wikipedia:Deacon Peak|Deacon Peak]], an actual mountain on Penguin Island.

==Characteristics==
Club Penguin Island's climate is extremely cold, and it's always covered in snow. As seen in the comic section of the website, the [[Club Penguin Times]] does not include weather reports because it is always cold. The only exception was during June 12–16, 2009, when the [[Adventure Party 2009]] turned Club Penguin into a tropical island or in October 1–8, 2009, when Sensei said: ''Stay ready. Look at the mountains, and watch for changes on the wind.'' On October 2, 2009, it could be seen that the smoke of the [[Volcano]] suddenly went to the left because of the wind.

[[Image:Seaweeds.PNG|right|thumb|Seaweed.]]
Several animals inhabit the island, the majority of them being [[penguin]]s. Other animals include [[crab]]s, [[worm]]s, [[jellyfish]], [[Starfish]], [[shark]]s, [[mullet]]s, [[grey fish]], [[Fluffy The Fish]] (which conform a large part of the feeding of the penguin population), clams (most notably [[Big Bertha]] off the northeast coast), corals, shrimp, squids, an octopus, a lone [[Herbert|polar bear]], squid, giant squid, a [[Puff Almighty|giant puffer fish]], and [[puffle]]s (which have been domesticated and can be [[Pet Shop|adopted as pets]], though wild ones still remain in the wilderness). Due to the extremely cold climate of the area, the flora of the island is limited to [[Pine Tree|pine trees]], [[seaweed]], and [[Puffle-O's|'O' Berry bushes]] (Except during the [[Adventure Party 2009]], where tropical palm trees, flowers, and even [[Snowball Eating Plant|other things]] grew).

==Disasters==
Club Penguin has also had disasters. The worst disaster was probably the [[Popcorn Explosion]]. Some of the disasters were caused during parties.

==Gallery==
<gallery>
File:View.PNG|A view of Club Penguin Island taken from the Iceberg.
File:Islands Around CP.PNG|Club Penguin Island (right) compared to [[Trading Post]] (middle) and [[Dinosaur Island]] (left)
</gallery>

==Surrounding islands==

===Trading Post===
:''See [[Trading Post]].''
[[File:RockhoppersQuestTradingPost.PNG|right|thumb|The Trading Post, which was the first island in the [[Rockhopper's Quest]].]]
The Trading Post was one of the islands in the [[Rockhopper's Quest]], and was the first one that the [[Migrator]] docked at. It's unknown if the island is inhabited by another group of [[penguins]] or [[puffles]], but it was inhabited by some intelligent creature because there are buildings located on the island. The island is the closest to Club Penguin of the three islands in the Rockhopper's Quest.

===Dinosaur Island===
:''See [[Dinosaur Island]].''
Dinosaur Island was the one of the islands in the Rockhopper's Quest, and the second one that the Migrator docked at. The island has a tropical climate, and is inhabited by dinosaurs. There's an ancient temple on the island that has [[puffle]] artwork on it, but [[penguin]] clothing is for sale in the catalog there. The island is the closest to Shipwreck Island.

===Shipwreck Island===
:''See [[Shipwreck Island]].''
Shipwreck Island is the third, and last island in the Rockhopper's Quest. It looks like it's the smallest island in the [[Rockhopper's Quest]], and appears to have been inhabited by Vikings. It was also the only island in the Rockhopper's Quest that had [[Viking Hall|two rooms]]. It was covered in many shipwrecked ships, hence the name.

====Rockhopper Island====
:''See [[Rockhopper Island]].''
Rockhopper Island is [[Rockhopper]]'s private island, where he finds rare items to bring back to Club Penguin Island. It's a small island with clean water and fruit trees. It is also the home of red [[puffle]]s.

====Three Little Islands====
:''See [[Three Little Islands]].''
[[Image:3LittleIslands.jpg|right|thumb|The three little islands next to the shore]]
According to a map in the [[HQ]] and another in the [[Ship Hold]] there are three mysterious islands at the left of Club Penguin Island, next to the [[Mountain]]. No-one knows what is located there. It was once believed that one of these islands was the [[Ninja Hideout]]. It is unknown if they are there for a reason.
Some believe that these islands are the Trading Post, Dinosaur Island, and Shipwreck Island. Club Penguin hasn't proved this yet, even though there's an overwhelming amount on evidence to support that those islands are the Three Little Islands. Many people don't believe this though because the island positioning on the [[Rockhopper's Quest]] map is different.

====Iceberg====
:''See [[Iceberg]].''
The [[Iceberg]] was mysteriously discovered in March 2006. It's just a small iceberg next to Club Penguin Island. It can only be accessed from the [[Map]], [[Elite Spy Phone]] and by crashing on Level 3 of the [[Jet Pack Adventure]] game. No one is sure where it came from, or how long it had been there, but it's been a popular party destination ever since. The Iceberg is only visited by [[penguin]]s and [[puffle]]s. It looks like a small section off the Club Penguin Island. Many penguins think the iceberg is the iceberg which [[Herbert P. Bear]] came to Club Penguin on. There are rumors that you can tip the Iceberg, but you cannot tip it. In spite of this, there are still penguins trying to tip it.

==See also==
*[[Club Penguin]]
*[[Map]]
*[[Three Little Islands]]
*[[Rockhopper Island]]
*[[Puffles]]

{{Geographic location
|Center    = Club Penguin Island
|North     = 
|Northeast = [[Iceberg]]
|East      =
|Southeast = 
|South     = [[Soda Seas]] 
|Southwest = 
|West      = [[Clam Waters]], [[Trading Post]], [[Dinosaur Island]]
|Northwest = [[Three Little Islands]], [[Rockhopper Island]], [[Shipwreck Island]], [[Swashbuckler Trading Post]], [[Dinosaur Island]]
}}

[[Category:Islands]]
[[Category:Places]]
[[pt:Ilha Club Penguin]]
