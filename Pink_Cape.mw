{{ItemInfobox 
|name= Pink Cape
|image= File:PinkCape.png
|available= Yes
|type= Neck Item
|member= Yes
|party= None
|cost= 200 [[coin]]s<br>formerly 500 coins
|found= [[Penguin Style]], [[Costume Trunk]]
|id= 315 (regular)<br> 10315 (unlockable)
|unlock= Yes (Series 2, 5, 8, 9 and 12)
}} 

The '''Pink Cape''' is a neck item in ''[[Club Penguin]]''. [[Members]] are able to buy this item for  500 [[coin]]s in the [[Penguin Style]] and [[Costume Trunk]] catalogs. [[Non-members]] could obtain this item by unlocking it from the [[Treasure Book]].

It goes with the [[Pink Superhero Mask]] and the [[Gamma Gal Costume]] (in Costume Trunk) and goes along with [[The Watermelon]], [[Pink Face Paint]], [[I Heart My Pink Puffle T-Shirt]] and [[Pink Checkered Shoes]] (in March 2012 Penguin Style). Similar items are the other [[Capes]].

==History==
The Pink Cape is a common item. It was a secret item in June 2012 ''Penguin Style''.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|''Penguin Style''||April 2006||???
|-
|rowspan=2|Costume Trunk||January 11, 2008||February 7, 2008
|-
|July 11, 2008||August 8, 2008
|-
|[[Treasure Book (Series 1)]]||October 24, 2008||rowspan=2|October 4, 2011
|-
|[[Treasure Book (Series 2)]]||December 22, 2008
|-
|rowspan=2|Costume Trunk||January 9, 2009||February 13, 2009
|-
|October 8, 2009||November 13, 2009
|-
|[[Treasure Book (Series 7)]]||March 1, 2010||rowspan=3|October 4, 2011
|-
|[[Treasure Book (Series 8)]]||April 10, 2010
|-
|[[Treasure Book (Series 9)]]||July 12, 2010
|-
|Costume Trunk||August 27, 2010||September 17, 2010
|-
|[[Treasure Book (Series 12)]]||February 7, 2011||October 4, 2011
|-
|rowspan=2|Costume Trunk||March 17, 2011||April 21, 2011
|-
|October 19, 2011||February 8, 2012
|-
|rowspan=2|Penguin Style||February 29, 2012||April 4, 2012
|-
|May 30, 2012||September 5, 2012
|-
|Costume Trunk||July 5, 2012||July 18, 2012
|-
|Penguin Style||March 6, 2013||May 8, 2013
|-
|rowspan=2|Costume Trunk||April 3, 2013||April 24, 2013
|-
|February 12, 2014||March 19, 2014
|-
|Penguin Style||April 2, 2014||June 4, 2014
|-
|rowspan=6|Costume Trunk||June 4, 2014||June 18, 2014
|-
|July 2, 2014||September 17, 2014
|-
|October 1, 2014||December 17, 2014
|-
|December 30, 2014||February 18, 2015
|- 
|March 4, 2015||March 25, 2015
|-
|April 9, 2015||{{Available|Items}}
|-
|Penguin Style||March 4, 2015||May 6, 2015
|}

==Gallery==
<gallery>
File:PinkCape1.PNG|The Pink Cape in-game.
File:PinkCape2.PNG|The Pink Cape on a [[player card]].
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Capa Rosa
|spanish= Capa rosa
|french= La Cape Rose
|german= Rosa Umhang
|russian= Розовая накидка с капюшоном
}}

==SWF==
===315===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/315.swf Pink Cape (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/315.swf Pink Cape (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/315.swf Pink Cape (paper)]
===10315===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10315.swf Pink Cape (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10315.swf Pink Cape (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10315.swf Pink Cape (paper)]

[[Category:Items]]
[[Category:Clothing]]
[[Category:Neck Items]]
[[Category:Treasure Book Items]]
[[Category:Secret Items]]
[[pt:Capa Rosa]]
