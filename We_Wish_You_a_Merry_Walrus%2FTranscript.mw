The following is a transcript from "[[We Wish You a Merry Walrus]]".<ref>Transcript retrieved from [http://clubpenguin.wikia.com/wiki/We_Wish_You_a_Merry_Walrus/Transcript here] on January 23, 2015.</ref>

==Act I==

===Scene 1: Rockhopper's Ship===
'''Rockhopper:''' Avast! Oho! What a merry party that was! I've never seen the like. Oh, hello! Captain Rockhopper, at yer service. What's that, ye say? Ye were off in the bathroom and ye missed the whole hullabaloo? Well, lucky for you, I downloaded it off the Internets! I ain't sayin' how, but I am a pirate!

''("[[A Very Merry Walrus]]" plays)''

===Scene 2: Herbert's Undersea Lair===

'''Herbert:''' Even from this undisclosed aquatic location, that noise is intolerable! But soon, Klutzy, I, Herbert P. Bear, will have my peace and quiet. And those penguins will have a Merry Walrus surprise they won't soon forget! Time to unwrap Operation: Silent Night!

===Scene 3: Club Penguin Island===

'''Roofhowse:''' ''(finishes hammering ship wheel to igloo door)'' Ah. That oughta do it. Home, sweet home! ''(door falls over)'' Or not.

'''Blizzard:''' Hey, new kid! Think fast!

''(Roofhowse struggles to catch the ball and the ball breaks his window)''

'''Blizzard:''' Wow. Where'd you learn to catch?

'''Roofhowse:''' Oh, I'm self-taught.

'''Blizzard:''' Ahah! I like this guy! You won't make the team, but you will get a nickname. We're gonna call you Fumbles!

'''Roofhowse:''' Uh, thanks, but the name's Roofhowse. ''(lifts door back up)''

'''Blizzard:''' Sure thing, Fumbles. I'm Blizzard. Looks like you could use some igloo decorating tips from the masters. Show 'em what we got, Sydmull.

''(Sydmull presses button and their igloo lights up)''

'''Roofhowse:''' Oh. I bet you could see that from space.

'''Sydmull:''' Ha, let's confirm! ''(on video phone)'' Can you see it up there, Sergei?

'''Sergei:''' Дa (Yes), Sydmull. Happy Merry Walrus!

'''Blizzard:''' Now let's see what you got.

''(Roofhowse plugs wires together, causing his igloo to burn down)''

''(door falls over again)''

'''Roofhowse:''' ''(sighs)'' I got nothin'.

'''Sydmull:''' Don't worry, Fumbles! You can stay with us!

'''Blizzard:''' Yeah! Assuming you're through burning igloos down.

'''Roofhowse:''' I'll pass, but thanks for the offer, guys. And again, my name's Roofhowse.

'''Blizzard:''' Anybody else find that ironic seeing as he's got neither a roof nor a house?

'''Jangrah:''' Blizzard! Do you know your lines for the play? And did you sign your actor's contract?

'''Blizzard:''' I don't sign nothin' without my lawyer present!

'''Sydmull:''' I give my client permission to sign.

''(Jangrah grunts in frustration)''

'''Blizzard:''' ''(signs paper)'' Done!

'''Jangrah:''' If anyone else fit in a Merry Walrus costume, I'd fire you. ''(gasps)'' Hello! Roofhowse, right?

'''Roofhowse:''' Yeah, how'd you know?

'''Jangrah:''' I'm Jangrah, president of the Igloo Owner's Association. Here's your fruit basket and-- ''(gasps)'' Oh my gosh, your igloo!

'''Roofhowse:''' What, you don't like the open floor plan?

'''Jangrah:''' ''(laughs)'' Enough banter. Now then, not to toot my own horn, but I'm directing the Merry Walrus play, and we could really use your help. Just, you know, maybe not with anything electrical.

'''Roofhowse:''' Thank you, but I don't know how long I'll be staying.

'''Lorna:''' Ooh, a drifter. Always chasing the next ice flow. Or maybe on the run from the law. I get it! But it must be tough to make friends.

'''Jangrah:''' Not at Club Penguin, it's not. We're all friends here, and nothing is more important than taking the time to-- ''(watch beeps)'' Oops! Gotta run. It's showtime! Lorna! Costumes! Sydmull! Special effects! Blizzard! Walrus up! Roofhowse, hope to see ya there!

'''Lorna:''' Just so you know, ''(clicks flashlight on)'' there's an ancient Club Penguin prophecy that a mysterious stranger would one day save Merry Walrus from complete annihilation. ''(clicks flashlight off)''

'''Roofhowse:''' Really?

'''Lorna:''' And not to be overly dramatic, ''(clicks flashlight on)'' I also had a dream about waffles. ''(backs away slowly and clicks flashlight off)''

'''Roofhowse:''' Wow, this place is crazy. Well, nothing keeping me here now. Except my fruit basket, heh. ''(fruit basket burns)'' Of course.

===Scene 4: The Merry Walrus Play===

''(stage curtains open)''

'''Lorna:''' Behind the protection of the Crystal Curtain, the Merry Walrus lives in a palace construct of snow, ice, and quality plumbing materials.

''(crowd "ooo"s)''

'''Lorna:''' Once a year, the Merry Walrus climbs aboard his sleigh to deliver gifts to all the good penguins. And bad penguins, too. He doesn't judge.

''(crowd laughs)''

'''Lorna:''' This magical sleigh is pulled by his six Blue Crystal Puffles! ''(clear throat)'' I said, by his six Blue Crystal Puffles!

'''Jangrah:''' Sydmull! That's your queue!

'''Sydmull:''' Sorry! I was distracted making a scale model of a digestive tract! ''(pulls a cord, making the model fart)''

'''Lorna:''' Touching the veil of the Crystal Curtain, the Blue Crystal Puffles lead the way to Club Penguin, where the Merry Walrus himself kicks off the party as we dance the Puffle Shuffle!

'''Blizzard:''' Oh! Uh. Uhhh.

''(crowd stares expectantly)''

'''Blizzard:''' Line?

'''Jangrah:''' Merry Walrus.

''(crowd laughs)''

'''Blizzard:''' Oh, uh. Merry Walrus! Which is both my name and my catchphrase! Line?

'''Sydmull:''' Don't worry. Nobody cares about acting. It's all about the special effects. ''(pulls lever)''

'''Blizzard:''' Tally-ho, my Blue Crystal Puffles!

''(sleigh lifts off from stage)''

'''Blizzard:''' Woah. Motion sickness.

''(Sydmull fumbles around with various buttons and levers)''

''(sleighs begins spinning around)''

'''Jangrah:''' They like it. Quick! Ask for donations! ''(hands bucket to Lorna)'' Go, go, go!

''(Lorna thanks crowd while they place coins in bucket)''

===Scene 5: Leaving Club Penguin Island===
'''Roofhowse:''' Well Roofhowse, you tried the igloo life, but it wasn't for you. ''(sighs)'' Shoulda said goodbye, though.

''(something twinkles in the sky)''

'''Roofhowse:''' Huh?

''(an object flies down to Roofhowse)''

'''Roofhowse:''' What? Huh? Wait, wait.

''(objects flies around Roofhowse, seeming as if trying to say something)''

'''Roofhowse:''' A Blue Crystal Puff-- ''(loses balance and falls backwards downhill)'' Wooaaa--

''(a snowball begins forming)''

===Scene 6: Back at the Play===
''(crowd cheers)''

'''Blizzard:''' They love me! Now for my big finish!

''(a giant snowball containing Roofhowse rolls through the crowd and bowls into the set)''

''(stage curtains close)''

''(crowd cheers)''

'''Blizzard:''' Now that's what I'm talkin' about!

'''Sydmull:''' A giant snowball was a surprisingly effective finale.

'''Jangrah:''' This is not what we rehearsed.

'''Lorna:''' Listen to that applause!

'''Blizzard:''' Fumbles? Why am I not surprised?

'''Roofhowse:''' Guys! You are not gonna believe this!

'''Jangrah:''' ''(gasps)'' A Blue Crystal Puffle!

'''Roofhowse:''' Woah, hey, woah, what's wrong?

''(Blue Crystal Puffle speaks Gibberish)''

'''Lorna:''' I think he's trying to tell us something. I think he's saying, ''(gasps)'' "The Merry Walrus needs our help!"

''(Blue Crystal Puffle nods "yes" and flies toward shore)''

'''Roofhowse:''' He's heading for the shore.

'''Jangrah:''' Everyone! Follow that puffle!

===Scene 7: At the Shore===
'''Jangrah:''' How are we supposed to follow him now?

'''Roofhowse:''' Oh! I know.

'''Jangrah:''' What are you doing?

'''Roofhowse:''' ''(gets into boat)'' This is my boat, guys, I got this. Sailing is the one thing I'm good at. ''(sails away)''

'''Blizzard:''' What do you know, he is good at something.

'''Jangrah:''' Yeah, good at leaving us behind. We could follow him if we only had a ship!

'''Rockhopper:''' Avast! Did someone say, "ship"? ''(Rockhopper's ship pulls up)''

'''Rockhopper:''' No really, did someone say, "ship"? Sometimes me hearing ain't so good.

'''Yarr:''' Ya.

===Scene 8: Out at Sea===
'''Roofhowse:''' I'm comin', pal! I just need a little more sail.

'''Jangrah:''' Hey, Fumbles! Need a lift?

'''Blizzard:''' Ohohoho, burn!

'''Rockhopper:''' Ahoy! Any lad crazy enough to sail in a peanut shell is welcome aboard the Migrator.

'''Roofhowse:''' Are you saying my boat is small?

'''Rockhopper:''' Yes, I am. Now bring it aboard! Let us stow it in me glove compartment.

''(Yarr laughs)''

===Scene 9: Later Out at Sea===
'''Rockhopper:''' Now, let's get down to business. Ye can't be hirin' a pirate ship without booty. ''(whispers to viewers)'' By that, I mean cash.

'''Jangrah:''' All we have is this bucket of donations we worked really hard fo--

'''Rockhopper:''' Deal! ''(grabs donation bucket and chuckles)'' Now then, if we be followin' a Blue Crystal Puffle, that can mean only one thing! We be headin' to Merry Walrus Island. ''(looks menacingly along with Yarr)''

''(lightning strikes)''

'''Yarr:''' Yarr.

''(Lorna, Jangrah, and Sydmull gasp)''

''(Blizzard screams in high-pitched voice and jumps in Lorna's arms)''

'''Rockhopper:''' This trip may be filled with hardship, deprivation, and unspeakable dangers! But on the plus side, we got fun hats! ''(opens chest with hats)''

'''Lorna:''' Ooh! Pirate couture!

===Scene 10: Even Later Out at Sea===
'''Rockhopper:''' Put on your hats! Because ye can't be havin' a real pirate adventure without singing a real pirate chantey! ''(plays accordion)''

'''Rockhopper and Enrique:''' Ohhhhh!

'''Roofhowse:''' Look! Crystal Curtain dead ahead!

'''Rockhopper:''' Oh. That was quick. ''(throws accordion)'' Ahh! Brace for impact!

''(all scream)''

'''Roofhowse:''' ''(grabs ship wheel)'' I can't stop her in time!

''(Blue Crystal Puffle unveils the Crystal Curtain)''

'''Rockhopper:''' The Migrator won't fit!

''(edges of ship grind against sides of Crystal Curtain)''

'''Rockhopper:''' ''(laughs)'' Like I said, she fit like a glove! Good job, lad.

''(others cheer)''

'''Rockhopper:''' Roofhowse, me boy, I see in ye a younger version of meself. Ye've earned yer honorary parrot. ''(places toy parrot on Roofhowse's shoulder)''

'''Toy parrot:''' The elephant says, ''(makes elephant noise)''

'''Rockhopper:''' I got it on discount.

===Scene 11: Arriving at Merry Walrus Island===
'''Jangrah:''' Wow! This looks just like the set from my play!

'''Blizzard:''' Yeah! Except good!

'''Lorna:''' It's the Merry Walrus!

''(Blue Crystal Puffle flies behind Roofhowse)''

'''Roofhowse:''' What's wrong?

'''Herbert:''' The holidays are full of surprises!

''(all gasp)''

'''Rockhopper:''' Why, it be Herbert P. Bear!

''(Klutzy cuts rope, causing net to fall on Roofhowse and friends)''

''(all scream)''

''(Herbert laughs evilly)''

==Act II==

===Scene 12: Inside the Ice Palace===
'''Herbert:''' Finally! I have a captive audience to present my ingeniously elaborate plan to. Poor Klutzy has seen it twenty times, haven't you, Klutzy?

''(Klutzy rolls eyes)''

''(Herbert pushes a button)''

'''Computer:''' Thank you for your interest in Herbert P. Bear Enterprises. We--

'''Herbert:''' ''(grumbles)'' How do you skip the intro?

'''Sydmull:''' I can help you with that.

'''Computer:''' Our regular office hours are--

''(Sydmull pushes a few buttons on the remote to silence the computer)''

'''Herbert:''' Uhh. Thank you! Now then. Phase one: Capture the Blue Crystal Puffles. Phase two: Use the puffles to breach the Crystal Curtain force field. Phase three: Storm the Ice Palace and take the Merry Walrus as my prisoner! Phase four... ''(fixes fingers)'' four: Impersonate the Merry Walrus and fly over Club Penguin to drop MY presents. Each of which is part of a larger mechanism which will create a huge dome of ice over the crowd, trapping them all in a sound-proof chamber which I call "The Snow Globe of Silence!" With those partying penguins permanently in prison, I will finally have my long-awaited peace and quiet! So, what do you think about that?! ''(laughs)''

''(everyone is watching Sydmull playing a video game)''

'''Blizzard:''' Just a sec, my buddy's about to level up.

'''Rockhopper:''' Avast! Get the banana! The banana!

'''Roofhowse:''' Nice!

'''Herbert:''' Oh, why do I bother? ''(drops snow globe)'' Let's wrap this party up! ''(pulls lever)''

''(giant present unwraps to reveal a present-wrapping machine)''

''(all gasp)''

'''Jangrah:''' Hey, watch it! ''(gasps)'' This is clearly a safety violation. ''(gasps)''

'''Lorna:''' ''(gasps)'' How decoratively diabolical!

'''Herbert:''' ''(laughs evilly)'' And now, to make my metamorphosis complete! ''(laughs evilly)'' Oh! Ahh! Brain freeze. ''(pushes button)''

''(sleigh with Blue Crystal Puffles comes out)''

'''Herbert:''' My "presents" is no longer required. Get it? "Presents" (presence)? ''(chuckles)'' Happy Merry Walrus! ''(laughs evilly)''

''(Herbert flies off on sleigh)''

''(machine grabs Roofhowse and friends)''

'''Blizzard:''' Hands off!

'''Sydmull: '''While completely terrifying, I can't help but be impressed by-- ''(muffled speech)''

'''Rockhopper:''' Yarr! They'll not be puttin' me in ribbons and bows! We must be untyin' these knots!

'''Roofhowse:''' Let's start with this Double Anchor Shake.

'''Rockhopper:''' Double Anchor Shake? That be a Longshoreman's Loop!

'''Roofhowse:''' No way! A Longshoreman's Loop has a half-twist.

'''Rockhopper:''' ''(scoffs)'' When yer mother tied 'em!

''(while the arguing over knot types continues, a Blue Crystal Puffle tries to stop the machine)''

'''Computer:''' Thank you for purchasing the Malfunction Notification System 1000. This machine will now go completely crazy.

''(whistle blows)''

''(warning lights and sounds come on)''

''(Roofhowse and Rockhopper scream)''

''(Blue Crystal Puffle gasps and pushes lever)''

''(while Roofhowse and Rockhopper scream, the Blue Crystal Puffle frantically tries to stop the machine)''

''(Blue Crystal Puffle uses its power and stops the machine)''

''(machine snips the rope tying up Rockhopper)''

'''Rockhopper:''' Now that be how ye does a Long Sherman's Loop!

'''Yarr:''' Yarr!

===Scene 13: Later in the Ice Palace===
'''Jangrah:''' This is not the paper I would've chosen.

'''Blizzard:''' Mine's cool.

'''Lorna:''' Mine too!

''(muffled speech starts coming from behind wrapping)''

'''Lorna:''' Who's that? ''(rips wrapping open)''

''(all gasp)''

'''Merry Walrus:''' Merry Walrus! ''(chuckles)'' It's my name and my catchphrase!

''(Blue Crystal Puffle flies toward Merry Walrus)''

'''Merry Walrus:''' Enrique! ¿Cómo estás?

'''Lorna:''' Oh great and wondrous Merry Walrus, we must stop Herbert! He has a dastardly plan to--

'''Merry Walrus:''' No need to recap. I was here the whole time.

'''Jangrah:''' Quick! Everyone to the Migrator!

'''Rockhopper:''' Arrgh. Me ship is fast, but it's no fancy flying machine. Not to be defeatist, but all hope is lost!

'''Roofhowse:''' If only we had another sleigh!

'''Merry Walrus:''' This calls for a Merry Walrus miracle! A two-sleigh garage! ''(pushes button)''

''(garage door opens to reveal a second sleigh, while game show music plays)''

''(all cheer)''

===Scene 14: Leaving Merry Walrus Island===
'''Merry Walrus:''' Merry Walrus away!

'''Rockhopper:''' Aye! And Rockhopper away, too! Only much, much slower.

'''Yarr:''' Aww.

'''Rockhopper:''' Come on!

===Scene 15: Back at Club Penguin Island===
''("[[Puffle Shuffle (song)|Puffle Shuffle]]" plays)''

'''Cadence:''' Hey, party penguins! Who's ready for the Merry Walrus? ''(gasps)'' And here he comes now!

''(crowd turns around and cheers)''

'''Herbert:''' Klutzy, take the reins! ''(chuckles)'' Geronimo! ''(begins dropping presents)'' Heads-up, penguins! Ho ho ho.

'''Cake Penguin 1:''' I spent two weeks making the Merry Walrus this beautiful eight-layer cake!

'''Cake Penguin 2:''' That's four layers a week! Awesome!

''(one of the presents lands on the cake and splatters it)''

'''Herbert:''' Bulls-eye! Time to close the circle with the final present and give everyone a happy Merry Walrus. ''(laughs evilly)'' Huh? What?! Those blasted penguins escaped! But they're too late. ''(drops final present)'' ''(laughs)''

'''Roofhowse:''' Oh no! Herbert dropped the last present! Take her down, Merry Walrus!

'''Merry Walrus:''' Righty-o! ''(steers the sleigh downwards)''

'''Blizzard:''' Uhh, guys, you do realize our catcher is Fumbles, right?

'''Roofhowse:''' No, I got this! I mean, I'm relatively sure I've got this. Ooh! ''(catches present with both hands)'' Got it! ''(begins falling)''

'''Jangrah:''' Roofhowse! ''(grabs Roofhowse's feet)'' He's too heavy! ''(screams)''

''(Sydmull grabs Jangrah's feet and screams)''

''(Lorna grabs Sydmull's feet and screams)''

''(Blizzard grabs Lorna's feet, screams, and grunts while keeping a grip on them)''

'''Herbert:''' Impossible! ''(spits out icicle tusks)''

''(icicle tusks land on Cadence's DJ table, cracking the records)''

'''Cadence:''' Huh?

'''Herbert:''' I've got to get that present! ''(steers sleigh downwards while hat flies off)''

'''Cadence:''' It's Herbert! Everybody, clear the dance floor!

''(crowd screams and clears dance floor)''

''(Cadence ducks under DJ table)''

'''Herbert:''' Not so fast, penguins! ''(jumps from his sleigh to the other sleigh)''

'''Roofhowse:''' Woah! ''(struggles to keep grip on present)''

'''Herbert:''' No one's going to stand in the way of my Merry Walrus!

'''Merry Walrus:''' ''(appears from behind Herbert)'' Oh yeah?

'''Herbert:''' Oh! Merry Walrus! Hello.

'''Merry Walrus:''' Herbert, you've been a very bad bear. But, despite that, I'll still give you a present. Now, get off my sleigh! ''(hands present to Herbert and pushes him off the sleigh)''

''(Herbert screams)''

''(Klutzy jumps after Herbert)''

'''Roofhowse:''' I've got it! I've got it! ''(loses grip on present)'' I don't got it.

'''Blizzard:''' Yep, saw that comin'.

'''Herbert:''' ''(falls into patch of snow and groans)'' I'm okay! ''(Klutzy lands on his head)'' Oh! Klutzy! ''(Merry Walrus's present lands on his head)'' D'ow! ''(sinks back into snow)''

''(Klutzy rears up in a fighting stance, but realizes Herbert is down already)''

''(Herbert's final present falls in front of him, forming a circle with the other presents)''

''(lasers shoot out of the presents and align with the other presents)''

'''Herbert:''' Oh dear.

''(dome of ice forms around Herbert and Klutzy)''

'''Blizzard:''' Fumbles! Woo hoo hoo!

'''Jangrah:''' You did it! You saved Merry Walrus!

'''Lorna:''' Just like the prophesy foretold! Except without the waffles.

'''Roofhowse:''' No, we ALL did it!

''(all cheer)''

'''Merry Walrus:''' Save the warm fuzzies for the rest of the island! We have cheer to spread! ''(laughs merrily)''

===Scene 16: Celebration===
''("[[Puffle Shuffle (song)|Puffle Shuffle]]" plays)''

'''Cadence:''' Everybody do the Puffle Shuffle!

'''Herbert:''' ''(from inside The Snow Globe of Silence)'' At least in here, I finally get some peace and quiet.

''(Klutzy begins dancing and snapping his claws to the music)''

'''Herbert:''' Noooo! Hm? ''(opens present from Merry Walrus and pulls out earmuffs)'' ''(puts them on and sighs in relief)''

'''Roofhowse:''' ''(sighs)'' ''(Enrique flies up and rubs Roofhowse)'' Thanks for everything, Enrique! Here, I've got something for ya! ''(gives Enrique a pirate hat)'' Matey!

'''Rockhopper:''' Avast! Here be me donation to Coins for Change. ''(gives Roofhowse Jangrah's donation bucket back)'' Didn't think I'd keep it, did ye?

'''Roofhowse:''' Oh! Thanks, Rockhopper!

'''Rockhopper:''' ''(whispers)'' Besides, me accountant says I'd have to pay taxes on it.

''(Yarr laughs)''

'''Rockhopper:''' Fine job today, matey! How about joining me as a pirate for your next great adventure?

'''Roofhowse:''' ''(looks at friends dancing)'' You know something? I think my next great adventure might be right here at Club Penguin.

'''Rockhopper:''' That be good. 'Cause I sold yer boat for scrap.

'''Roofhowse:''' What? Ah, that's okay. Hey guys, wait for me!

'''Sydmull:''' Fumbles is one of us now!

''(Lorna and Jangrah cheer)''

'''Blizzard:''' I always liked this kid.

===Scene 17: Back on Rockhopper's Ship===
'''Rockhopper:''' Arr. I'll say it again. That were some party. There's a lesson to be learned here. But lessons are for scurvy dogs! In the meantime, it's good to be a pirate! Aye, mateys?

''(Blue Crystal Puffles cheer)''

'''Merry Walrus:''' Merry Walrus to all, and to all a good flight!

'''Rockhopper:''' I see what ye did there! Rockhopper away! ''(laughs heartily)''

''(Blue Crystal Puffles fly the Migrator through the sky)''

==Sources and references==
{{Reflist}}
