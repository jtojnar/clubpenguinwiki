{{Archive}}
{{FAOTW}}
{{Archive}}
{{PlayInfobox
|name = The Twelfth Fish
|image= File:TwelfthFishStage.PNG
|imagesize = 230px
|story = This play is about a Bard, a Jester and a Countess all going fishing.
|dates = December 14, 2007
|characters = Countess, Bard, Jester and Fish
|music = [http://media1.clubpenguin.com/play/v2/content/global//music/31.swf Twelfth Fish Music]
|background = [[Twelfth Fish Background]] (Only in the May 2008 exhibition)
}}
The '''Twelfth Fish''' is a play at the [[Stage]]. It features a [[Fluffy the Fish]]. The Twelfth Fish hadn't been to the [[Stage]] for three years. It came back on January 11, 2012 with a new script written by Rookie.
[[Image:PenguinsStage.png|right|thumb|200px|The Twelfth Fish.]]

==Plot==
{|class="wikitable"
!2012 version||Original version
|-
|The Countess loves to count- and her goal since she was young: To find the dozen rarest fish, and count them one by one.<br>So the jester and the Bard are going to help her with her wish. Can the Countess find and count the rare Twelfth Fish?||This Shakespeare-like play is about a Countess, a Jester, and a Bard going to fish. A fish goes BLUB BLUB many times, and the two try to catch it. The two do catch it and they wish to eat it.
|}

==Characters==
{|class="wikitable"
!Image!!Name!!Info!!Clothing
|-
|[[File:Countness.PNG|100px|center]]||Countess||Countess had a dream of counting 12 rare fish. With Bard's and Jester's help she was able to count the twelfth fish and accomplish her goal.||[[Countess Steeple Hat]], [[Countess Dress]]
|-
|[[File:Bard.PNG|100px|center]]||Bard||Bard helped Countess to find the twelfth fish for her to count. He is a poet. In the original play, he suggested to fish for a meal.||[[Bard Hat]], [[Bard Outfit]]
|-
|[[File:Jester.PNG|100px|center]]||Jester||Jester helped the Countess to find the twelfth fish for her to count.||[[Court Jester Hat]], [[Court Jester Outfit]]
|-
|[[File:FishActor.PNG|100px|center]]||Fish||Fish was the twelfth rare fish to be counted by the Countess.||[[Fish Costume]], [[Yellow Flippers]]
|}
==Script==
===Current Script===
''Twelfth Fish''
*'''Countess''': Guys we're NEVER going to find that fish
*'''Countess''': We've been searching for 3,429 days!
*''' Bard''': You've kept count of exact amount?
*'''Countess''': What else is a countess supposed to do?
*'''Jester''': Let's split up! Or better yet... let's banana split up!
*'''Countess''': Bard and I will check the castle, Jester check by the sea
*'''Bard''': I pledge to check every hedge!
*'''Jester''': Now let's see here... I see the sea
*'''Fish''': Hello there
*'''Jester''': Oh my! And the sea sees me!
*'''Fish''': Oh I'm not the sea. I'm a fish.
*'''Jester''': Are you a rare fish?
*'''Fish''': Well my mom says I'm one of a kind
*'''Jester''': Pefect! Would you mind if my friend counted you?
*'''Fish''': I guess. I've never really been counted on before
*'''Countess''': Five hedges. Seven flowers. 4,365 stones
*'''Jester''': Countess! Come quickly!
*'''Countess''': Oh wondrous fish! Will you grant my fondest wish?
*'''Fish''': Are you the one who wants to count me?
*'''Countess''': Yes! Oh please, I've searched 82,310 hours for you!
*'''Fish''': Would you mind telling me a poem first?
*'''Bard''': Oh golden fish of sheen most rare
*'''Bard''': We've searched the world from here to there
*'''Bard''': Of all the places we have been
*'''Bard''': You're the greatest fish we've ever seen
*'''Fish''': Oh how lovely. Thank You?
*'''Countess''': So may I count you, dear fish?
*'''Fish''': Yup. Go ahead. That was a great poem
*'''Countess''': Then it is my pleasure to say
*'''Countess''': After eleven other fish before you
*'''Countess''': You are the rarest, final Twelfth Fish
'''THE END'''

===Old Script===
*'''Countess:''' The iceberg's a stage and we are penguins!
*'''Jester:''' A stage where every penguin plays a part.
*'''Bard:''' Fair maiden, shall we go and catch some fish?
*'''Jester:''' To fish or not to fish, that is the question!
*'''Countess:''' Good plan! Fishing is such a sweet comfort.
*'''Bard:''' Now is the winter of our fishing trip.
*'''Jester:''' As good luck would have it!
*'''Bard:''' The first thing we do, let's catch all the fish.
*'''Fish:''' BLUB BLUB!
*'''Jester:''' O fishing line, fishing line! Wherefore art thou doing fine?
*'''Fish:''' BLUBBETH!
*'''Countess:''' What fish through yonder ocean swim?
*'''Fish:''' DOUBLE, DOUBLE BLUB AND BUBBLE!
*'''Bard:''' But hark! What fish through yonder water peeks?
*'''Jester:''' A fish! A fish! My puffle for a fish!
*'''Fish:''' AY, THERE'S THE BLUB!
*'''Countess:''' Something fishy this way comes.
*'''Jester:''' With my empty tummy my eye doth feast.
*'''Bard:''' Now please get me a dish fit for the fish!
*'''Fish:''' BUT NEVER DOUBT I BLUB!
*'''Countess:''' Get thee to a fishery!
*'''Jester:''' "To dine, perchance to eat!
*'''Bard:''' If fish be the food of life, waddle on!
*'''Fish:''' Ha ha BLUB!
(Chases bard around the stage)
*'''Bard:'''YAAAAAAH! HELP! FISH! HELP! FISH!
*'''Countess:'''I've never saw a fish chase a penguin before.

==Exhibition history==
{|class="wikitable sortable"
! scope="col"| Start
! scope="col"| End
|-
|December 14, 2007
|January 11, 2008
|-
|May 9, 2008
|June 3, 2008
|-
|January 11, 2012
|February 9, 2012
|}

==Improvisation==
*For what is best, that best I wish in thee.
*Fish, puffles, penguins, lend me your ears!
*We crew, we happy crew, we land of penguins.
*Small things make penguins proud.

==Trivia==
*The phrase ''We've been searching for 3,429 days!'' means they've been looking for the fish since August 11th, 2002

==Names in other languages==
{{OtherLanguage
|portuguese= A 12ª Raridade
|spanish= El Doceavo Pez
|french= Le Douzième Poisson
|german= "Der Zwölfte Fisch"
}}

{{Stage}}
[[Category:Stage]]
