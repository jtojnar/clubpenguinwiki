{{ItemInfobox
|name= Messenger Bag
|image= File:MessengerBag.png
|available= No
|type= Neck Item
|member= Yes
|party= None
|cost= 290 [[coins]]
|found= [[Penguin Style]]<br>[[Treasure Book]]   
|id= 307 (Penguin Style)<br> 10307 (unlockable)  
}}
:''For other uses of the Messenger Bag, see [[Messenger Bag (disambiguation)]].''

The '''Messenger Bag''' (also known as the '''Brown Mail Bag''') is a neck item in ''[[Club Penguin]]''. Members were able to buy it for 290 [[coins]] in the ''[[Penguin Style]]'' catalog. This item can also be found in the Series 4 ''[[Treasure Book]]'' as well as the Series 13 ''Treasure Book''.

In the January 2011 Penguin Style catalog, it was paired with the [[The Chill Out]], [[Pilot's Jacket]], and [[Running Shoes]]. A year later in the January 2012 Penguin Style catalog, it was paired with the [[Tallest Haircut]] and the [[Hip Red Jacket]].

==History==
This item was first available in April 2008. It was re-released in the Series 4 ''Treasure Book'' at the end of July 2009. It was again re-released in the January 2011 ''[[Penguin Style]]'' catalog. On January 4, 2012, it was re-released for the fourth time in the ''[[Penguin Style]]'' catalog. 

{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|Penguin Style||April 4, 2008||July 1, 2008
|-
|[[Treasure Book (Series 4)|Treasure Book 4]]||July 11, 2009||October 4, 2011
|-
|Penguin Style||January 7, 2011||June 3, 2011
|-
|[[Treasure Book (Series 13)|Treasure Book 13]]||April 5, 2011||October 4, 2011
|-
|Penguin Style||January 4, 2012||May 2, 2012
|}

==Gallery==
<gallery>
File:MessengerBag1.PNG|The Messenger Bag in-game.
File:MessengerBag2.PNG|The Messenger Bag on a [[Player Card|player card.]]
</gallery>

==Appearances==
*[[Rookie]] wore a messenger bag in [[Secret Missions|Secret Mission]] 7, as he was handing out notes.
*The [[Light Blue|light blue]] [[penguin]] on the old [[ClubPenguin.com|main page]] was wearing the Messenger Bag.

==Names in other languages==
{{OtherLanguage
|portuguese= Bolsa de Mensageiro
|spanish= Bolso de mensajero
|french= Sacoche Messager Brune
|german= Kuriertasche
|russian= Курьерская сумка
}}

==See also==
*[[Blue Mail Bag]]

==SWF==
===307===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/307.swf Messenger Bag (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/307.swf Messenger Bag (sprite)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/307.swf Messenger Bag (paper)]
===10307===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10307.swf Messenger Bag (icons)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10307.swf Messenger Bag (sprite)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10307.swf Messenger Bag (paper)]

[[Category:Clothing]]
[[Category:Neck Items]]
[[pt:Bolsa de Mensageiro]]
