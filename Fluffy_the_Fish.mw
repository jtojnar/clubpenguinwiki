{{CreatureInfobox
|name = Fluffy
|image = File:Fish2.png
|caption = Fluffy the Fish.
|appearances = See [[Fluffy the Fish#Appearances|Appearances]].
|related = [[Mullet]]<br>[[Grey Fish]]<br>[[Green fish]]<br>[[Purple fish]]
|color = Yellow
}}
:''Fluffy redirects here. Were you looking for [[Fluffy (puffle)|Fluffy the Puffle]] or [[Fluffy The Fish (Game)]]?''

'''Fluffy the Fish''' is a recurring character in games, [[Secret Mission|missions]], [[rooms]], and others. However, his name is only mentioned in the first [[Penguin Style]] catalog, in the [[Lighthouse]], in the tour description in the [[Cave (Party Room)|Cave]], in some [[Club Penguin Times]] issues and in the [[Get Fluffy stamp]].

==Prehistoric Fluffy the Fish==
[[File:Prehistoric Fluffy.png|175px|thumb|left|The Prehistoric Fluffy the Fish]]
The '''Prehistoric Fluffy the Fish''', also known as the '''Dreaded Sabretoothed Fish'''<ref>[https://twitter.com/polofield/status/497245091715878912 "Beware the '''dreaded sabretoothed fishies'''..."] -Polo Field</ref> is a prehistoric saber-toothed version of Fluffy the Fish. It's larger and it also has fangs. 

It was seen at the [[Cave (Mountain Expedition)|Cave]] during the [[Mountain Expedition]] and [[The Great Snow Race]] and was frozen in ice. During [[Rockhopper's Quest]] there was a presumably deceased Saber-toothed Fluffy the Fish. It was also seen in the [[Viking Hall]] stuffed and on a plate to be eaten. It must have been native to [[Shipwreck Island]], since there is a viking ship in both [[Shipwreck Island]] and the [[Cave (Mountain Expedition)|Cave]]. They may have moved to [[Club Penguin Island]] because the viking ship in the [[Cave (Mountain Expedition)|Cave]] is similar to the broken ship on [[Shipwreck Island]]. There is a Prehistoric Fluffy the Fish skeleton hanging on the classroom wall in the [[School]]. 

Those fish are still alive and can breathe out of water. Many prehistoric sabretoothed fish have been spotted in the [[Tallest Mountain]], digging holes and scaring [[penguin]]s who try to sled down the mountain. They are the main villain in the new game [[Sled Racer]]. They are larger not only than the regular Fluffy the Fish, but they are also larger than [[penguin]]s and some of them are even larger.

Besides the Prehistoric Fluffy the Fish, other prehistoric creatures were revealed to be still alive. Among them are the [[Shark#Prehistoric Shark|Prehistoric Shark]] and the [[Grey Fish#Enormous Grey Fish|Enormous Grey Fish]]. In fact, every prehistoric species can still be alive.

The other prehistoric creature found frozen in the same [[Cave (Mountain Expedition)|Cave]] as the Prehistoric Fluffy the Fish was the [[Shocktopus#Prehistoric Shocktopus|Prehistoric Shocktopus]].

==Appearances==
*On the back of the first [[Penguin Style]] in an advertisement. The ad reads: ''HAVE YOU SEEN ME? Lost: One fish. Answers to the name 'Fluffy'. If found, please return to Zippo Penguin at 2993 West Iceberg Lane.''
*In the [[Lighthouse]], in a picture on the wall. His name is under this picture as well.
*As a structure concept for the [[Clock Tower]] in [[The Penguin Times]] #71.
*Engraved on the bottom of the Grandfather Clock [[furniture]] item.
*In [[The Stage|The Twelfth Fish]], as a prop, and as a character.
*Thrown out of the truck in [[Bean Counters]].
*Main [[fish]] in [[Ice Fishing]].
*Has appeared in various editions of [[The Penguin Times]].
*On the [[Club Penguin]] [[coin]].
*In the Fridge and BBQ [[furniture]] items.
*In the cooler in the [[Lighthouse]].
*On the [[P.S.A.]] Logo.
*In the 3rd and 4th levels of [[Jet Pack Adventure]].
*In the 2nd [[secret mission]] at [[Spy Headquarters]] at the [[River]].
*In [[Aqua Grabber]], Fluffy increases when got the pearl in [[Clam Waters]].
*Also on Aqua Grabber, you can get a [[stamp]] by feeding a [[worm]] to him in [[Soda Seas]] and dropping him in the net.
*As the second hand in the [[medal]] from spy [[mission]] #7.
*As part of the [[Lace Background]].
*Through the [[Cove]] [[Binoculars]].
*In the [[Captain's Quarters]].
*In numerous parties such as the [[Party|Submarine Party or Adventure Party]].
*In the Pizza Oven [[furniture]] [[item]].
*As part of the architecture on the roof of the [[Dojo]].
*In the windows in the [[Underground]] [[Pool]].
*In [[Ice Fishing (DS Version)|DS version]] there are three different sizes of fluffy and you can use the smaller fluffies as bait to catch the larger ones.
*[[Gongs|Gong]] in the [[Ninja Hideout]].
*On one of the [[Easter Egg Hunt 2009]] eggs.
*Fluffy was swimming through the [[Wilderness]].
*On the board track side of the flaming puffle of the black puffle room in the [[Puffle Party 2009]] and the [[Puffle Party 2010]].
*There is a prehistoric Fluffy the Fish, as shown in the [[Mountain Expedition]]. It may be a reference to real prehistoric fish.
*In the [[Bean Balance]] mini-game found in [[Club Penguin: Game Day!]], as one of the items that the player can get.
*It makes a slightly appearance in the game [[Sumo Smash]] for [[Club Penguin: Game Day!]].
*In the [[Viking Hall]] a stuffed Saber-toothed Flufy the Fish was seen on a plate to be eaten.
*Two Fluffies appeared in the game [[Sushi Drop]] from the [[My Penguin|My Penguin App]]. When you hit them they will give you coins. Each one got 100 and 150.
*A prehistoric Fluffy skeleton hangs on the wall in the classroom inside the [[School]].

==Trivia==
*According to the first [[Penguin Style]] catalog, Fluffy belonged to Zippo the Penguin. Fluffy's appearance in the [[Penguin Style]] catalog was also his first official appearance on [[Club Penguin Island]] as Fluffy the Fish. When asked, [[rsnail]] said that Fluffy is "everywhere" and that it might be connected with an unfinished mission.<ref>[http://web.archive.org/web/20060323033205/http://blog.clubpenguin.com/archives/2005_08_01_archive.html "'''Where is fluffy?''' Fluffy is all around Club Penguin. Hmmm, I see a future adventure here."] — rsnail, [[What's New Blog]]</ref>
*According to [[Secret Mission]] 2, puffles don't like them.

==Gallery==
<gallery captionalign="left">
Image:HungryMullet.JPG|A [[mullet]] about to eat a Fluffy the Fish
Image:Fish Clock.PNG|The "Fish Clock"
File:Fluffy1.png|Many yellow fish, which are usually considered to be "Fluffy the Fish."
File:Fluffy2.png|A picture of Fluffy the Fish, which is hung on the walls of the lower level of the [[Lighthouse]].
File:FluffyCoin.png|Fluflly as seen in the [[Club Penguin]] coin.
File:IceFishingFluffy.PNG|Fluffy as seen in [[Ice Fishing]].
File:FluffytheFishGameFluffy2.png|Fluffy as seen in [[Fluffy the Fish (Game)]].
File:FluffytheFishGameFluffy4.PNG|Fluffy at full size in [[Fluffy the Fish (Game)]].
File:Prehistoric Fluffy the Fish Viking Hall.png|A deceased Prehistoric Fluffy the Fish in the [[Viking Hall]].
File:Prehistoric Fluffy the Fish skeleton.png|The skeleton of a Prehistoric Fluffy the Fish as seen in the [[School]].
File:Prehistoric Fluffy the Fish Sled Racer.png|Living Prehistoric Fluffy the Fish as seen in the [[Sled Racer]] game.
File:Prehistoric Fluffy The Fish Sled Racer1.PNG|The Prehistoric Fluffy the Fish's holes.
File:Prehistoric Fluffy the Fish Sled Racer2.PNG|You jumping over some Prehistoric Fluffy the Fish.
File:Prehistoric Fluffy the Fish Sled Racer3.PNG|The Prehistoric Fluffy the Fish compared to a penguin (note how just the fish's fin is as tall as a penguin!)
File:Prehistoric Fluffy the Fish Sled Racer4.PNG|Prehistoric Fluffy the Fish scaring you.
File:Prehistoric Fluffy the Fish Sled Racer5.PNG|The dreaded Sabertoothed Fish hitting you, making you lose the game.
File:Prehistoric Fluffy the Fish Sled Racer6.PNG|The biggest living Dreaded Sabretoothed Fish specimen ever found.
File:Prehistoric Fluffy the Fish7.PNG|The biggest Prehistoric Fluffy the Fish roaring at you.
File:The Complete Prehistoric Fluffy the Fish.png|The complete Prehistoric Fluffy the Fish.
</gallery>

==See also==
*[[Grey Fish]]
*[[Shark]]
*[[Whale]]
*[[Squid]]
*[[Mullet]]

==References==
{{reflist}}

{{Creatures}}
{{Food}}

[[Category:Creatures]]
