{{ItemInfobox
|name = Blue Duck
|image = File:Blue_Duck.png
|available = No
|type = Body Item
|member = Yes
|party = None
|cost = 50 [[coin]]s
|found = [[Penguin Style]]
|id = 4257
|unlock = No
}}The '''Blue Duck''' is a body item in ''[[Club Penguin]]''. It costs 50 [[coin]]s in the [[Penguin Style]] catalog. It is a [[Membership|members-only]] item. The Blue Duck item was available in the August 2010 edition of [[Penguin Style]]. It was also a [[Secret Items|hidden item]]. You can not do the swimming action while wearing any kind of [[Water Wings]].  

==History==
The Blue Duck is a common and secret item.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|Penguin Style||August 5, 2010||November 4, 2010
|- 
|Penguin Style||August 31, 2011||January 4, 2012
|}

==Trivia==
*Due to a bug, when this item was first released, it could not be used to get the "Go Swimming" [[stamp]]. However, this issue was fixed shortly after.
*It was released to allow newer penguins to get the [[Go Swimming stamp]].

==Gallery==
<gallery captionalign="left">
File:BlueDuck2.PNG|The Blue Duck on a [[player card]].
File:BlueDuck1.PNG|The Blue Duck in-game.
File:BlueDuckAction.PNG|A penguin doing the swimming action while wearing the Blue Duck.
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese = Boia de Pato Azul
|french = La Bouée Canard Bleue
|spanish = Patito celeste
|german = Blaue Ente
|russian = Синяя утка-круг
}}

==See also==
*[[Yellow Duck]]
*[[Green Duck]]
*[[Penguin Style]]

==SWF==
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/4257.swf Blue Duck (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/4257.swf Blue Duck (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/4257.swf Blue Duck (paper)]

[[Category:Clothing]]
[[Category:Clothes released in 2010]]
[[Category:Body Items]]
[[Category:Secret Items]]
