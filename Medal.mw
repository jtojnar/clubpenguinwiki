[[File:Secretagentmedals.JPG|thumb|right|Medals in Missions 1, 2, 3, 4 and 5]]

:''Not to be confused with the [[Gold Medal]] from the [[Penguin Games]], or the [[Medal (Field-Ops)|Medals]] that agents can earn by completing [[Field-Op]]s.''

'''Medals''' are won by [[Secret Agents]] if they successfully complete [[Secret Missions]]. Letters and cards are optional awards that may also come with medals. They are obtained by performing certain sub-tasks in the mission.



==Mission 1==
In Mission 1, you will receive the Golden Puffles medal for terrific performance in locating and returning [[Aunt Arctic|Aunt Arctic's]] two missing [[puffle]]s. You will also receive a letter from Aunt Arctic if you give her the pictures of her puffles from the [[Ice Rink]].

==Mission 2==
In Mission 2, you would receive the Wilderness Survival medal for surviving in the wild after the sled accident. You would also receive a letter from [[Gary the Gadget Guy|G]] if you made a fishing rod and caught a [[Fluffy the Fish|fish]].

==Mission 3==
In Mission 3, you would receive the Electromagnet Medal for disconnecting the [[Electromagnets|Electromagnet]] on top of the [[Gift Shop]]. You would also receive a letter from the [[Dancing Penguin]] for restoring the lights in the [[Night Club]].

==Mission 4==
In Mission 4, you would receive the Golden Tube Medal for rescuing the penguins from the [[Sled Racing|sled mountain]]. You would also receive a special award for fixing the [[Ski lift]]. You also receive a (self-destructing) letter from the director of the PSA for finding the white fur.

==Mission 5==
In Mission 5, you would receive the Golden Investigation Medal for trapping the crab and finding the [[Polar Bear|Polar Bear's]] fur. You would also receive a pizza for delivering the pizza to the penguin fishing. When you open the pizza box, you can click on the pizza. For every click you click on the pizza, you would eat one slice until the whole pizza is gone. Then, you can open and close the box to get another pizza!

==Mission 6==
In Mission 6, you would receive the Golden Box Medal for thinking "outside the box".You will also receive electromagnet blueprints if you bring G them from the bear's cave.

==Mission 7==
In Mission 7, you would receive the Silver Watch Medal for timely repairs on the Club Penguin Clock Tower. You can open the medal by clicking on the silver circle with the [[Penguin Secret Agency|PSA]] logo. When it is open, you can see what time it is! To close it, just click on the blue [[penguin]] on the clock. You will also receive a blue "[YOUR NAME] 00 HONORARY MEMBER BLUE TEAM" pennant for finding a replacement target for the two penguins at the [[Dock]]. (Hint: The replacement target can be found at the [[Sports Shop]])

If you click on the medal, it will pop open to reveal a watch that actually tells time. It is the also only mission as of now to give a silver medal instead of a gold one. 

==Mission 8==
In Mission 8, you would receive the Boiler-Spoiler Foiler Medal for perfect precision plumbing, performed under profound pressure. If you managed to retrieve the Clock Tower's gear from Herbert, you would also have gotten a Cool Gift from G. The Cool Gift is a blue box which if you click on it, it will open. Taped on the outside of the blue box is a note from G, and when you finish reading it, click on the note to close it. Click on the white lid to open the box. Inside is the snow gear you made in Mission 7 (Clockwork Repairs) under a pile of snow.

==Mission 9==
In Mission 9, you would receive the Stealthy Spy Surveillance Medal for covert camera coverage and considerable cunning. You will also receive Chocolate: The Game if you find and give all the [[Find Four]] pieces to the two playing [[penguin]]s. Click on a Chocolate to "eat" it.

==Mission 10==
In Mission 10, you would receive Secret Squad Success Medal for magnificent multi-member mission management. You will also receive the Employee of The Month Trophy if you help the [[Gift Shop]] and the [[Gift Shop Manager]]. Click on the head, and it will shake.

==Mission 11==
In Mission 11, you will have to go through a maze to find [[Herbert P. Bear|Herbert]] and [[Klutzy]]. You will be offered it no matter what because you '''must''' complete the maze to continue the mission, although you don't have to accept it. Also, if you give lenses to the [[Dancing Penguin]], you'll win a snow globe from [[Dot]] (when you press red button, it opens and you see there's a letter from Dot and [[Spy Googles]] inside). You can click and drag your Amazing Maze Master Medal after you open it to finish the mazes on it.

If you move your mouse over the medal, you can play a maze game.

==Gallery of Individual Medals==
<gallery widths="160">
File:Golden Puffles Medal.jpg|Golden Puffles Medal
File:Wilderness Survival Medal.jpg|Wilderness Survival Medal
File:Electromagnet Medal.jpg|Electromagnet Medal
File:Golden Sled Medal.jpg|Golden Sled Medal
File:Golden Investigative Medal.jpg|Golden Investigative Medal
File:Golden Box Medal.jpg|Golden Box Medal
File:Silver Watch Medal.jpg|Silver Watch Medal
File:Boiler-Spoiler Foiler_Medal.PNG|Boiler-Spoiler Foiler Medal
File:09SSSM.JPG|Stealthy Spy Surveillance Medal
File:10SSSM.JPG|Secret Squad Success Medal
File:Club Penguin Mission 11 Medal.jpg|Amazing Maze Master Medal
</gallery>

[[Category:Items]]
[[Category:PSA]]
[[Category:Lists]]
