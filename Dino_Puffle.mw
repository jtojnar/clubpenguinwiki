{{CreatureInfobox
|name = Dino Puffle
|image =File:DinoPuffles1.PNG
|imagesize =220px
|caption = All the Dino Puffles
|appearances = [[Club Penguin Island]]<br>Prehistoric Club Penguin Island<br>[[Igloo]]
|related = [[Penguin]]<br>[[Puffles]]<br>[[Dog Puffles]]<br>[[Cat Puffles]]
|color = [[Blue Triceratops]]<br>[[Red Triceratops]]<br>[[Yellow Stegosaurus]]<br>[[Pink Stegosaurus]]<br>[[Black T-Rex]]<br>[[Purple T-rex]]
}}
The '''Dino Puffle''' is a type of [[puffle]] [[Puffle Hybrids|creature]] in [[Club Penguin]]. [[Membership|Members]] were able to collect and keep it during the [[Prehistoric Party 2014]].  It was the first adoptable puffle creature. There are 6 different dino puffles: the [[Blue Triceratops]], the [[Red Triceratops]], the [[Yellow Stegosaurus]], the [[Pink Stegosaurus]], the [[Black T-rex]] and the [[Purple T-rex]].<ref>http://www.clubpenguin.com/help/help-topics/puffles/what-species-dinosaur-puffles-exist</ref> Unlike other puffles, they cannot be cared for in the owner's igloo and must be taken to the [[Puffle Hotel]].

==Gallery==
<gallery>
File:BlueDinoPuffle.PNG|The [[Blue Triceratops]]
File:BlackDinoPuffle.PNG|The [[Black T-rex]].
File:PinkDinoPuffle.PNG|The [[Pink Stegosaurus]].
File:RedDinoPuffle.PNG|The [[Red Triceratops]]
File:PurpleDinoPuffle.PNG|The [[Purple T-rex]].
File:YellowDinoPuffle.PNG|The [[Yellow Stegosaurus]].
</gallery>

==Trivia==
*There are no [[Brown Puffle|brown]], [[Gold Puffle|gold]], [[Green Puffle|green]], [[Orange Puffle|orange]], [[Rainbow Puffle|rainbow]] or [[White Puffle|white]] Dino Puffles.
*Dino Puffles can't wear [[puffle hats]].

==Names in other languages==
{{OtherLanguage
|portuguese= Dinopuffle
|spanish= Dinopuffle
|french= Puffle Dino
|german= Dino-Puffle
|russian= Пафлозавры
}}

==References==
{{reflist}}

{{Succession box|Puffle|Blue Border Collie}}
{{creatures}}

[[Category:Puffles]][[Category:Puffle Creatures]][[pt:Dinopuffle]]
