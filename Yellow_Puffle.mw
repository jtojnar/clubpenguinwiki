{{PuffleInfobox
|name = Yellow Puffle
|image = File:Puffle 2014 Transformation Player Card Yellow.png ‎
|color = #FFFF00
|textcolor = black
|available = Yes
|attitude = Artistic, spontaneous.
|member = Yes
|toys = Paint Pad, Paintbrush, easel, movie camera, Slake, directors chair, black directors/artists hat, paint, canvas.
|food = [[Popcorn (puffle food)|Popcorn]]
|play = '''Super''': Closes a slate and starts directing a movie.<br>'''Normal''': Paints a Cannon with its paint.<br>'''Weak''': Paints a [[Jet Pack]] with its paint.
|dance = Singing
|speed = 6th fastest
|special = Very creative, amazing artistic ability.
|epf item = Powerful flute, Blue Artist's hat
|game = [[DJ3K]]
|id = 756
|bodycode1 = #FFFF00
|tonguecode1 = #09ceff
}}

'''Yellow Puffles''' are very creative and artistic, and are known to sculpt and paint perfect pieces of art. They are versatile in practically every single art medium. Yellow Puffles were first sighted at the [[Forest]] in October 2007, during the [[Halloween Party]], and made available for [[members]] in November.

== List of actions ==
* '''Bath''': Adds paint into the water while bathing. Later paints itself like a rainbow [[puffle]].
* '''Sleep''': You can see it dreaming when it's sleeping.
* '''Postcard''': Is in a picture carrying a hobo stick.
* '''Groom''': It gets a curled up hair style.

<gallery>
File:puffle-amarillo-jugando1.png|A Yellow Puffle painting.
File:puffle-amarillo-jugando2.png|A Yellow Puffle directing a movie.
</gallery>

== Appearances ==
* The Yellow [[Puffle]] could be seen during the construction of the [[The Stage|Stage]] in 2007.
* The [[Keeper of the Stage]] is a yellow puffle. The player can see it by clicking the yellow lever on the [[Switchbox 3000]].
* In a PSA mission, you have to play music to lure the Keeper of the Stage out by playing certain notes of a piano, whose music sheets you find at the [[Pizza Parlor]].
* Yellow Puffles can join the player when playing [[DJ3K]].
* Yellow Puffles are the mascot for the Yellow Team in ''[[Club Penguin: Game Day!]]''

== Gallery ==
=== In-game ===
<gallery>
File:Yellow-pufle-1.png|A wild Yellow Puffle in the [[Forest]]. ([[Halloween Party 2007]])
File:Yellow-puffle-2.png|A wild Yellow Puffle in the [[Ski Hill]]. ([[Halloween Party 2007]])
File:Yellow-puffle-3.png|A wild Yellow Puffle in the [[Plaza]] while [[The Stage]] was being built, in 2007.
File:Yellow Puff In Game.png|What a yellow puffle looked like while being walked in game.
File:Cp kid-Snuggles.png|A fully happy Yellow Puffle on the old [[Puffle Card]].
File:Heath Health.png|A moderately happy Yellow Puffle on the old Puffle Card.
File:Yellow Puff Player Card.png|A Yellow Puffle on a penguin's [[Player Card]] while it was being walked.
File:HomsoloyellowpuffleDJ3K.PNG|A Yellow Puffle playing [[DJ3K]] with its owner.
File:Low quality article.PNG|A sad yellow puffle.
File:Medium quality article.PNG|A moderately happy yellow puffle.
File:High quality article.PNG|A fully happy yellow puffle.
New puffle graphics.png|A Yellow Puffle being walked by its owner.
</gallery>

=== Artwork ===
<gallery>
File:Images-5.png|A Yellow Puffle
File:yellow-puffle.gif|An old appearance in the catalog.
File:yellow-puffle-once-more.jpg|Yellow puffles in the [[Club Penguin Times]]
File:YellowRan.PNG|The postcard a yellow puffle sends when it runs away.
File:Yellowpuffy.png|Another Yellow Puffle.
File:YellowPufflePin.PNG|The [[Yellow Puffle Pin]].
File:Yello_Puffle.png|A Yellow Puffle.
File:YELLOWpuffle.png|Another Yellow Puffle.
</gallery>

== Names in other languages ==
{{OtherLanguage
|portuguese = Puffle Amarelo
|french = Le Puffle Jaune
|spanish = Puffle amarillo
|german = Gelber Puffle
|russian = Жёлтый пафл
}}

== See also ==
* [[Puffle]]

{{Puffleswf|yellow}}
=== Old SWFs ===
* [http://media1.clubpenguin.com/play/v2/content/global/puffle/icons/yellow.swf Yellow Puffle (icon)]
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/756.swf Yellow Puffle (sprites)]
* [http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/756.swf Yellow Puffle (paper)]


{{Succession box|Red Puffle|White Puffle}}

{{Creatures}}

[[Category:Puffles]]
[[Category:Puffles that play in games]]

[[pt:Puffle Amarelo]]
