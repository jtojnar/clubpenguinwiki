{{PuffleInfobox
|name= Gold Puffle
|image= File:Puffle 2014 Transformation Player Card Golden.png
|color= #ffe02a
|textcolor= Black
|available= Yes
|attitude= Lucky, energetic
|member= Yes                                                                                                                      
|toys= [[Gold Jackhammer]]
|food=[[Chocolate Coin]]
|play= With the Gold Jackhammer
|dance= Spin in the air
|speed= Unknown
|special= Can dig for rare items on [[Puffle Digging]]
|epf item= None
|game= None
|id= 11<ref>http://cdn.clubpenguin.com/play/en/web_service/game_configs/puffles_v2.json and http://cdn.clubpenguin.com/play/en/web_service/game_configs/puffles.json</ref>
|bodycode1 = #ffe02a
|tonguecode1 = #CE8625
}}
The '''Gold Puffle''' is a [[Puffle]] discovered in [[Club Penguin]].  It resembles a shinier version of a [[Yellow Puffle]], a puffle-shaped [[chocolate]] in a golden wrapper, or a golden statue of a puffle. It was confirmed to be real in November 2013 and was able to be adopted starting November 14th. It is the second puffle to be discovered during 2013, the first being the [[Rainbow Puffle]]. It can find exclusive items in [[Puffle Digging]]<ref>https://secure.clubpenguin.com/membership/login.php</ref>. Members can adopt it in the [[Gold Mine]] after finding 15 gold nuggets.  

Like [[Blue Puffle]]s, [[Brown Puffle]]s, [[White Puffle]]s, [[Orange Puffle]]s, [[Rainbow Puffle]]s, and [[Puffle Creatures]] it currently cannot join penguins in any mini-games.

==History==
After the [[Medieval Party 2013]], [[Gary]] gave [[PH]] the seeds of Gold O'Berries that were used to prepare the potions. PH planted the seeds and a bush of Gold O'Berries sprouted. When her puffle, Pete, ate one O'Berry, she discovered that these Gold O'Berries increase the puffle's capacity to find treasures on [[Puffle Digging]]. She thought that this case is could be related to the Gold Puffles. On November 6, the [[Cave Mine]] collapsed and pages from [[Garianna]] were found after cave in. These pages were similar to the [[Puffle Handbook]] because they are a kind of guide of the Gold Puffle, with his interests and tips how to care it. A week later, on November 13, the Gold Mine opened and a machine with the bush of Gold O'Berries was installed on the [[Pet Shop]]. With Gold O'Berries, the member's puffles have the hability to find golden nuggets. It's necessary 15 golden nuggets to adopt a Gold Puffle.

==Appearances==
*In the story of "[[The Legend of the Golden Puffle]]" in ''[[Penguin Tales: Spring '07]]'', the Golden Puffle is a [[Blue Puffle]] that fell into a pot with golden color.
*In the play [[Quest for the Golden Puffle]], the Golden Puffle is a chocolate puffle in a golden wrapper.
*In [[Waddle Squad]], a fake Golden Puffle is found at [[Night Club]] and used as bait to capture Herbert. 
*During the [[Medieval Party 2012]], a golden puffle was in the [[Lighthouse]]. It was later seen at the [[Wizard Library]] and at the back of the potions book at the [[Medieval Party 2013]].
*During the [[8th Anniversary Party]], there was a gold slab with a puffle drawn, looking like a Gold Puffle.
*During the [[Muppets World Tour]], there was a gold puffle on the Museum ([[Clothes Shop]]).

==Puffle Item Compatibilities==
{|border="1" class="wikitable" 
!'''Item'''!!'''Dislikes'''!!'''Okay'''!!'''Loves'''
|-
|[[O-Berry]]||-||-||✓
|-
|[[Cookie]]||-||✓||-
|-
|[[Bubble Gum]]||-||-||✓
|-
|[[Carrots]]||-||✓||-
|-
|[[Pizza]]||-||-||✓
|-
|[[Cake]]||-||✓||-
|-
|[[Cheese]]||-||✓||-
|-
|[[Apple]]||-||✓||-
|-
|Walk||?||?||?
|-
|Bath||-||-||✓
|-
|Brush||-||-||✓
|-
|Rest||-||✓||-
|-
|[[Blue Earmuffs (Puffle Hat)|Blue Earmuffs]]||✓||-||-
|-
|[[Candy Cane Cap]]||-||✓||-
|-
|[[Crown]]||-||-||✓
|-
|[[Gear Hat]]||-||✓||-
|-
|[[Headphones (Puffle Hat)|Headphones]]||-||✓||-
|-
|[[Jester Hat (Puffle Hat)|Jester Hat]]||✓||-||-
|-
|[[Mini Squid Lid]]||-||✓||-
|-
|[[Pink Bow]]||✓||-||-
|-
|[[Pirate Hat]]||-||-||✓
|-
|[[Princess Cap]]||✓||-||-
|-
|[[Sombrero (Puffle Hat)]]||-||✓||-
|-
|[[Tiara (Puffle Hat)]]||-||-||✓
|-
|[[All-Star Curls]]||?||?||?
|-
|[[Disco Dome]]||?||?||?
|-
|[[Franken Hat]]||?||?||?
|-
|[[Gold Viking Helmet (Puffle Hat)|Gold Viking Helmet]]||?||?||?
|-
|[[Heavy Metal Hair]]||?||?||?
|-
|[[Jingle Jangle Hat]]||?||?||?
|-
|[[Jolly Roger Bandanna]]||?||?||?
|-
|[[Night Cap]]||?||?||?
|-
|[[Pharaoh Hat]]||?||?||?
|-
|[[Princess Braid]]||?||?||?
|-
|[[Sherwood Hat]]||?||?||?
|-
|[[Snowflake Helmet]]||?||?||?
|-
|[[Sundae Swirl]]||?||?||?
|-
|[[Surf Swoop]]||?||?||?
|-
|[[The Big Bang]]||?||?||?
|-
|[[The Overgrown (Puffle Hat)|The Overgrown]]||?||?||?
|-
|[[The Rad Scientist]]||?||?||?
|-
|[[The Scene-Stealer]]||?||?||?
|-
|[[Top Hat (puffle hat)|Top Hat]]||?||?||?
|-
|[[Knight Helmet (Puffle Hat)|Knight Helmet]]||?||?||?
|-
|[[Shock of Hair]]||?||?||?
|-
|[[Turquoise Toque]]||?||?||?
|-
|[[Mini Polka Dot Puffle Hat]]||?||?||?
|}

==Gallery==
===Gold Puffle in-game===
<gallery>
File:GoldPuffleInGame1.PNG|Gold Puffle along with a penguin (in a [[player card]]).
File:GoldPuffleInGame2.PNG|Gold Puffle along with a penguin (in-game).
</gallery>

===Actions===
<gallery>
File:GoldPuffleGoldJackhammer.PNG|A Gold Puffle playing with the Gold Jackhammer.
File:GoldPuffleDumpTruck.PNG|A Gold Puffle playing with the Gold Puffle's Dump Truck.
File:GoldPuffleSleeping.PNG|A Gold Puffle sleeping.
File:GoldPuffleAngry.PNG|An unsure Gold Puffle.
File:GoldPuffleSad.PNG|A sad Gold Puffle.
File:GoldPuffleEating.PNG|A Gold Puffle eating.
File:GoldPuffleLaughing.PNG|A Gold Puffle laughing.
File:GoldPufflePocked.PNG|A Gold Puffle when pocked.
‎File:GoldPuffle.PNG|Another Gold Puffle
</gallery>

===Artwork===
<gallery>
File:Yellow-puffle-wink.png|A winking blue puffle with golden paint on it from [[The Legend of the Golden Puffle]].
File:GoldenPuffle.PNG|A puffle-shaped chocolate in golden wrapper.
File:GoldenPuffleWaddleSquad.PNG|A fake golden puffle as seen in Waddle Squad.
File:GoldenPufflePotionBook.PNG|The Golden Puffle as seen at the back of the potions book.
File:GoldPuffle1.PNG|A Gold Puffle
File:GoldPuffle2.PNG
File:GoldPuffle3.PNG
File:GoldPuffle4.PNG|A Gold Puffle eating.
File:GoldPuffle5.PNG|
</gallery>

==Videos==
<youtube>8465WY1vpP0</youtube>
<youtube>9ZL7WCyrINg</youtube>

==Glitch==
They were playable in [[Puffle Launch]] and [[Pufflescape]] for some time after their release. If used in either of these games, it would take the appearance of a [[Red Puffle]] (Puffle Launch) or a White Puffle (Pufflescape). This glitch has been fixed and Gold Puffles no longer can be used in either of these games.

==Names in other languages==
{{OtherLanguage
|portuguese= Puffle Dourado
|spanish= Puffle dorado
|french= Puffle d’or
|german= Goldener Puffle
}}

{{Puffleswf|gold}}

{{SWFArchives}}

==References==
{{reflist}}

{{Succession box|Rainbow Puffle|}}

{{creatures}}

[[Category:Puffles]]
[[pt:Puffle Dourado]]
