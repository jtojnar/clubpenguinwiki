[[Image:Game-card.jpg|thumb|The old [[Membership Cards|Game Card]], one method of acquiring a membership status.]]
[[Penguin]]s who have '''Membership'''<ref>http://www.clubpenguin.com/membership/</ref> can dress up in various clothing, decorate their [[igloo]]s, adopt more than two [[puffles]], have more choices, more room on [[servers]], and go to exclusive [[parties]]. The members can get clothes not only from the [[Gift Shop]], but if they are [[Elite Penguin Force|EPF]] agents, then they can buy clothes from the [[Elite Gear]]. Also, if they are [[ninja]]s, they can also get ninja furniture, buy a hand [[gong]] or get one of the [[Ninja Costume]]s. [[The Stage]] also has clothes in the [[Costume Trunk]] (note: The stage changes plays, which also affect the costumes). Also, they may open their igloo to the public to have a "party." Simply, a membership results in many additional features. The main countries for membership are the US, Canada, the UK, and Australia.<ref>http://free-club-penguin-membership.com/</ref> When a player becomes a member, a "Member Badge" will appear in the upper left corner of their [[Player Card]]. Member Badges can vary according to how long you have been a member on [[Club Penguin]].

==Member Features==
*Access all mini-game levels.
**Record their [[SoundStudio]] creations for up to 3 minutes.
**Play [[Astro Barrier]] level 6 and above.
**Play all levels of [[Bits & Bolts]].
**Play the [[Pink Puffle|Pink]] and [[Black Puffle]] levels in [[Puffle Rescue]].
**Play [[Puffle Launch]]'s [[Soda Sunset]] and [[Box Dimension (Puffle Launch)|Box Dimension]] levels.
**Play [[Catchin' Waves]] in Competition and Survival modes.
**Play [[Aqua Grabber]]'s Soda Seas level.
**Play [[Thin Ice]] level 11 and above.
**Play [[Jet Pack Adventure]] level 2 and above.
**Play multi-player or dance to 'Let's Bounce', 'Go West' or 'Patrick's Jig' in [[Dance Contest]].
*Purchase mini-game upgrade items.
*Earn EPF agent items (Agent, Tactical, Comm, Tech and Stealth)
**Access the [[Underwater]].
*Unlimited access to exclusive features at Parties.
**Access special [[member-only|Member-Only]] areas.
**Receive free member items.
**Buy member items from party catalogs.
**Attend the [[Member Parties|Member-Only]] parties.
*Wear any clothing items for your penguin. 
**Get all the items for all during parties and the Items for Everyone in the Penguin Style catalog.
*Adopt any color of puffle and adopt up to 75 puffles.
*Get furniture and items for your igloo.
*Get interactive igloo items for your pet puffles.
*Upgrade your igloo from its default state.
*Choose background music for your igloo.
*Open [[igloo]]s to the public and host parties and events.
*Customize the [[Stamp]] Book.
*Get member stamps.
*Buy [[Puffle Hats]] in [[Puffle Catalog]].
*Dig for rare items and puffle food in [[Puffle Digging]].

==Legend==
{|border="1" class="wikitable" 
!'''Months'''!!'''Badge'''
|-
|1 - 6 Months||[[File:Memberbadge.png|50px]]
|-
|7 - 12 Months||[[File:6-12 EN.png|50px]]
|-
|13 - 18 Months||[[File:12-18 EN.png|50px]]
|-
|19 - 24 Months||[[File:18-24 EN.png|50px]]
|-
|25+ Months||[[File:24-plus EN.png|50px]]
|}

==Membership Prices==
Membership can be paid in Monthly, Bi-Annual or Annual installments. The annual option is cheaper over a long term period; however, many users prefer to use Monthly, as they can cancel it after any monthly period.

Memberships can be purchased online (as can gift certificates), or residents of the United States, United Kingdom, and Canada may purchase a membership card.

===Currency===
{|border="1" class="wikitable"
!'''Currency initials'''!!'''What they mean'''!!'''[[wikipedia:Currency symbols|Currency symbols]]'''
|-
|GBP||Great British Pound||£
|-
|BRL||Brazilian Real||R$
|-
|AUD||Australian Dollar||$
|-
|NZD||New Zealand Dollar||$
|-
|USD||United States Dollar||$
|-
|CAN||Canadian Dollar||$
|-
|EUR||Euro||€
|-
|MXN||Mexican Peso||Mex$
|-
|ARS||Argentine Peso||$
|-
|MYR||Malaysian Ringgit||RM
|-
|INR||Indian Rupees||₹
|-
|PHP||Philippine Pesos||₱
|-
|CHI||Chilean Pesos||$
|-
|COP||Colombian Pesos||$
|-
|PLN||Polish Złoty||zł
|-
|TL||Turkish Lira||TL
|-
|NOK||Norwegian Kroner||kr
|-
|DKK||Danish Krone||k
|-
|SEK||Swedish krona||kr
|-
|PEN||Nuevo Sol||S/.
|-
|CRC||Costa Rican Colón||₡
|-
|ILS||Israeli New Shekel||₪
|-
|ZAR||South African Rand||R
|-
|UYU||Uruguayan Peso||$U
|-
|GTQ||Guatemalan Quetzal||Q
|-
|DOP||Dominican Peso||DOP
|-
|SGD||Singaporean Dollar||$
|}

===Membership Prices===
*Monthly:
**£4.95 GBP
**R$8.95 BRL
**$8.95 AUD/NZD
**$7.95 USD/CAN
**€4.95 EUR
**Mex$55.00 MXN
**$49.90 ARS
**RM13.95 MYR
**₹199 INR
**$2.500 CHI
**$9.500 COP
**14.00 PLN
**$8.90 TL
**kr39.00 DKK
**kr45.00 NOK
**kr49.00 SEK
**₱195.00 PHP
**$8.95 SGD

*3 Months '''(Limited Edition):'''
**£9.95 GBP
**R$22.95 BRL
**$17.95 AUD/NZD
**$14.95 USD/CAN
**€12.95 EUR
**Mex$128.00 MXN

*6 Months:
**£19.95 GBP
**R$44.95 BRL
**$34.95 AUD/NZD
**$39.95 USD/CAN
**€24.95 EUR
**Mex$255.00 MXN
**$219.90 ARS
**RM69.95 MYR
** ₹990 INR
**12.300 CHI
**$47.500 COP
**$44.90 TL
**70.00 PLN
**kr195.00 DKK
**kr225.00 NOK
**kr245.00 SEK
**₱990.00 PHP
**$35.95 SGD

*12 Months:
**£29.95 GBP
**R$84.95 BRL
**$59.95 AUD/NZD
**$59.95 USD/CAN
**€39.95 EUR
**Mex$495.00 MXN
**$399.90 ARS
**RM134.95 MYR
**₹1900 INR
**22.750 CHI
**$91.500 COP
**$86.90 TL
**130.00 PLN
**kr295.00 DKK
**kr345.00 NOK
**kr395.00 SEK
**₱1900.00 PHP
**$59.95 SGD

*<small>As of July 13 the prices will be raising with '''2.00 USD''' (1 Month and 12 Months Memberships) and '''10.00 USD''' (6 Months Membership) in United States of America and Canada.</small>

==Trivia==
*If players purchased a membership between December 9 and 31, 2010, they would get 12,000 coins added onto their account.
*If players purchased a 1-month membership between January 18th to the 31st, 2013, it would cost $1.99 and you will receive, a custom igloo, a bone toga, special igloo items, 5000 coins, and a triceratops shield.

==Sources==
{{reflist}}

{{Types of Penguins}}

[[Category:Members]]
[[Category:Items]]
[[Category:Penguins]]
[[Category:Misc. Merchandise]]
[[pt:Assinatura]]
