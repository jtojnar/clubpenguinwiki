{{ItemInfobox
|name= Blizzard Wizard Robe
|image= File:BlizzardWizardRobes.png
|available= No
|type= Body Item
|member= Yes
|party= None
|cost= 450 [[coins]]
|found= [[Penguin Style]], [[Treasure Book]] Series 9
|id= 4128 (members)<br>14128 (unlockable)
|unlock= Yes (Series 9)
}}The '''Blizzard Wizard Robe''' is a body item in ''[[Club Penguin]]''. It was only available to [[member]]s at the price of 450 [[coin]]s in the ''[[Penguin Style]]'' catalog. This costume is similar to the [[Waterfall Coat]] earned in [[Card-Jitsu Water]], only lighter blue and with snowflakes.

==History==
The Blizzard Wizard Robe is a common item.
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|Penguin Style||October 2009||Unknown
|-
|[[Treasure Book (Series 9)|Treasure Book 9]]||July 12, 2009||October 4, 2011
|-
|Penguin Style||October 2010||January 2011
|-
|Penguin Style||May 2, 2012||August 1, 2012
|}


==Appearances==
*The Blizzard Wizard Robe has been featured on a Penguin Plush toy and on one of the Penguin Figurine toys.
*On the cover of the October 2009 Penguin Style catalog.
*On the [[Tree Forts postcard]].

==Gallery==
<gallery> 
File:Blizzard Wizard Robe Game.PNG|The Blizzard Wizard Robe in-game.
File:Blizzard Wizard Robe PlayerCard.PNG|The Blizzard Wizard Robe on a [[Player Card|player card.]]
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Manto do Mago Gelado
|spanish= Túnica de mago
|french= La Robe de Magicien à Flocons
|german= Eiskristall-Zaubermantel
|russian= Плащ чародея
}}

==See Also==
*[[Blizzard Wizard Hat]] (Paired with this item in the Oct. 2009 and 2010 Penguin Style(s), and the Series 9 Treasure Book)
*[[Crystal Staff]] (Paired with this item in the Oct. 2009 and 2010 Penguin Style(s), and the Series 9 Treasure Book)

==SWF==
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/4128.swf Blizzard Wizard Robe (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/4128.swf Blizzard Wizard Robe (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/4128.swf Blizzard Wizard Robe (paper)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/14128.swf Blizzard Wizard Robe (unlockable icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/14128.swf Blizzard Wizard Robe (unlockable sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/14128.swf Blizzard Wizard Robe (unlockable paper)]

[[Category:Clothing]]
[[Category:Body Items]]
