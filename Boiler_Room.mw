{{Seeideas}}
{{RoomInfobox
|title = Boiler Room
|image = File:Boiler Room.png
|place = Underneath the [[Night Club]], and beside the [[Cave]].
|open = April 7, 2005 ([[Penguin Chat 3]])<br>May 21, 2006
|games = None.
|roomid = 804
|tourguide = "{{TourBoiler}}"
}}The '''Boiler Room''' is the basement of the [[Night Club]]. It is a place most notable for holding the last six editions of ''[[The Penguin Times]]'', called the "[[Old News]]". This room can either be accessed from a speaker in the [[Dance Club]], green door in the [[Cave]], or via [[Elite Spy Phone]].

==History==
According to the [[Book Room]]'s [[Library]] book "''Truth or Dare''", the Boiler Room once had no electricity, and was dark. The "[[Keeper of the Boiler Room]]," or just the "Keeper", is a green [[puffle]] found on the bottom-right speaker of the Dance Club. This room can be accessed by the top-right speaker in the [[Night Club]] or the green door of the pool. The boiler was once broken by Herbert. P. Bear in [[Secret Mission|Mission #8]]. It also has the last six [[Club Penguin Times]] issues. It appeared in [[Penguin Chat 3]], though it was [[secret]] then.

==Trivia==
*Unlike the Cave and the Mine, this [[room]] was not flooded during the 2007 Waddle On [[Water Party]], even though water was spilling underneath the entrance to the [[Cave]].
*During the [[Celebration of Water]], you could walk on the walls because the underground rooms were flooded.
*As said by Aunt Arctic, there are ink and paper supplies in the Boiler Room. They are most likely in the locked drawers.

==Pins==
{|border="1" class="wikitable" 
!'''Pin #'''!!'''Pin name'''!!'''Available from'''!!'''Available until'''
|-
|40||[[File:Baseball Pin.PNG|30px]][[Baseball pin]]||August 31, 2007||September 14, 2007
|-
|50||[[File:Red Snow Shovel Pin.PNG|30px]][[Red Snow Shovel pin]]||January 4, 2008||January 18, 2008
|-
|56||[[File:Book Pin.PNG|30px]][[Book pin]]||March 14, 2008||March 28, 2008
|-
|62||[[File:Anvil Pin.PNG|30px]][[Anvil pin]]||May 23, 2008||June 6, 2008
|-
|69||[[File:150th Newspaper Pin.PNG|30px]][[150th Newspaper pin]]||August 29, 2008||September 12, 2008
|-
|89||[[File:KingsCrownPin.PNG|30px]][[King's Crown pin]]||May 8, 2009||May 22, 2009
|-
|94||[[File:Beach Umbrella Pin.PNG|30px]][[Beach Umbrella pin]]||July 17, 2009||July 31, 2009
|-
|103||[[File:4th Anniversary Cake Pin.PNG|30px]][[4th Anniversary Cake pin]]||October 23, 2009||November 6, 2009
|-
|129||[[File:Compass Pin.PNG|30px]][[Compass Pin]]||July 30, 2010||August 12, 2010
|-
|160||[[File:BrazierPin.PNG|30px]][[Brazier Pin]]||May 19, 2011||June 2, 2011
|-
|197||[[File:CheesePin.PNG|30px]][[Cheese pin]]||March 7, 2012||March 22, 2012
|-
|203||[[File:ScornCrest.PNG|30px]][[Scorn Crest pin]]||May 16, 2012||May 30, 2012
|-
|222||[[File:HeavyWeightsPin.PNG|30px]][[Heavy Weights pin]]||November 14, 2012||November 29, 2012
|-
|255||[[File:TombstonePin.PNG|30px]][[Tombstone pin]]||October 16, 2013||November 1, 2013
|-
|262||[[File:HolidayGiftPin.png|30px]][[Holiday Gift pin]]||December 26, 2013||January 8, 2014
|-
|290||[[File:GhostPin.PNG|30px]][[Ghost Pin]]||October 29, 2014||November 12, 2014
|}

==Gallery==
===Room designs===
<gallery>
File:Boiler Room.png|The normal design.
File:PC3BoilerRoom.jpg|The Boiler Room shortly after it's release in [[Penguin Chat 3]].
Image:100 Issue Boiler Room.jpg|The Boiler Room celebrating the 100th Issue of [[The Penguin Times]] (c. 2007).
Image:Water_Party@Mine_pic.jpg|The Boiler Room during the 2007 Water [[Party]].
Image:Cpboilerrmaprilfools1.png|The Boiler Room 15 seconds after enter on April Fool's Day 2008, 2009 and 2010 Parties.
Image:Medieval_Boiler_Room.PNG|The Boiler Room during the [[Medieval Party]] in May 2008. Also known as "The Smithy" during that [[party]].
File:AprilFoolsDay2011 Boiler.PNG|During the [[April Fools' Party 2011]].
File:MedievalParty2011BoilerRoom.PNG|During the [[Medieval Party 2011]].
File:MedievalParty2012Boiler.PNG|During the [[Medieval Party 2012]].
File:OperationPuffleBoiler.PNG|During [[Operation: Puffle]].
</gallery>
===Other===
<gallery>
Image:Old_Newspapers.PNG|The [[Old News|file cabinet]] with older editions of [[The Penguin Times]].
Image:Vintage Boiler Room.png|The Boiler Room without electricity.
Image:Boilerroomnews.PNG|The Old News icon accessible in the bottom right corner of the screen.
Image:Tehboiler.png|The Boiler Room's boiler
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Caldeira
|spanish= Sala de calderas
|french= Chauferrie
|german= Heizraum
|russian= Котельная
}}

{{SWFArchives}}

==See also==
*[[Old News]]
*[[Underground]]
*[[Night Club]]
*[[Map]]
{{Geographic location
|Centre    = Boiler Room
|North     = 
|Northeast = [[Night Club]]
|East      = 
|Southeast = 
|South     = 
|Southwest = 
|West      = 
|Northwest = [[Cave]]
}}
{{places}}
[[Category:Rooms]]
