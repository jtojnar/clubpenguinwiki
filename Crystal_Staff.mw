{{ItemInfobox
|name= Crystal Staff
|image= File:CrystalStaff.png
|available= No
|type= Hand Item
|member= Yes
|party= [[Medieval Party 2012]]
|cost=  250 [[coins]]
|found= [[Penguin Style]], [[Treasure Book]] Series 9, ''[[Medieval Catalog]]
|id= 718
}}

The '''Crystal Staff''' is a hand item in ''[[Club Penguin]]''. It costs 250 coins in the ''[[Penguin Style]]'' catalog or in [[Medieval Catalog]], and only [[members]] can buy this item. It's also unlockable in the Series 9 [[Treasure Book]], where it goes with the [[Blizzard Wizard Robes]] and the [[Blizzard Wizard Hat]]. 

==History==
This item was first released in May 2008 for the [[Medieval Party 2008]] as a secret item. It was released again in May 2009 for a Medieval Party, and then again in October 2009. It was available once again in the October 2010 ''Penguin Style'' catalog for the [[Halloween Party 2010]]. Its most recent re-release was on May 16, 2012 in the Medieval Catalog for the Medieval Party 2012. 
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan="4"|[[Penguin Style]]||May 2, 2008||August 1, 2008
|-
|May 1, 2009||September 4, 2009
|-
|October 2, 2009||November 6, 2009
|-
|October 1, 2010||February 4, 2011
|-
|Medieval Catalog||May 16, 2012||May 30, 2012
|}

==Gallery==
<gallery>
File:CrystalStaff1.PNG|The Crystal Staff in-game.
File:CrystalStaff2.PNG|The Crystal Staff on a player card.
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Cetro de Cristal
|spanish= Cetro de cristal
|french= Le Bâton Cristal
|german= Kristallstab
|russian= Посох с кристаллом
}}

{{Itemswf|718}}

[[Category:Clothing]]
[[Category:Hand Items]]
[[Category:Secret Item]]
