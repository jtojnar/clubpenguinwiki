{{Archive}}
{{PartyInfobox 
|name = Prehistoric Party 2014
|image= File:PrehistoricParty2014Logo.PNG
|imagesize=190px
|caption = Prehistoric Party 2014 logo
|membersonly = No
|when = January 22, 2014 - February 5, 2014
|freeitems = ''[[Prehistoric Party 2014#Free items|See list]]''
|wherehappening = [[Snow Forts]] and Prehistoric [[Club Penguin Island]]
|famouspenguins = [[Gary]]
|previous= [[Holiday Party 2013]]
|next= [[The Fair 2014]]
}}
The '''Prehistoric Party 2014''' was a party on ''[[Club Penguin]]''. It started on January 22, 2014 and ended on February 5, 2014. The Prehistoric Party 2014 was the second annual [[Prehistoric Party]]. It was the first party of 2014. [[Members]] were able to adopt up to six [[Dino Puffle|Dino Puffles]].

==History==
The party was confirmed by [[Polo Field]] on his Twitter<ref>https://twitter.com/polofield/status/406552337034268673</ref> and the [[Club Penguin Magazine]] Issue 25. On January 2, 2014 a new slide on the mainpage and an ad on the login and exit screens confirmed that [[members]] will be able to adopt the [[Dino Puffle|dino puffles]] and membership page confirmed that members, like the last year, will be able to transform into dinosaurs. A tweet on the official Club Penguin Twitter account revealed that [[Gary]] is going to visit the island during the party.<ref>https://twitter.com/clubpenguin/status/419606154977607680</ref> On January 6, Polo Field posted on What's New Blog a sneak peek of the dinosaur eggs and the dino puffles as eggs.<ref>http://www.clubpenguin.com/blog/2014/01/dinosaur-puffles</ref>

On January 8, Polo Field revealed a Concept Art of the party.<ref>http://www.clubpenguin.com/blog/2014/01/prehistoric-party-concept-art</ref> On January 20, a episode of [[The Spoiler Alert]] showed some rooms, how the new dinosaurs are and how the dinopuffles are.<ref>http://www.clubpenguin.com/blog/2014/01/video-spoiler-alert-prehistoric-party-sneak-peek-0</ref> On January 21, Polo Field said more details about the dino puffles.<ref>http://www.clubpenguin.com/blog/2014/01/dino-puffles-%E2%80%93-frequently-asked-questions</ref>
===In-game development===
On January 9, a message that read ''"Gary help! Ooga Ooga!"'' appeared in the [[Snow Forts]]. [[Jet Pack Guy]] deemed this as a suspicious message and advised [[Agents]] to secure the perimeter and search for clues. On the issue #429 of the Club Penguin Times, Gary said that the message is from his own distantancestor, [[Gary|Garugg the Ugg Ugg]]. According to Gary, even though Garugg is a caveguin, he has a scientific mind. Gary thinks that if he left the message for us, then there must be a scientific crisis. On the issue #430 of the Club Penguin Times, [[Aunt Arctic]] confirmed that we need to travel to the Stone Age because Garugg needs our help. On January 16, the message on the Snow Forts was closed and the Time Trekker appeared. There was a message inside saying that the Time Trekker is being calibrated. From January 23 to February 5, the penguins were able to travel to the prehistoric time to save the Dino Puffles. After a week, on January 30, the Megalodon on the [[Scary Ice]] disappeared. Penguins could seen it every hour on the modern [[Iceberg]].

==Free items==
A total of eighteen free items were made available for this party. Items marked with a badge ([[File:Memberbadge.png|20px]]) indicate that the item can only be obtained by members.
<gallery>
File:PrimalHelmet.PNG|link=Primal Helmet|'''[[Primal Helmet]]'''<br/>After finding all the triceratops eggs in [[Dino Dig]]
File:DinoClaws.PNG|link=Dino Claws|'''[[Dino Claws]]'''<br/>After finding all the raptor eggs in [[Dino Dig]]
File:StoneHatchet.PNG|link=Stone Hatchet|'''[[Stone Hatchet]]'''<br/>After finding all the stegosaurus eggs in [[Dino Dig]]
File:RockHardHat.PNG|link=Rock Hard Hat|'''[[Rock Hard Hat]]'''<br/>After finding all the triceratops eggs in [[Dino Dig]]
File:PrimalBracers.PNG|link=Primal Bracers|'''[[Primal Bracers]]'''<br/>After finding all the t-rex eggs in [[Dino Dig]]
File:PrehistoricSlingShot.PNG|link=Prehistoric Sling Shot|'''[[Prehistoric Sling Shot]]'''<br/>After finding the [[Blue Triceratops]] egg in [[Dino Dig]]
File:Dino-leatherShield.PNG|link=Dino-leather Shield|'''[[Dino-leather Shield]]'''<br/>After finding the [[Black T-rex]] egg in [[Dino Dig]]
File:DinoHorns.PNG|link=Dino Horns|'''[[Dino Horns]]'''<br/>After finding the [[Pink Stegosaurus]] egg in [[Dino Dig]]
File:PrehistoricKite.PNG|link=Prehistoric Kite|'''[[Prehistoric Kite]]'''<br/>After finding the [[Red Triceratops]] egg in [[Dino Dig]]
File:StoneHairDryer.PNG|link=Stone Hair Dryer|'''[[Stone Hair Dryer]]'''<br/>After finding the [[Purple T-Rex]] egg in [[Dino Dig]]
File:PrehistoricBetaHat.PNG|link=Prehistoric Beta Hat|'''[[Prehistoric Beta Hat]]'''<br/>After finding the [[Yellow Stegosaurus]] egg in [[Dino Dig]]
File:Gary'sPrehistoricGiveawayIcon.PNG|link=Gary's Prehistoric Giveaway|'''[[Gary's Prehistoric Giveaway]]'''<br/>Meet [[Gary]]
File:BlueDinoPuffle.PNG|link=Blue Triceratops|[[File:Memberbadge.png|20px|Members only]] '''[[Blue Triceratops]]'''<br/>After finding its egg in [[Dino Dig]]
File:BlackDinoPuffle.PNG|link=Black T-Rex|[[File:Memberbadge.png|20px|Members only]] '''[[Black T-rex]]'''<br/>After finding its egg in [[Dino Dig]]
File:PinkDinoPuffle.PNG|link=Pink Stegosaurus|[[File:Memberbadge.png|20px|Members only]] '''[[Pink Stegosaurus]]'''<br/>After finding its egg in [[Dino Dig]]
File:RedDinoPuffle.PNG|link=Blue Triceratops|[[File:Memberbadge.png|20px|Members only]] '''[[Red Triceratops]]'''<br/>After finding its egg in [[Dino Dig]]
File:PurpleDinoPuffle.PNG|link=Purple T-Rex|[[File:Memberbadge.png|20px|Members only]] '''[[Purple T-rex]]'''<br/>After finding its egg in [[Dino Dig]]
File:YellowDinoPuffle.PNG|link=Yellow Stegosaurus|[[File:Memberbadge.png|20px|Members only]] '''[[Yellow Stegosaurus]]'''<br/>After finding its egg in [[Dino Dig]]
</gallery>

==Gallery==
===Party Pictures===
<gallery widths=180>
File:PrehistoricParty2014Party6.PNG|The [[Fancy Plaza]]
File:PrehistoricParty2014Party10.PNG|The [[Hunting Spot]]
File:PrehistoricParty2014Party11.PNG|The [[Ptero Town]]
File:PrehistoricParty2014Party14.PNG|The [[Rocky's Pizza]]
File:PrehistoricParty2014Party13.PNG|The [[Salon]]
File:PrehistoricParty2014PreForts-2.PNG|The [[Snow Forts]]
File:PrehistoricParty2014Party12.PNG|The [[Scary Ice]] (Week 1)
File:PrehistoricParty2014Party12Week2.png|The [[Scary Ice]] (Week 2)
File:PrehistoricParty2014Party3.PNG|The [[Stony Town]]
File:PrehistoricParty2014Party1.PNG|The [[Time Trekker (Party Room)|Time Trekker]]
File:PrehistoricParty2014Party2.PNG|The [[Tree Place]]
File:PrehistoricParty2014Party8.PNG|The [[Tricera Town]]
File:PrehistoricParty2014Party9.PNG|The [[Tyranno Town‎]]
File:PrehistoricParty2014Party15.PNG|The [[Volcano Entrance]]
File:PrehistoricParty2014Party16.PNG|The [[Volcano (Party Room)|Volcano]]
File:PrehistoricParty2014Party17.PNG|The [[Volcano (Party Room)|Volcano]]
File:PrehistoricParty2014Party5.PNG|The [[Water Place]]
File:PrehistoricParty2014Party7.PNG|The [[Yuck Swamp]]
File:PrehistoricParty2014Party4.PNG|The [[Yum Yum]]
</gallery>

===Sneak Peeks===
<Gallery widths=180>
File:PrehistoricParty2014sneakpeek.png|The tweet that confirmed it.
File:ConceptArtPrehistoricParty2014.PNG|The concept art of a dinosaur.
DinoPuffleEggsSneakPeek.jpg|Dinosaur eggs and Dino Puffles as eggs.
File:ConceptArtPrehistoricParty2014-2.PNG|The concept art of the Prehistoric Party.
File:DinoPuffles1.PNG|The 6 types of Dinosaur Puffles.
</gallery>

===Login and Exit Screens===
<gallery widths=180>
File:PrehistoricParty2014LoginScreen1.PNG|The first [[Login Screen]].
File:PrehistoricParty2014LoginScreen2.PNG|The second [[Login Screen]].
File:PrehistoricParty2014ExitScreen1.PNG|The first [[Exit Screen]].
File:PrehistoricParty2014ExitScreen2.jpg|The second [[Exit Screen]].
</gallery>

===Pre-Party===
<gallery widths=180>
File:PrehistoricParty2014PreForts.PNG|The [[Snow Forts]] (Jan. 8)
File:PrehistoricParty2014PreForts-2.PNG|The [[Snow Forts]] (Jan. 15)
File:PrehistoricParty2014PreParty1.PNG|The [[Time Trekker (Party Room)|Time Trekker]]
</gallery>

===Miscellaneous===
<gallery widths=180>
File:PrehistoricParty2014Homepage1.jpg|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:PrehistoricParty2014Homepage2.jpg|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 2).
File:FacebookHeaderPrehistoric2014.jpg|The Club Penguin's Facebook Header
File:MapPrehistoricParty2014.PNG|The map of the prehistoric island
</gallery>

==References==
{{reflist}}
{{SWFArchives}}


{{Party}}

[[Category:Parties of 2014]]
[[pt:Viagem Pré-Histórica 2014]]
