{{OutOfDate}}
[[File:EmotesHelp.jpg|right]]
'''Emoticons''' are little faces and symbols that can symbolize how [[Penguin]]s feel and they usually appear in speech bubbles accessed  by clicking on the Emote Menu or by pressing E and various keys.

== List of Emoticons ==
[[File:Emote Menu.JPG|thumb|The original Emote "menu".]]
===Face Emoticons===
<gallery>
File:Emote1.PNG|The "'''Laughing'''" emoticon (E+1)
File:Emote2.PNG|The "'''Happy'''" emoticon (E+2)
File:Emote3.PNG|The "'''Straight'''" emoticon (E+3)
File:Emote4.PNG|The "'''Sad'''" emoticon<br>(E+4)
File:Emote5.PNG|The "'''Surprised'''" emoticon (E+5)
File:Emote6.PNG|The "'''Sticking Out Tongue'''" emoticon (E+6)
File:Emote7.PNG|The "'''Winking'''" emoticon (E+7)
File:Emote8.PNG|The "'''Sick'''" emoticon<br>(E+8)
File:Emote9.PNG|The "'''Angry'''" emoticon (E+9)
File:Emote10.PNG|The "'''Frowning'''" emoticon (E+0)
</gallery>

===Food Emoticons===
<gallery>
File:Emote11.PNG|The "'''Chocolate Ice Cream'''" emoticon (E+W)
File:Emote12.PNG|The "'''Strawberry Ice Cream'''" emoticon (E+Q)
File:Emote13.PNG|The "'''Pizza'''" emoticon (E+Z)
File:Emote14.PNG|The "'''Cake'''" emoticon (E+K)
File:Emote15.PNG|The "'''Popcorn'''" emoticon (E+O)
File:Emote16.PNG|The "'''Coffee'''" emoticon (E+C)
</gallery>

===Other Emoticons===
<gallery>
File:Emote17.PNG|The "'''Igloo'''" emoticon<br>(E+I)
File:Emote18.PNG|The "'''Blue Puffle'''" emoticon (E+P)
File:Emote19.PNG|The "'''Sun'''" emoticon<br>(E+D)
File:Emote20.PNG|The "'''Moon and Stars'''" emoticon (E+N)
File:Emote21.PNG|The "'''Joystick'''" emoticon (E+G)
File:Emote22.PNG|The "'''Shamrock'''" emoticon (E+L)
File:Emote23.PNG|The "'''Flower'''" emoticon (E+F)
File:Emote24.PNG|The "'''Heart'''" emoticon (E+H)
File:Emote25.PNG|The "'''Question Mark'''" emoticon (?)
File:Emote26.PNG|The "'''Exclamation Mark'''" emoticon (!)
File:Emote27.PNG|The "'''Light Bulb'''" emoticon (E+B)
File:Emote28.PNG|The "'''Coin'''" emoticon<br>(E+M)
File:Emote29.PNG|The "'''Music Note'''" emoticon (E+T)
</gallery>

===Former Emoticons===
These Emoticons are no longer available in-game.
<gallery>
File:OldLaughEmoticon.PNG|The old "'''Laughing'''" emoticon
File:SkullEmoticon.PNG|The "'''Skull'''" emoticon
</gallery>

==Limited Party Emoticons==
Every party since the 2012 Holiday Party has had at least 3 limited time emoticons.

<gallery>
File:CookieEmote.PNG|The "'''Cookie'''" emoticon <br> [[Holiday Party 2012]]
File:FrozenEmote.PNG|The "'''Frozen'''" emoticon <br> [[Holiday Party 2012]] <br> [[Frozen Party]] <br> [[Frozen Fever Party]]
File:MusicalNotesEmote.PNG|The "'''Musical Notes'''" emoticon <br> [[Holiday Party 2012]] <br> [[Frozen Party]] <br> [[Merry Walrus Party]] <br> [[Frozen Fever Party]] 
File:CavemanEmote.PNG|The "'''Caveman'''" emoticon <br> [[Prehistoric Party 2013]] <br> [[Prehistoric Party 2014]]
File:DinosaurFoodEmote.PNG|The "'''Dinosaur Food'''" emoticon <br> [[Prehistoric Party 2013]] <br> [[Prehistoric Party 2014]]
File:FireEmote.PNG|The "'''Fire'''" emoticon <br> [[Prehistoric Party 2013]] <br> [[Prehistoric Party 2014]]
File:HollywoodPartyEmote3.PNG|The "'''Cheese'''" emoticon <br> [[Hollywood Party]]
File:HollywoodPartyEmote2.PNG|The "'''Clapperboard'''" emoticon <br> [[Hollywood Party]]
File:HollywoodPartyEmote1.PNG|The "'''Superstar'''" emoticon <br> [[Hollywood Party]]
File:PufflePartyEmote1SP.PNG|The "'''Rainbow Puffle'''" emoticon <br> [[Puffle Party 2013]]
File:PufflePartyEmote2SP.PNG|The "'''O-Berry'''" emoticon <br> [[Puffle Party 2013]]
File:PufflePartyEmote3SP.PNG|The "'''Rainbow'''" emoticon <br> [[Puffle Party 2013]]
File:MSHT2013Emote3.PNG|The "'''Money Bag'''" emoticon <br> [[Marvel Super Hero Takeover 2013]]
File:MSHT2013Emote1.PNG|The "'''Police Siren'''" emoticon <br> [[Marvel Super Hero Takeover 2013]]
File:MSHT2013Emote2.PNG|The "'''Pow'''" emoticon <br> [[Marvel Super Hero Takeover 2013]]
File:CJPEmote1.PNG|The "'''Fortune Cookie'''" emoticon <br> [[Card-Jitsu Party 2013]]
File:CJPEmote2.PNG|The "'''Chopsticks'''" emoticon <br> [[Card-Jitsu Party 2013]]
File:CJPEmote3.PNG|The "'''Teapot'''" emoticon <br> [[Card-Jitsu Party 2013]]
File:MUTEmote1.PNG|The "'''Nerd'''" emoticon <br> [[Monsters University Takeover]]
File:MUTEmote2.PNG|The "'''Scary'''" emoticon <br> [[Monsters University Takeover]]
File:MUTEmote3.PNG|The "'''OK Foam Finger'''" emoticon <br> [[Monsters University Takeover]]
File:MUTEmote4.PNG|The "'''JOX Foam Finger'''" emoticon <br> [[Monsters University Takeover]]
File:MUTEmote5.PNG|The "'''PNK Foam Finger'''" emoticon <br> [[Monsters University Takeover]]
File:MUTEmote6.PNG|The "'''ROR Foam Finger'''" emoticon <br> [[Monsters University Takeover]]
File:SWTEmote1.PNG|The "'''Galactic Empire'''" emoticon <br> [[Star Wars Takeover]] <br> [[Star Wars Rebels Takeover]]
File:SWTEmote2.PNG|The "'''Rebel Alliance'''" emoticon <br> [[Star Wars Takeover]] <br> [[Star Wars Rebels Takeover]]
File:SWTEmote3.PNG|The "'''Green Lightsaber'''" emoticon <br> [[Star Wars Takeover]] <br> [[Star Wars Rebels Takeover]]
File:TBMSJEmote1.PNG|The "'''Surfboard'''" emoticon <br> [[Teen Beach Movie Summer Jam]]
File:TBMSJEmote2.PNG|The "'''Beach Ball'''" emoticon <br> [[Teen Beach Movie Summer Jam]]
File:TBMSJEmote3.PNG|The "'''Flower'''" emoticon <br> [[Teen Beach Movie Summer Jam]]
File:MP2013Emote1.PNG|The "'''Ogre'''" emoticon <br> [[Medieval Party 2013]]
File:MP2013Emote2.PNG|The "'''Shield and Sword'''" emoticon <br> [[Medieval Party 2013]]
File:MP2013Emote3.PNG|The "'''Wand'''" emoticon <br> [[Medieval Party 2013]]
File:HP2013Emote1.PNG|The "'''Vampire Smile'''" emoticon <br> [[Halloween Party 2013]] <br> [[Halloween Party 2014]]
File:HP2013Emote2.PNG|The "'''Jack-o'-lantern'''" emoticon <br> [[Halloween Party 2013]] <br> [[Halloween Party 2014]]
File:HP2013Emote3.png|The "'''Candy'''" emoticon <br> [[Halloween Party 2013]]
File:HP2013Emote4.PNG|The "'''Ghost'''" emoticon <br> [[Halloween Party 2013]] <br> [[Halloween Party 2014]]
File:HP2013Emote5.PNG|The "'''Bat'''" emoticon <br> [[Halloween Party 2013]]
File:HP2013Emote6.PNG|The "'''Scratch'''" emoticon <br> [[Halloween Party 2013]]
File:Operation Puffle Emote EPF.svg|The "'''EPF'''" emoticon <br> [[Operation: Puffle]]
File:Operation Puffle Emote Agent.svg|The "'''EPF Agent'''" emoticon <br> [[Operation: Puffle]]
File:Operation Puffle Emote Herbert Paw.svg|The "'''Herbert Paw'''" emoticon <br> [[Operation: Puffle]]
File:Holiday2013Emote1.png|The "'''Present'''" emoticon <br> [[Holiday Party 2013]] <br> [[Merry Walrus Party]]
File:Holiday2013Emote2.png|The "'''Train Whistle'''" emoticon <br> [[Holiday Party 2013]]
File:Holiday2013Emote3.png|The "'''Coins for Change'''" emoticon <br> [[Holiday Party 2013]] <br> [[Merry Walrus Party]]
File:PP2014Emote1.png|The "'''Hammer and Wheel'''" emoticon <br> [[Prehistoric Party 2014]]
File:PP2014Emote2.png|The "'''Clock'''" emoticon <br> [[Prehistoric Party 2014]]
File:PP2014Emote3.png|The "'''Animated O-Berry'''" emoticon <br> [[Prehistoric Party 2014]] <br> [[Puffle Party 2014]] <br> [[Puffle Party 2015]]
File:FairEmote1.png|The "'''8 Bit'''" emoticon <br> [[The Fair 2014]] <br> [[The Fair 2015]]
File:FairEmote2.png|The "'''Clown'''" emoticon <br> [[The Fair 2014]] <br> [[The Fair 2015]]
File:FairEmote3.png|The "'''Woo Hoo'''" emoticon <br> [[The Fair 2014]] <br> [[The Fair 2015]]
File:MWTEmote1.png|The "'''Constantine'''" emoticon <br> [[Muppets World Tour]]
File:MWTEmote2.png|The "'''Cream Pie'''" emoticon <br> [[Muppets World Tour]]
File:MWTEmote3.png|The "'''Clapping'''" emoticon <br> [[Muppets World Tour]] <br> [[Music Jam 2014]] <br> [[SoundStudio Party]]
File:Puffle2014Emote1.png|The "'''Dog Biscuit'''" emoticon <br> [[Puffle Party 2014]] <br> [[Puffle Party 2015]]
File:Puffle2014Emote2.png|The "'''Cat Yarn'''" emoticon <br> [[Puffle Party 2014]] <br> [[Puffle Party 2015]]
File:FP2014Emote1.png|The "'''Alien'''" emoticon <br> [[Future Party]]
File:FP2014Emote2.png|The "'''Shooting Star'''" emoticon <br> [[Future Party]]
File:FP2014Emote3.png|The "'''No Protobot'''" emoticon <br> [[Future Party]]
File:PCEmote1.png|The "'''Red Card'''" emoticon <br> [[Penguin Cup]]
File:PCEmote2.png|The "'''Yellow Card'''" emoticon <br> [[Penguin Cup]]
File:PCEmote3.png|The "'''Whistle'''" emoticon <br> [[Penguin Cup]]
File:PCEmote4.png|The "'''Go Team!'''" emoticon <br> [[Penguin Cup]]
File:PCEmote5.png|The "'''Green Foam Finger'''" emoticon <br> [[Penguin Cup]]
File:PCEmote6.png|The "'''Blue Foam Finger'''" emoticon <br> [[Penguin Cup]]
File:PCEmote7.png|The "'''Red Foam Finger'''" emoticon <br> [[Penguin Cup]]
File:PCEmote8.png|The "'''Yellow Foam Finger'''" emoticon <br> [[Penguin Cup]]
File:MJ2014Emote1.png|The "'''Cute Face'''" emoticon <br> [[Music Jam 2014]] <br> [[SoundStudio Party]]
File:MJ2014Emote2.png|The "'''Fist Pump'''" emoticon <br> [[Music Jam 2014]] <br> [[SoundStudio Party]]
File:FrozenPartyEmote.png|The "'''Snowflakes'''" emoticon <br> [[Frozen Party]] <br> [[Frozen Fever Party]]
File:SASEmote1.png|The "'''Helmet'''" emoticon <br> [[School and Skate Party]]
File:SASEmote2.png|The "'''Skateboard'''" emoticon <br> [[School and Skate Party]]
File:SASEmote3.png|The "'''Apple'''" emoticon <br> [[School and Skate Party]]
File:PirateEmote1.png|The "'''Crab Claws'''" emoticon <br> [[Pirate Party 2014]]
File:PirateEmote2.png|The "'''Pirate'''" emoticon <br> [[Pirate Party 2014]]
File:PirateEmote3.png|The "'''Treasure Chest'''" emoticon <br> [[Pirate Party 2014]]
File:MerryWalrusEmote.png|The "'''Snowed On'''" emoticon <br> [[Merry Walrus Party]]
File:SWREmote.png|The Inquisiter's Lightsaber'''" emoticon <br> [[Star Wars Rebels Takeover]]
File:Puffle2015Emote1.png|The "'''Rabbit Puffle LOL'''" emoticon <br> [[Puffle Party 2015]]
File:Puffle2015Emote2.png|The "'''Cat Puffle ILY'''" emoticon <br>  [[Puffle Party 2015]]
</gallery>

== Trivia ==
*The Heart and Skull emotes were both removed by the [[Club Penguin]] Team due to parent complaints. Club Penguin later brought back the Heart emote, saying it could be used in a positive light. The Skull emote was never brought back, with the team saying there are better ways to show you are feeling angry, such as using the Angry emote.
*The Smile/Laughing emote was removed and revamped due to a dispute with Yahoo.com over the similarities of the icon to one that Yahoo.com used {{fact}}.
*The Music Note emote when used emits a farting sound.

[[Category:Interface]]
