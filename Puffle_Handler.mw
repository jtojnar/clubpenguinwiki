<!--do NOT replace File:CurrentPH.PNG with ANY other file in the infobox. Simply upload the new image again as a new file on CurrentPH.PNG, and put new image in the gallery.-->
{{CharacterInfobox
|name = Paige
|image = File:CurrentPH.PNG
|imagesize = 200px
|fullname = Paige
|species = [[Penguin]]
|position = [[Elitist]]
|appeared = [[Club Penguin: Elite Penguin Force]], [[Club Penguin: Elite Penguin Force: Herbert's Revenge]]
|color = [[Brown]]<br>Originally [[Pink]]
|clothes = [[PH Hat/Hair|Hair and Hat]], [[PH Outfit‎|Outfit]], [[Puffle Whistle]] (in Game and Newspaper)
|related = Unknown
|friends = You, [[Elite Puffles]], [[Aunt Arctic|The Director]], [[Gary]], [[Dot]], [[Rookie]]
|walk = Yes
}}

{{Cquote2|G'day! I'm here to teach you how to care for your puffle!|Puffle Handler}}

'''Paige'''<ref>http://www.clubpenguinwiki.info/static/images/www/5/59/PaigethePuffleHandler.png</ref> (better known as '''Puffle Handler''', '''PH''', and '''Agent PH''') is the [[EPF Agent]] who trained all of the [[Elite Puffles]]. She has also been a meetable [[mascot]] since the [[Puffle Party 2012]], and is responsible for the discovery of [[Rainbow Puffle]]s.

== Biography ==
=== 2008 ===
Her first appearance was in ''[[Club Penguin: Elite Penguin Force]]''. As the [[Elite Puffle]] trainer, she gives you a [[Puffle Whistle]] . She also shows you how to utilize the Elite Puffle’s abilities in missions 2, 5, 7, and 8, after which she takes you to the [[Puffle Training Room]] for the Elite Puffle Test. In Mission 7, you must get her out of a bubble made by [[Pop]]. Her last appearance was in Mission 13, when she expressed her concern about the Elite Puffles after they were taken by [[Ultimate Proto-Bot 10,000]].

=== 2010 ===
Agent PH appeared once in ''[[Club Penguin: Elite Penguin Force: Herbert's Revenge]]'', despite her larger role in the previous game. In mission 1, she is in the [[EPF Training Facility]], where she sends the agents into special doors that require the [[Elite Puffles]] to exit.

=== 2011 ===
PH’s next appearance, and first appearance online, was on February 24, 2011, in the 280th issue of [[The Club Penguin Times]]. In it, she was called “The island’s resident [[puffle]] expert”. Her appearance had severely changed though when compared to her video game counterpart. She still wore an Australian-style Hat and Puffle Whistle, however she was brown instead of pink, had brown hair, and she also wore a survival-style belt and shoes. In addition, she now also had freckles, like [[Aunt Arctic]]. In March she was the head of a new project to renovate the [[Pet Shop]], to make it more comfortable and fun for the puffles; with activities such as [[Puffle Launch]]. Starting on March 7, 2011, she appears on the [[Puffle Card]] the first time you look at it to show you how to take care of your puffle(s). In October she came up with the idea of [[Puffle Hats]] for puffles to wear.

=== 2012 ===
In 2012 PH’s outfit was updated again. She still had an Australian-style hat and hair, but the hat was changed slightly. Also, instead of a belt she now wore a pink shirt with a jacket over it and brown shorts; and she no longer had shoes. She became an official [[mascot]] during the [[Puffle Party 2012]].

=== 2013 ===
From January until March 2013 PH was following a trail of hints and evidence, which eventually lead to her discovering [[Rainbow Puffle]]s in March 2013. Along with the new art style, PH’s outfit was slightly changed again. Rather than brown shorts, she wore a dress with a belt on it.

After the [[Medieval Party 2013]], she joined [[Gary]] and researched golden [[Puffle O's]]/[[O-Berries]]. She then visited the island during the [[Operation: Puffle]] and helped discover the [[Gold Puffle]].

== List of Quotes ==
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Quotes</big></center>
<div class="mw-collapsible-content">
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Hi</big></center>
<div class="mw-collapsible-content">
* Hi there!
* G'day!
* It's great to see so many puffle fans!
* Wow so many puffles!
* Hey how's it going mates?
* How is everyone and their puffles?
* Great to see ya!
* How ya goin?
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Puffle Feeding</big></center>
<div class="mw-collapsible-content">
* Great puffle ya got there
* What's its name?
* Great name
* Good on ya!
* Here's a treat for your puffle
* one treat for your puffle...
* ...one for me!
* eats puffle o
* TASTY!
* Puffle O's are DELICIOUS!
* here try one!
* tasty right?
* no?
* ok suit ya self
* If you don't like Puffle O's, there's always cookies!
* you can find more treats in the Puffle Catalog at the Pet Shop
* what does your puffle like best?
* carrots?
* gives carrot
* Puffle O's?
* gives puffle o
* Pizza?
* gives pizza
* Cookies?
* gives cookie
* All this talk about puffle treats...
* ...is making me hungry for more puffle os!
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>(Playzone) Puffle Feeding</big></center>
<div class="mw-collapsible-content">
* Here take this treat!
* throws
* nice catch!
* hahaha
* TASTY!
* puffle o's are DELICIOUS!
* want another?
* here ya go!
* good right?
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>(Playzone) Roleplay</big></center>
<div class="mw-collapsible-content">
* ya look familiar
* do i know you from the wilds?
* blows whistle
* ok!
* let's have a race
* line up here puffles!
* let's cool off in the pool
* make sure not to run around the pool...
* ...I mean...
* ...don't bounce too fast
* cannonball!
* let's do some laps!
* alright let's tackle this rock wall
* it looks hard but it just takes practice
* take it one rock at a time
* don't look down!
* well done!
* you climbed up really fast
* you've definitely got the hang of it
* have ya ever tried Puffle O'berry coffee
* it's fantastic
* perks you right up
* who needs a new hair-do?
* let me style your hair!
* washes and conditions
* brushes hair
* makes mohawk
* makes braids
* makes dreadlocks
* colors red
* colors black
* colors hot pink
* adds bow
* adds bandana
* adds hat
* Ya look great!
* who's next?
* let's play PH says!
* when i say 'PH says'...
* ...you have to show me the emote i say
* BUT if i don't say PH says...
* ...don't do anything!
* PH says...
* ...show me hearts!
* ...show me coins!
* ...show me smiles!
* ...show me barf faces!
* ...show me cake!
* ...show me frowns!
* ...show me mad faces!
* ...show me moons!
* ...show me puffles!
* AHA! i didn't say 'PH says'!
* did i get ya?
* good job!
* i didn't fool ya!
* want to play again?
* let's play again
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>(Playzone) Training</big></center>
<div class="mw-collapsible-content">
* do ya know any tricks?
* let's see
* SIT!
* STAY!
* STAAAAAAAY!
* ROLL OVER!
* RUN REALLY FAST!
* good job mate!
* can ya do a trick for me?
* watches
* Crikey! What a trick!
* that deserves five puffle os!
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Training</big></center>
<div class="mw-collapsible-content">
* have ya taught your pet any tricks?
* what tricks does your puffle know?
* that's cool!
* wow you're a puffle whisperer!
* i've never seen that trick before!
* ok try teaching your puffle to stay
* watch...
* STAY!
* SIT!
* see?
* it worked
* hahaha
* now you try!
* what's your puffles favorite toy?
* what about games?
* your pet seems well behaved
* you're a great puffle handler!
* too right mate!
* what's your puffle's favorite food?
* yum!
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Puffle Trivia</big></center>
<div class="mw-collapsible-content">
* wanna play a game?
* i'll describe a kind of puffle...
* ...and you say what it is!
* ready?
* ok try to guess
* that's it!
* you're right!
* too right!
* nice try
* ooh close
* good guess mate!
* try again
* this puffle is majestic
* it farts sparkles
* it's from the Cloud Forest
* it's the rainbow puffle!
* this puffle loves inventing
* its favorite toy is a rocket
* we found it on the Wilderness Expedition
* it's the brown puffle!
* this puffle is very loyal
* everyone can adopt it
* its favorite toy is a bouncy ball
* it's the blue puffle!
* this puffle loves dancing
* Cadence has one named Lolz!
* its favorite toy is a disco ball
* it's the purple puffle!
* this puffle is smaller than the others
* but it's very powerful!
* it likes to ice skate
* it's the white puffle!
* this puffle is very creative
* its favorite game is DJ3K
* it likes to paint
* it's the yellow puffle!
* this puffle is silly
* its favorite game is Jet Pack Adventure
* it can ride a unicycle!
* it's the green puffle!
* this puffle is very adventurous
* Rockhopper says he discovered it
* its favorite game is Catchin Waves
* it's the red puffle!
* this puffle comes from the Box Dimension
* it likes to chew on everything!
* it's the only puffle with buck teeth
* it's the orange puffle!
* this puffle is intense
* its favorite game is Cart Surfer
* it LOVES hot sauce
* it's the black puffle!
* this puffle is sporty
* it's a great swimmer
* its favorite game is Aqua Grabber
* it's the pink puffle!
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Directions</big></center>
<div class="mw-collapsible-content">
* this way puffles!
* here puffles!
* this way everyone
* blows whistle
* follow me
* where do ya want to go?
* great idea
* to the Play Zone!
* to the Puffle Hotel!
* upstairs!
* downstairs!
* to the spa
* to the roof
* come on everyone
</div></div>
</div></div>

== Gallery ==
=== General ===
<gallery>
File:PHInGame2013.PNG|PH’s 2013 In-game avatar.
File:PHPlayerCardMar2013.PNG|PH’s 2013 player card.
File:Phsignature.png|PH’s signature.
File:PHGiveaway.PNG|PH’s latest giveaway [[PH Giveaway|background]].
File:PH Character.png|PH’s [[Puffle Party 2012|2012]] In-game avatar.
File:PH Character Dance.png|PH [[Actions|dancing]]
File:PH Character Wave.png|PH [[Actions|waving]]
File:PH Character Sit S.png|PH [[Actions|sitting]] South
File:PH Character Sit W.png|PH [[Actions|sitting]] West
File:PH Character Sit N.png|PH [[Actions|sitting]] North
File:PH Character Sit E.png|PH [[Actions|sitting]] East
File:Happy77 meets PH.jpg|[[Happy77]] meeting her at the [[Puffle Party 2012]].
File:PHPlayerCard.PNG|PH’s 2012 [[Player Card|player card]].
</gallery>

=== Artwork ===
<gallery>
File:PHPuffleHandler.png|PH’s original appearance. (Elite Penguin Force games)
File:Agent Puffle Handler News1.png|PH in 2011. (Club Penguin Times)
File:PuffleHandlerPuffleCard.PNG|PH from 2012 - without the pants. ([[Puffle Card]])
File:PHIssue385.png|PH in Club Penguin’s new art style. (Club Penguin Times <nowiki>#</nowiki> 385)
File:PHnewlook2.PNG|PH looking at plans for [[Puffle Launch]].
File:Agent PH.png|PH’s old look as she looks at the [[Puffle Card]].
File:PHhats.png|Agent PH being interviewed about the [[Puffle Hats]].
File:PHparade.png|PH at a [[Puffle Hats]] parade.
File:PHAsk.GIF|Ask PH.
File:PHStamp.PNG|PH stamp.
File:Puffle HandlerNote.png|PH planning [[Puffle Party 2012]].
File:PuffleHandler.png|PH carrying a crate for the [[Puffle Party 2012]].
File:PuffleHandler4.png|PH holding a [[Skateboard (Puffle Toy)|Skateboard]].
File:PHBuddyList.PNG|PH in the Buddy List if you search her.
File:PuffleHandler2.png
File:PuffleHandler3.png
File:PHIssue384.PNG|PH holding a blueprint.
File:PHinGiveaway.PNG|PH in her [[Puffle Party 2013]] [[PH Giveaway|Giveaway]].
</gallery>

== Trivia ==
* In the DS game [[Club Penguin: Elite Penguin Force|Elite Penguin Force]] she was [[Pink]] and had no hair but when she appeared online she was [[Brown]] and had hair.
** She changed her color to [[Brown]] after finding [[Brown Puffle]]s in the wilderness.{{fact}}
* PH is the third mascot to be meetable during a Puffle Party. The second being [[Cadence]] and the first being [[Rockhopper]].
** She is also the fourth meetable [[EPF Agent]], the first was [[Gary]], the second was [[Aunt Arctic]], and the third was [[Rookie]].
* The “Ask [[Aunt Arctic]]” section in the [[Club Penguin Times]] has been temporarily changed to have PH answer questions on multiple occasions.
:* It was first renamed in March 2012 to “Ask PH”, due to the then upcoming [[Puffle Party 2012]].
:* The second time in March 2013 to “Ask PH”, due to the then upcoming [[Puffle Party 2013]].
* With her [[penguin]] name only two characters long, PH is the first meetable [[penguin]] to have a name shorter than four characters.
* Her favorite food is [[Puffle-Os]].
* Her favorite game is [[Puffle Paddle]].
* She has an Australian accent, according to the [[Elite Penguin Force]] video game.
* '''PH''' stands for “Puffle Handler”, since that is her occupation.
* PH has changed or added to her outfit 4 times.
* According to Disney Australia, PH’s name is Paige.
* When penguins met her during her visits at the [[Puffle Party 2014]] her stamp earned message appeared green, saying that it was an easy stamp instead of a hard stamp.
* Midway through the [[Puffle Party 2014]], penguins could get her Gold Puffle Giveaway background from her player card on their friends list.
* She owns a orange puffle named Pete.

== Names in other languages ==
{{OtherLanguage
|portuguese = TP
|spanish = EP
|french = DP
|german = PH
|russian = ПВ
}}

{{PHSightings}}


== References ==
{{reflist}}


{{EPF}}
{{Characters}}

{{DEFAULTSORT:PH, Agent}}
[[Category:Elite Agents]]
[[Category:DS]]
[[Category:Penguins]]
[[Category:Characters]]
[[Category:Famous Penguins]]

[[pt:Treinadora de Puffles]]
