{{Conjecture}}
{{Archive}}
{{RoomInfobox
|title= Grey Maintenance Shed
|image= File:Grey Maintenance Shed.png
|open= October 2005
|closed= January 22, 2014
|place= Behind the [[Ski Hill]] (formely)
|games= None
}}
[[File:Hill.png|thumb|Ski Hill with the shed marked.]]


The '''Grey Maintenance Shed''' (or '''''Silver Lodge''''') can be seen at the lower left wooded area when looking at the [[Ski Hill]]. It is unknown what is inside, or why it is there. It is thought to be the [[Ski Lodge]], but physics and distance make this impossible, as they are on opposite sides (as seen at the lower left and upper right of the picture).

It is currently unknown whether it will be created into a room or if it will be kept there for decorative purposes.

It can't be the [[Ski Lodge]], because the Ski Lodge faces east of the [[Ski Hill]] yet this building faces north.

It was removed when [[Ski Hill]] was renovated on January 22, 2014.

There was a rumor recently saying that the shed was actually [[Rocket Snail]]'s workshop<ref>https://twitter.com/OfficialCPFacts/status/457644925358571520</ref>. However, it was denied by [[Screenhog]].<ref>https://twitter.com/_screenhog/status/457708688900251648</ref>

It was confirmed by [[Screenhog]] that the shed wasn't either the [[Ski Lodge]] or anything else. The Club Penguin Team has never come up with an explanation for that place.<ref>https://twitter.com/_screenhog/status/457708688900251648</ref>

==Trivia==
*During some parties there have been lights inside it, so it's possible that someone lives in there.
*There was a picture of it in ''[[Club Penguin: Elite Penguin Force: Herbert's Revenge]]'', just before you play Mission 5 (Spy and Seek). The Grey Maintenance Shed looks like it is being viewed through the [[Binoculars 3000]], and the mission is all about using them to watch the wilderness behind the [[Ski Lodge]], so this is further evidence it is in fact the Ski Lodge.
*The shed is definitely not the [[Ski Lodge]], because the Lodge's chimney has a square top and the grey shed has a pointy chimney and also the Ski Lodge has a window on the side and the Grey Maintenance Shed doesn't have it. 
*Another point that makes it impossible for the shed be the [[Ski Lodge]] is that during the [[Underwater Expedition]] the Ski Lodge was inside a dome and the [[Ski Village]] was completely flooded, but the shed was intact with no dome and no water around it<ref>http://www.clubpenguinwiki.info/static/images/www/0/07/UnderwaterExpeditionMtn.PNG</ref><ref>http://www.clubpenguinwiki.info/static/images/www/e/e0/UnderwaterExpeditionVillage.PNG</ref>.

==Source and References==
{{reflist}}
[[Category:Places]]
