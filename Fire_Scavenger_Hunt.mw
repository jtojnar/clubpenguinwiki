{{Archive}}
{{ScavengerInfobox
|Name= Sensei's Scavenger Hunt
|image= File:SenscavLIST.png
|imagesize= 200px
|when= September 14th - 28th 2009
|prize= [[Fire pin]]
|member= No
}}

After the [[The Fair 2009]] ended, a '''Fire Scavenger Hunt''' appeared, called Sensei's Scavenger Hunt. It is similar to Easter Egg Hunts, but you mainly need to find items related to fire. It ended on September 28, 2009. Inside the [[Ninja Hideout]] was a construction. In the [[Dojo Courtyard]], there's a big sign saying "Prepare for a Ninja Journey", and you can see a conveyor belt with fiery items on it.<br>

==Scavenger Hunt Items==
{{Spoiler}}
The items could be found in these places:

1. Clue: Your Hunt Starts in a skiing place, where this fire helps to warm your face. <br />
Answer: [[Ski Lodge]] (simply click the log in the fireplace).

2. Clue: In a place where books delight, this flame is a reading light. <br />
Answer: [[Book Room]] (click on the left candle (because it is the only candle on fire).

3. Clue: Look for a grumpy puffle's flare, and find yourself a sizzling hair. <br />
Answer: [[Pet Shop]] (click the black [[puffle]] in the cage once and then again to get the hair that is on fire).

4. Clue: Underground you'll need a light, this lamp makes a dark place light. <br />
Answer: [[Mine]] (click on the lamp in the top left corner).

[[File:Fire Pin.PNG|thumb|120px|The prize: Fire Pin.]]

5. Clue: By an oven is where you'll find, a fiery sauce to blow your mind! <br />
Answer: [[Pizza Parlor]] (on the pizza oven).

6. Clue: By the sea you'll find a flame, not far from a [[surfing]] game! <br />
Answer: [[Cove]] (in the camp fire). 

7. Clue: You'll find this gadget way up high, burning fuel will help you fly! <br />
Answer: [[Beacon]] (you will find the [[Jet Pack]] used in [[Jet Pack Adventure]]. Click on it to make it hover for a second, then click it again to collect it).

8. Clue: One last clue and then you're done, find a lantern, the left one! <br />
Answer: [[Dojo Courtyard]], it is (as the clue suggests) the left hand lantern.

Once you have completed the scavenger hunt, you will receive the [[Fire Pin]], which is in the shape of the fire symbol used in the [[Card-Jitsu]] game. You also get a message saying "You found them all! You're very wise, enjoy the reward when you click "Claim Prize"."
{{Spoiler2}}

==Glitches==

When the list of items was loading, it would say "Loading easter_hunt". That happened again at the [[Water Scavenger Hunt]].



==Trivia==
*During the scavenger hunt, the whole [[Club Penguin]] island is turned to sunset and all the buildings have turned on their lights.
*Sensei sometimes made appearances in the [[Ninja Hideout]].
*It was the 2nd Scavenger hunt that Sensei had organised in 2009. The first was the [[Easter Egg Hunt 2009]].
*[[Gary]] hinted the coloring is caused by smoke, later it was revealed that a [[Volcano]] was causing this.
*It was the first step in preparing [[Card-Jitsu Fire]] and in preparing the [[Volcano]].

==Gallery==
<gallery>
File:New ninja hideout.jpg|The Ninja Hideout during the hunt.
File:SenscavICON.png|This icon appeared in the upper right corner of the screen. When clicked, it brought up your list of items. This is also the prize (the [[Fire Pin]])
File:SenscavLIST.png|The item list before all of the items were collected. 
File:FSH2009-Beach.PNG|The [[Beach]]
File:FSH2009-Beacon.PNG|The [[Beacon]]
File:FSH2009-Cave.PNG|The [[Cave]]
File:FSH2009-Coffee.PNG|The [[Coffee Shop]]
File:FSH2009-Cove.PNG|The [[Cove]]
File:FSH2009-Dojo.PNG|The [[Dojo]]
File:FSH2009-Courtyard.PNG|The [[Dojo Courtyard]]
File:FSH2009-Dock.PNG|The [[Dock]]
File:FSH2009-Forest.PNG|The [[Forest]]
File:FSH2009-Shop.PNG|The [[Gift Shop]]
File:FSH2009-HQ.PNG|The [[HQ]]
File:FSH2009-Berg.PNG|The [[Iceberg]]
File:FSH2009-Attic.PNG|The [[Lodge Attic]]
File:SenscavHIDEOUT.png|The [[Ninja Hideout]] (with the [[Ninja Construction]])
File:FSH2009-Shack.PNG|The [[Mine Shack]]
File:FSH2009-Pet.PNG|The [[Pet Shop]]
File:FSH2009-Pizza.PNG|The [[Pizza Parlor]]
File:FSH2009-Plaza.PNG|The [[Plaza]]
File:FSH2009-SkiLodge.PNG|The [[Ski Lodge]]
File:FSH2009-Hill.PNG|The [[Ski Hill]]
File:FSH2009-Village.PNG|The [[Ski Village]]
File:FSH2009-Forts.PNG|The [[Snow Forts]]
File:FSH2009-SoccerPitch.PNG|The [[Stadium|Soccer Pitch]]
File:FSH2009-Sport.PNG|The [[Sport Shop]]
File:FSH2009-Town.PNG|The [[Town]]
</gallery>

==SWF==
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/hunt.swf Fire Scavenger Hunt]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/fire_icon.swf Fire Scavenger Hunt Icon]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/town.swf Town]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/coffee.swf Coffee]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/shop.swf Gift Shop]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/forts.swf Snow Forts]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/rink.swf Soccer Pitch]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/cave.swf Cave]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/plaza.swf Plaza]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/pet.swf Pet Shop]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/pizza.swf Pizza Parlor]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/shack.swf Mine Shack]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/forest.swf Forest]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/cove.swf Cove]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/berg.swf Iceberg]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/dock.swf Dock]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/beach.swf Beach]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/beacon.swf Beacon]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/village.swf Ski Village]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/mtn.swf Ski Hill]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/lodge.swf Ski Lodge]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/attic.swf Lodge Attic]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/sport.swf Sport Shop]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/agent.swf HQ]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/dojoext.swf Dojo Courtyard]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/dojo.swf Dojo]
*[http://archives.clubpenguinwiki.info/swf.cpcheats.info/rooms/2009/senseis_scavenger_hunt_september_2009/dojohide.swf Ninja Hideout]

{{NinjaInfo}}
{{Event}}
{{Hunt}}

[[Category:Ninjas]]
[[Category:Scavenger Hunts]]
