{{MinigameInfobox
|name = Cart Surfer
|image = File:CartSurfer Logo.PNG
|caption = The game's logo.
|players = 1
|room = [[Mine]]
|date = June 6, 2006
|stamps = Yes (12)
}}

'''''Cart Surfer''''' is a ''[[Club Penguin]]'' mini-game that can be played in the [[Mine]]. In Cart Surfer, you do various tricks and stunts to earn points, while riding a [[Mine Cart|mine cart]] on a track. When the player finishes the game, they are directed to the [[Mine Shack]]. With the right combination of moves, Cart Surfer can be one of the fastest ways to earn coins in Club Penguin. In the ''[[Club Penguin: Elite Penguin Force]]'' version, you have to jump over rocks and duck to avoid obstacles. In a game where you don't crash, there will be 8 corners. In this game, 'surfing' means to stand up and ride the cart.

== History ==
{{Expansion}}
Cart Surfer was originally known as Cave Runner or Egyptian Cart Surfer. Cave Runner required you to ride around in a mine cart collecting coins. The game was to be played in top-view (eagle eye).<ref>{{ArchivedURL|https://twitter.com/_screenhog/status/282895066828263424|archivedate=20150721140413}}</ref> This version was scrapped when the idea to make Cart Surfer came.{{fact}} The music that was composed to accompany the game can be found <span class="plainlinks">[http://media1.clubpenguin.com/play/v2/content/global/music/104.swf here]</span>.

Cart Surfer was released in June 6, 2006. [[Stamp]]s were released for Cart Surfer on October 4, 2010. Along with stamps, [[Black Puffle]]s could also play with you.

== ''[[Club Penguin: Elite Penguin Force]]'' version ==
[[File:Cart surfer ds.jpg|thumb|right|150px|Cart Surfer being played on a DS.]]
'''Cart Surfer''' in ''[[Club Penguin: Elite Penguin Force]]'' is similar to the web version of '''Cart Surfer'''. You can play either by using the D-Pad or the stylus. Unlike the web version, there are two modes, Normal Mode and Extreme Mode. Normal Mode is the most similar to the web version with the classic obstacles. Extreme Mode introduces new obstacles such as fallen rocks, vertical tunnels and wooden planks. You cannot play Extreme Mode until you've built your own Mine Cart in Mission 6 to explore the mines.

There is a side-mission where you meet a [[pink]] [[penguin]] who is scared to play extreme mode. To convince the penguin that it's safe, you have to survive Extreme Mode.

== Tricks ==
{{spoiler}}
The first time a trick is performed, the player receives a certain number of points. All the times that the same trick is performed in a row, only half the number of points is awarded. The only exception to this is the Rail Grind. You can take turns with the Rail Grind and the Surf as well as leaning.

{|class="wikitable"
!class="color1"|Stunt
!class="color1"|Score
!class="color1"|Keys
|-
!Turn
|10 (turn)||[right arrow] or [left arrow]
|-
!Surf Turn
|15-60||[right arrow] or [left arrow] while surfing
|-
!Rail Grind
|20-90||[down arrow] and then [right arrow] or [left arrow]
|-
!Air Twist
|80 - 40||[space] and then [right arrow] or [left arrow]
|-
!Back Flip
|100 or 50||[down arrow], wait 1 second and then [space]
|-
!Free-as-a-Bird
|50 or 25||[space] and then [up arrow]
|-
!Surf
|no score||[up arrow], to stop surfing, [down arrow] to get down
|-
!Surf Jump
|20 or 10||[space] (while surfing)
|-
!Hand stand
|80 or 40||[up arrow], [up arrow]
|-
!Run on Rails
|80 - 40||[down arrow], [down arrow]
|-
!Slam
|30 - 15||[space], [down arrow]
|-
!Jump
|no score||[space]
|}
{{spoiler2}}
{{Tip|Alternate the Back Flip and any of the 80 scoring tricks to earn more coins. Also use Rail Grind for the turns.}}

== Coin Earning Tips ==
The most efficient way to get coins on ''Cart Surfer'' is to alternate the Back flip and Air Twist, Hand Stand or Run on Rails: a 100 scoring stunt and any of the 80 scoring ones. Rail Grind is the highest scoring stunt for the turns. It also helps to crash a few times near the end, which gives the player more time to do tricks.
When you lose a life you get the chance to get in about 2-3 moves in before the next turn so use your lives well and you can get an extra 540-840 points in cart surfer.

An efficient player can earn approximately 500 coins a game. If an efficient player ''also'' has every stamp for Cart Surfer, they can earn 1,000 coins.

{{Tip|Crash in 2000, 3000 and 4000 points to have more time.}}

== Stamps ==
==== Easy ====
{|class="wikitable" 
!Name !! Description
|-
|[[Great Balance stamp]]|| Recover from a wobble.
|}

==== Medium ====
{|class="wikitable" 
!Name !! Description
|-
|[[Cart Pro Stamp]]|| Earn 150 coins in one game.
|-
|[[Mine Marvel stamp]]|| Perform 10 tricks in the cart.
|-
|[[Mine Mission stamp]]|| Perform 10 tricks out of the cart.
|-
|[[Trick Master stamp]]|| Perform 14 different tricks.
|-
|[[Puffle Power stamp]]|| Recover from a wobble with a puffle.
|}

==== Hard ====
{|class="wikitable" 
!Name !! Description
|-
|[[Cart Expert stamp]]|| Earn 250 coins in one game.
|-
|[[Mine Grind stamp]]|| Grind around 8 corners.
|-
|[[Surf's Up stamp]]|| Surf around 8 corners.
|-
|[[Flip Mania stamp]]|| Flip 20 times in a row without crashing.
|-
|[[Ultimate Duo stamp]]|| Perform 20 tricks with your puffle.
|}

==== Extreme ====
{|class="wikitable" 
!Name !! Description
|-
|[[Cart Master stamp]]|| Earn 350 coins in one game.
|}

== Gallery ==
<gallery>
File:Cart Steer.png|Original Start Screen.
File:CartSurfer StartScreen.PNG|The Start Screen.
File:Cart Surfer Gameplay.png|Gameplay.
</gallery>

== Names in Other Languages ==
{{OtherLanguage
|portuguese = Surfe na Caçamba
|spanish = Carrito Surfero
|french = Surf des Mines
|german = Karren-Skater
|russian = Виражи
}}

== See also ==
* [[Mine Cart]]

== Notes and references ==
{{reflist}}


{{Games}}

[[Category:Games]]

[[pt:Surfe na Caçamba]]
