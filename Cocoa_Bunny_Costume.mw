{{ItemInfobox
|name= Cocoa Bunny Costume
|image= File:CocoaBunnyCostume.png
|available= No (Catalog)
No (Unlockable)
|type= Body Item
|member= Yes
|party= None
|cost= 550 coins
|found= [[Penguin Style]], [[Treasure Book]](Formerly)
|id= 782 (catalog)<br> 10782 (unlockable) 
|unlock= Yes (Series 7 and 12)
}}

The '''Cocoa Bunny Costume''' is a body item in ''[[Club Penguin]]''. Members were able to buy this item for 550 [[coins]] in the ''[[Penguin Style]]'' catalog. It goes with the [[Cocoa Bunny Ears]]. It is common around [[Easter Egg Hunt|Easter]]. Similar items are the [[White Cocoa Bunny Costume]] and [[Dark Cocoa Bunny Costume]].  

==History==
It was a secret item in the April 2010 ''Penguin Style''. It was also secret in the April–August 2011 ''Penguin Style'' catalogs. It was also a Secret item in the ''Treasure Book'' (Series 12). 
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan=5|Penguin Style||March 7, 2008||June 6, 2008
|-
|April 3, 2009||July 3, 2009
|-
|March 31, 2010||May 6, 2010
|-
|April 1, 2011||August 4, 2011
|-
|March 6, 2013||May 8, 2013
|-
|[[Treasure Book (Series 7)]]||March 1, 2010||October 4, 2011
|-
|[[Treasure Book (Series 12)]]||February 7, 2011||October 4, 2011
|}

==Appearances==
*On the [[Easter postcard]].

==Gallery==
<gallery>
File:CocoaBunnyCostume2.png|The Cocoa Bunny Costume in-game.
File:CocoaBunnyCostume1.png|The Cocoa Bunny Costume on a player card.
File:Bow-of-cocoa-bunny-ears-for-viking-helmet.jpg|Before this item was a secret item, you could get the Viking helmet by clicking on the bow.
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Fantasia de Coelhinho de Chocolate
|spanish= Disfraz de conejito de chocolate
|french= Le Costume de Lapin en Chocolat
|german= Schokohasen-Kostüm
|russian= Костюм шоколадного зайца
}}

==SWF==
===782===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/782.swf Cocoa Bunny Costume (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/782.swf Cocoa Bunny Costume (sprite)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/782.swf Cocoa Bunny Costume (paper)]
===10782===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10782.swf Cocoa Bunny Costume (unlockable icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10782.swf Cocoa Bunny Costume (unlockable sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10782.swf Cocoa Bunny Costume (unlockable paper)]

[[Category:Clothing]]
[[Category:Body Items]]
[[Category:Treasure Book Items]]
[[Category:Secret Items]]
[[Category:Clothes released in 2008]]
