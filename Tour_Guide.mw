[[File:CPWN_tour.png|right|thumb|92px|A [[penguin]] standing at the tour booth.]]
[[File:Example_network.png|A [[penguin]] holding the "Tours Here" sign.|thumb|80px|left]]
:''Were you looking for [[Tour Guide?|a comic with a similar name]]?''

The '''Tour Guide''' project began on January 26, 2007, with the intent of helping newcomers find their way around ''[[Club Penguin]]''. To become a tour guide, a [[penguin]] must be at least 45 days old and have had no more than one [[ban]] (or sufficiently good behavior after a ban). Being a [[member]] is not required.

To apply, one goes to the [[Tour Booth]] in the [[Ski Village]] and takes a quiz. The eight-question quiz tests one's knowledge such as to where things are located, [[puffle]] facts, game play, and so forth. Answering seven out of the eight questions correctly wins a Tour Guide hat, which can be found in one's [[clothing]] inventory. Only those who have passed the test and have the hat are official tour guides. Tour Guides get [[Salary|paid]] 250 coins per month for doing their duties.

When a penguin only wears the Tour Guide hat and waves, a "tours here" sign appears. Another option while wearing the hat (with or without other items) is giving pre-written messages about the current room. To do this, one clicks the Messages icon, selects Activities, then clicks "Give a Tour". Messages about the room will then appear. 

==Tour Guide Tips==
These tips came from the [[Penguin Times]] newspaper:

*Stop your tour group from getting lost by sending them a [[post card]]. It is an easy way to invite [[penguins]] to rooms you want them to visit, without them going missing on the way there.

*Try having a tour planned out ahead of time, so you know where you are going next!

*Make sure not to move too quickly for your tour group. Give them a chance to see everything and then let them follow you to the next room.

*The sign is your friend! Use your tour guide sign to show other [[penguins]] you are ready to show them around.

*Use phrases from the tour guide chat menu to help you describe a room. Just click the 'Message' button on the chat bar, highlight 'Activities', and press 'Give a tour'.

*Giving a tour is a great way to make penguins feel welcome. Why not offer to be your tour team's [[Club Penguin]] guru? Add them to your [[Buddy List]] so whenever they have a question, they can come to you!

*Why not plan your tour around a theme? You could take your group on a mystery tour of areas of [[Club Penguin]] they might not know about like the [[Underground]] or the [[Dojo]]. You could teach them some cool facts about [[Club Penguin History|''Club Penguin''<nowiki>'</nowiki>s history]] too!

*Showing your tour group an activity they can join in with is a great way to end a tour. Why not start a play, a band rehearsal or even a party?

*Make sure you stand out from the crowd. Sure, you are wearing the tour guide hat. But what about the rest of you? Why not offer some fashion advice and help new [[penguin]]s pick cool [[clothes]]?

*Why not take your [[puffle]] on your tour too? They make a great talking point. You can give to advise your tour on how to look after their puffles and tell them what your puffles like to do.

*You could take other [[penguin]]s on a tip swapping sports tour. Why not take penguins to the Cove and give them ideas on how to improve their surfing?

*If you are a tour guide, you should try taking other [[penguin]]s' tours. They will probably give you a different look at the places you know well.
*Like [[Secret Agent]]s, they get paid 250 coins per month.

==Tour Guide Question Answers==
{{Spoiler}}
----
Before becoming a Tour Guide, you must take a quiz. There are 8 questions in the quiz and answering at least 7 out of the 8 correctly will earn a Tour Guide Hat. You must own one of these hats to be an official Tour Guide. To help you answer the quiz, here are the possible questions you may be asked with the answers:

[[Image:Snap32.png|thumb|278px|The window that appears when you pass the Tour Guide Test.]]
[[Image:Snap33.png|thumb|278px|The postcard that you receive when you pass the Tour Guide Test.]]

*How many [[Sled Racing]] courses are there?
**4

*How does the pink [[puffle]] play?
**Skips with skipping rope

*What is the name of [[Mullet|the big fish]] in [[Ice Fishing]]?
**[[Mullet]]

*What day does the [[newspaper]] come out?
**Thursday

*Which of these games has a [[shark]] in it?
**[[Ice Fishing]], [[Jet Pack Adventure]]

*What color of [[puffle]] can catch on fire?
**Black

*How many [[coins]] does it cost to buy a [[player card]] [[background]]?
**60

*What is thrown out of the truck in level 4 of [[bean counters]]?
**A Flowerpot

*In what room can you find old copies of [[The Penguin Times]]?
**The [[Boiler Room]]

*What is the name of Captain [[Rockhopper]]’s ship?
**The [[Migrator]]

*Which of these rooms does not have a game in it?
**[[Beach]]

*Which room has a [[Fred the Clockwork Cuckoo|cuckoo clock]]?
**[[Ski Lodge]]

*How do you get a [[pin]]?
**Walk on top of it

*Which of these places doesn’t have music playing in the background?
**[[Pet Shop]]

'''(Questions do not always come in this order.)'''

----
{{Spoiler2}}
----

==Appearances in-game==
*A tour guide is featured as a power card in [[Card-Jitsu]].
*In [[Club Penguin: Elite Penguin Force]], a tour guide actually gets lost in [[The Wilderness]] while he is giving a tour but he is eventually rescued.
*There is a Tour Guide on the [[Club Penguin]] homepage.

==Gallery==
===Tour Guides look===
<gallery>
File:Tour_bg_network.png|A typical tour guide look in a player card.
File:Tour_in-game.png|A typical tour guide look in-game.
</gallery>

===Others===
<gallery>
File:Tour_giving_Network11.png|A penguin guiding a tour.
File:OldTourBooth.PNG|The old Tour Booth.
File:TourBooth2012.PNG|The new Tour Booth.
Image:Do_you_want_ot_be_a_tour_guide_textbox.jpg|A notice in the [[Club Penguin Times|newspaper]].
File:Jetpack2.png|A tour guide and another [[Penguin]] with a [[Jet Pack]].
File:TourGuideNews126.PNG|A tour guide, as seen in Issue #126.
File:FoS-Plaza.jpg|The tour guide booth at the [[Plaza]] in 2007 before it was moved at the [[Ski Village]].
</gallery>

==Trivia==
*Most Tour Guides only signed up to get the hat and the money. This is referenced in one comic on the comics section on the Club Penguin website, where a tour guide gives a very unsuccessful tour and admits that he only signed up for the hat.
*The Tour Guide Booth used to be in the [[Plaza]] before the [[Forest]] and [[Cove]] were found and opened. Now it is located in the [[Ski Village]].
*[[Happy77]] has a penguin named Tour Guide.

==See also==
*[[Club Penguin]]
*[[Tour Booth]]
*[[How to be a Great Tour Guide]]
*[[Tour Guide Hat]]

<!--Roles template removed-->

[[Category:Types of Penguins]]
[[Category:Penguins]]
