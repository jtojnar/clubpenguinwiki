These are all the scavenger hunts in the history of ''[[Club Penguin]]''.
{{Spoiler}}


==Easter Egg Hunt 2006==
*Egg 1: [[Pet Shop]]
*Egg 2: [[Beach]]
*Egg 3: [[Book Room]]
*Egg 4: [[Ice Rink]]
*Egg 5: [[Snow Forts]]
*Egg 6: [[Iceberg]]
*Egg 7: Currently Unknown

Prize: [[Pink Bunny Ears]]

==Instrument Hunt==
*Drum: [[Pizza Parlor]] - Holding up the table on the left.
*Cymbal: [[Dock]] - In the inner tubes,
*Guitar: [[Ski Village]] - Riding on the [[Ski Lift]].
*Piano: [[Pet Shop]] - Hidden behind a puffle house.
*Cello: [[Ski Lodge]] - Under the orange couch cushions.
*Cymbal 2: [[Ski Hill]] - Behind the pole.
*Banjo: [[Cave]] - The banjo was floating by in the window.

Prize: [[Band Background]]

==Lightbulb Hunt==
*Lightbulb 1: [[Mine]]
*Lightbulb 2: [[Ski Lodge]]
*Lightbulb 3: [[Plaza]]
*Lightbulb 4: [[Night Club]]
*Lightbulb 5: [[Boiler Room]]

Prize: [[Lighthouse Background]]

==Easter Egg Hunt 2007==
*Egg 1: [[Lighthouse]] - In the picture frame replacing the lighthouse.
*Egg 2: [[Coffee Shop]] - Open up the Coffee Maker and an egg will jump out.
*Egg 3: [[Mine Shack]] - An egg will pop out the pipe and roll down the rain trough into the bin of water.
*Egg 4: [[Pizza Parlor]] - In the spotlight.
*Egg 5: [[Cave]] - Egg floats through a window.
*Egg 6: [[Town]] - Jumps out of the present on the sign for the Gift Shop.
*Egg 7: [[Boiler Room]] - The drawer next to the old news opens up and an egg pops out
*Egg 8: [[Beacon]] - Look through the telescope and an egg will be floating.

Prize: [[Blue Bunny Ears]]

==Halloween Scavenger Hunt 2007==
''Before starting this scavenger hunt you needed the [[Pumpkin Basket]].''
*Piece of Candy 1: Pizza Parlor - Flew out of the pipe organ on the stage.
*Piece of Candy 2: Coffee Shop - Under the "candy bowl" in the Coffee Shop.
*Piece of Candy 3: Forest - Hidden in one of the trees.
*Piece of Candy 4: Beacon - In the life ring hanging off the side.
*Piece of Candy 5: Ski Lodge - Falls out of the eye in [[Mullet]].
*Piece of Candy 6: Dance Club - You need to answer a riddle from [[The Keeper of the Boiler Room|The Keeper]] and if you answer correct and candy apple will come out of the apple bobbing barrel.
*Piece of Candy 7: Ski Hill - Behind the scarecrow.
*Piece of Candy 8: Cove - Look through the binoculars and an octopus will float by wearing a [[Clown Wig]] and holding a lollipop.

Prize:[[Halloween Scarf]]

==Paper Boat Scavenger Hunt 2008==
:''See [[Paper Boat Scavenger Hunt (2008)|main article]].''

*Boat 1: Mine Shack - Floating in the bin of water at the bottom of the rain trough.
*Boat 2: Cove - Floating in front of the rope in the water.
*Boat 3: Coffee Shop - Floating in the green bowl where cookies usually are.
*Boat 4: Beach - Floating behind the rocks.
*Boat 5: Cave: - Seen floating in the windows where the fish would normally swim.
*Boat 6: Pet Shop - Floating in the water bowl.
*Boat 7: Dock - Under the dock.
*Boat 8: Iceberg - Floating next to the [[Aqua Grabber]].

Prize: [[Blueprints Background]]

==Easter Egg Hunt 2008==
*Egg 1: [[Mine]] - This egg was replacing the flame in the lantern.
*Egg 2: [[Dock]] - Under the snow in the dock.
*Egg 3: [[Pet Shop]] - The egg is in the cage next to the toys.
*Egg 4: [[Book Room]] - This egg was replacing the cactus.
*Egg 5: [[Gift Shop]] - The green penguin in the ''Dress Up!'' was replaced by an egg.
*Egg 6: [[Plaza]] - This egg was where one of the lanterns were at the stage.
*Egg 7: [[Lodge Attic]] - The egg pops out out the box.
*Egg 8: [[Dojo]] - The final egg was a ninja egg that would teleport around the dojo. This led many more penguins to believe in [[ninjas]].

Prize: [[Green Bunny Ears]]

==Halloween Scavenger Hunt 2008==
*Candy 1: Snow Forts - The Blue flag went down and was replaced with a chocolate bar.
*Candy 2: Dance Lounge - Turn on the lamp and a piece of candy corn will jump out.
*Candy 3: Lodge Attic - Jumps out of the blue box next to the crates and suitcases.
*Candy 4: Plaza - Candy in wrapper floats to the top of the cauldron.
*Candy 5: Cove - Candy will leap out of danger sign and on top of the surf hut.
*Candy 6: Iceberg - Click a star and a constellation will appear which will form into a candy.
*Candy 7: Beacon - In the pumpkin's eye a candy apple will swing like a pendulum.
*Candy 8: Book Room - ''Unknown Where''

==Easter Egg Hunt 2009==
*Egg 1: [[Town]] - In the Night Club Spotlight.
*Egg 2: [[Cove]] - Floating next to the rocks.
*Egg 3: [[Mine]] - Comes by in a mine cart.
*Egg 4: [[Dojo Courtyard]] - A lantern replacement.
*Egg 5: [[Gift Shop]] - Under a hat.
*Egg 6: [[Ski Lodge]] - Appears with fish in cooler.
*Egg 7: [[Mountain]] - Jumps out of pole in middle.
*Egg 8: [[Beacon]] - Turn off the light and the egg will be visible.

Prize: [[Pink Bunny Ears]]

==Halloween Candy Hunt 2009==
*Candy Bar: Lodge Attic - Jumped out from behind a jack-o-lantern.
*Triangular Candy: Dock - On one of the pumpkins.
*Lollipop: Pet Shop - Lift up a chandellier  to reveal this item.
*Oval Wrapped Candy: Cove - Inside the book the ghost is holding on the sign.
*Pumpkin Head: Cave - Disguised as a pumpkin hanging from a string.
*Box: Soccer Pitch - In one of the lights.
*Candy on a stick: Forest - Click on the bushes where a [[Yellow Puffle]] would appear 2 years ago and a candy apple will jump out.
*Rectangular wrapped candy: Mine - Floating in a pool of slime.
Prize: [[Jack O' Lanterns Background]]

==Fire Scavenger Hunt==
''See Main Article, [[Fire Scavenger Hunt]].''
*Fiery Item 1: Ski Lodge - Log inside the fireplace.
*Fiery Item 2: Book Room - Candle on the bookshelf.
*Fiery Item 3: Pet Shop - Black Puffle's hair.
*Fiery Item 4: Mine - Lantern.
*Fiery Item 5: Pizza Parlor - Hot Sauce on top of oven.
*Fiery Item 6: Cove - Stick in the bonfire.
*Fiery Item 7: Beacon - Jet Pack.
*Fiery Item 8: Dojo Courtyard - On of the lanterns.
Prize: [[Fire Pin]]

==Recycle Hunt 2010==
''See Main Article, [[Recycle Hunt]].''
*Recyclable Item 1: Coffee Shop - Broken Tea Cup on top of the tree.
*Recyclable Item 2: Pet Shop - Fallen over box of puffle-o's.
*Recyclable Item 3: Cove - Cream soda barrel in the bottom left corner of the screen.
*Recyclable Item 4: Dojo Courtyard - Bottle of Hot Sauce on the roof of the Ninja Hideout.
*Recyclable Item 5: Book Room - Bag of coffee beans on top of shelf.
*Recyclable Item 6: Forest - Newspaper in the upper-area of the forest.
*Recyclable Item 7: Ski Village - Box of pizza on top of innertubes.
*Recyclable Item 8: Mine Shack/Community Garden - Barrel full of water next to the mine carts.
Prize: [[Recycle Pin]] (Allowed you access to the [[Recycling Plant]].)


==Paper Boat Hunt 2010==
:''See [[Paper Boat Scavenger Hunt|main article]].''
*Paper boat 1: Forest - Behind some bushes in the bottom left of the screen.
*Paper boat 2: Beach - On top of the sign on the lighthouse.
*Paper boat 3: Snow Forts - Behind some bushes in the upper left of the screen.
*Paper boat 4: Hidden Lake - On top of the sign that said mermaid cove.
*Paper boat 5: Plaza - On top of the puffle on the pet shop sign.
*Paper boat 6: Town - Next to the coffee shop in the wooden carved penguin.
*Paper boat 7: Ski Village - On top of the sign next to the ski lodge.
*Paper boat 8: Iceberg - On the palm tree.
Prize: [[Treasure Cove Background]] (Once obtained this item one could go to the cove and drill on the X to get the [[Pirate Bandanna]]

==Halloween Scavenger Hunt 2010==
*Piece of candy 1: Beach - Behind a tree.
*Piece of candy 2: Snow Forts - Behind a tree.
*Piece of candy 3: Iceberg - Behind the pumpkin.
*Piece of candy 4: Gift Shop - Replacing a manequin head displaying [[The Funster]].
*Piece of candy 5: Pizza Parlor - Disguised as a pumpkin decoration on the pipe organ.
*Piece of candy 6: Ski Lodge - In the mirror.
*Piece of candy 7: Plaza - In the cauldron.
*Piece of candy 8: Beacon - Replacing a bar on the railing.
Prize: [[Candy Forest Path Background]]


==Water Scavenger Hunt==
:''See [[Water Scavenger Hunt|main article]].''
*Water Item 1: Stadium - Soda at the snack shack.
*Water Item 2: Coffee Shop - Coffee cup on the table.
*Water Item 3: Pet Shop - Fish bowl.
*Water Item 4: Cove - Water bottle in surf hut.
*Water Item 5: Book Room - Coffee cup on the table next to the red chair.
*Water Item 6: Dance Lounge - Glass of Water on table.
*Water Item 7: Beach - Bucket in snow.
*Water Item 8: Dojo Courtyard - Gold fish statue on roof of Dojo.
Prize: [[Water Tank Background]]

==Silly Scavenger Hunt ''(Members Only)''==
''See the [[Silly Scavenger Hunt|main article]].''

*Box Piece 1: Box Dimension - In the middle of the room.
*Box Piece 2: Desert Dimension - Go to the right of the screen to reveal the piece of cardboard in the mountains.
*Box Piece 3: Doodle Dimension - Use the pencil to draw the piece of cardboard.
*Box Piece 4: Space Dimension - Connect the stars to make a constellation which turns into a stapler.
*Box Piece 5: A Silly Place - Sitting on the spring cushion where the [[King Jester Hat]] was in 2010.
*Box Piece 6: Stair Dimension - Replacing a floorboard on the ceiling (If turned right side up floor).
*Box Piece 7: Cream Soda Dimension - Get through the Teleporting barrels and get the tape out of the cage.
*Box Piece 8: Candy Dimension - Click on the spoon on top of the candy shop. It will lift up and the ice cream will disappear. Then it will dive back in and scoop out a box.
Prize: [[Box Costume]]

==Easter Egg Hunt 2011==
*Egg 1: Dojo - behind the bonsai tree.
*Egg 2: Dock - floating in the water.
*Egg 3: Forest - behind the rock in the bottom left of the screen.
*Egg 4: Cave - behind the lifeguard chair.
*Egg 5: Mine Shack - in the tree.
*Egg 6: Lodge Attic - on the box between the red and blue couch.
*Egg 7: Ski Hill - next to the tobaggon holding the Game UPgrades catalog.
*Egg 8: Snow Forts - behind a snowy rock.
Prize: [[Safari Park Background]]

==Easter Egg Hunt 2012==

''See [[Easter Egg Hunt 2012|main article]].''

*Egg 1: Underwater Lake - in the Aqua Grabber
*Egg 2: Book Room - on the lamp
*Egg 3: Beach - beside the left arrow
*Egg 4: Mine Shack - on the snow-ball generator
*Egg 5: Cove - on the lodge
*Egg 6: Gift Shop - beside the face mannequins
*Egg 7: Ski Village - in the ski lift
*Egg 8: Beacon - near the big lamp
Prize: [[Yellow Bunny Ears]]

==Cream Soda Scavenger Hunt==

''See [[Cream Soda Scavenger Hunt|main article]].''

*Note 1: Iceberg
*Note 2: Dock
*Note 3: Tree Forts
*Note 4: Cove
Prize: [[The Continental]] 

==Candy Ghost Scavenger Hunt==

''See [[Candy Ghost Scavenger Hunt|main article]].''

*Candy Ghost 1: Cove

*Candy Ghost 2: Haunted House Entrance

*Candy Ghost 3: Mine Shack

*Candy Ghost 4: Ski Village

*Candy Ghost 5: Ski Hill

*Candy Ghost 6: Forest

*Candy Ghost 7: Beach

*Candy Ghost 8: Dock

Prize: [[Candy Ghost BG]]

[[Category:Events]]
[[Category:Parties]]
