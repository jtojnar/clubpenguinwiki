{{CatalogInfobox
|name = Snow & Sports
|image = File:SnowandSportsDec2014.PNG
|imagesize = 200px
|caption = The [[December 2014 Snow & Sports|current edition]] of Snow and Sports.
|date = August 31, 2007
|location = [[Sport Shop]] (former)<br>[[Ski Village]] (May 25–27, 2010)<br>[[Stadium]]<br>[[Skatepark]]
|sell = Sport Clothing, Backgrounds and Furniture.
|available = Yes
|updates = Semiannually
}}

'''Snow & Sports''' (formerly '''Snow and Sports''') is the name of the sports catalog in ''[[Club Penguin]]''. It can currently be found in the [[Stadium]] or in the [[Skatepark]], but was formerly located in the [[Sports Shop]] before the [[Popcorn Explosion]]. 

== History ==
{{Outdated}}
The [[Sports Shop]] was created on November 3, 2005 and contained a ''[[Penguin Style]]'' catalog to help contain the overflow of penguins in the [[Gift Shop]] when new items came out. However, the shop was given a redesign and a new catalog which was released on August 31, 2007 so penguins could actually buy ''sports'' items in the ''sports'' shop; needless to say the new catalog was the [[September 2007 Snow and Sports|first ever edition]] of ''Snow and Sports''. 

On May 25, the catalog was moved into the [[Ski Village]] temporarily due to the [[Popcorn Explosion]]. After the Sport Shop was officially closed on May 27, the catalog was moved to the newly named [[Stadium]], where it is still located today. 

When the [[June 2012 Snow & Sports|June 2012 version]] of the catalog was released, it received a minor name change along with a new title font (the word “and” was changed to the symbol “&”) to go along with all of the other catalogs receiving updates. 

== Items ==
=== Furniture ===
There is furniture like basketball hoops, scoreboards, and other gym and arena-like things for your [[igloo]].

=== Backgrounds ===
There are usually about three backgrounds to choose from each time a new catalog comes out.

=== Former Items ===
Both [[Wakeboard]]s, all three [[Surfboard]]s and the [[Flashing Lure Fishing Rod]] were located in the Snow and Sports catalog before the smaller [[Game Upgrades]] catalogs were released. 

== Gallery ==
<gallery captionalign="left">
File:Penguinsport.PNG|link=September 2007 Snow and Sports|[[September 2007 Snow and Sports|September 2007 edition]]
File:snowandsportsjune2008.png|link=June 2008 Snow and Sports|[[June 2008 Snow and Sports|June 2008 edition]]
File:snowsport-nov-08.png|link=November 2008 Snow and Sports|[[November 2008 Snow and Sports|November 2008 edition]]
File:Snow-n-sport-jan-09.png|link=January 2009 Snow and Sports|[[January 2009 Snow and Sports|January 2009 edition]]
File:Snow-and-sports-aug-nov.png|link=August 2009 Snow and Sports|[[August 2009 Snow and Sports|August–November 2009 edition]]
File:Snow-and-sports-feb-10.png|link=February 2010 Snow and Sports|[[February 2010 Snow and Sports|February 2010 edition]]
File:SnowandSportsMay2010.PNG|link=May 2010 Snow and Sports|[[May 2010 Snow and Sports|May 2010 edition]]
File:SeptemberCatalog.png|link=September 2010 Snow and Sports|[[September 2010 Snow and Sports|September 2010 edition]]
File:SportsDecember2010.PNG|link=December 2010 Snow and Sports|[[December 2010 Snow and Sports|December 2010 edition]]
File:SportsJanuary2011.PNG|link=January 2011 Snow and Sports|[[January 2011 Snow and Sports|January 2011 edition]]
File:SnowandSportsMay2011.PNG|link=May 2011 Snow and Sports|[[May 2011 Snow and Sports|May 2011 edition]]
File:SnowAndSportsDec2011.PNG|link=December 2011 Snow and Sports|[[December 2011 Snow and Sports|December 2011 edition]]
File:SnowandSportsJun2012.PNG|link=June 2012 Snow & Sports|[[June 2012 Snow & Sports|June 2012 edition]]
File:SnowandSportsDec2012.PNG|link=December 2012 Snow & Sports|[[December 2012 Snow & Sports|December 2012 edition]]
File:SnowandSportsMar2013.PNG|link=March 2013 Snow & Sports|[[March 2013 Snow & Sports|March 2013 edition]]
File:SnowandSportsDec2013.PNG|link=December 2013 Snow & Sports|[[December 2013 Snow & Sports|December 2013 edition]]
File:SnowandSportsJun2014.PNG|link=June 2014 Snow & Sports|[[June 2014 Snow & Sports|June 2014 edition]]
File:SnowandSportsSep2014.PNG|link=September 2014 Snow & Sports|[[September 2014 Snow & Sports|September 2014 edition]]
File:SnowandSportsDec2014.PNG|link=December 2014 Snow & Sports|[[December 2014 Snow & Sports|December 2014 edition]]
</gallery>

{{SWFArchives|Snow & Sports}}

* [http://media1.clubpenguin.com/play/v2/content/local/en/catalogues/sport.swf Current ''Snow & Sports'' catalog]

== Names in other languages ==
{{OtherLanguage
|portuguese = Esportes
|spanish = Deportes y nieve
|french = Neige et Sports
|german = Schnee und Sport
|russian = Снег и спорт
}}

== See also ==
* [[Secret Items]]
* [[Member]]
* [[Furniture]]
* [[Sports Shop]]
* [[Teams]]
* [[Game Upgrades]]

{{PrintedMedia}}

[[Category:Printed Media]]
[[Category:Catalogs]]

[[pt:Catálogo Esportes]]
