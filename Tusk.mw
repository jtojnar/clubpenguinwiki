{{CharacterInfobox
|name= Tusk
|image= File:CardJitsuSnow-Tusk.PNG
|imagesize= 250px
|fullname= Tusk
|species= Walrus
|position= Master of Snow
|appeared= [[Card-Jitsu Snow]]
|color= [[Brown]]
|clothes= '''Normal'''<br>Helmet, [[Tusk's Cloak|Cloak]], Suit, Arm Armor<br>'''Former'''<br>[[Card-Jitsu Belts]]
|related= Unknown
|friends= [[Scrap]], [[Tank]], [[Sly]], [[Sensei]] (formerly)
|walk= No
}}
<center>{{Cquote2|I devoted my focus to one goal: I would master snow, I would take revenge!|Tusk}}</center>
'''Tusk''' is a walrus and [[Sensei]]'s former best friend. Tusk seems to have a hatred for Sensei and his Ninjas because of his jealousy. You can battle him after receiving the Snow Gem in your Amulet.

==History==
===Background History===
Tusk was Sensei's former best friend when they were both young. After they both began training in [[Card-Jitsu]] their friendship split apart. Soon, on a sunny day, Sensei challenged Tusk to a [[Card-Jitsu Snow]] battle. When Tusk created a huge snowball and Sensei deflected it, an avalanche was created. One of his tusks broke that day and Tusk was buried in the snow for years. Tusk decided to take revenge on Sensei by mastering the element of snow.

===2013===
He sent Sly, Scrap and Tank to defeat Sensei and the ninjas, but the final battle was against him. After Tusk was defeated by 3 ninjas and Sensei, he vanished claiming he would return. Tusk has not been heard from since.

==Gallery==
===Card-Jitsu Saga===
<gallery>
File:YoungTusk.PNG|Tusk when he was young.
File:Tusk.PNG|Tusk while training in [[Card-Jitsu]].
File:Tusk1.PNG|Tusk mastering the element of Water.
File:SenseiTusk.PNG|Tusk and [[Sensei]] mastering the element of Snow.
File:Tusk-vs-sensei.png|Tusk after being hit by Sensei.
File:TuskSenseiRunningAvalanche.png|Tusk and Sensei running from the avalanche.
File:TuskInIceCave.png|Tusk in the Ice Cave.
File:Tusk-final.png|Tusk after his defeat.
</gallery>

===Card-Jitsu Snow===
<gallery>
File:CardJitsuSnow-TuskBattle.PNG|The beginning of the game.
File:CardJitsuSnow-TuskBattleLose.PNG|If the battle against Tusk is lost.
</gallery>

===Artwork===
<gallery>
File:Tusk2.PNG|Tusk training.
TuskMyPenguin.PNG|Tusk on [[Club Penguin (app)]]
</gallery>

==Trivia==
*Tusk was the only known walrus on [[Club Penguin Island]] until [[Merry Walrus]] was introduced.
*His mask looks like [[Wikipedia:Darth_Vader|Darth Vader]]'s, as well as his story.
*[[Sensei]]'s walking stick is actually Tusk's broken tusk.<ref>http://www.clubpenguinwiki.info/wiki/File:Support_Sensei_Walking_Stick.png</ref>

==Sources and References==
{{reflist}}
{{clear}}
{{NinjaInfo}}
{{Characters}}
{{Creatures}}
[[Category:Characters]][[Category:Villains]][[Category:Card-Jitsu Snow]]
[[Category:Ninjas]]
[[pt:Tusk]]
