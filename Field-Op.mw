{{Archive}}
A '''Field-Op''' was a mini-mission that [[EPF Agent]]s can solve every week. Unlike [[Secret Missions|regular missions]], they were played in the online rooms of ''[[Club Penguin]]'', and you couldn't go back and do previous Field-Ops. When an [[EPF Agent]] completes a Field-Op, he or she would get a [[Medal (Field-Ops)|medal]]. With these medals, [[penguins]] can get [[Elite Gear]] from a shop in their [[EPF Spy Phone]].

During the Field-Ops you find out EPF secrets, like finding out the pole on the [[Ski Hill]] is the EPF Antenna and the red couch in the [[Lodge Attic]] contains a hidden device.

Sometimes the Field-Op is actually located in and around the EPF Headquarters.

==History==
Field-Ops were released in June 2010. They were temporarily suspended by [[The Director]] on June 8, 2011 because of everyone's hard work at the [[Battle of Doom]]. They returned on June 27, 2011. The Field-Op which was launched on November 7, 2012 was just a message without a task, because of [[Gary]]'s disappearance. Field-Ops ended on November 14, 2012 when [[Herbert]] attacked the [[Everyday Phoning Facility]] and [[EPF Command Room]]. They were officially replaced with [[Spy Drills]] in May 2013.

==List of Field-Ops==
{{main|List of Field-Ops}}

==Mini-games==
Once the player has found the location, they must complete a minigame to complete the Field-Op.
===Destroy the circuits!===
You must match the circles' symbols in 60 seconds. Once a pair is matched they will turn green and explode. Match all of them to overload the circuits. This mini-game appears in Field-Ops 1, 3, 5, 8, 11, 18, 22, 29, 33, 45, 47, 53, 95, 101 and 104.

===Power up the chipset!===
You have to guide the micro battery to the microchips to recharge them using the arrow keys. When the battery power is drained out go back to the recharger to refill it. Avoid the traps scattered around. If you get hit by a trap the battery power goes down. Get hit three times and the battery is destroyed. Keep repeating the process until all the microchips are recharged. This mini-game appears in Field-Ops 2, 4, 7, 10, 12, 14, 16, 20, 24, 26, 30, 48, 55, 59, 64, 66, 70, 74, 81, 83, 88, 99 and 100.

===Bypass the System!===
You have to match matching blocks together as they fall from the top of the screen. There are five non-moving blocks on top and one moving block on the bottom to move around and join the falling blocks onto. Eventually after matching a few blocks the speed of the falling blocks gets faster. Match the blocks until the meter on the top of the screen is full. This mini-game appears in Field-Ops 6, 9, 13, 15, 23, 43, 49, 57, 69, 76, 86 and 106.

===Crack the Code!===
You have to crack the code by selecting a symbol. Once you click "Scan", red/yellow/green light appears. Red means that symbol is not in any of the slots and that it is wrong. Yellow means that symbol is the right symbol but is in the wrong spot. Green means that symbol is correctly placed. If player gets 4 green lights, the Field-Op is completed. It is similar to a game in the [[PSA Secret Missions#The Veggie Villain|The Veggie Villain]]. This mini-game appears in Field-Ops 17, 19, 25, 27, 31, 38, 46, 51, 63, 65, 72, 78, 80, 84, 89, 91, and 96. 

===Repair the System!===
You have to control the machine passing all the red switches by pressing the keys at the bottom left of the screen. You only get five chances to input the codes so utilize the chances and finish it before you have to use all five chances. This mini-game appears in Field-Op 21, 28, 32, 52, 61, 67, 71, 73, 85, 90 and 105.

===Decrypt the Passcode!===
You have to match the shape on the right using shapes on the left. You have to do this 10 times to win a medal. This mini-game appears in Field-Op 34, 42, 50, 56, 58, 68 and 77, 93, 94 and 103.

===Destroy the Targets!===
You have to destroy the red targets by clicking on the x and y axis coordinates to fire the cannon and destroy the targets. This mini-game appears in Field-Op 35, 44, 54, 62, 82, 87, 92 and 98.

===Lock Detected===
You have to match the tempo of blocks on other bars by repeating the pattern of them. This mini-game appears in Field-Op 36, 60, 79, 97 and 102.

===Match the Frequency===
You have to match the spinning radio scanner to the frequency number. This mini-game appears in Field-Op 37, 39 and 40.

===Target in Range!===
You have to guide a snowball through a passageway where you stop at circles and must press the arrow key of the arrow to keep going. You must do it quickly as you have limited time and your snowball will explode if you don't do it quick enough. This mini-game appears in Field-Op 41.

==Errors==
*In the first small period where Field Op #7 was current, there was a glitch where you had to do it at the [[Recycling Plant]] again.
**The same thing happened with Field Op #8 except you had to go back to the [[Beacon]]. 
**The same thing also recurred with the [[Dojo Courtyard]] and so forth.
*There was an error on Club Penguin's part, where in Field-Op 31, the location was in the [[Wilderness (room)|Wilderness]]. However, it was still there after the [[Wilderness Expedition]] ended, and wasn't moved. This was later fixed and moved to the [[Forest]].
*If you failed Field Op 58, It said "ACCE DEIED" instead of "ACCESS DENIED".
*If you failed Field Op 60, it said "ou were detected!" instead of "You were detected!"

==Trivia==
*Sometimes Field-Ops can be related to a certain event (like Field-Op 5 being related to the [[Music Jam 2010]], Field-Op 31 being related to the [[Wilderness Expedition]], Field-Op 41 being related to the [[Earth Day 2011]], Field-Op 48 being related to the [[Great Snow Race]], Field-Op 58 related to the [[Card-Jitsu Party]], Field-Op 70 related to the [[Rockhopper's Quest]], Field-Op 86 related to the [[Marvel Super Hero Takeover]], Field-Op 96 related to the [[Adventure Party: Temple of Fruit]] and Field-Op 100 related to [[The Fair 2012]]).

==Gallery==
===Field Ops Assignments===
<gallery widths="160">
File:Clubpenguin-field-ops-assignment_1.png|First Field-Op assignment
File:Field-ops-assignment-2.png|Second Field-Op assignment
File:fieldopassignment3.png|Third Field-Op assignment
File:Fieldop4.png|Fourth Field-Op assignment
File:Fieldop5.PNG|Fifth Field-Op assignment
File:field-op6.png|Sixth Field-Op assignment
File:FieldOp7.png|Seventh Field-Op assignment
File:Field_op_7.PNG|Eighth Field-Op assignment
File:Field-Op_8.png|Ninth Field-Op assignment
File:Field-Op_9.png|Tenth Field-Op assignment
File:FieldOp11.png|Eleventh Field-Op assignment
File:FieldOp12.png|Twelfth Field-Op assignment
File:Fieldop13.png|Thirteenth Field-Op assignment
File:FieldOp14.png|Fourteenth Field-Op assignment
File:FieldOp15.png|Fifteenth Field-Op assignment
File:FieldOp16.png|Sixteenth Field-Op assignment
File:FieldOp17.png|Seventeenth Field-Op assignment
File:FieldOp18.png|Eighteenth Field-Op assignment
File:FieldOp19.png|Nineteenth Field-Op assignment
File:FieldOp20.png|Twentieth Field-Op assignment
</gallery>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Click expand to view the rest of the gallery</big></center>
<div class="mw-collapsible-content">
<gallery widths="160" captionalign="left">
File:FieldOp21.png|Twenty-first Field-Op assignment
File:FieldOp22.PNG|Twenty-second Field-Op assignment
File:FieldOp23.PNG|Twenty-third Field-Op assignment
File:FieldOp24.PNG|Twenty-fourth Field-Op assignment
File:Field Op 25.png|Twenty-fifth Field-Op assignment
File:FieldOp26.png|Twenty-sixth Field-Op assignment
File:FieldOp27.png|Twenty-seventh Field-Op assignment
File:FieldOp28.PNG|Twenty-eighth Field-Op assignment
File:Field-Op 29.png|Twenty-ninth Field-Op assignment
File:Field-Op_30.png|Thirtieth Field-Op assignment
File:FieldOp31.PNG|Thirty-first Field-Op assignment
File:FieldOp32.PNG|Thirty-second Field-Op assignment
File:FieldOp33.PNG|Thirty-third Field-Op assignment
File:Thirty-fourth Field-Op assignment.png|Thirty-fourth Field-Op assignment
File:FieldOp35.PNG|Thirty-fifth Field-Op assignment
File:FieldOp36.PNG|Thirty-sixth Field-Op assignment
File:FieldOp37.PNG|Thirty-seventh Field-Op assignment
File:FieldOp38.PNG|Thirty-eighth Field-Op assignment
File:FieldOp39.PNG|Thirty-ninth Field-Op assignment
File:FieldOp40.PNG|Forty Field-Op assignment
File:FieldOp41.PNG|Forty-first Field-Op assignment
File:FieldOp42.PNG|Forty-second Field-Op assignment
File:Fieldop43.png|Forty-third Field-Op assignment
File:FieldOp44.PNG|Forty-fourth Field-Op assignment
File:FieldOp45.PNG|Forty-fifth Field-Op assignment
File:FieldOp46.PNG|Forty-sixth Field-Op assignment
File:FieldOp47.PNG|Forty-seventh Field-Op assignment
File:Fieldop48.jpg|Forty-eighth Field-Op assignment
File:FieldOp49.PNG|Forty-ninth Field-Op assignment
File:FieldOp50.PNG|Fiftieth Field-Op assignment
File:FieldOp51.PNG|Fifty-first Field-Op assignment
File:FieldOp52.PNG|Fifty-second Field-Op assignment
File:FieldOp53.PNG|Fifty-third Field-Op assignment
File:FieldOp54.PNG|Fifty-fourth Field-Op assignment
File:FieldOp55.PNG|Fifty-fifth Field-Op assignment
File:FieldOp56.PNG|Fifty-sixth Field-Op assignment
File:FieldOp57.PNG|Fifty-seventh Field-Op assignment
File:FieldOp58.PNG|Fifty-eighth Field-Op assignment
File:FieldOp59.PNG|Fifty-ninth Field-Op assignment
File:FieldOp60.PNG|Sixtieth Field-Op assignment
File:FieldOp61.PNG|Sixty-first Field-Op assignment
File:FieldOp62.PNG|Sixty-second Field-Op assignment
File:FieldOp63.PNG|Sixty-third Field-Op assignment
File:FieldOp64.PNG|Sixty-fourth Field-Op assignment
File:FieldOp65.PNG|Sixty-fifth Field-Op assignment
File:FieldOp66.PNG|Sixty-sixth Field-Op assignment
File:FieldOp67.PNG|Sixty-seventh Field-Op assignment
File:FieldOp68.PNG|Sixty-eighth Field-Op assignment
File:FieldOp69.PNG|Sixty-ninth Field-Op assignment
File:FieldOp70.PNG|Seventy Field-Op assignment
File:FieldOp71.PNG|Seventy-first Field-Op assignment
File:FieldOp72.PNG|Seventy-second Field-Op assignment
File:FieldOp73.PNG|Seventy-third Field-Op assignment
File:FieldOp74.PNG|Seventy-fourth Field-Op assignment
File:FieldOp75.PNG|Seventy-fifth Field-Op assignment
File:FieldOp76.PNG|Seventy-sixth Field-Op assignment
File:FieldOp77.PNG|Seventy-seventh Field-Op assignment
File:FieldOp78.PNG|Seventy-eighth Field-Op assignment
File:FieldOp79.PNG|Seventy-ninth Field-Op assignment
File:FieldOp80.PNG|Eighty Field-Op assignment
File:FieldOp81.PNG|Eighty-first Field-Op assignment
File:FieldOp82.PNG|Eighty-second Field-Op assignment
File:FieldOp83.PNG|Eighty-third Field-Op assignment
File:FieldOp84.PNG|Eighty-fourth Field-Op assignment
File:FieldOp85.PNG|Eighty-fifth Field-Op assignment
File:FieldOp86.PNG|Eighty-sixth Field-Op assignment
File:FieldOp87.PNG|Eighty-seventh Field-Op assignment
File:FieldOp88.PNG|Eighty-eighth Field-Op assignment
File:FieldOp89.PNG|Eighty-ninth Field-Op assignment
File:FieldOp90.PNG|Ninety Field-Op assignment
File:FieldOp91.PNG|Ninety-first Field-Op assignment
File:FieldOp92.PNG|Ninety-second Field-Op assignment
File:FieldOp93.PNG|Ninety-third Field-Op assignment
File:FieldOp94.PNG|Ninety-fourth Field-Op assignment
File:FieldOp95.PNG|Ninety-fifth Field-Op assignment
File:FieldOp96.PNG|Ninety-sixth Field-Op assignment
File:FieldOp97.PNG|Ninety-seventh Field-Op assignment
File:FieldOp98.PNG|Ninety-eighth Field-Op assignment
File:FieldOp99.PNG|Ninety-ninth Field-Op assignment
File:FieldOp100.PNG|Hundredth Field-Op assignment
File:FieldOp101.PNG|Hundredth-first Field-Op assignment
File:FieldOp102.PNG|Hundredth-second Field-Op assignment
File:FieldOp103.PNG|Hundredth-third Field-Op assignment
File:FieldOp104.PNG|Hundredth-fourth Field-Op assignment
File:FieldOp105.PNG|Hundredth-fifth Field-Op assignment
File:FieldOp106.PNG|Hundredth-sixth Field-Op assignment
</gallery>
</div>
</div>


===Mini Games===
<gallery>
File:Field-ops-mission_1.png|Circuit Match
File:Field_op_mission_2.jpg|Chip Maze
File:BypassTheSystem.PNG|Firewall
File:fieldop17complete.png|Code Break
File:Repair The System!.png|The Navigator
File:Decrypt the Passcode.png|Code Decrypt
File:Destroy the Targets.PNG|Grid Command
File:141F62U2Q.png|Rhythm Lock
File:Match the Frequency.PNG|Tumblers
File:1-11042H1440b21.png|Radar Strike
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Operações
|spanish= Minimisión
|french= Opérations Specialés
|german= Feld-Op
}}

==SWFs==
*[http://media1.clubpenguin.com/play/v2/games/codebreak/codebreak.swf Crack the Code]
*[http://media1.clubpenguin.com/play/v2/games/circuitmatch/circuitmatch.swf Destroy the Circuits!]
*[http://media1.clubpenguin.com/play/v2/games/gridcommand/gridcommand.swf Destroy the Targets!]
*[http://media1.clubpenguin.com/play/en/v2/games/commandline/commandline.swf Repair the System]
*[http://media1.clubpenguin.com/play/v2/games/commandline/commandline.swf Repair the System (French)]

{{Games}}
{{EPF}}

[[Category:Games]]
[[Category:Elite Penguin Force]]
