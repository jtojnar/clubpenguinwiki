{{ItemInfobox
|name= Keytar
|image= File:Keytar.png
|available= No
|type= Hand Item
|member= No
|party= None
|cost= None (Unlockable)
|found= [[Treasure Book]]
|id= 15017
}}

The '''Keytar''' is a hand item in ''[[Club Penguin]]''. It was unlockable from the [[Treasure Book]] with a [[coin]] code from a Series 2 ''[[Club Penguin]]'' [[Merchandise|toy]], and it is available to all [[penguins]]. It is based on the [[wikipedia:Keytar|real instrument]]. A similar item is the [[Green Keytar]], which is a member item.

==History==
This item is an uncommon item.
===Release history===
{|border="1" class="wikitable" 
!'''Party/Catalog'''!!'''Available from'''!!'''Available until'''
|-
|Treasure Book||December 22, 2008||October 4, 2011
|}

==Gallery==
<gallery>
File:Keytar2.png|The Keytar in-game.
File:Keytar1.png|The Keytar on a player card.
</gallery>

== Trivia ==
*It was made for [[Polo Field]] as a surprise based on his red keytar in real life.<ref>https://twitter.com/polofield/status/445617272527552512</ref>
*It is [[Polo Field]]'s favorite item.

==Names in other languages==
{{OtherLanguage
|portuguese= Teclarra
|spanish= Kitara
|french= La Guitare Clavier
|german= Umhängekeyboard
|russian= Клавитара
}}

==Sources and References==
{{reflist}}

==See also==
*[[Green Keytar]]

==SWF==
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/15017.swf Keytar (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/15017.swf Keytar (sprite)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/15017.swf Keytar (paper)]

[[Category:Clothing]]
[[Category:Instruments]]
[[Category:Hand Items]]
[[Category:Treasure Book Items]]
[[Category:Clothes released in 2008]]
