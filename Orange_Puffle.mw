{{PuffleInfobox
|name= Orange Puffle
|image= File:Puffle 2014 Transformation Player Card Orange.png 
|color = #fe9900
|available= Yes 
|attitude= Zany, Curious
|member= Yes
|toys= Box, Wagon
|food= [[Sock]]
|play= Monster Truck
|dance= Spins hula hoop
|teeth= Buck teeth
|speed= Very slow (formerly fastest)
|special= Sleeps very deeply
|epf item= None
|game= None
|id= 758
|Postcard= It rides on the Monster wagon saying goodbye
|bodycode1 = #ff6600
|tonguecode1 = #9966cc
}}
The '''Orange Puffle''' is a [[puffle]] that was released on February 25, 2010.<ref>http://www.youtube.com/watch?v=lDmVPVC8lJ0</ref> It was previously only rumored to exist. Eventually, there were many sightings on the [[Club Penguin Island]], as well as in a video on the [[Club Penguin]] website homepage.

A lot was unknown about the Orange Puffle. The first sighting was in a [[puffle]] video. It was later spotted in the [[Box Dimension]] and the [[Ski Lodge]], coming out of the Cuckoo Clock on the left wall in the place of [[Fred the Clockwork Cuckoo]] every half hour. Another sighting of the Orange Puffle was at the [[Ski Village]], where the Orange Puffle would come when there are 10 orange [[penguin]]s there.

Like the [[Blue Puffle]], the [[Brown Puffle]] and the [[White Puffle]], the Orange Puffle does not not have a game it can join in (other than [[Puffle Launch]] and [[Pufflescape]]).

==Description==
The Orange Puffle is a zany, curious puffle. Its eccentric actions and buck teeth enforce this. It is also known that this kind of puffle likes to eat, dance and play, it is one of the most active and sporty puffles discovered in Club Penguin yet. It has a visible black strand of hair on its head, which is curly and long.

==Description in the [[Puffle Handbook]]==
This species has teeth strong enough to demolish petrified [[Puffle O's]]. And everything else.

*Personality: Curious, zany
*Toys: Box wagon, truck
*Observed in the wild: Making quick decisions
*Favorite snacks: Everything (eats puffle bowl)
*Cool fact: Sleeps very deeply and drools... and drools...
*Favorite place: [[Pizza Parlor]]

==List of actions==
*'''Bath''': Jumps on a diving board three times before the board snapping and the puffle falling in.
*'''Postcard''': Rides away on a wagon by saying goodbye without a hobo stick.
*'''Groom''': Its hair is combed from side to side.

==Sightings around Club Penguin before availability==
*[[Ski Lodge]]: It came out every 30 minutes (for example, 00:00 and 00:30). The Orange Puffle replaces [[Fred the Clockwork Cuckoo|Fred]] in the cuckoo clock.
*[[Box Dimension]]: It came out every 15 minutes (for example, 00:15, 00:30 and 00:45). A box floats past and an Orange Puffle pops out covered in styrofoam.
*[[Ski Village]]: It came out when 10 [[Penguins|penguins]] are wearing [[Orange]]. The Orange Puffle comes down on the [[Ski Lift]] and then goes back up the [[Mountain]] on the Ski Lift.

==Gallery==
<gallery>
File:Wow an orange puffle!!.png|The Orange Puffle seen in the [[Puffle]] Video.
File:Orange Puffle at Box Dimension.gif|An Orange Puffle at the Box Dimension.
File:Orange Puffle in coco clock Ski Lodge-5-.gif|The Orange puffle in the [[Ski Lodge]].
File:Goofy+Orange+Puffle+in+Ski+Lift.jpg|The Orange Puffle at the Ski Village.
File:Orange Puffle Plays with Hula Hoop-4-.gif|The Orange puffle dancing.
File:Orange_Puffle_Pics.png|6 Pictures of the Orange Puffle shown on many blogs.
File:Toys puffle.png|The Orange puffle plush along with the other [[puffles]].
File:Orange Puffle Monster Truck Wagon.png|The Orange Puffle driving its "Monster Truck Wagon."
File:Orange Puffle Play with Other Puffle.png|Orange Puffle playing with the [[Blue Puffle|Blue]] and [[Green Puffle|Green]] [[puffles]].
File:Orangepufflestartpage.png|The Orange Puffle at the [[Login Screen]].
File:Orange Escape.png|The postcard your Orange Puffle gives you when it runs away.
File:Orangepuffy.png|Another Orange Puffle.
File:OrangePufflePin.PNG|The [[Orange Puffle Pin]].
File:Cp kid-Zane.png|An Orange Puffle on the old [[Puffle Card]].
File:OrangePuffleHomepage.png|The Orange Puffle in Club Penguin's Homepage.
File:OrangePuffle-Postcard111.PNG|The Orange Puffle on the postcard you receive after adopting a puffle. (2010)
</gallery>

==Trivia==
*The Orange Puffle still appears at the [[Box Dimension]] to this day even after its release.
*The Orange Puffle wasn't adapted into the [[My Puffle]] book, so a [[Blue Puffle]] replaced it. This also happened if you used a [[Brown Puffle]]. Neither were added, as the game was taken out on 2012.
*According with [[Puffle Handbook]], the Orange Puffle favorite place is [[Pizza Parlor]].

==Names in other languages==
{{OtherLanguage
|portuguese= Puffle Laranja
|spanish= Puffle naranja
|french= Le Puffle Orange
|german= Oranger Puffle
}}

==See also==
*[[Puffle]]
*[[White Puffle]]
*[[Puffle Party 2010]]

==Sources and references==
{{reflist}}

==SWF==
*[http://media1.clubpenguin.com/play/v2/content/global/puffle/icons/orange.swf Orange Puffle (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/758.swf Orange Puffle (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/758.swf Orange Puffle (paper)]

{{Succession box|White Puffle|Brown Puffle}}
{{creatures}}

[[Category:Puffles]]
[[pt:Puffle Laranja]]
