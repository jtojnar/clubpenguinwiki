[[File:OBerry.png|thumb|An O-Berry]]

'''O-Berries''' (sometimes called Puffle O's) are "O"-shaped berries, native only to [[Club Penguin]]. O-Berries are orange rings with leaves on them, growing on bushes. O-Berries, however, seem to only grow in [[The Wilderness]], so it is unknown how [[penguin]]s get to them. However, some research and a lot of thinking shows the Outback Pond behind the Ski Lodge might lead to the Wilderness. [[Box of Puffle O's (25 O's)|Boxes of these berries]] are sold in the [[Pet Shop]].

==Involvement in Missions==
*[[Mission]] #2 - You must cheer up a Black [[puffle]] by giving it O-Berries. You also use them to start a fire. If you do not catch a [[Fluffy the Fish|fish]] and/or you do not use the fishing pole, you can eat them.
*Mission #6 - Someone tied up the O-Berries to a [[Pine Tree]] (Possibly [[Herbert]]) and you must use the scissors in your [[Spy Phone]] to get them down. You then use them to feed a hungry Black [[Puffle]]. Then, when Herbert traps you in a cage, you must throw the berries onto parts of the Cage releases so the [[puffle]] can free you. You also learn that giving an O-Berry dipped in [[Hot Sauce]] to a Black [[Puffle]] can be dangerous.

==Appearances==
*During [[Puffle Parties]], you could throw O-Berries to [[puffles]] in the [[Puffle Feeding Area]].
*[[Black Puffle]]s seem to like them more than other [[puffle]]s, igniting (going up in flames) themselves once they consume them.
*The O-Berries are often seen in the old Puffle Video.
*As with many objects in [[Club Penguin]], the O-Berry was once available as a [[pin]].
*They are seen in the application [[Puffle Launch (application)|Puffle Launch]].
*A special type of O-Berry was seen at the [[Medieval Party 2013]] as an ingredient in the potions game.

==Gallery==
<gallery>
File:SnowballOBerry.png|An O-Berry as seen in the [[Puffle Feeding Area]].
File:O-Berry_Pin.PNG|The O-Berry pin.
File:O-Berry Bush.png‎|An O-Berry Bush.
File:GourmetO'Berries.PNG|[[Gourmet O'Berries]]
</gallery>

== Trivia ==
* O-Berries are edible to [[penguins]], as proven by [[Rookie]] in [[Elite Penguin Force: Missions|Mission 3]] in [[Club Penguin: Elite Penguin Force]], but they do not taste very good. He said, “I had to wash the taste of these berries out of my mouth. It felt like it was on fire.“ In fact, O-berries are “bitter and spicy”, as mentioned in the Club Penguin Times Edition #244.
[[File:Rookie Ate Some O-Berries.png]]

==See also==
*[[Puffle-O]]
*[[Puffle]]
*[[O-Berry Pin]]

{{Food}}

[[Category:Flora]]
[[Category:Puffle Food]]
[[pt:Puffito]]
