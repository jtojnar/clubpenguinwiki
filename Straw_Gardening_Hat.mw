{{ItemInfobox
|name= Straw Gardening Hat
|image= File:Straw Hat.png
|available= No
|type= Head item
|member= No
|party= [[Earth Day 2010]]
|cost= Free
|found= [[Mine Shack]]
|id= 1134<br>11134 ([[Treasure Book]])
|unlock= Yes (Series 12)
}}The '''Straw Gardening Hat''' (known as the '''Straw Hat''' in the ''[[Treasure Book]]'') is a head item in ''[[Club Penguin]]''. It was found at the [[Mine Shack]] for all players during the [[Earth Day 2010]] party. This hat could also be unlocked in the Series 12 ''Treasure Book''. When you dance with it without other clothes on, you will bring out a watering can, waving it side to side. If you water the Community Garden wearing just the straw hat, you will grow vegetables. A similar item is [[The Fiesta]].

==History==
The Straw Gardening Hat is an uncommon item, not having been available since the fall of 2011.
===Release date===
====Parties====
{|border="1" class="wikitable" 
!'''Party'''!!'''Available from'''!!'''Available until'''
|-
|[[Earth Day 2010]]||April 21, 2010||April 27, 2010
|}

====Catalogs====
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|Series 12 Treasure Book||February 7, 2011||October 4, 2011
|}

==Appearances==
*It first appeared in the [[Club Penguin Times]] before the party.

==Trivia==
*This item is called the [[Straw Hat]] in the Series 12 Treasure Book, causing some confusion.

==Gallery==
<gallery>
File:StrawGardeningHat1.PNG|The Straw Gardening Hat in-game.
File:StrawHatinPlayerCard.PNG|The Straw Gardening Hat on a [[player card]].
File:Gardening Straw Hat-5-.gif|The gardening straw hat in action.
File:Strawhatwateringcan.png|A frozen picture of the Straw Hat in action. 
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Chapéu de Jardinagem (1134)<br>Chapéu de Palha (11134)
|spanish= Sombrero de jardinero (1134)<br>Sombrero de paja (11134)
|french= Le Chapeau Champêtre (1134 and 11134)
|german= Garten-Strohhut (1134)<br>Strohhut (11134)
|russian= Огородная соломенная шляпа (1134)<br>Соломенная шляпа (11134)
}}

==SWF==
===1134===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/1134.swf Straw Gardening Hat (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/1134.swf Straw Gardening Hat (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/1134.swf Straw Gardening Hat (paper)]
===11134===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/11134.swf Straw Hat (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/11134.swf Straw Hat (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/11134.swf Straw Hat (paper)]
[[Category:Clothing]]
[[Category:Free Items]]
[[Category:Head Items]]
[[Category:Treasure Book Items]]
[[Category:Clothes released in 2010]]
