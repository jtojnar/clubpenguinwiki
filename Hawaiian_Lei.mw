{{ItemInfobox    
|name= Hawaiian Lei   
|image= File:HawaiianLei.png
|available= No   
|type= Neck Item   
|member= No    
|party= [[Winter Luau]]  
|cost= Free   
|found= [[Dock]]   
|id= 171   
}}The '''Hawaiian Lei''' (also known as the '''Red Lei''') is a very rare neck item in ''[[Club Penguin]]''. All players can get it during the [[Winter Luau]] at the [[Dock]]. [[Actions|Dancing]] causes players to do a hula dance. Optionally, players can also wear the [[Grass Skirt]] to hula dance with this [[item]]. Similar items include other [[Lei|Leis]]. 

==Rarity==
The Hawaiian Lei is the second rarest item on Club Penguin.

==Release history==
{|border="1" class="wikitable" 
!'''Party'''!!'''Available from'''!!'''Available until'''
|-
|[[Winter Luau]]||January 27, 2006||January 29, 2006
|-
|Tato Maxx's Birthday Party||August 15, 2013 12:30 AM||August 15, 2013 01:45 AM
|}

==Trivia==
*It is the second rarest item in Club Penguin, after the [[Party Hat]]
*It is one of the only free items to not be available for the entire length of the party it was released in, being available for only 2 days
*It is similar to the [[Blue Lei]] (released six months later) as well as the [[Friends Forever Lei]] (released over 7 years later)
*The [[Club Penguin]] moderator [[Tato Maxx]] decided to add this item to the inventory of 50 penguins who attended his birthday party. This does not count as a full release, but somehow reduces the rarity of this item

==Appearances==
*It was seen on a Tiki Statue during the [[Adventure Party 2009]], [[Island Adventure Party 2010]], and [[Island Adventure Party 2011]]
*One of these can be seen in the [[Clothes Shop]] display case

==Gallery==
<gallery>
File:HawaiianLei1.PNG|The Hawaiian Lei in-game.
File:HawaiianLei2.PNG|The Hawaiian Lei on a [[player card]].
File:HawaiianLei_AdventureParty.png|The Hawaiian Lei seen on a Tiki Statue during the [[Island Adventure Party 2011]]. 
</gallery>

==Names in Other Languages==
{{OtherLanguage
|portuguese= Colar Havaiano
|spanish= Collar hawaiano
|french= Le Collier Hawaiien
|german= Hawaiikette
|russian= Гавайская цветочная гирлянда
}}

==SWF==
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/171.swf Hawaiian Lei (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/171.swf Hawaiian Lei (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/171.swf Hawaiian Lei (paper)]

[[Category:Clothing]]
[[Category:Clothes released in 2006]]
[[Category:Free Items]]
[[Category:Neck Items]]
[[Category:Rare Items]]
[[pt:Colar Havaiano]]
