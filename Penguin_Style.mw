{{CatalogInfobox
|name = Penguin Style
|image = File:PenguinStyleJul2015.PNG
|imagesize = 200px
|caption = The [[June 2015 Penguin Style|current edition]] of the Penguin Style
|date = October 2005
|location = [[Gift Shop]]<br>[[Sports Shop]] (until August 2007)
|sell = Colors, Clothes, Backgrounds, Flags
|available = Yes
|updates = Monthly
}}

{{cquote2|One catalog: all fashion|[[Big Wigs]]}}

'''Penguin Style''' (also known as '''Herbert Style''' during the [[Operation: Blackout]]) is ''[[Club Penguin]]''<nowiki>'</nowiki>s monthly [[clothing]] catalog located in the [[Gift Shop]]. It is, as stated in the ''Ultimate Guide To Club Penguin: Volume 1'', updated on the first Thursday/Friday of every month. Each issue has a different color and cover design. Graphics on the covers have also changed over the years.

[[Member]] players can buy everything available, while [[non-members]] can only buy limited items like colors, flag pins, player card [[backgrounds]], and some clothes at the beginning of the catalog. Each catalog features a “''Penguins at Work''” section which features a new job for [[penguin]]s every month. Hidden items are usually found on certain pages.

According to issue 175 of the [[Club Penguin Times|Penguin Times]], every item will return except for [[Party Hats]] and [[pin]]s. You can only get them by [[cheating]], and that will [[ban]] your penguin.

Different versions of the Penguin Style catalog have been available in the past. Some examples include the Penguin Style catalogs in the [[Club Penguin: Elite Penguin Force (series)|Elite Penguin Force video game series]], where penguins could buy the items in the video game and then transfer the items into online Club Penguin (and unlock the items). The most recent non-online version was released on May 9, 2013, and is located in the [[My Penguin]] app. It (unlike the other versions) has new, exclusive items to be bought that can't be found on online Club Penguin. This version of the Penguin Style is also a monthly catalog.

== Penguins at Work ==
Below is a list of job-related clothing that has been sold in Penguin Style in previous months. The “Penguins at Work” section used to be called “On the job”. Some jobs have been repeated.

{|class="wikitable collapsible" style="width: 750px;"
!Month
!Job
!Reason
!#
|-
|[[January 2008 Penguin Style|January 2008]]
|Pizza Chef
|[[Winter Fiesta 2008]]
|1
|-
|[[February 2008 Penguin Style|February 2008]]
|Construction Worker
|[[Save the Migrator]]
|1
|-
|[[March 2008 Penguin Style|March 2008]]
|Coffee Waitor
|[[St. Patrick's Day Party 2008]]
|1
|-
|[[April 2008 Penguin Style|April 2008]]
|Rescue Squad
|April edition of [[Snow and Sports]].
|1
|-
|[[May 2008 Penguin Style|May 2008]]
|Blacksmither
|[[Medieval Party 2008]]
|1
|-
|[[June 2008 Penguin Style|June 2008]]
|Lifeguard
|[[Water Party 2008]]
|1
|-
|[[July 2008 Penguin Style|July 2008]]
|Music Conductor
|[[Music Jam 2008]]
|1
|-
|[[August 2008 Penguin Style|August 2008]]
|Firefighter
|[[Penguin Games]]
|1
|-
|[[September 2008 Penguin Style|September 2008]]
|Painter
|Construction for [[Ruby and the Ruby]]
|1
|-
|[[October 2008 Penguin Style|October 2008]]
|Rad Scientist
|[[Halloween Party 2008]]
|1
|-
|[[November 2008 Penguin Style|November 2008]]
|Shoveler
|[[Dig out the Dojo]]
|1
|-
|[[December 2008 Penguin Style|December 2008]]
|Cake Baker
|[[Christmas Party 2008]]
|1
|-
|[[January 2009 Penguin Style|January 2009]]
|Coffee Server
|[[Winter Fiesta 2009]]
|2
|-
|[[February 2009 Penguin Style|February 2009]]
|Puffle Feeder
|[[Puffle Party 2009]]
|1
|-
|[[March 2009 Penguin Style|March 2009]]
|Painter
|Construction of the [[Penguin Play Awards 2009]]
|2
|-
|[[April 2009 Penguin Style|April 2009]]
|Farmer
|Earth Day
|1
|-
|[[May 2009 Penguin Style|May 2009]]
|Blacksmither
|[[Medieval Party 2009]]
|2
|-
|[[June 2009 Penguin Style|June 2009]]
|Construction Worker
|Construction of the [[Adventure Party 2009]]
|2
|-
|[[July 2009 Penguin Style|July 2009]]
|Music Conductor
|[[Music Jam 2009]]
|2
|-
|[[August 2009 Penguin Style|August 2009]]
|Pizza Chef
|The release of [[Underwater Adventure]]
|2
|-
|[[September 2009 Penguin Style|September 2009]]
|Firefighter
|[[Fire Scavenger Hunt]]
|2
|-
|[[October 2009 Penguin Style|October 2009]]
|Rad Scientist
|[[Halloween Party 2009]]
|2
|-
|[[November 2009 Penguin Style|November 2009]]
|Shoveler
|[[Winter Party 2009]]
|2
|-
|[[December 2009 Penguin Style|December 2009]]
|Cake Baker
|[[Holiday Party 2009]]
|2
|-
|[[January 2010 Penguin Style|January 2010]]
|Construction Worker
|[[Cave Expedition]]
|3
|-
|[[February 2010 Penguin Style|February 2010]]
|Puffle Feeder
|[[Puffle Party 2010]]
|2
|-
|[[March 2010 Penguin Style|March 2010]]
|Rescue Squad
|[[Puffle Rescue]] release
|2
|-
|[[April 2010 Penguin Style|April 2010]]
|Gardener
|[[Earth Day 2010]]
|1
|-
|[[May 2010 Penguin Style|May 2010]]
|Blacksmither
|[[Medieval Party 2010]]
|3
|-
|[[June 2010 Penguin Style|June 2010]]
|Construction Worker
|[[Island Adventure Party 2010]] ([[Ship Battle]] and [[Cove]])
|4
|-
|[[July 2010 Penguin Style|July 2010]]
|Garden Worker
|Construction for the [[Music Jam 2010]]
|1
|-
|[[August 2010 Penguin Style|August 2010]]
|Rescue Squad
|[[Mountain Expedition]]
|3
|-
|[[September 2010 Penguin Style|September 2010]]
|Painter
|[[The Fair 2010]]
|3
|-
|[[October 2010 Penguin Style|October 2010]]
|Rad Scientist
|[[Halloween Party 2010]]
|3
|-
|[[November 2010 Penguin Style|November 2010]]
|Mop and Bucket
|[[The Great Storm of 2010]]
|1
|-
|[[December 2010 Penguin Style|December 2010]]
|Cake Baker
|[[Holiday Party 2010]]
|3
|-
|[[January 2011 Penguin Style|January 2011]]
|Coffee Server
|[[Elite Penguin Force]] (Big launches in this month)
|3
|-
|[[February 2011 Penguin Style|February 2011]]
|Popcorn Server
|[[Puffle Party 2011]]
|1
|-
|[[March 2011 Penguin Style|March 2011]]
|Puffle Feeder
|Revamped [[Pet Shop]]
|3
|-
|[[April 2011 Penguin Style|April 2011]]
|Pizza Chef
|[[Earth Day 2011]]
|3
|-
|[[May 2011 Penguin Style|May 2011]]
|Blacksmither
|[[Medieval Party 2011]]
|4
|-
|[[June 2011 Penguin Style|June 2011]]
|One Man Band
|[[Music Jam 2011]]
|1
|-
|[[July 2011 Penguin Style|July 2011]]
|Pizza Chef
|[[Island Adventure Party 2011]]
|4
|-
|[[August 2011 Penguin Style|August 2011]]
|Ice Cream Vendor
|[[Great Snow Race]]
|1
|-
|[[September 2011 Penguin Style|September 2011]]
|Balloon Vendor
|[[The Fair 2011]]
|1
|-
|[[October 2011 Penguin Style|October 2011]]
|Ghoul Detector
|[[Halloween Party 2011]]
|1
|-
|[[November 2011 Penguin Style|November 2011]]
|Sushi Master
|[[Card-Jitsu Party]]
|1
|-
|[[December 2011 Penguin Style|December 2011]]
|Shoveler
|[[Holiday Party 2011]]
|3
|-
|[[January 2012 Penguin Style|January 2012]]
|Lifeguard
|[[Underwater Expedition]]
|2
|-
|[[February 2012 Penguin Style|February 2012]]
|Mop and Bucket
|[[Rockhopper's Quest]]
|2
|-
|[[March 2012 Penguin Style|March 2012]]
|Pet Shop Apron
|[[Puffle Party 2012]]
|4
|-
|[[April 2012 Penguin Style|April 2012]]
|Water Suit 3000
|[[Earth Day 2012]]
|2
|-
|[[May 2012 Penguin Style|May 2012]]
|Blacksmith Apron
|[[Medieval Party 2012]]
|5
|-
|[[June 2012 Penguin Style|June 2012]]
|Firefighter
|[[Marvel Super Hero Takeover 2012]]
|3
|-
|[[July 2012 Penguin Style|July 2012]]
|One Man Band
|[[Make Your Mark: Ultimate Jam]]
|2
|-
|[[August 2012 Penguin Style|August 2012]]
|Ice Cream Vendor
|[[Smoothie Smash]] launch
|2
|-
|[[September 2012 Penguin Style|September 2012]]
|Popcorn Tray
|[[The Fair 2012]]
|2
|-
|[[October 2012 Penguin Style|October 2012]]
|Gravedigger
|[[Halloween Party 2012]]
|1
|-
|[[November 2012 Penguin Style|November 2012]]
|Firefighter
|[[Operation: Blackout]]
|4
|-
|[[December 2012 Penguin Style|December 2012]]
|Shoveler
|[[Holiday Party 2012]]
|5
|-
|[[January 2013 Penguin Style|January 2013]]
|None
|None
|N/A
|-
|[[February 2013 Penguin Style|February 2013]]
|Coffee Server
|[[Hollywood Party]]
|4
|-
|[[March 2013 Penguin Style|March 2013]]
|Puffle Handler
|[[Puffle Party 2013]]
|5
|-
|[[April 2013 Penguin Style|April 2013]]
|Firefighter
|[[Marvel Super Hero Takeover 2013]]
|5
|-
|[[May 2013 Penguin Style|May 2013]]
|Sashimi Master
|[[Card-Jitsu Party 2013]]
|1
|-
|[[June 2013 Penguin Style|June 2013]]
|Pizza Chef
|[[Monsters University Takeover]]
|5
|-
|[[July 2013 Penguin Style|July 2013]]
|Construction Worker
|N/A
|5
|-
|[[August 2013 Penguin Style|August 2013]]
|Lifeguard
|[[Teen Beach Movie Summer Jam]]
|3
|-
|[[September 2013 Penguin Style|September 2013]]
|Blacksmith
|[[Medieval Party 2013]]
|6
|-
|[[October 2013 Penguin Style|October 2013]]
|Gravedigger
|[[Halloween Party 2013]]
|2
|-
|[[November 2013 Penguin Style|November 2013]]
|Ski Patrol
|[[Operation: Puffle]]
|4
|-
|[[December 2013 Penguin Style|December 2013]]
|Craftman
|[[Holiday Party 2013]]
|1
|-
|[[January 2014 Penguin Style|January 2014]]
|Prehistoric Pizza Chef
|[[Prehistoric Party 2014]]
|1
|-
|[[February 2014 Penguin Style|February 2014]]
|Popcorn Tray
|[[The Fair 2014]]
|3
|-
|[[March 2014 Penguin Style|March 2014]]
|One Man Band
|[[Muppets World Tour]]
|3
|-
|[[April 2014 Penguin Style|April 2014]]
|Puffle Handler
|[[Puffle Party 2014]]
|6
|-
|[[May 2014 Penguin Style|May 2014]]
|Holo DJ
|[[Future Party]]
|1
|-
|[[June 2014 Penguin Style|June 2014]]
|Coach
|[[Penguin Cup]]
|1
|-
|[[July 2014 Penguin Style|July 2014]]
|Mop and Bucket
|[[Music Jam 2014]]
|3
|-
|[[August 2014 Penguin Style|August 2014]]
|Ice Cream Vendor
|[[Frozen Party]]
|3
|-
|[[September 2014 Penguin Style|September 2014]]
|Mascot Costume
|[[School & Skate Party]]
|1
|-
|[[October 2014 Penguin Style|October 2014]]
|Cake Baker
|[[Halloween Party 2014]]
|4
|-
|[[November 2014 Penguin Style|November 2014]]
|Blacksmith
|[[Pirate Party 2014]]
|7
|-
|[[December 2014 Penguin Style|December 2014]]
|Craftman
|[[Merry Walrus Party]]
|2
|-
|[[January 2015 Penguin Style|January 2015]]
|Holo DJ
|[[Star Wars Rebels Takeover]]
|2
|-
|[[February 2015 Penguin Style|February 2015]]
|One Man Band
|[[SoundStudio Party]]
|4
|-
|[[March 2015 Penguin Style|March 2015]]
|Puffle Handler
|[[Puffle Party 2015]]
|7
|-
|[[April 2015 Penguin Style|April 2015]]
|Water Suit 3000
|[[Frozen Fever Party]]
|3
|-
|[[May 2015 Penguin Style|May 2015]]
|Balloon Vendor
|[[The Fair 2015]]
|2
|-
|[[June 2015 Penguin Style|June 2015]]
|Painter
|Construction of the [[Puffle Berry Mall]]
|4
|-
|[[July 2015 Penguin Style|July 2015]]
|Coffee Server
|N/A
|5
|}

== Gallery ==
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2005</big></center>
<div class="mw-collapsible-content">
<gallery>
<gallery widths="120" captionalign="left">
File:PenguinStyleDec2005.png|link=December 2005 Penguin Style|[[December 2005 Penguin Style|December 2005 Catalog]]
</gallery>
</div>
</div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2006</big></center>
<div class="mw-collapsible-content">
<gallery>
File:penguin-style-jan-06.png|link=January 2006 Penguin Style|[[January 2006 Penguin Style|January 2006 Catalog]]
File:Newcat-716306.jpg|link=March 2006 Penguin Style|[[March 2006 Penguin Style|March 2006 Catalog]]
File:Cp_april_catalogue.jpg|link=April 2006 Penguin Style|[[April 2006 Penguin Style|April 2006 Catalog]]
File:PenguinStyleJun2006.PNG‎|link=June 2006 Penguin Style|[[June 2006 Penguin Style|June 2006 Catalog]]
File:Wild_west_sneak_peek.png|A sneak peek of the July 2006 Catalog.
File:July06PenguinStyle.jpg|link=July 2006 Penguin Style|[[July 2006 Penguin Style|July 2006 Catalog]]
File:Sep06PenguinStyle.jpg|link=September 2006 Penguin Style|[[September 2006 Penguin Style|September 2006 Catalog]]
File:Oct06PenguinStyle.jpg|link=October 2006 Penguin Style|[[October 2006 Penguin Style|October 2006 Catalog]]
File:Dec06PenguinStyle.jpg|link=December 2006 Penguin Style|[[December 2006 Penguin Style|December 2006 Catalog]]
</gallery>
</div></div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2007</big></center>
<div class="mw-collapsible-content">
<gallery>
File:Catalogjan07style.jpg|link=January 2007 Penguin Style|[[January 2007 Penguin Style|January 2007 Catalog]]
File:Feb06PenguinStyle.jpg|link=February 2007 Penguin Style|[[February 2007 Penguin Style|February 2007 Catalog]]
File:Mar07PenguinStyle.png|link=March 2007 Penguin Style|[[March 2007 Penguin Style|March 2007 Catalog]]
File:Apr07PenguinStyle.jpg|link=April 2007 Penguin Style|[[April 2007 Penguin Style|April 2007 Catalog]]
File:Penguin_style_may_07.PNG|link=May 2007 Penguin Style|[[May 2007 Penguin Style|May 2007 Catalog]]
File:June-catalog.jpg|link=June 2007 Penguin Style|[[June 2007 Penguin Style|June 2007 Catalog]]
File:Penguincatalog_july_07.png|link=July 2007 Penguin Style|[[July 2007 Penguin Style|July 2007 Catalog]]
File:PenguinStyleAug2007.PNG|link=August 2007 Penguin Style|[[August 2007 Penguin Style|August 2007 Catalog]]
File:PenguinStyleSep2007.PNG|link=September 2007 Penguin Style|[[September 2007 Penguin Style|September 2007 Catalog]]
File:PenguinStyleOct2007.PNG|link=October 2007 Penguin Style|[[October 2007 Penguin Style|October 2007 Catalog]]
File:PenguinStyleNov2007.PNG|link=November 2007 Penguin Style|[[November 2007 Penguin Style|November 2007 Catalog]]
File:penguin-style-dec-07.png|link=December 2007 Penguin Style|[[December 2007 Penguin Style|December 2007 Catalog]]
</gallery>
</div>
</div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2008</big></center>
<div class="mw-collapsible-content">
<gallery>
File:PenguinStyle_Jan_08.png|link=January 2008 Penguin Style|[[January 2008 Penguin Style|January 2008 Catalog]]
File:PenguinStyle_Feb_08.png|link=February 2008 Penguin Style|[[February 2008 Penguin Style|February 2008 Catalog]]
File:PenguinStyleMar2008.PNG|link=March 2008 Penguin Style|[[March 2008 Penguin Style|March 2008 Catalog]]
File:clothing-catalog-april.png|link=April 2008 Penguin Style|[[April 2008 Penguin Style|April 2008 Catalog]]
File:penguin-style-may-081.png|link=May 2008 Penguin Style|[[May 2008 Penguin Style|May 2008 Catalog]]
File:PenguinStyle_Jun_08.png|link=June 2008 Penguin Style|[[June 2008 Penguin Style|June 2008 Catalog]]
File:PenguinStyle_Jul_08.png|link=July 2008 Penguin Style|[[July 2008 Penguin Style|July 2008 Catalog]]
File:Aug08PenguinStyle.jpg|link=August 2008 Penguin Style|[[August 2008 Penguin Style|August 2008 Catalog]]
File:penguin-style-sept-08.jpg|link=September 2008 Penguin Style|[[September 2008 Penguin Style|September 2008 Catalog]]
File:Pengstyleoct08.png|link=October 2008 Penguin Style|[[October 2008 Penguin Style|October 2008 Catalog]]
File:penguin-style-nov08.png|link=November 2008 Penguin Style|[[November 2008 Penguin Style|November 2008 Catalog]]
File:penguin-style-december-cover1.png|link=December 2008 Penguin Style|[[December 2008 Penguin Style|December 2008 Catalog]]
</gallery>
</div>
</div>

<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2009</big></center>
<div class="mw-collapsible-content">
<gallery>
File:PenguinStyle_Jan_09.jpg|link=January 2009 Penguin Style|[[January 2009 Penguin Style|January 2009 Catalog]]
File:penguin-style-catalog-2009-02-12.png|link=February 2009 Penguin Style|[[February 2009 Penguin Style|February 2009 Catalog]]
File:PenguinStyle Mar '09.png|link=March 2009 Penguin Style|[[March 2009 Penguin Style|March 2009 Catalog]]
File:penguin-style-april09-frount-cover.png|link=April 2009 Penguin Style|[[April 2009 Penguin Style|April 2009 Catalog]]
File:Cataloguemay2009.png|link=May 2009 Penguin Style|[[May 2009 Penguin Style|May 2009 Catalog]]
File:Penguin-style-jun-09.png|link=June 2009 Penguin Style|[[June 2009 Penguin Style|June 2009 Catalog]]
File:ZaJuly.png|link=July 2009 Penguin Style|[[July 2009 Penguin Style|July 2009 Catalog]]
File:Penguinstyleaug09.png|link=August 2009 Penguin Style|[[August 2009 Penguin Style|August 2009 Catalog]]
File:PenguinStyleSept2009.png|link=September 2009 Penguin Style|[[September 2009 Penguin Style|September 2009 Catalog]]
File:October Penguin Style.jpg|link=October 2009 Penguin Style|[[October 2009 Penguin Style|October 2009 Catalog]]
File:November09pengstyle.png|link=November 2009 Penguin Style|[[November 2009 Penguin Style|November 2009 Catalog]]
File:Penguin_Style_Dec_09.png|link=December 2009 Penguin Style|[[December 2009 Penguin Style|December 2009 Catalog]]
</gallery>
</div>
</div>

<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2010</big></center>
<div class="mw-collapsible-content">
<gallery>
File:Penguin_Style_Glitch.png|link=January 2010 Penguin Style|[[January 2010 Penguin Style|January 2010 Catalog]] typo appeared for a few days after its release
File:Catalogfebruary.png|link=February 2010 Penguin Style|[[February 2010 Penguin Style|February 2010 Catalog]]
File:March_2010_catalog.png|link=March 2010 Penguin Style|[[March 2010 Penguin Style|March 2010 Catalog]]
File:April_2010_penguin_style.png|link=April 2010 Penguin Style|[[April 2010 Penguin Style|April 2010 Catalog]]
File:PS1.png|link=May 2010 Penguin Style|[[May 2010 Penguin Style|May 2010 Catalog]]
File:jun 10.jpg|link=June 2010 Penguin Style|[[June 2010 Penguin Style|June 2010 Catalog]]
File:Penguin_style.png|link=July 2010 Penguin Style|[[July 2010 Penguin Style|July 2010 Catalog]]
File:Aug2010PenguinStyle.PNG|link=August 2010 Penguin Style|[[August 2010 Penguin Style|August 2010 Catalog]]
File:PS Sept 10.png|link=September 2010 Penguin Style|[[September 2010 Penguin Style|September 2010 Catalog]]
File:PenguinStyle_October10.png|link=October 2010 Penguin Style|[[October 2010 Penguin Style|October 2010 Catalog]]
File:Nov2010PenguinStyle.PNG|link=November 2010 Penguin Style|[[November 2010 Penguin Style|November 2010 Catalog]]
File:Dec2010PenguinStyle.PNG|link=December 2010 Penguin Style|[[December 2010 Penguin Style|December 2010 Catalog]]
</gallery>
</div>
</div>

<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2011</big></center>
<div class="mw-collapsible-content">
<gallery>
File:Jan11PenguinStyle.png|link=January 2011 Penguin Style|[[January 2011 Penguin Style|January 2011 Catalog]]
File:February 2011 Penguin Style.png|link=February 2011 Penguin Style|[[February 2011 Penguin Style|February 2011 Catalog]]
File:March 2011 Penguin Style.png|link=March 2011 Penguin Style|[[March 2011 Penguin Style|March 2011 Catalog]]
File:April 2011 Penguin Style.png|link=April 2011 Penguin Style|[[April 2011 Penguin Style|April 2011 Catalog]]
File:May 2011 Penguin Style.PNG|link=May 2011 Penguin Style|[[May 2011 Penguin Style|May 2011 Catalog]]
File:June 2011 Penguin Style.PNG|link=June 2011 Penguin Style|[[June 2011 Penguin Style|June 2011 Catalog]]
File:July 2011 Penguin Style.PNG|link=July 2011 Penguin Style|[[July 2011 Penguin Style|July 2011 Catalog]]
File:August2011ClothingCover.PNG|link=August 2011 Penguin Style|[[August 2011 Penguin Style|August 2011 Catalog]]
File:PenguinStyleSept11.png|link=September 2011 Penguin Style|[[September 2011 Penguin Style|September 2011 Catalog]]
File:PenguinStyleOct11.PNG|link=October 2011 Penguin Style|[[October 2011 Penguin Style|October 2011 Catalog]]
File:Nov2011PenguinStyleCover.PNG|link=November 2011 Penguin Style|[[November 2011 Penguin Style|November 2011 Catalog]]
File:Dec2011PenguinStyleCover.PNG|link=December 2011 Penguin Style|[[December 2011 Penguin Style|December 2011 Catalog]]
</gallery>
</div>
</div>

<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2012</big></center>
<div class="mw-collapsible-content">
<gallery>
File:Jan2012PenguinStyle.PNG|link=January 2012 Penguin Style|[[January 2012 Penguin Style|January 2012 Catalog]]
File:Feb12Clothing.PNG|link=February 2012 Penguin Style|[[February 2012 Penguin Style|February 2012 Catalog]]
File:Mar2012ClothingCover.PNG|link=March 2012 Penguin Style|[[March 2012 Penguin Style|March 2012 Catalog]]
File:Apr2012ClothingCover.PNG|link=April 2012 Penguin Style|[[April 2012 Penguin Style|April 2012 Catalog]]
File:PenguinStyleMay2012.PNG|link=May 2012 Penguin Style|[[May 2012 Penguin Style|May 2012 Catalog]]
File:PenguinStyleJune2012.PNG|link=June 2012 Penguin Style|[[June 2012 Penguin Style|June 2012 Catalog]]
File:PenguinStyleJuly2012.PNG|link=July 2012 Penguin Style|[[July 2012 Penguin Style|July 2012 Catalog]]
File:PenguinStyleAugust2012.PNG|link=August 2012 Penguin Style|[[August 2012 Penguin Style|August 2012 Catalog]]
File:PenguinStyleSeptember2012.PNG|link=September 2012 Penguin Style|[[September 2012 Penguin Style|September 2012 Catalog]]
File:PenguinStyleOctober2012.PNG|link=October 2012 Penguin Style|[[October 2012 Penguin Style|October 2012 Catalog]]
File:NovPenguinStyleCover.png|link=November 2012 Penguin Style|[[November 2012 Penguin Style|November 2012 Catalog]]
File:ClothingOperationBlackout.PNG|link=November 2012 Herbert Style|[[November 2012 Herbert Style|November 2012 Catalog]] (during the [[Operation: Blackout]])
File:PenguinStyleDecember2012.PNG|link=December 2012 Penguin Style|[[December 2012 Penguin Style|December 2012 Catalog]]
</gallery>
</div>
</div>

<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2013</big></center>
<div class="mw-collapsible-content">
<gallery>
File:PenguinStyleJan2013.PNG|link=January 2013 Penguin Style|[[January 2013 Penguin Style|January 2013 Catalog]]
File:PenguinStyleFeb2013.PNG|link=February 2013 Penguin Style|[[February 2013 Penguin Style|February 2013 Catalog]]
File:PenguinStyleMar2013.PNG|link=March 2013 Penguin Style|[[March 2013 Penguin Style|March 2013 Catalog]]
File:PenguinStyleCoverApr2013.PNG|link=April 2013 Penguin Style|[[April 2013 Penguin Style|April 2013 Catalog]]
File:PenguinStyleCoverMay2013.PNG|link=May 2013 Penguin Style|[[May 2013 Penguin Style|May 2013 Catalog]]
File:PenguinStyleJun2013.PNG|link=June 2013 Penguin Style|[[June 2013 Penguin Style|June 2013 Catalog]]
File:PenguinStyleJuly2013.PNG|link=July 2013 Penguin Style|[[July 2013 Penguin Style|July 2013 Catalog]]
File:PenguinStyleAugust2013.PNG|link=August 2013 Penguin Style|[[August 2013 Penguin Style|August 2013 Catalog]]
File:PenguinStyleSep2013.PNG|link=September 2013 Penguin Style|[[September 2013 Penguin Style|September 2013 Catalog]]
File:PenguinStyleOct2013.PNG|link=October 2013 Penguin Style|[[October 2013 Penguin Style|October 2013 Catalog]]
File:PenguinStyleNov2013.PNG|link=November 2013 Penguin Style|[[November 2013 Penguin Style|November 2013 Catalog]]
File:PenguinStyleDec2013.png|link=December 2013 Penguin Style|[[December 2013 Penguin Style|December 2013 Catalog]]
</gallery>
</div>
</div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2014</big></center>
<div class="mw-collapsible-content">
<gallery>
File:PenguinStyleJan2014.png|link=January 2014 Penguin Style|[[January 2014 Penguin Style|January 2014 Catalog]]
File:PenguinStyleFeb2014.png|link=February 2014 Penguin Style|[[February 2014 Penguin Style|February 2014 Catalog]]
File:PenguinStyleMar2014.png|link=March 2014 Penguin Style|[[March 2014 Penguin Style|March 2014 Catalog]]
File:PenguinStyleApr2014.png|link=April 2014 Penguin Style|[[April 2014 Penguin Style|April 2014 Catalog]]
File:PenguinStyleMay2014.png|link=May 2014 Penguin Style|[[May 2014 Penguin Style|May 2014 Catalog]]
File:PenguinStyleJun2014.PNG|link=June 2014 Penguin Style|[[June 2014 Penguin Style|June 2014 Catalog]]
File:PenguinStyleJul2014.PNG|link=July 2014 Penguin Style|[[July 2014 Penguin Style|July 2014 Catalog]]
File:PenguinStyleAug2014.PNG|link=August 2014 Penguin Style|[[August 2014 Penguin Style|August 2014 Catalog]]
File:PenguinStyleSep2014.PNG|link=September 2014 Penguin Style|[[September 2014 Penguin Style|September 2014 Catalog]]
File:PenguinStyleOct2014.PNG|link=October 2014 Penguin Style|[[October 2014 Penguin Style|October 2014 Catalog]]
File:PenguinStyleNov2014.PNG|link=November 2014 Penguin Style|[[November 2014 Penguin Style|November 2014 Catalog]]
File:PenguinStyleDec2014.PNG|link=December 2014 Penguin Style|[[December 2014 Penguin Style|December 2014 Catalog]]
</gallery>
</div>
</div>
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>2015</big></center>
<div class="mw-collapsible-content">
<gallery>
File:PenguinStyleJan2015.PNG|link=January 2015 Penguin Style|[[January 2015 Penguin Style|January 2015 Catalog]]
File:PenguinStyleFeb2015.PNG|link=February 2015 Penguin Style|[[February 2015 Penguin Style|February 2015 Catalog]]
File:PenguinStyleMar2015.PNG|link=March 2015 Penguin Style|[[March 2015 Penguin Style|March 2015 Catalog]]
File:PenguinStyleApr2015.PNG|link=April 2015 Penguin Style|[[April 2015 Penguin Style|April 2015 Catalog]]
File:PenguinStyleMay2015.PNG|link=May 2015 Penguin Style|[[May 2015 Penguin Style|May 2015 Catalog]]
File:PenguinStyleJun2015.PNG|link=June 2015 Penguin Style|[[June 2015 Penguin Style|June 2015 Catalog]]
File:PenguinStyleJul2015.PNG|link=July 2015 Penguin Style|[[July 2015 Penguin Style|July 2015 Catalog]]
</gallery>
</div>
</div>

== Release Dates ==
To see the release dates for every Penguin Style catalog, go [[Penguin Style release dates|here]].

== Trivia ==
* There was a note hidden in the back of the catalog in April 2008, giving hints about the next party, which turned out to be the [[Medieval Party 2008]].
* The catalog was also used in the Sport Shop until its remodeling. The ''Penguin Style'' there was then replaced with ''[[Snow and Sports]]''.
* Two ''Penguin Style'' catalogs were released on May 3 and May 31, 2012. It is one of the three months where two catalogs are released in the same calendar month in ''Club Penguin'' history.
* The January 2013 catalog was the first Penguin Style catalog where non-members could buy clothes other than colors, flags, and backgrounds.
* Some of the penguins in the catalog have the same pose as penguins of old catalogs.

== Fixed Glitches ==
* The front cover of the Jan '10 catalog said “Jan '09” for a short time.

== See also ==
* [[Clothes]]
* [[Member]]
* [[Big Wigs]]
* [[Costume Trunk]]

== Names in other languages ==

{{OtherLanguage
|portuguese = Estilo Pinguim
|spanish = Moda pingüina
|french = Mode Pingouin
|german = Pinguin-Mode
|russian = Стильный пингвин
}}

{{SWFArchives|PenguinStyle}}

* [http://media1.clubpenguin.com/play/v2/content/local/en/catalogues/clothing.swf Current ''Penguin Style'' catalog]

{{Penguin Style}}
{{PrintedMedia}}

[[Category:Printed Media]]
[[Category:Catalogs]]

[[pt:Estilo Pinguim]]
