{{MobileAppInfobox 
|name = ''Puffle Wild''
|image = File:PuffleWildIcon.PNG
|caption = The app's icon
|platform = Mobile
|developer = Walt Disney
|publisher = Disney Mobile
|rating = 4+ (Apple's App Store)
|genre = Match-Three Puzzle
|year = '''''iOS'''''<br>November 3, 2014 <small>(Australia and New Zealand - Beta)</small><br>December 4, 2014 <small>(Wordwide)</small>
}}'''Puffle Wild''' (originally titled '''Puffle Wild ADVENTURE!''') is a mobile application for [[wikipedia:iPad|iPad]]s 2 or higher, [[wikipedia:iPhone|iPhone]]s 4 or higher and [[wikipedia:iPod Touch|iPods Touch]] 4th generation or higher with [[wikipedia:iOS|iOS 6]] or higher. It was launched as a beta test on November 3, 2014 for Australia and New Zealand<ref>http://clubpenguinmemories.com/2014/11/club-penguin-puffle-wild-app-now-available-on-new-zealand-and-australian-app-stores/</ref> and on December 4, 2014 as final version globally <ref>http://www.clubpenguin.com/blog/2014/12/whats-coming-december-2014</ref>. The game was confirmed by [[Polo Field]] on the ''[[What's New Blog]]'' on October 21, 2014<ref>http://www.clubpenguin.com/blog/2014/10/puffle-wild-ios-app</ref>.

== Description ==
Brave explorers wanted! Strange things have been happening in Club Penguin's
untamed wilderness... Are you ready to EXPLORE the mysterious area and DISCOVER powerful puffles? In this epic Match 3 adventure, journey to CONQUER the Wilds! Along the way, collect powerful puffles to help match 3. Take on 60+ FREE levels and challenge unique game puzzles where skillful switching and sliding will create crazy combos! Try for 3 stars on every level!<ref>https://itunes.apple.com/nz/app/club-penguin-puffle-wild/id767801968?mt=8</ref>

== Gameplay ==
''"Your goal is to match three or more O'berries on the game board. Keep matching O'berries until you run out of moves! See how many moves or how much time you have left in the upper left-hand corner of the screen, above the platforms. You can also see your score and how many stars you've earned. As you progress through the levels, you'll start to see obstacles like snow, rocks, and vines. Clear them away to complete the level!"''<ref>http://help.disney.com/articles/en_US/FAQ/How-do-I-play-Puffle-Wild?section=clubpenguin</ref>

==Content Updates==
===Version 1.1===
Launched on December 4, 2014 - ''"Minor bug fixes and optimized performance."''

== Exclusive Puffle Creatures ==
=== [[The Wilds]] ===
<gallery>
Green Raccoon Puffle.png|link=Green Raccoon Puffle|'''[[Green Raccoon Puffle]]'''<br>Score at least 21.000 points
Red Rabbit Puffle.png|link=Red Rabbit Puffle|'''[[Red Rabbit Puffle]]'''<br>Clear all the snow (40)
Orange Raccoon Puffle.png|link=Orange Raccoon Puffle|'''[[Orange Raccoon Puffle]]'''<br>Pop the Pink, Brown and Orange O'berries (40 each)
Blue Deer Puffle.png|link=Blue Deer Puffle|'''[[Blue Deer Puffle]]'''<br>Score at least 28.000 points
Pink Rabbit Puffle.png|link=Pink Rabbit Puffle|'''[[Pink Rabbit Puffle]]'''<br>Pop the Pink O'berries (7)
Blue Raccoon Puffle.png|link=Blue Raccoon Puffle|'''[[Blue Raccoon Puffle]]'''<br>Clear all the snow (41)
White Rabbit Puffle.png|link=White Rabbit Puffle|'''[[White Rabbit Puffle]]'''<br>Pop the White O'berries (3)
</gallery>

=== [[Wild Woods]] ===
<gallery>
Red Deer Puffle.png|link=Red Deer Puffle|'''[[Red Deer Puffle]]'''<br>Clear all the snow (18)
Green Rabbit Puffle.png|link=Green Rabbit Puffle|'''[[Green Rabbit Puffle]]'''<br>Clear all the snow (16)
Black Deer Puffle.png|link=Black Deer Puffle|'''[[Black Deer Puffle]]'''<br>Clear all the snow (18)
Purple Deer Puffle.png|link=Purple Deer Puffle|'''[[Purple Deer Puffle]]'''<br>Score at least 27.000 points
Pink Raccoon Puffle.png|link=Pink Raccoon Puffle|'''[[Pink Raccoon Puffle]]'''<br>Clear all the snow (24)
YellowUnicornPuffle.png|link=Yellow Unicorn Puffle|'''[[Yellow Unicorn Puffle]]'''<br>Pop the White and Yellow O'berries (50 each)
</gallery>

== Gallery ==
=== Icons ===
<gallery>
File:PuffleWildIcon.PNG|The icon of the app.
</gallery>
=== Logos ===
<gallery>
File:PuffleWildLogo.PNG|The logo of the app.
File:PuffleWildLogo2.png|Original logo found in game files.
</gallery>

=== Berries ===
<gallery>
File:Puffle Wild Black Berry Icon.png|Black Berry
File:Puffle Wild Blue Berry Icon.png|Blue Berry
File:Puffle Wild Brown Berry Icon.png|Brown Berry
File:Puffle Wild Gold Berry Icon.png|Gold Berry
File:Puffle Wild Green Berry Icon.png|Green Berry
File:Puffle Wild Orange Berry Icon.png|Orange Berry
File:Puffle Wild Pink Berry Icon.png|Pink Berry
File:Puffle Wild Purple Berry Icon.png|Purple Berry
File:Puffle Wild Rainbow Berry Icon.png|Rainbow Berry
File:Puffle Wild Red Berry Icon.png|Red Berry
File:Puffle Wild White Berry Icon.png|White Berry
File:Puffle Wild Yellow Berry Icon.png|Yellow Berry
</gallery>

=== Backgrounds ===
<gallery>
File:01 nationalpark home bg bridge.png|
File:01 nationalpark home bg nobridge.png|
</gallery>

=== Official Screenshots ===
<gallery>
PuffleWildScreenshot1.JPG|
PuffleWildScreenshot2.JPG|
PuffleWildScreenshot3.JPG|
PuffleWildScreenshot4.JPG|
PuffleWildScreenshot5.JPG|
</gallery>
=== Miscellaneous ===
<gallery>
PuffleWild.PNG|The picture in the What's New Blog that confirmed the Puffle Wild app.
PuffleWildHomepage1.jpg|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
PuffleWildHomepage2.jpg|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 2).
</gallery>

== Videos ==
<youtube>loSweZJNlb8</youtube>
<youtube>SamtfxRaCzs</youtube>
<youtube>7pPit1izqr8</youtube>

== Names in other languages ==
{{OtherLanguage
|portuguese= Floresta Puffle
|spanish= Puffle Wild
|french= La Vallèe des Puffles
|german= Puffle-Tal
|russian= Пафломания
}}

==External Links==
*[https://itunes.apple.com/us/app/club-penguin-puffle-wild/id767801968?mt=8 iTunes]

== Sources and References ==
{{reflist}}
{{Versions}}
[[Category:Video Games]]
[[pt:Floresta Puffle]]
