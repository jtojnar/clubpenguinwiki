{{Archive}}
{{PartyInfobox
|name= Holiday Party 2011
|image=File:Holidaypartylogo.png
|imagesize= 200px
|caption=The logo for Holiday Party 2011.
|membersonly= No
|when= December 14, 2011 - December 28, 2011
|famouspenguins = [[Aunt Arctic]]<br>[[Rockhopper]]
|freeitems= '''''Non-Member'''''<br />[[Aunt Arctic's Autograph]], [[Rockhopper Background (ID 9075)|Rockhopper Background]], [[CFC 2011 Pin]], [[Globe Hat]], [[Santa Seat background]], [[The Jingle Bell]], [[Holiday Tree Background]], [[Candy Cane Duo Pin]], [[CFC Cap]], [[North Pole Background]], [[Holiday Stocking Pin]], [[The Tree Topper]], [[Sweet Treat Background]], [[Milk 'N Cookies Pin]], [[The Hornament Hat]], [[Holiday Magic Background]], [[Gingerbread Cookie Costume]]<br />
----
'''''Member'''''<br/>[[Cranberry Decoration]], [[Evergreen Decoration]], [[Gold Decoration]], [[Cookie Serving Apron]], [[Deluxe Gingerbread House]]
|catalog= None
|wherehappening= [[Club Penguin Island|Everywhere]]
|previous= [[Card-Jitsu Party 2011]]
|next= [[Underwater Expedition]]
}}
The '''Holiday Party 2011''' was a [[party]] in ''[[Club Penguin]]''. It was confirmed by the ''Disney'' website. Like the previous few Holiday Parties, [[Rockhopper]] returned as usual. This year, [[Aunt Arctic]] returned as well. The party started on December 14, 2011 and ended on December 28, 2011. A new feature this year was an [[Advent Calendar]] in [[Forest]], with free items everyday for all players. There were three new free items in the [[Magic Sleigh Ride]]. <ref>http://recreionline.abril.com.br/fique-ligado/a-recreio-revela-como-vai-ser-o-natal-2011-do-club-penguin - In Portuguese</ref> There was a big donation station in [[Beach]]. During the party, the [[Lighthouse]] and [[Beacon]] filled with coins of [[Coins For Change]].

== Free items ==
{|class="wikitable sortable"
! scope="col"| Image
! scope="col"| Item
! scope="col"| Location
! scope="col"| Members only?
|-
|[[File:TheJingleBell.PNG|45px]]
|[[The Jingle Bell]]
|[[Forest]] (Advent Calendar - Day 1)
|No
|-
|[[File:HolidayTreeBackgroundCard.PNG|45px]]
|[[Holiday Tree Background]]
|[[Forest]] (Advent Calendar - Day 2)
|No
|-
|[[File:CandyCaneDuoPin.PNG|45px]]
|[[Candy Cane Duo Pin]]
|[[Forest]] (Advent Calendar - Day 3)
|No
|-
|[[File:CFCCap.PNG|45px]]
|[[CFC Cap]]
|[[Forest]] (Advent Calendar - Day 4)
|No
|-
|[[File:NorthPoleBackgroundCard.PNG|45px]]
|[[North Pole Background]]
|[[Forest]] (Advent Calendar - Day 5)
|No
|-
|[[File:HolidayStockingPin.PNG|45px]]
|[[Holiday Stocking Pin]]
|[[Forest]] (Advent Calendar - Day 6)
|No
|-
|[[File:TheTreeTopperNew.PNG|45px]]
|[[The Tree Topper]]
|[[Forest]] (Advent Calendar - Day 7)
|No
|-
|[[File:SweetTreatBackgroundCard.PNG|45px]]
|[[Sweet Treat Background]]
|[[Forest]] (Advent Calendar - Day 8)
|No
|-
|[[File:MilkNCookiesPin.PNG|45px]]
|[[Milk 'N Cookies Pin]]
|[[Forest]] (Advent Calendar - Day 9)
|No
|-
|[[File:TheHornamentHat.PNG|45px]]
|[[The Hornament Hat]]
|[[Forest]] (Advent Calendar - Day 10)
|No
|-
|[[File:HolidayMagicBackgroundCard.PNG|45px]]
|[[Holiday Magic Background]]
|[[Forest]] (Advent Calendar - Day 11)
|No
|-
|[[File:GingerbreadCookieCostume.PNG|45px]]
|[[Gingerbread Cookie Costume]]
|[[Forest]] (Advent Calendar - Day 12)
|No
|-
|[[File:CFC2011Pin.PNG|45px]]
|[[CFC 2011 Pin]]
|After you donate to [[Coins for Change]]
|No
|-
|[[File:GlobeHatNew.PNG|45px]]
|[[Globe Hat]]
|[[Rockhopper's Rare Items]]
|No
|-
|[[File:SantaSeatCard.PNG|45px]]
|[[Santa Seat background]]
|[[Book Room]]
|No
|-
|[[File:AuntArctic'sAutographPlayerCard.PNG|45px]]
|[[Aunt Arctic's Autograph]]
|Obtained by meeting [[Aunt Arctic]]
|No
|-
|[[File:RockhopperBackground3rdCard.PNG|45px]]
|[[Rockhopper Background (ID 9075)|Rockhopper Background]]
|Obtained by meeting [[Rockhopper]]
|No
|-
|[[File:CranberryDecoration.PNG|45px]]
|[[Cranberry Decoration]]
|[[Magic Sleigh Ride]]
|Yes
|-
|[[File:EvergreenDecoration.PNG|45px]]
|[[Evergreen Decoration]]
|[[Magic Sleigh Ride]]
|Yes
|-
|[[File:GoldDecoration.PNG|45px]]
|[[Gold Decoration]]
|[[Magic Sleigh Ride]]
|Yes
|-
|[[File:CookieServingApron.PNG|45px]]
|[[Cookie Serving Apron]]
|[[Bakery]]
|Yes
|-
|[[File:DeluxeGingerbreadHouseIcon.png|45px]]
|[[Deluxe Gingerbread House]]
|[[Bakery]]
|Yes
|}

==Gallery ==
===General===
<gallery widths="180">
File:CFC2011Construction.PNG|Construction for [[Coins For Change]] and Holiday Party 2011.
File:CFC2011Donation.PNG|Penguins at [[Coins For Change]].
</gallery>

===Login Screen===
<gallery widths="180">
File:Holidaypartylogonscreen11.PNG|The first log-in screen advertising the Holiday Party 2011. 
File:Coinsforchangelogonscreen11.PNG|The first log-in screen advertising [[Coins for Change]]. 
File:12giftsnowlogonscreen11.PNG|The second log-in screen advertising the Holiday Party 2011
File:Changetheworldlogonscreen11.PNG|The second log-in screen advertising [[Coins for Change]]
</gallery>

===Construction===
<gallery widths="180">
File:HolidayParty2011ConstructionBeach.PNG|The [[Beach]] 
File:HolidayParty2011ConstructionTown.PNG|The [[Town]]
File:CP.comHP11Construction.png|[[Clubpenguin.com]]
</gallery>

===Party Pictures===
<gallery widths="180">
File:CP.comHP11.png|[[ClubPenguin.com]]
File:HolidayParty11PR.png|The [[Bakery]]
File:HolidayParty11Beach.png|The [[Beach]]
File:HolidayParty11Beacon.png|The [[Beacon]] (December 14 - 22)
File:HolidayParty11Beacon2.png|The [[Beacon]] (December 23)
File:HolidayParty11Beacon3.png|The [[Beacon]] (December 24 - 25)
File:HolidayParty11Beacon4.png|The [[Beacon]] (December 26 - 27)
File:HolidayParty11Book.png|The [[Book Room]]
File:HolidayParty11ShipQuarters.PNG|The [[Captain's Quarters]]
File:HolidayParty11Coffee.png|The [[Coffee Shop]]
File:HolidayParty11Cove.PNG|The [[Cove]]
File:HolidayParty11ShipNest.PNG|The [[Crow's Nest]]
File:HolidayParty11Lounge.png|The [[Dance Lounge]]
File:HolidayParty11Dock.png|The [[Dock]]
File:HolidayParty11Dojo.PNG|The [[Dojo]]
File:HolidayParty11DojoCourtyard.png|The [[Dojo Courtyard]]
File:Screenshot2011-12-15at34706PM-1.png|The [[EPF Command Room]] (Note the Herbert cam)
File:HolidayParty11DojoFire.PNG|The [[Fire Dojo]]
File:HolidayParty11Forest.png|The [[Forest]]
File:HolidayParty11Shop.PNG|The [[Gift Shop]]
File:HolidayParty11Iceberg.png|The [[Iceberg]]
File:HolidayParty11Light1.PNG|The [[Lighthouse]] (December 14 - 16)
File:HolidayParty11Light2.PNG|The [[Lighthouse]] (December 17 - 19)
File:HolidayParty11Light3.PNG|The [[Lighthouse]] (December 20 - 21)
File:HolidayParty11Light4.PNG|The [[Lighthouse]] (December 22 - 27)
File:HolidayParty11Attic.PNG|The [[Lodge Attic]]
File:HolidayParty11Sleigh.png|The [[Magic Sleigh Ride]]
File:HolidayParty11Shack.PNG|The [[Mine Shack]]
File:HolidayParty11Dance.png|The [[Night Club]]
File:HolidayParty11DojoHide.PNG|The [[Ninja Hideout]]
File:HolidayParty11Ship.PNG|The [[Pirate Ship (room)|Pirate Ship]]
File:HolidayParty11Pizza.png|The [[Pizza Parlor]]
File:HolidayParty11Plaza.png|The [[Plaza]]
File:HolidayParty11ShipHold.PNG|The [[Ship Hold]]
File:HolidayParty11Hill.png|The [[Ski Hill]]
File:HolidayParty11Lodge.png|The [[Ski Lodge]]
File:HolidayParty11Village.png|The [[Ski Village]]
File:HolidayParty11Forts.png|The [[Snow Forts]]
File:HolidayParty11Stadium.png|The [[Stadium]]
File:HolidayParty2011Town.png|The [[Town]]
</gallery>

==Videos==
<youtube>JaxOTMhaqN4</youtube>
<youtube>IF5XkGIwVg8</youtube>

==Trivia==
*There was a glitch when you clicked the sign it said Be a Pirate. This glitch was fixed.
*This party had the most free items out of any Club Penguin party until the [[Hollywood Party]], beating the previous record-holder, the [[Medieval Party 2011]].

== Sources and References ==
{{reflist}}

{{SWFArchives}}

==See also==
*[[Holiday Parties]]
{{Party}}

[[pt:Festa de Natal 2011]]
[[Category:Parties of 2011]]
