{{RoomInfobox
|title= Pizza Parlor
|image= File:NewPizzaParlor2013.PNG
|place= In the [[Plaza]], next to [[The Stage]]
|open= February 26, 2006
|games= [[Pizzatron 3000]]
|roomid = 330
|tourguide= "{{TourPizza}}"
}}The '''Pizza Parlor''' is a [[pizza]] restaurant that is located in the [[Plaza]] and is a common place for penguins to come and get "jobs". It opened in February 2006, along with the rest of the [[Plaza]]. During this opening, there was a Pizza Party with an additional [[Pizza Apron|apron]] as the free item. Inside, there is a stage with a piano, a reception desk, and a large stove. There is also an entrance to the kitchen, where [[penguin]]s can play [[Pizzatron 3000]].

==Notable Affiliation==
*[[Scrooge]]
*[[Jacob Mukluk]]
*Bob Crackern

==Gallery==
===Normal===
<gallery>
File:Pizzaparlor.png|Old Pizza Parlor (February 26, 2006 - October 31, 2012)
File:New_Pizza_Parlor.png|Old Pizza Parlor (November 1, 2012 - October 2, 2013)
File:NewPizzaParlor2013.PNG|Current Pizza Parlor (October 2, 2013 - present)
</gallery>
===Parties===
<div class="toccolours mw-collapsible mw-collapsed">
<center><big>Parties</big></center>
<div class="mw-collapsible-content">
<gallery>
File:PizzaOpening-PizzaParlor.png|During the [[Pizza Parlor Opening Party]]
File:HalloweenParty2006-PizzaParlor.jpeg|During the [[Halloween Party 2006]]
File:ChristmasParty2006-PizzaParlor.png|During the [[Christmas Party 2006]]
Image:New Pizza Parlour.PNG|During the opening day of [[Pizzatron 3000]]
File:Pizza-camp.jpg|During the [[Camp Penguin]]
File:FFP.jpg|During the [[Fall Fair 2007]]
File:Pizza-HalloweenParty2007.png|During the [[Halloween Party 2007]]
File:April-fools-day-10.png|During the [[April Fools' Party 2008]]
File:Music_Jam_Pizza_Parlor.PNG|During the [[Music Jam 2008]]
File:Pgpizza.png|During the [[Penguin Games 2008]]
File:Fallfairpizza.png|During the [[Fall Fair 2008]]
File:Halloween 2008 Pizza Parlor.PNG|During the [[Halloween Party 2008]]
File:ChristmasParty2008Pizza.PNG|During the [[Christmas Party 2008]]
File:Pizza_Fiesta.png|During the [[Winter Fiesta 2009]]
File:Midievalpizza.png|During the [[Medieval Party 2009]]
File:Ff0913.png|During [[The Fair 2009]]
File:FSH2009-Pizza.PNG|During the [[Fire Scavenger Hunt]]
File:Christmas Party 2009 Pizza Parlor.PNG|During the [[Holiday Party 2009]]
File:PPAPizza.png|The Pizza Parlor during the [[Penguin Play Awards 2010]]
File:MusicJam2010Pizza Parlor.PNG |During the [[Music Jam 2010]]
File:Holiday Party 2010 Pizza Parlor.PNG|During the [[Holiday Party 2010]]
File:AprilFoolsDay2011 Pizza.PNG|During the [[April Fools' Party 2011]]
File:MedievalParty2011PizzaParlor.PNG|During the [[Medieval Party 2011]]
File:MusicJam2011 PizzaParlor.PNG|During the [[Music Jam 2011]]
File:IAP2011-PizzaParlor.PNG|During the [[Island Adventure Party 2011]]
File:TheFair2011Pizza.PNG|During [[The Fair 2011]]
File:HalloweenParty2011-Pizza.PNG|During the [[Halloween Party 2011]]
File:Card-JitsuPartyPizza.PNG|During the [[Card-Jitsu Party 2011]]
File:HolidayParty11Pizza.png|During the [[Holiday Party 2011]]
File:MedievalParty2012Pizza.PNG|During the [[Medieval Party 2012]]
File:MarvelSuperheroTakeoverPizza.PNG|During the [[Marvel Super Hero Takeover 2012]]
File:MakeYourMarkUltimateJamPizza.png|During the [[Make Your Mark: Ultimate Jam]]
File:APToFPizzaParlor.png|During the [[Adventure Party: Temple of Fruit]]
File:TheFair2012Pizza.PNG|During [[The Fair 2012]]
File:HalloweenParty2012Pizza.PNG|During the [[Halloween Party 2012]]
File:BlackOutPizza.PNG|During the [[Operation: Blackout]]
File:HolidayParty2012Pizza.PNG|During the [[Holiday Party 2012]]
File:HollywoodPartyPizza.PNG|During the [[Hollywood Party]]
File:OperationHotSaucePizza.PNG|During the [[Operation: Hot Sauce]]
File:MSHT2013Pizza.PNG|During the [[Marvel Super Hero Takeover 2013]]
File:CardJitsuParty2013Pizza.PNG|During the [[Card-Jitsu Party 2013]]
File:MUTPizza.PNG|During the [[Monsters University Takeover]]
File:MedievalParty2013Pizza.PNG|During the [[Medieval Party 2013]]
File:HalloweenParty2013Pizza.PNG|During the [[Halloween Party 2013]]
File:HolidayParty2013Pizza.PNG|During the [[Holiday Party 2013]]
File:TheFair2014Pizza.PNG|During [[The Fair 2014]]
File:School&SkatePartyPizza.PNG|During the [[School & Skate Party]]
File:HalloweenParty2014Pizza.PNG|During the [[Halloween Party 2014]]
File:TheFair2015Pizza.PNG|During [[The Fair 2015]]
</gallery>
</div>
</div>

===Sneak Peeks===
<gallery>
File:EPFPizzaParlor.PNG|A destroyed Pizza Parlor
</gallery>

==Glitch==
On May 13, 2012, there was a glitch that penguin can walk on the wall of the Pizza Parlor. The glitch ended when the [[Medieval Party 2012|Medieval Party]] started.

==Names in other languages==
{{OtherLanguage
|portuguese= Pizzaria
|spanish= Pizzería
|french= Pizzeria
|german= Pizza
|russian= Пиццерия
}}

==Trivia==
*[[Herbert P. Bear]] once came to the Pizza Parlor to get hot sauce.
*There is a [[Launch Pad]] on the roof that penguins can land on when they play [[Jet Pack Adventure]].
*[[Scrooge]] is the owner of the Pizza Parlor, as revealed in the book [[A Penguin Christmas Carol]].
*[[G Billy]] used to be a chef at the Pizza Parlor before he joined the [[Penguin Band]].
* As of October 3rd, 2013, the Pizza Parlor gives pizza to puffles.

==Pins==
{|border="1" class="wikitable" 
!'''Pin #'''!!'''Pin name'''!!'''Available from'''!!'''Available until'''
|-
|2||[[File:Music Note Pin.PNG|30px]][[Music Note pin]]||March 31, 2006||April 14, 2006
|-
|23||[[File:Apple Pin.PNG|30px]][[Apple pin]]||January 5, 2007||January 19, 2007
|-
|37||[[File:Cart Pin.PNG|30px]][[Cart pin]]||July 20, 2007||August 3, 2007
|-
|64||[[File:Basketball Pin.PNG|30px]][[Basketball pin]]||June 20, 2008||July 4, 2008
|-
|90||[[File:Ice Cream Sundae Pin.PNG|30px]][[Ice Cream Sundae pin]]||May 22, 2009||June 5, 2009
|-
|99||[[File:101 Days of Fun Pin.PNG|30px]][[101 Days of Fun pin]]||September 11, 2009||September 25, 2009
|-
|126||[[File:Light Bulb Pin.PNG|30px]][[Light Bulb pin]]||June 17, 2010||June 30, 2010
|-
|151||[[File:PurplePufflePin.png|30px]][[Purple Puffle Pin]]||March 10, 2011||March 24, 2011
|-
|186||[[File:ReindeerPin.PNG|30px]][[Reindeer Pin]]||December 14, 2011||December 28, 2011
|-
|248||[[File:R2-D2Pin.PNG|30px]][[R2-D2 Pin]]||July 24, 2013||August 7, 2013
|-
|291||[[File:WoodenSwordPin.PNG|30px]][[Wooden Sword Pin]]||November 12, 2014||November 26, 2014
|}

==SWFs==
*[http://media1.clubpenguin.com/play/v2/content/global/rooms/pizza.swf Current Pizza Parlor]

===Music===
*[http://media1.clubpenguin.com/play/v2/content/global//music/676.swf Current Music]
*[http://media1.clubpenguin.com/play/v2/content/global//music/344.swf Old Music]
*[http://media1.clubpenguin.com/play/v2/content/global//music/20.swf Older Music]

{{SWFArchives}}

{{Geographic location
|Centre    = Pizza Parlor
|North     = [[Plaza]]
|Northeast = 
|East      = 
|Southeast = 
|South     = 
|Southwest = 
|West      = 
|Northwest = 
}}

{{Places}}

[[Category:Places]]
[[Category:Plaza]]
[[pt:Pizzaria]]
