{{ItemInfobox
|name= Hockey Stick
|image= File:Hockey Stick.PNG
|available= Yes
|type= Hand Item
|member= Yes (catalog)<br>No (unlockable)
|party= None
|cost= 260 [[coin]]s
|found= [[Snow and Sports]], [[Treasure Book]] 
|id= 220 ([[members]])<br> 10220 ([[non-members]])
|unlock= Yes (Series 5, 6, 11)
}}The '''Hockey Stick''' is a hand item in ''[[Club Penguin]]''. [[Members]] were able to buy this item for 260 [[coin]]s in the ''[[Snow and Sports]]'' [[catalog]]. It was also unlockable in the Series 5, 6, and 11 [[Treasure Book]]s. A similar [[item]] is the [[Goalie Hockey Stick]]. This item goes with the [[Hockey Helmet]], the [[Hockey Jersey]]s and the [[Ice Skates]] or the [[Hockey Skates]].

==History==
The Hockey Stick was first released in the December 2005 ''Penguin Style'' catalog. It has been re-released in the September and November 2007, and the November 2009-March 2010 ''Snow and Sports'' catalogs. It has been re-released many times in between those dates, with the most recent in the December 2012 ''Snow and Sports'' catalog.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan=3|[[Penguin Style]]||August 22, 2005||rowspan=2|Unknown
|-
|December 1, 2005
|-
|August 4, 2006||March 2, 2007
|-
|[[Snow and Sports]]||August 31, 2007||June 27, 2008
|-
|[[Treasure Book (Series 5)]]||October 19, 2009||October 4, 2011
|-
|[[Treasure Book (Series 6)]]||December 15, 2009||October 4, 2011
|-
|[[Treasure Book (Series 11)]]||December 6, 2010||October 4, 2011
|-
|rowspan=5|Snow and Sports||January 13, 2011||May 31, 2011
|-
|December 8, 2011||June 7, 2012
|-
|December 5, 2012||March 13, 2013
|-
|December 4, 2013||June 4, 2014
|-
|December 3, 2014||{{Available|Items}}
|}

==Appearances==
*On the [[Ice Rink postcard]].
*The Hockey Stick made appearances in Issues #43, #51, #59 and #105 of the Club Penguin Times.

==Gallery==
<gallery>
File:HockeyStick1.PNG|The Hockey Stick in-game.
File:HockeyStick2.PNG|The Hockey Stick on a player card.
</gallery>

==Trivia==
*The Hockey Stick is the first hand item to be released on Club Penguin.
*There is also a [[Hockey Stick Pin]].

==Names in other languages==
{{OtherLanguage
|portuguese= Taco de Hóquei
|spanish= Palo de hockey
|french= La Crosse de Hockey
|german= Eishockeyschläger
|russian= Хоккейная клюшка
}}

==SWF==
===220===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/220.swf Hockey Stick (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/220.swf Hockey Stick (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/220.swf Hockey Stick (paper)]

===10220===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10220.swf Hockey Stick (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10220.swf Hockey Stick (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10220.swf Hockey Stick (paper)]

[[Category:Clothing]]
[[Category:Hand Items]]
[[Category:Clothes released in 2005]]
[[pt:Taco de Hóquei]]
