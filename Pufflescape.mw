{{MinigameInfobox
|name= Pufflescape
|image= File:Pufflescapelogo.PNG
|caption= Pufflescape logo
|players= 1
|room= [[Pet Shop]]
|date= May 3, 2011 ([[Beta Team]])<br>November 2, 2011 (Officially)
|stamps= Yes (11)
}}
'''Pufflescape''' (originally titled '''Rollerscape: The Ice Caves''') is a mini-game in ''[[Club Penguin]]''. It can be played at the [[Pet Shop]] or in [[My Penguin]] app. Players can chose any puffle color to play with, as long as they own a puffle with the desired color. If a player owns no puffles, the default puffle color is white. The objective of this game is to collect keys and O-berries to unlock a door to the next level. It is in the [[Pet Shop]].

==History==
Pufflescape was first revealed on [[Happy77]]'s YouTube channel.<ref>http://www.youtube.com/watch?v=Iq03lQC3NSg</ref> In [[Beta Team]], it was known as ''Rollerscape: The Ice Caves''. During its time in the Beta Team, the only known puffle to be playable in this game was the [[White Puffle]], henceforth it being in ''ice'' caves. Players were able to rate and comment on the game and officially beta test it. It was retired from the Beta Team as of August 9, 2011. It was launched in Club Penguin on November 2, 2011 as ''Pufflescape''.

==Controls==
There are two modes in '''Rollerscape''': normal mode and extreme mode. 
===Normal===
In Normal Mode, to move the puffle, you either click the space in front or behind of him to move him or use the left and right arrow keys to move him. You may also use A and D for movement (using WASD keys). After getting the first O-berry, you can click the second icon on the top left corner to get a map that gives you a hint to which object you move to get the bunch of O-berries. You can click on objects to move them and place them in such a way so that you can get the bunch of O-berries. There are 8 levels for non-members and 15 levels for members. There are three parts for the game:
*Entrance
*Depths (only for members)
*Legend (only for members)

===Extreme===
Extreme Mode is the same as Normal Mode except there is a timer on the key, O-berry, and the bunch of berries. If you are too slow, they will disappear. If the key disappears, the game will automatically restart. To enter extreme mode for a level, you need to get all the O-berries from the normal levels.

==Stamps==
===Easy===
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Bonus Snack stamp]]||Eat a bonus Puffle O.
|-
|[[On a Roll stamp]]||Complete level 4.
|-
|[[Puzzle Pro stamp]]||Complete level 8.
|}

===Medium===
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Puffle O Feast stamp]]||Eat all regular Puffle O's in one set of levels.
|-
|[[Bite in Time stamp]]||Eat a disappearing Puffle O.
|-
|[[Extreme Puzzler stamp]] <small>''(members only)''</small>||Complete level 12.
|-
|[[Master Roller stamp]]  <small>''(members only)''</small>||Complete level 17.
|}

===Hard===
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9;border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!'''Name''' !! Description
|-
|[[Quick Snack stamp]]||Eat all disappearing O's in one level.
|-
|[[Fast Food stamp]]||Eat all disappearing O's in one set of levels.
|-
|[[Epic Roller stamp]] <small>''(members only)''</small>||Complete level 22.
|-
|[[Ice Master stamp (Pufflescape)|Ice Master stamp]] <small>''(members only)''</small>||Complete the final level.
|}

==Glitch==
*On level 5, the movable icicle can end up inside the bottom hill.
*When you exit the game, sometimes, your game can't be saved and you will lose your current progress.

==Gallery==
<gallery widths="120">
File:RollerscapeIntro.png|The old intro for this game.
File:Rollerscapelevelselect.png|The old level selection for the game.
File:Rollerscapelevelselectnew.PNG|The level selection for the game.
File:ConstructionPufflescape.PNG|Construction in the [[Pet Shop]] for Pufflescape during the [[Halloween Party 2011]].
File:Rollerscape.PNG|The original level 1 for the game.
File:PuffleScapeL1.png|Level 1 in the current version.
File:PufflescapePuffleC.PNG|The [[Puffle]] selection screen.
File:Puffle escapes.PNG | A white puffle playing Pufflescape
File:PufflescapeCPT.png|Pufflescape in [[The Club Penguin Times]].
</gallery>

==Sources and references==
{{Reflist}}
{{SWFArchives}}

{{Games}}
[[Category:Games]][[Category:Beta Team Games]]
