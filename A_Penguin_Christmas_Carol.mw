[[File:APCC Cover.PNG|right|thumb|The cover for A Penguin Christmas Carol.]]
'''''A Penguin Christmas Carol''''' is a parody of the book  ''[[Wikipedia:A Christmas Carol|A Christmas Carol]]'', written by Victorian author, [[Wikipedia:Charles Dickens|Charles Dickens]]. It was first available during the [[Christmas Party 2008]]. It returned during the [[Holiday Party 2010]] and [[Holiday Party 2011]].

== Synopsis ==
A rather vicious penguin called [[Scrooge]] refuses to donate to [[Coins for Change]]. After going to bed, a [[ghost]] appears outside. It is his old business partner, [[Jacob Mukluk]]. After a frightful meeting, he is warned he will be haunted by the [[ghosts]] of Christmas Past, Present and Yet to Come. After journeying through time with these ghosts, Scrooge realizes his attitude to others. He decides to correct the errors of his ways and the story finishes.

=== Differences from the parodied book ===
The primary difference between ''Penguin Christmas Carol'' and the real thing is that the Penguin rendition was toned down for a far younger audience. Deaths and illnesses were stripped and family ties were removed for a quicker and easier plot.

{|cellspacing="1" cellpadding="5" border="1" style="text-align: left; width: 100%; margin: 0 auto; border-collapse: collapse;"
|-style="background-color: #99FFCC"
!Club Penguin edition
!The real character
|-
|Mukluk - the business partner - is a mobile spirit who is eternally miserable by his actions.
|Marley - the business partner in Dickens' book -is a ghost shackled by chains, safes, locks, and weights. He struggles to move in even the slightest. These weights were earned by his actions. He, like Mukluk, is miserable.
|-
|The Ghost of Christmas Past reminds CP's Scrooge how much fun he once had amongst friends. He makes him feel remorse as he has changed.
|Dickens' Ghost of Christmas Past takes him to when he first became miserly, and when his girlfriend left him because he was in love with his money and not her. Instead of remorse, this Ghost showed how Scrooge came to be.
|-
|The Ghost of Christmas Present takes the Club Penguin Scrooge to a Coins for Change alms bucket, and sends him on a guilt trips as he watches even the impoverished make a donation while he spares not even one coin.
|In the real book, the Ghost of Christmas Present shows Scrooge his employee's penniless home and terrible life, even though they manage to eek out a loving Christmas.
|-
|In the Club Penguin book, Tiny Fin is in perfect health, although he is still smaller and kinder than the rest of the group, and he still feels for Scrooge.
|Dickens' Tiny Tim is terminally ill and has but weeks to live. He still loves Scrooge in all his innocence for paying for their meals. He could be cured if Scrooge funded his healthcare.
|-
|The Ghost of Christmas Yet to Come makes a very short appearance and explains that Scrooge, in the short future, will be alone and unloved. His greed separated him from society.
|In Dickens' book, Scrooge is taken to the deep future, to his death, where he learns that his home was looted, his estate taken (because he left no will), and his grave dishonored as he was just thrown in, unloved and with little funeral. Like CP's Scrooge, it was his fault that he ended as he did.
|-
|After having reformed, the CP Scrooge decorated his igloo, made amends with what few friends he had in his [[Buddy List|list]], danced alongside his employee, and donated a chunk of his fortune to Coins for Change. He then shows mercy when he showed up late to work the next day, gives the penguin a raise, and decides to ease his workload with more employees, ensuring he has enough salary to live by.
|In ''A Christmas Carol'', the reformed Scrooge pardons his employee's debt, pays to cure Tiny Tim, and joins them in a mighty Christmas feast, ensuring they are never poor again.
|-
|The CP Scrooge's paranormal encounters occur at 1:00 PM over the course of Christmas Day.
|Dickens' Scrooge has the fright of his life at 1:00 ''AM'', Christmas Day.
|-
|In the Club Penguin book, Tiny Fin and Scrooge's employee are not related.
|''A Christmas Carol'' has Tiny Tim and Scrooge's employee as ailing son and father, respectively.
|-
|The past and future in the CP book are roughly a year behind and a year ahead, respectively.
|Scrooge's past and future in the Dickens book are many decades before and at his death.
|-
|The Present shown in ''A Penguin Christmas Carol'' is near an alms bucket in Coins for Change.
|The Present shown in the Dickens classic is at Scrooge's employee's house.
|-
|In Club Penguin's tale, the future of the unreformed Scrooge does not include the death of anyone, be it Scrooge or Fin. Scrooge is left alone and unloved the entire Christmas season of that future.
|Dickens' book shows that Tiny Tim dies mere weeks (or months) after Christmas Present, and Scrooge himself dies unloved, and his properties are stolen.
|-
|It is not explained why the Club Penguin Scrooge needs a cane, has a beard, or dresses like a senior citizen. The audience is not provided with a description of the young Scrooge: the picture showing Scrooge viewing his past has a penguin that bares the same scarf as him, but they are barely even the same color, otherwise. Assuming this is one year prior to Scrooge's present, the current Scrooge's appearance does not make sense.
|Dickens' Scrooge brought his "old man" appearance on himself. Years of stress, bitterness, refusal to pay for care, and simple aging required him to take a cane. He was too cheap to have a barber cut his hair, thus the beard. Lastly, he is over sixty years of age, explaining the limping, wrinkles, and old language.
|-
|Club Penguin's Scrooge refuses to pay for a heating system. Since he is a penguin, the cold does not harm him in the slightest.
|The Scrooge in ''A Christmas Carol'' is plenty warm, since he has a roaring fireplace and, when working, uses all the coal for his room and none for his employee.
|-
|''A Penguin Christmas Carol'' uses simple words and a short plot to entertain younger children, leaving little for older people to analyze or indulge in. Like the Dickens novel, though, the theme is universal.
|Dickens' classic uses elevated diction, difficult words, and signs of the times that would baffle the Club Penguin audience without explanation, and even then. However, the theme is universal.
|-
|}

Both end with the same lessons: ''"it is better to give than to receive, charity is a righteous cause that should be practiced, mercy does good for the soul, greed is a corrupting thing, and Christmas should be a time of joy, not sadness"''.

== Characters ==
*[[Scrooge]]
*[[Bambadee]]
*[[Smulley]]
*[[Suneroo]]
*[[Jacob Mukluk]]
*[[The Three Ghosts]]

== See also ==
* [[Exclusive Books]]

== SWF ==
*''[http://media1.clubpenguin.com/play/v2/content/local/en/books/xmas_book1.swf A Penguin Christmas Carol]''

{{Books}}
[[Category:Books]][[Category:Printed Media]]
[[pt:Um Conto de Natal de Pinguim]]
