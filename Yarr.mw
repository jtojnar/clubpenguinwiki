{{FAOTW}}
{{CharacterInfobox
|name = Yarr
|image = File:YarrSmile.PNG
|imagesize = 150px
|fullname = Yarr
|species = Puffle
|position = [[Rockhopper]]'s Puffle 
|appeared = As [[Rockhopper]]'s puffle when he found him stuck on floating driftwood 
|color = Red
|clothes = None
|related = Unknown
|friends = [[Rockhopper]], [[Bambadee]], [[Blast]]
|walk = Yes (when he is walked)
}}
:''Were you looking for [[Yarr (item)|the item]] used by [[Rockhopper]]?''

'''Yarr''' is [[Rockhopper]]'s [[Red Puffle]]. He was the first [[Red Puffle]] ever seen on [[Club Penguin Island]]. Yarr can be seen on top of the mast of the [[Migrator]]. Yarr was named so because he always responded to [[Rockhopper]] when he shouted "Yarr". Yarr is one the most popular [[puffle]]s in [[Club Penguin]], along with the [[Keeper of the Boiler Room]]. [[Penguin]]s have been seen attempting to speak to him. When [[Rockhopper]] landed in his lifeboat, many [[penguins]] were seen standing under Yarr and pretending to be him. Yarr loves to go surfing in [[Catchin' Waves]] and is an adventurous [[puffle]]. Yarr's design was changed in 2013.
[[File:Rh yarr card.png|thumb|Rockhopper's player card when walking Yarr.]]

==Surfing==
Ever since Yarr's passion for [[Catching Waves|surfing]] was discovered, the [[Club Penguin Team]] added a feature that allowed [[penguin]]s to surf with their [[red puffles]] if they walked them to [[Catchin' Waves]]. It has also been found that [[Rockhopper]] wants to get Yarr a silver surfboard for [[Christmas]], as he says it on the notice board in the [[Captain's Quarters]]. You can sometimes see Yarr surfing in front of the [[Migrator]] when it's coming towards [[Club Penguin Island]].

==Appearances==
*Yarr appears in the book, [[The Great Puffle Switch]].
*Yarr is featured in the [[Rockhopper Plant Background]].
*Yarr appeared in the video [[The Party Starts Now]].
*Yarr also appeared in the video [[Rockhopper vs. Mighty Squid!!]]

==Gallery==
<gallery captionalign="left">
Image:Yarr Surfing.jpg|Yarr, surfing.
Image:Telescope_yarr.PNG|Yarr looking through a telescope.
Image:YarrPlayer.png|Yarr on [[Rockhopper]]'s [[Player Card]].
Image:Yarr_sleeping.PNG|Yarr, sleeping.
Image:Yarr_christmas.PNG|Yarr at Christmas time.
Image:5020.png|[[Yarr (item)|Yarr's]] inventory icon.
File:Yarr (3).png
File:YarrShortClip.PNG|Yarr on the short clip [[Rockhopper vs. Mighty Squid!!]].
File:YarrNew.PNG|Yarr in the image icon
File:Yarr2013.PNG
File:YarrMedievalParty2013.PNG|Yarr on a [[Medieval Party 2013]] ad
File:YarrSmile.PNG|Yarr smiling
File:YarrExcited.PNG
Image:YarrPlayer2013.png|Yarr on the new [[Rockhopper]]'s [[Player Card]].
</gallery>

==Trivia==
*He is from the [[Rockhopper Island]] along with all the others [[red puffle|red puffles]].
*When [[Rockhopper]] arrived in February 2009 and May 2009, Yarr couldn't be seen on the [[Migrator]] because [[Rockhopper]] was seen walking Yarr on his [[Player card]].
*If anyone slides his/her mouse cursor over the [[Migrator]] when on the [[Club Penguin]] homepage, they would see Yarr jump along the [[Crow's Nest]].
*When [[Rockhopper]] throws a [[snowball]], Yarr pulls out a small telescope.
*According to the 229th issue of [[The Club Penguin Times]], [[Rockhopper]] said Yarr went missing on March 4, 2010, as soon as the [[Migrator]] docked onto the [[Beach]].
*He is friends with [[Blast]].
*Some [[penguins]] were confused when Yarr went missing, due to [[Rockhopper]] leaving a day before [[Puffle Rescue]] was released.
*Yarr can speak just one word: his name. This is proved in [[Rockhopper vs. Mighty Squid!!]].

==See Also==
*[[Rockhopper]]
*[[Yarr (item)]]
*[[Lolz]]


{{Characters}}
{{Famous puffles}}

[[Category:Creatures]]
[[Category:Rockhopper]]
[[Category:Pets]]
[[Category:Puffles]]
[[Category:Characters]]
[[pt:Yarr]]
