{{Archive}}

{{Succession box|July 2014 Penguin Style|September 2014 Penguin Style}}

<center>The August 2014 ''[[Penguin Style]]'' catalog was released on August 6, 2014 and was available until September 3, 2014.</center>

== Cover ==
[[File:PenguinStyleAug2014.PNG|center|250px]]

__NOTOC__

== Items ==
=== Colors ===
<gallery widths="60" heights="60">
File:BlueClothingIcon.PNG|[[Blue]]
File:GreenClothingIcon.png|[[Green]]
File:PinkClothingIcon.png|[[Pink]]
File:PurpleClothingIcon.png|[[Dark Purple]]
File:BrownClothingIcon.png|[[Brown]]
File:PeachClothingIcon.png|[[Peach]]
File:LightBlueClothingIcon.png|[[Light Blue]]
File:LimeGreenClothingIcon.png|[[Lime Green]]
File:AquaClothingIcon.png|[[Aqua]]
File:RedClothingIcon.png|[[Red]]
File:OrangeClothingIcon.png|[[Orange]]
File:YellowClothingIcon.png|[[Yellow]]
File:BlackClothingIcon.png|[[Black]]
File:DarkGreenClothingIcon.png|[[Dark Green]]
File:ArcticWhiteClothingIcon.png|[[Arctic White]]
</gallery>

=== Backgrounds ===
<gallery>
File:SnowflakesBackground(ID9274).PNG|[[Snowflakes Background (ID 9274)|Snowflakes Background]]
File:SummerSnowmanBackground.PNG|[[Summer Snowman Background]]
File:PatioViewBackground.PNG|[[Patio View Background]]
File:IceBackground.png|[[Ice Background]]
File:MusicCruiseBackground.PNG|[[Music Cruise Background]]
File:AnchorsAweighBackground.PNG|[[Anchors Aweigh Background]]
File:Melody Background.png|[[The Melody Background]]
File:MusicJamGuitarsBG.PNG|[[Music Jam Guitars Background]]
</gallery>

=== Items for everyone ===
<gallery>
File:Star T-Shirt.png|[[Star T-Shirt]]
File:GreenBallCap.png|[[Green Ball Cap]]
File:BaseballGlove.png|[[Baseball Glove]]
File:Bandana.PNG|[[Red Bandana]]
File:Pink Purse.png|[[Pink Purse]]
File:TheFunster.png|[[The Funster]]
File:ButterflyT-Shirt.png|[[Butterfly T-Shirt]]
</gallery>

=== Member clothes ===
<gallery>
File:TheCrownedQueen.PNG|[[The Crowned Queen]]
File:Elsa'sCoronationDress.PNG|[[Elsa's Coronation Dress]]
File:TheCoronation.PNG|[[The Coronation]]
File:CoronationNecklace.PNG|[[Coronation Necklace]]
File:Anna'sCoronationDress.PNG|[[Anna's Coronation Dress]]
File:TheIceSeller.PNG|[[The Ice Seller]]
File:Kristoff'sOutfit.PNG|[[Kristoff's Outfit]]
File:kristoff'sBoots.PNG|[[Kristoff's Boots]]
File:SvenCostume.PNG|[[Sven Costume]]
File:SvenHooves.PNG|[[Sven Hooves]]
File:TheWinterTraveler.PNG|[[The Winter Traveler]]
File:IcyEyelashes.PNG|[[Icy Eyelashes]]
File:Anna'sTravelingClothes.PNG|[[Anna's Traveling Clothes]]
File:Anna'sTravelingShoes.PNG|[[Anna's Traveling Shoes]]
File:Olaf'sCostume.PNG|[[Olaf's Costume]]
File:MarshmallowCostume.PNG|[[Marshmallow Costume]]
File:TheIceQueen.PNG|[[The Ice Queen]]
File:RoyalEyelashes.PNG|[[Royal Eyelashes]]
File:Elsa'sIceQueenDress.PNG|[[Elsa's Ice Queen Dress]]
File:Elsa'sIceQueenShows.PNG|[[Elsa's Ice Queen Shoes]]
File:TrollMask.PNG|[[Troll Mask]]
File:TrollCape.PNG|[[Troll Cape]]
File:TrollCostume.PNG|[[Troll Costume]]
File:TrollFeet.PNG|[[Troll Feet]]
File:TheOaken.PNG|[[The Oaken]]
File:Oaken'sBunad.PNG|[[Oaken's Bunad]]
File:Oaken'sShoes.PNG|[[Oaken's Shoes]]
File:TheThirteenthSon.PNG|[[The Thirteenth Son]]
File:Hans'Uniform.PNG|[[Hans' Uniform]]
File:Hans'Boots.PNG|[[Hans' Boots]]
File:IceCreamVendor.PNG|[[Ice Cream Vendor]]<br>''Penguin at Work''
File:WideAwakeEyes.PNG|[[Wide Awake Eyes]]
File:KittyCatEyes.PNG|[[Kitty Cat Eyes]]
File:DivaGlamEyes.PNG|[[Diva Glam Eyes]]
File:StarletEyes.PNG|[[Starlet Eyes]]
</gallery>

=== “Build your own Hoodie” items ===
<gallery>
File:Clothing4500.PNG|[[Custom Hoodie|Black Hoodie with white dots]]
File:Clothing4582.PNG|[[Custom Hoodie|Black Hoodie with red and yellow splatter]]
File:Clothing4583.PNG|[[Custom Hoodie|Black Hoodie with white stars]]
File:Clothing4584.PNG|[[Custom Hoodie|Black Hoodie with white stripes]]
File:Clothing4585.PNG|[[Custom Hoodie|Black Hoodie with Black Puffle in fire]]
File:Clothing4495.PNG|[[Custom Hoodie|White Hoodie with black dots]]
File:Clothing4586.PNG|[[Custom Hoodie|White Hoodie with red and yellow splatter]]
File:Clothing4587.PNG|[[Custom Hoodie|White Hoodie with black stars]]
File:Clothing4588.PNG|[[Custom Hoodie|White Hoodie with grey stripes]]
File:Clothing4589.PNG|[[Custom Hoodie|White Hoodie with Black Puffle in fire]]
File:Clothing4591.PNG|[[Custom Hoodie|Yellow Hoodie with black dots]]
File:Clothing4590.PNG|[[Custom Hoodie|Yellow Hoodie with red and yellow splatter]]
File:Clothing4592.PNG|[[Custom Hoodie|Yellow Hoodie with white stars]]
File:Clothing4593.PNG|[[Custom Hoodie|Yellow Hoodie with white stripes]]
File:Clothing4594.PNG|[[Custom Hoodie|Yellow Hoodie with Black Puffle in fire]]
File:Clothing4595.PNG|[[Custom Hoodie|Orange Hoodie with black dots]]
File:Clothing4596.PNG|[[Custom Hoodie|Orange Hoodie with red and yellow splatter]]
File:Clothing4597.PNG|[[Custom Hoodie|Orange Hoodie with white stars]]
File:Clothing4598.PNG|[[Custom Hoodie|Orange Hoodie with white stripes]]
File:Clothing4599.PNG|[[Custom Hoodie|Orange Hoodie with Black Puffle in fire]]
File:Clothing4600.PNG|[[Custom Hoodie|Red Hoodie with black dots]]
File:Clothing4601.PNG|[[Custom Hoodie|Red Hoodie with red and yellow splatter]]
File:Clothing4602.PNG|[[Custom Hoodie|Red Hoodie with white stars]]
File:Clothing4603.PNG|[[Custom Hoodie|Red Hoodie with white stripes]]
File:Clothing4604.PNG|[[Custom Hoodie|Red Hoodie with Black Puffle in fire]]
</gallery>

=== “LAST CHANCE!” items ===
<gallery>
File:CruiseCaptainHat.PNG|[[Cruise Captain Hat]]
File:CruiseCaptainJacket.PNG|[[Cruise Captain Jacket]]
File:TheReplay.PNG|[[The Replay]]
File:BeachDress.png|[[Beach Dress]]
File:CottonSandals.PNG|[[Cotton Sandals]]
File:TheFlowerChild.PNG|[[The Flower Child]]
File:HipsterThreads.png|[[Hipster Threads]]
File:ThePunkedOutNew.PNG|[[The Punked Out]]
File:CityLightsT-Shirt.PNG|[[City Lights T-Shirt]]
File:PartyBracelets.PNG|[[Party Bracelets]]
File:PunkFluffyOutfit.PNG|[[Punk Fluffy Outfit]]
File:Brady'sShoes.PNG|[[Brady's Shoes]]
File:ThePunkNew.PNG|[[The Punk]]
File:Microphone.PNG|[[Microphone]]
File:TheSummerJam.PNG|[[The Summer Jam]]
File:SummerSuit.PNG|[[Summer Suit]]
File:SandyFlipFlops.PNG|[[Sandy Flip Flops]]
File:DJHeadphones.PNG|[[DJ Headphones]]
File:IndieRockerOutfit.PNG|[[Indie Rocker Outfit]]
File:LooseToque.PNG|[[Loose Toque]]
File:SharkCostume.PNG|[[Shark Costume]]
File:SushiandSoda.PNG|[[Sushi and Soda]]
File:CruiseHost.PNG|[[Cruise Host]]
File:TheFrontman.PNG|[[The Frontman]]
File:RescueBuoy.PNG|[[Rescue Buoy]]
File:LifesaverOutfit.PNG|[[Lifesaver Outfit]]
File:IndieRockerGlasses.PNG|[[Indie Rocker Glasses]]
File:StripedSkaterThreads.PNG|[[Striped Skater Threads]]
File:Blue&WhiteFlipFlops.PNG|[[Blue & White Flip Flops]]
File:TheRows.PNG|[[The Rows]]
File:Pop-n-LockMusicShirt.PNG|[[Pop-n-Lock Music Shirt]]
File:UntiedVioletSneakers.PNG|[[Untied Violet Sneakers]]
</gallery>

=== Flags ===
<gallery widths="60" heights="60">
File:Argentina flag.png|[[Argentina flag|Argentina]]
File:Australia.png|[[Australia flag|Australia]]
File:AustriaFlag.PNG|[[Austria Flag|Austria]]
File:Belgium flag.PNG|[[Belgium flag|Belgium]]
File:Belize flag.PNG|[[Belize flag|Belize]]
File:Brazil flag.PNG|[[Brazil flag|Brazil]]
File:CanadaFlag.png|[[Canada flag|Canada]]
File:Chile flag.PNG|[[Chile flag|Chile]]
File:China flag.PNG|[[China flag|China]]
File:Columbia.png|[[Colombia flag|Colombia]]
File:540.png|[[Costa Rica flag|Costa Rica]]
File:CroatiaFlag.PNG|[[Croatia flag|Croatia]]
File:CzechRepublicFlag.PNG|[[Czech Republic flag|Czech Republic]]
File:Denmark Flag.PNG|[[Denmark flag|Denmark]]
File:DominicanRepublicFlag.PNG|[[Dominican Republic flag|Dominican Republic]]
File:EcuadorFlag.PNG|[[Ecuador flag|Ecuador]]
File:Egypt Flag.PNG|[[Egypt flag|Egypt]]
File:Finland flag.PNG|[[Finland flag|Finland]]
File:France flag.PNG|[[France flag|France]]
File:Germany flag.PNG|[[Germany flag|Germany]]
File:GibraltarFlag.PNG|[[Gibraltar flag|Gibraltar]]
File:GreeceFlag.PNG|[[Greece flag|Greece]]
File:Guatemala flag.PNG|[[Guatemala flag|Guatemala]]
File:Haiti flag.png|[[Haiti flag|Haiti]]
File:Hungary flag.PNG|[[Hungary flag|Hungary]]
File:India flag.png|[[India flag|India]]
File:IrelandFlag.PNG|[[Ireland flag|Ireland]]
File:Israel Flag.PNG|[[Israel flag|Israel]]
File:Italy flag.PNG|[[Italy flag|Italy]]
File:Jamaica Flag.PNG|[[Jamaica flag|Jamaica]]
File:Japanflag.PNG|[[Japan flag|Japan]]
File:SouthKoreaFlag.png|[[Korea flag|Korea]]
File:LatviaFlag.PNG|[[Latvia flag|Latvia]]
File:LiechtensteinFlag.PNG|[[Liechtenstein Flag|Liechtenstein]]
File:Malaysiaflag.PNG|[[Malaysia flag|Malaysia]]
File:MaltaFlag.PNG|[[Malta flag|Malta]]
File:Mexico flag.PNG|[[Mexico flag|Mexico]]
File:Netherlands flag.PNG|[[Netherlands flag|Netherlands]]
File:New Zealand flag.PNG|[[New Zealand flag|New Zealand]]
File:Norway Flag.PNG|[[Norway flag|Norway]]
File:PakistanFlag.PNG|[[Pakistan flag|Pakistan]]
File:Peru.png|[[Peru flag|Peru]]
File:Philippines.png|[[Philippines flag|Philippines]]
File:Poland flag.PNG|[[Poland flag|Poland]]
File:Portugal flag.PNG|[[Portugal flag|Portugal]]
File:537.png|[[Puerto Rico flag|Puerto Rico]]
File:Romania flag.PNG|[[Romania flag|Romania]]
File:Russia flag.PNG|[[Russia flag|Russia]]
File:SingaporeFlag.png|[[Singapore flag|Singapore]]
File:SloveniaFlag.PNG|[[Slovenia flag|Slovenia]]
File:South Africa flag.PNG|[[South Africa flag|South Africa]]
File:Spain flag.PNG|[[Spain flag|Spain]]
File:Sweden flag.PNG|[[Sweden flag|Sweden]]
File:Switzerland flag.PNG|[[Switzerland flag|Switzerland]]
File:Turkey flag.PNG|[[Turkey flag|Turkey]]
File:Great Britain flag.PNG|[[United Kingdom flag|United Kingdom]]
File:United States flag.png|[[United States flag|United States]]
File:UruguayFlag.PNG|[[Uruguay flag|Uruguay]]
File:VenezuelaFlag.png|[[Venezuela flag|Venezuela]]
</gallery>

== Secret items ==
=== Build your own hoodie! ===
* Click the Purple Penguin’s flipper → [[Viking Helmet]]
:* Repeat the steps to get the Viking Helmet 3 times (so you’ll find the Blue Viking Helmet on the fourth time) → [[Blue Viking Helmet]]

=== LAST CHANCE! items ===
* Click on the Denim Purse → [[Denim Purse]]
* Click on the Fish Necklace → [[14K Fish Necklace]]
* Click the pink penguins’ flipper → [[Green Slouch Purse]]
* Click the cream soda → [[Gold Charm Necklace]]

== Gallery ==
=== Artwork ===
<gallery>
File:Anna1.PNG|Anna
File:Elsa1.png|Elsa
File:Hans1.PNG|Hans
File:Kristoff1.png|Kristoff
File:Marshmallow1.PNG|Marshmallow (Frozen)
File:Oaken1.PNG|Oaken
File:Olaf1.PNG|Olaf
File:Sven1.PNG|Sven
File:Troll1.PNG|Troll
</gallery>

== SWF ==
* [[archives:Media:ENCataloguesClothingAugust2014.swf|August 2014 Penguin Style]]


{{Penguin Style}}

[[Category:Penguin Style]]

[[pt:Estilo Pinguim Agosto 2014]]
