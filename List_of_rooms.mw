{{seeideas|rooms}}
{{Under Construction}}
{{List|Rooms}}
[[File:Coffee Shop.JPG|thumb|The [[Coffee Shop]] - the very first room in [[Club Penguin]].]]

'''Rooms''' are the places in [[Club Penguin]]. Click [[Map:Club Penguin|here]] to travel through the rooms in Club Penguin via an interactive [[map]]. There are many different rooms in [[Club Penguin]], most for both [[members]] and [[non-member]]s. There are some secret rooms that only members can access. Those member-only rooms usually appear during [[parties]] or other special events.

==Rooms accessible to all players==
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%; text-align: center;"
!! colspan="2" |'''Room'''!!'''Tour Guide'''!!'''ID'''
|-
|[[File:Beach2012.PNG|80px]]||[[Beach]]||This relaxing place is the Beach. You can lounge in a beach chair... and build snow castles. Sometimes the Migrator shows up!||400
|-
|[[File:Beacon1.png|80px]]||[[Beacon]]||The Beacon has a giant spotlight... and a telescope you can look through. What IS that in the distance? And if you thought penguins can't fly... try Jet Pack Adventure.||411
|-
|[[File:BoilerCurrent.png|80px]]||[[Boiler Room]]||This lovely spot is the Boiler Room. It's where all the old issues... of the newspaper are stored. You can read up on the island history... or just stare at the steam pipe!||804
|-
|[[File:BookCurrent.png|80px]]||[[Book Room]]||These are the CP Times offices. Here you can peruse the library... check out the striking wall art... and pass the time with Mancala... while you wait for news to roll in!||111
|-
|[[File:Box Dimension Normal.png|80px]]||[[Box Dimension]]||You've entered another dimension... The Box Dimension! This place is the source of all boxes. Please leave the laws of physics at the door.||811
|-
|[[File:Captain's Quarters.PNG|80px]]||[[Captain's Quarters]]||This is the Captain's Quarters! Rockhopper spends a lot of time here. Look at the corkboard for his photos! Or, grab a friend and hunt for some treasure!||422
|-
|[[File:Cave.png|80px]]||[[Cave]]||Ever seen an underwater pool? Well now you have! You can swim laps... or enjoy the undersea life... that passes by the windows.||806
|-
|[[File:New Gift Shop.png|80px]]||[[Clothes Shop]]||This is our Clothes Shop. Check out the latest fashions... and show them off on the runway!||130
|-
|[[File:PuffleParty2013CloudForest.PNG|80px]]||[[Cloud Forest]]||It's the majestic rainbow puffle! Best discovery EVER! Must. Tell. Everyone!||433
|-
|[[File:CoffeeCurrent.png|80px]]||[[Coffee Shop]]||Welcome to the Coffee Shop! Here you'll find the best cups of coffee on the island... and also smoothies, tea, and pickle juice! Try out the Smoothie Smash or Bean Counters games... or head upstairs for the Club Penguin Times Newsroom.||110
|-
|[[File:NewCove.PNG|80px]]||[[Cove]]||This secluded spot is the Cove. Here you can swim by the shore... grab a board and catch some waves... or throw a beach party! Just make sure there's a lifeguard!||810
|-
|[[File:Nest.png|80px]]||[[Crow's Nest]]||Welcome to the Crow's Nest! That snowman looks a lot like someone I know...||423
|-
|[[File:Dock2012.PNG|80px]]||[[Dock]]||This bit of shoreline is the Dock. Here you can take a ride on a tube... behind a speedboat in Hydro-Hopper! It's as crazy as it sounds... and as fun!
||800
|-
|[[File:Dojo20132.PNG|80px]]||[[Dojo]]||Welcome to the Dojo! Card-Jitsu takes time to master... but Sensei is here to teach you. The elements of Fire, Water, and Snow... can be yours to master!||320
|-
|[[File:DojoExt2013.PNG|80px]]||[[Dojo Courtyard]]||This is the Dojo Courtyard. Enter the Dojo to play Card-Jitsu... and learn the ways of the ninja!||321
|-
|[[File:AgentCom2015.PNG|80px]]||[[EPF Command Room]]||Agents this is the Command Room. Defend the mainframe in System Defender. Hone your skills with Spy Drills. Be ready to protect the island. We are the best of the best.||323
|-
|[[File:EverydayPhoningFacilityNew.PNG|80px]]||[[Everyday Phoning Facility]]||TThis is the Everyday Phoning Facility, which is ACTUALLY a secret entrance to Elite Penguin Force Command. That's still technically classified info. But I think you got the right stuff to be an agent. Hit that target with a snowball to volunteer.||211<br>212
|-
|[[File:Fire Dojo complete.png|80px]]||[[Fire Dojo]]||This hot spot is the Fire Dojo. Here Card-Jitsu ninjas... learn to master the element of Fire. Talk to Sensei to begin your training.||812
|-
|[[File:Forest2014.PNG|80px]]||[[Forest]]||Here we are in the Forest. It's a great place to enjoy nature. But watch out for the wildlife! Go down those stairs to go to the Cove. Or head up to the university... and learn something .||809
|-
|[[File:Hidden Lake Without Pin.png|80px]]||[[Hidden Lake]]||This is the Hidden Lake. Guess it's not so hidden anymore! Look at all this treasure! You'll need a key for that water door. Play Puffle Rescue to find it.||814
|-
|[[File:GoldMine.PNG|80px]]||[[Gold Mine]]||This is the Gold Mine. Rumor is if you dig up gold nuggets... you can find the legendary gold puffle! If you think you have what it takes... head to the Pet Shop to start the quest||813
|-
|[[File:Current Iceberg.png|80px]]||[[Iceberg]]||Here we are on the Iceberg. It might not look like much... but it's a jackhammering hot spot! You can also dive for treasure... in the Aqua Grabber!||805
|-
|[[File:Basic Igloo.PNG|80px]]||[[Igloos]]||N/A||N/A
|-
|[[File:Lighthouse.PNG|80px]]||[[Lighthouse]]||Here's the island's tallest building!The Lighthouse has a stage... and a picture gallery... and, um, random storage. Go upstairs to light the Beacon!||410
|-
|[[File:Attic2015.PNG|80px]]||[[Lodge Attic]]||This is the Mystery Attic. It's full of strange objects... from the mysteries of Club Penguin... there's even a Box Portal by the buoy.||221
|-
|[[File:DL DL.png|80px]]||[[Lounge]]||This is the Arcade, where fun happens! You can play video games... like Bits & Bolts, Thin Ice, or Astro Barrier... or throw snowballs at targets.||121
|-
|[[File:OperationPufflePreMine.PNG|80px]]||[[Mine]]||This is the Mine. Puffles like exploring tunnels... and sometimes get trapped inside. They need your help to rescue them!||808
|-
|[[File:MineShack2014.PNG|80px]]||[[Mine Shack]]||Welcome to Club Penguin University! Chill out on the lawn... start a game of volleyball... or head inside to class. When school gets out, head down to the Mine... for a game of Cart Surfer!||807
|-
|[[File:New Night Club.png|80px]]||[[Night Club]]||If you like music and moving... then you'll love the Dance Club! Here you can DJ tune... or bust moves on the floor.||120
|-
|[[File:PetShop2013.PNG|80px]]||[[Pet Shop]]||In the Pet Shop you can adopt puffles. They have different colors and traits. Play Puffle Launch, Pufflescape... Puffle Roundup. Or pamper them in the Puffle Hotel!||310
|-
|[[File:Migrator.png|80px]]||[[Pirate Ship (room)|Pirate Ship]]||This is the pirate ship, the Migrator. Arrrr! Rockhopper sails the seas in this ship.||420
|-
|[[File:New Pizza Parlor.png|80px]]||[[Pizza Parlor]]||Mmm... Pizza Parlor! Here you can grab a delicious slice... or make pizzas in the Pizzatron game|. The stage is great for entertaining... or go behind the counter and help out!
||330
|-
|[[File:Plaza2015.PNG|80px]]||[[Plaza]]||This is the Plaza! Grab a snack at the Pizza Parlor... or hang out at the Mall. Adopt a puffle from the Pet Shop... then play in the Puffle Park... and pamper them in the Puffle Hotel.||300
|-
|[[File:Mall.PNG|80px]]||[[Puffle Berry Mall]]||This is Puffle Berry Mall. A great place to meet friends... or just ride the escalators. Costumes are available here too!||340
|-
|[[File:PuffleParty2013Lobby.PNG|80px]]||[[Puffle Hotel Lobby]]||Welcome to the Puffle Hotel! Treat your puffle to food, toys and hats... get pampered at the Puffle Spa... or head upstairs to relax by the pool.||430
|-
|[[File:PuffleParty2013Roof.PNG|80px]]||[[Puffle Hotel Roof]]||This is the rooftop of the Puffle Hotel. Treat your pet to a delicious snack... go for a dip or relax by the poolside... Members can launch into the Cloud Forest!||432
|-
|[[File:PuffleParty2013Spa.PNG|80px]]||[[Puffle Hotel Spa]]||Here your puffle can enjoy a workout... on the treadmill or at a step class... Then indulge them with a spa treatment. Careful not to get soap in your eyes!||431
|-
|[[File:PufflePark-2.PNG|80px]]||[[Puffle Park]]||Welcome to the Puffle Park. It's made just for you and your puffle. You can go on a slide or zipline... or wash up at the hydrant! The cat and dog puffles... especially love this spot!||890
|-
|[[File:PuffleWild2.PNG|80px]]||[[Puffle Wild (room)|Puffle Wild]]||Wild puffle creatures roam here. Keep your eyes out for them... we might even see a unicorn puffle! This table is their feeding area... so don't leave your snacks there.||436
|-
|[[File:School-Thursday.PNG|80px]]||[[School]]||Class is in session! Everyone take your seats What are we studying today? When it's time for lunch... head into the cafeteria and check out the daily lunch special!||122
|-
|[[File:Ship Hull.PNG|80px]]||[[Ship's Hold]]||Here is the ship's cargo hold. This is where Rockhopper sells his rare items.||421
|-
|[[File:Skatepark.PNG|80px]]||[[Skatepark]]||Welcome to the Skatepark! A place to learn tricks. And show off sweet skills!||435
|-
|[[File:Mtn2014.PNG|80px]]||[[Ski Hill]]||If you like to sled, come to Ski Hill! I guess it should be called Sled Hill. Bunny Hill is for newcomers. Ridge Run is only for pros. The drop is almost straight down!||230
|-
|[[File:Lodge2015.PNG|80px]]||[[Ski Lodge]]||Welcome to the new Puffle Lodge! You can join the Puffle Guides... explore the Mystery Attic upstairs... or stay and play Find Four and Ice Fishing!||220
|-
|[[File:Village2015.PNG|80px]]||[[Ski Village]]||Here we have the Ski Village. Race down the Ski Hill or check out the EPF... that's Everyday Phoning Facility of course. Puffle creatures can be adopted in the Wilds... just through the Puffle Lodge!||200
|-
|[[File:SnowDojo.PNG|80px]]||[[Snow Dojo]]||This mountain peak is the Snow Dojo. The time to master Snow has come. Head across the bridge to battle snow minions. You are ready brave ninja!||326
|-
|[[File:Forts2012.PNG|80px]]||[[Snow Forts]]||These forts are for snowball fights. Some great battles have happened here! The clock shows Penguin Standard Time. Hit the target and watch it go crazy!||801
|-
|[[File:Stadium2014.PNG|80px]]||[[Stadium]]||Welcome to the Stadium! Score goals for your team... before time runs out or cheer them on in the stands!||802

|-
|[[File:Town2012.PNG|80px]]||[[Town]]||Here is the Town Center. It's always a busy, exciting place and a great spot to meet friends... or be a Tour Guide!||100
|-
|[[File:Underwater_room.png|80px]]||[[Underwater]]||BLUB! This place is Underwater... Look at all the colorful fish and plants! And treasure! This place sure is unusual....||815
|-
|[[File:DojoWaterCurrent.png|80px]]||[[Water Dojo]]||Welcome to the Water Dojo. Water ninjas earn their suits here... by battling in Card-Jitsu. Some say they're the strongest ninjas. Others say they're all wet!||816
|}

==Former Rooms==
{|border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%; text-align: center;"
!! colspan="2" |'''Room'''!!'''Tour Guide'''!!'''Closed on'''!!'''ID'''
|-
|[[File:VR Room.png|80px]]||[[Agent VR]]||This is called the VR Room. VR stands for Virtual Reality. Here you can see what the old HQ looked like, before it was destroyed by a popcorn device.||November 15, 2012||213
|-
|[[File:Mmine_Cave.png|80px]]||[[Cave Mine]]||Here we have the Mine Cave... Some workers say they've found buried treasure... So grab a hardhat and start digging!||November 6, 2013||813
|-
|[[File:Command room.png|80px]]||[[Command Room]]||Welcome to the Command Room! Only EPF agents can access this room... Remember... this room is top secret!||May 18, 2010||323
|-
|[[File:New HQ command.PNG|80px]]||[[HQ]]||Whoa! What happened in here? It looks like a popcorn tornado hit the HQ, I think we should report in immediately!||May 27, 2010||803
|-
|[[File:Ninjahideout.png|80px]]||[[Ninja Hideout]]||Welcome to the Ninja Hideout. Here we can learn to master the elements. The glowing tablets open the Elemental Dojos.... where everyone can play Card-Jitsu Fire and Water.||May 22, 2013||322
|-
|[[File:EcoCurrent.png|80px]]||[[Recycling Plant]]||This is the Recycling Plant! Throw your recycling into the Recycletron 3000... and turn rubbish into useful things!||July 11, 2013||122
|-
|[[File:Sportshop.png|80px]]||[[Sports Shop]]||Look at all this popcorn! Popcorn can be a healthy snack for athletes. Hmm, it looks like there's some construction going on today...||May 25, 2010||210
|-
|[[File:Squidward vs ya know who.png|80px]]||[[The Stage]]||The tour guide varies by the play.||June 4, 2015||340
|-
|[[File:WelcomeRoom.PNG|80px]]||[[Welcome Solo]]||N/A||November 2013||112
|}

== List of game rooms ==
*[[Soda Seas]] ([[Aqua Grabber]])
*[[Clam Waters]] ([[Aqua Grabber]])
*Beacon ([[Jet Pack Adventure]])
*[[Outback Pond]]&nbsp;([[Ice Fishing]])(Secret mission 6-Questions for a Crab)
*Cove ([[Catchin' Waves]])
*Dock ([[Hydro Hopper]])
*[[Pet Shop]] Employees Room&nbsp;([[Puffle Roundup]])
*Coffee Shop ([[Bean Counters]])

== List of mission rooms==
*[[Mine Tunnels]]
*[[Puffle Training Room]]
*[[The Wilderness]]
*[[Tool Shed]]
*[[Coin]] Vault
*[[Outback Pond]]
*[[Gift Shop Office]]
*[[Gift Shop Rooftop]]
*[[Underground Tunnels]]
*[[Gary's room]]
*[[Herbert's Lair]]

== [[Party Rooms]] ==
*[[Treetop Fort]] ([[Medieval Party 2008]], [[Medieval Party 2009|2009]], [[Medieval Party 2010|2010]], [[Medieval Party 2011|2011]] and [[Medieval Party 2012|2012]])
*[[Mega Whale]] ([[Water Party 2008]])
*[[Backstage]] ([[Music Jam 2008]], [[Music Jam 2009|2009]], [[Music Jam 2010|2010]] and [[Music Jam 2011|2011]])
*[[Arcade Circle|Arcade Circle and Bonus Games Room]] ([[Fall Fair 2008]] and [[The Fair 2009]], [[The Fair 2010|2010]] and [[The Fair 2011|2011]])
*[[Secret Lab]] ([[Halloween Party 2008]], [[Halloween Party 2009|2009]] and [[Halloween Party 2011|2011]])
*[[Night Club Rooftop]] ([[Dance-A-Thon]], [[Music Jam 2009]], [[Music Jam 2010|2010]] and [[Music Jam 2011|2011]] and [[Puffle Party 2011]] and [[Puffle Party 2012|2012]])
*[[Puffle Feeding Area]] ([[Puffle Party 2009]], [[Puffle Party 2010|2010]], [[Puffle Party 2011|2011]] and [[Puffle Party 2012|2012]])
*[[Leprechaun House]] ([[St Patrick's Day 2009]])
*[[Backstage]] ([[Penguin Play Awards 2009]] and [[Penguin Play Awards 2010|2010]])
*[[Back Stage]] ([[Music Jam 2008]], [[Music Jam 2009|2009]], [[Music Jam 2010|2010]] and [[Music Jam 2011|2011]])
*[[Box Store]] ([[April Fools' Party 2009]])
*[[Ye Knight's Quest (And Princesses too!)|Knight's Quest]] ([[Medieval Party 2009]], [[Medieval Party 2010|2010]], [[Medieval Party 2011|2011]] and [[Medieval Party 2012|2012]])
#Orb Room
#Target Room
#Maze Rooms (36)
#Lost Room 1
#Lost Room 2
#Lost Room 3
#Final Quest
*[[Tree Forts]] ([[Adventure Party 2009]] and [[Island Adventure Party 2010]] and [[Island Adventure Party 2011|2011]])
*[[Music Maker 3000]] ([[Music Jam 2009]], [[Music Jam 2010|2010]] and [[Music Jam 2011|2011]])
*[[Hot Air Balloon Ride]] ([[Festival of Flight 2009]])
*[[Tallest Mountain (room)|Tallest Mountain]] ([[Festival of Flight 2009]])
*[[Great Puffle Circus Entrance]] ([[The Fair 2009]], [[The Fair 2010|2010]] and [[The Fair 2011|2011]])
*[[Great Puffle Circus]] ([[The Fair 2009]], [[The Fair 2010|2010]] and [[The Fair 2011|2011]])
*[[The Great Snow Maze|Snow Maze Rooms]] (28)
*[[Yeti Cave]] ([[The Great Snow Maze]])
*[[Magic Sleigh Ride]] ([[Holiday Party 2009]], [[Holiday Party 2010|2010]] and [[Holiday Party 2011|2011]])
*[[Cave Expedition]]:
#[[Mine Cave]]
#[[Hidden Lake]]
#[[Underwater]]
*[[Puffle Show]] ([[Puffle Party 2010]], [[Puffle Party 2011|2011]] and [[Puffle Party 2012|2012]])
*[[Silly Place]] ([[April Fools' Party 2010]], [[April Fools' Party 2011|2011]] and [[April Fools' Party 2012|2012]])
*[[Ye Knight's Quest 2]] ([[Medieval Party 2010]], [[Medieval Party 2011|2011]] and [[Medieval Party 2012|2012]])
#Statue Puzzle Room
#Puzzle Door
#Dragon's Drawbridge
#Dragon's Lair
#Dragon's Treasure Trove
*[[Ships]] ([[Island Adventure Party 2010]] and [[Island Adventure Party 2011|2011]])
*[[Casa Fiesta]] ([[Music Jam 2010]] and [[Music Jam 2011|2011]])
*[[Mountain Expedition]] and [[The Great Snow Maze]]:
#[[Supply Camp]]
#[[Ice Cavern]]
#[[Base Camp]]
#[[Mountain (Mountain Expedition)|Mountain]]
#[[Mountain Top]]
#[[Cave (Mountain Expedition)|Cave]]
*[[Classified Area]] ([[The Great Snow Maze]])
*[[Laser Maze]] ([[The Great Snow Maze]])
*[[Halloween Party 2010]] and [[Halloween Party 2011|2011]]:
#[[Haunted House Entrance]]
#[[Dark Swamp]]
#[[Dark Chamber]]
#[[Monster Room]]
*[[???]] ([[Holiday Party 2010]])
*[[Wilderness Expedition]]:
#[[Glade]]
#[[Wilderness (room)|Wilderness]]
#[[Cliff]]
#[[Shore]]
#[[Bay]]
#[[Brown Puffle Cave]]
*[[April Fools' Party 2011]] and [[April Fools' Party 2012|2012]]:
#[[Desert Dimension]]
#[[Stair Dimension]]
#[[Space Dimension]]
#[[Candy Dimension]]
#[[Cream Soda Dimension]]
#[[Doodle Dimension]]
#[[A Strange Dimension]]
*[[April Fools' Party 2012]]:
#[[Fun House Dimension]]
#[[Orange Dimension]]
*[[Ye Knight's Quest 3]] ([[Medieval Party 2011]] and [[Medieval Party 2012|2012]])
#Chamber of Gems
#Place of Puzzles
#Hall of Hints
#Cave of Battle
#Royal Court
*[[Battle of Doom (Room)|Battle of Doom]] ([[Medieval Party 2011]])
*[[Swamp Maze]] ([[Halloween Party 2011]])
*[[Ninja Headquarters]] ([[Card-Jitsu Party]])
*[[Bakery]] ([[Holiday Party 2011]])
*[[Underwater Expedition]]:
#[[Maze (Underwater Expedition)|Maze]]
#[[The Deep]]
*[[Rockhopper's Quest]]:
#[[Migrator (Party Room)|Migrator]]
#[[Trading Post]]
#[[Dinosaur Island]]
#[[Shipwreck Island]]
#[[Viking Hall]]
*[[Puffle Spa]] ([[Puffle Party 2012]])
*[[Medieval Party 2012]]:
#[[Dragon's Path]]
#[[Bridge]]
#[[Misery Mountain]]
#[[Sky Kingdom]]
*[[Marvel Super Hero Takeover 2012]]:
#[[Downtown]]
#[[Hero HQ]]
#[[Villain Lair]]

==Timeline of Rooms==
{|border="1" cellspacing="0" cellpadding="5" style="border-collapse:collapse"
! Room
! Released
|-
! Colspan=2|Penguin Chat 3
|-
| Town
| March 31, 2005
|-
| Coffee Shop
|  March 31, 2005
|-
| Night Club
|  March 31, 2005
|-
| Boiler Room
| April 7, 2005 (Reopened May 29, 2006)
|-
! colspan=2|2005
|-
| Book Room
|  October 24, 2005 (Beta Testing:  August 22, 2005)
|-
| Dance Lounge
|  October 24, 2005 (Beta Testing:  August 22, 2005)
|-
| Gift Shop
|  October 24, 2005 (Beta Testing:  August 22, 2005)
|-
| Ski Village 
|  October 24, 2005 (Beta Testing:  August 22, 2005)
|-
| Dock
|  October 24, 2005 (Beta Testing:  August 22, 2005)
|-
| Dojo
|  October 24, 2005 (Beta Testing:  August 22, 2005)
|-
| Snow Forts
| October 24, 2005 (Beta Testing:  September 22, 2005)
|-
| HQ
|  October 24, 2005 (Closed: May 27, 2010)
|-
| Sport Shop
|  November 3, 2005 (Closed: May 27, 2010)
|-
| Ski Hill
|  November 3, 2005
|-
| Ski Lodge
|  November 3, 2005
|-
! colspan=2|2006
|-
| Plaza
|  February 26, 2006
|-
| Pizza Parlor
|  February 26, 2006
|-
| Pet shop
|  March 17, 2006
|-
| Iceberg
|  March 26, 2006
|-
| Lodge Attic
|  April 27, 2006
|-
| Cave
|  May 29, 2006
|-
| Mine
|  May 29, 2006
|-
| Mine Shack
|  May 29, 2006
|-
| Beach
|  June 15, 2006
|-
| Lighthouse
|  September 22, 2006
|-
| Beacon
|  September 22, 2006
|-
! colspan=2|2007
|-
| Forest
|  May 25, 2007
|-
| Cove
|  May 29, 2007 (Members:  May 25, 2007)
|-
| Stage
|  November 16, 2007 (Closed:  June 4, 2015)
|-
! colspan=2|2008
|-
| Stadium
|  August 22, 2008
|-
| Dojo Courtyard
|  November 14, 2008
|-
| Ninja Hideout
|  November 17, 2008 (Closed: May 23, 2013)
|-
| Command Room
|  November 25, 2008 (Closed: May 27, 2010)
|- 
! colspan = 2|2009
|-
| Box Dimension
|   April 1, 2009
|-
| Fire Dojo
| November 14, 2009
|-
! colspan = 2|2010
|-
| Cave Mine 
|  January 22, 2010 (Closed: November 6, 2013)
|-
| Hidden Lake 
|  January 22, 2010 (Permanent: March 15, 2010)
|- 
| Underwater Room
|  January 22, 2010 (Permanent: March 15, 2010)
|-
| Recycling Plant
|  April 21, 2010     (Closed: July 11, 2013)
|-
| Everyday Phoning Facility
|  May 27, 2010
|-
| EPF Command Room
|  May 27, 2010
|-
| VR Room
|  June 10, 2010 (Closed: November 15, 2012)
|-
| Water Dojo
|  November 16, 2010
|-
!colspan = 2|2011
|-
| Welcome Room
|  June 27, 2011 (Closed: November 2013)
|-
!colspan = 2|2013
|-
| Puffle Hotel Lobby
|  March 21, 2013 
|-
| Puffle Hotel Spa
|  March 21, 2013
|-
| Puffle Hotel Roof
|  March 21, 2013
|-
| Cloud Forest
|  March 21, 2013 
|-
| Snow Dojo
|  May 23, 2013
|-
| School
|  July 11, 2013
|-
| Gold Mine 
|  November 14, 2013
|-
!colspan = 2|2014
|-
| Puffle Park 
|  April 17, 2014
|-
| Skatepark
|  September 17, 2014
|-
!colspan = 2|2015
|-
| Puffle Wild
| March 25, 2015
|-
| Puffle Berry Mall
| June 4, 2015
|}
[[pt:Lista de Salas]]
