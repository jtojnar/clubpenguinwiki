:''This is about the original hard hat. To see the other hard hats, see [[Hard Hat (disambiguation)]]''.
{{ItemInfobox 
|name= Hard Hat
|image= File:HardHat.png
|available= No
|type= Head Item
|member= Yes<br>No (Unlocked)
|party= None
|cost= 50 [[coins]]
|found= [[Penguin Style]]<br>[[Treasure Book]]
|id= 403 (members)<br>10403 (unlockable)
|unlock= Yes (Series 1, 6)
}}
The '''Hard Hat''' is a head item in ''[[Club Penguin]]''. [[Membership|Members]] are able to purchase this item for 50 [[coin]]s in the [[Penguin Style]] catalog. Similar items are the [[Miner's Helmet]] and [[Red Construction Hat]]. When you dance with it, you will drill with a jackhammer. It is one of the oldest items in Club Penguin history.

==History==
This item is one of the oldest items in ''Club Penguin''. It first appeared in ''[[Penguin Chat 3]]'' as a hidden item, and was re-introduced into ''Club Penguin''. It was an "On The Job" item with the [[Safety Vest]] in February 2008.
===Release history===
{|border="1" class="wikitable" 
!'''Catalog'''!!'''Available from'''!!'''Available until'''
|-
|rowspan="3"|[[Penguin Style]]||August 22, 2005||January 1, 2006
|-
|May 4, 2006||May 4, 2007
|-
|February 1, 2008||March 7, 2008
|-
|[[Treasure Book (Series 1)]]||October 24, 2008||rowspan=2|October 4, 2011
|-
|[[Treasure Book (Series 6)]]||December 15, 2009
|-
|rowspan="4"|Penguin Style||January 1, 2010||February 5, 2010
|-
|June 5, 2009||July 3, 2009
|-
|June 4, 2010||July 2, 2010
|-
|June 11, 2013||August 7, 2013
|}

==Appearances==
*This item is one of the items that are assigned at the top of the Head Items inventory. That's because it's so old.
*[[Rory]] is seen wearing this item. 
*In the 278th and 280th issues of [[The Penguin Times]].
*On the [[Join Team Yellow postcard]].
*On the [[Thanks CPIP postcard]].
==Gallery==
<gallery>
File:HardHat2.png|The Hard Hat in-game.
File:HardHat3.png|The Hard Hat in-game in [[My Penguin]].
File:Hard Hat Drill.gif|A [[penguin]] drilling.
File:Drill theres gold there!!!.png|A penguin (with a black puffle) wearing the [[Safety Vest]] while drilling.
File:HardHat1.PNG|The Hard Hat on a [[player card]].
</gallery>

==Names in other languages==
{{OtherLanguage
|portuguese= Capacete de Proteção
|spanish= Casco
|french= Le Casque de Chantier Jaune
|german= Schutzhelm
|russian= Желтая каска
}}

==SWF==
===403===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/403.swf Hard Hat (icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/403.swf Hard Hat (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/403.swf Hard Hat (paper)]
===10403===
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/icons/10403.swf Hard Hat (unlockable icon)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/sprites/10403.swf Hard Hat (sprites)]
*[http://media1.clubpenguin.com/play/v2/content/global/clothing/paper/10403.swf Hard Hat (paper)]

==See also==
{{Navbox
| name  = Hard Hats
| title = <span style="color:white !important">Hard Hats</span>
| list1 = {{:Hard Hat (disambiguation)}}
}}

[[Category:Clothing]]
[[Category:Head Items]]
[[Category:Treasure Book Items]]
[[Category:Clothes released in 2006]]
