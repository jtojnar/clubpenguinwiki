{{Archive}}
{{PartyInfobox
|name = Star Wars Rebels Takeover
|image= File:StarWarsRebelsTakeoverLogo.PNG
|imagesize = 200px
|caption = Star Wars Rebels Takeover logo.
|membersonly = No
|when = January 21, 2015 - February 4, 2015
|freeitems = ''[[Star Wars Rebels Takeover#Free Items|See list]]''
|wherehappening = [[Club Penguin Island]]
|previous= [[Merry Walrus Party]]
|next= [[SoundStudio Party]]
}}The '''Star Wars Rebels Takeover''' was a party in ''[[Club Penguin]]''. It started on January 21, 2015 and ended on February 4, 2015. This was the first takeover party of 2015 and the second Star Wars themed party after the [[Star Wars Takeover]]. It was based off the [[wikipedia:Lucasfilm|Lucasfilm]] and [[wikipedia:Lucasfilm Animation|Lucasfilm Animation]] series, ''[[wikipedia:Star Wars Rebels|Star Wars Rebels]]'', which premiered in October 2014 on [[wikipedia:Disney Channel|Disney Channel]] and [[wikipedia:Disney XD|Disney XD]].

== History ==
The party was officially confirmed on November 26, 2014 by [[Megg]] on the [[What's New Blog]]<ref>http://www.clubpenguin.com/blog/2014/11/guess-whats-coming-club-penguin-january-2015</ref>. On January 2, 2015, Club Penguin's Facebook page revealed a sneak peek of the Ghost freighter<ref>https://www.facebook.com/clubpenguin/photos/a.305420669583242.1073741826.131799513612026/394271140698194/?type=1</ref>. On January 5, Club Penguin's Facebook header was updated showing some clothes of the party<ref>https://www.facebook.com/clubpenguin/photos/a.171896209602356.20800.131799513612026/395126577279317/?type=1&theater</ref>.

On January 8, advertisements of the party were added on the mainpage and on the login screen. A day later, on January 9, Club Penguin posted a behind the scenes sneak peek video showing some items, tasks, rooms and other features of the party. 

===In-game development===
On January 8, the Ghost became visible from the [[Beacon Telescope]]. A week later, it became closer. On January 15, on the issue #482 of the [[Club Penguin Times]], [[Aunt Arctic]] said that Club Penguin will soon be caught up in a conflict with the Galactic Empire because the Empire rules many planets in the galaxy and they are coming to the island. She also said that the planets they took over are unhappy and there are some rebels waiting for a chance to strike back.

==Free Items==
A total of twenty four free items were made available for this party. Items marked with a badge ([[File:Memberbadge.png|20px]]) indicate that the item can only be obtained by member players.
<gallery>
File:Sabine'sSprayGun.PNG|link=Sabine's Spray Gun|'''[[Sabine's Spray Gun]]'''<br/>After completing Sabine's Task.
File:Ezra'sSlingshot.PNG|link=Ezra's Slingshot|'''[[Ezra's Slingshot]]'''<br/>After completing Ezra's Task.
File:Rebel'sFlag.PNG|link=Rebel's Flag|'''[[Rebel's Flag]]'''<br/>After completing Zeb's Task.
File:Hydrospanner.PNG|link=Hydrospanner|'''[[Hydrospanner]]'''<br/>After completing Hera's Task.
File:JointheCrewBackgroundIcon.PNG|link=Join the Crew background|'''[[Join the Crew background|Join the Crew]]'''<br/>After completing Kanan's Task.
File:SpeederBike.PNG|link=Speeder Bike|[[File:Memberbadge.png|20px|Members only]]  '''[[Speeder Bike]]'''<br/>During The Rebel Journey.
File:PurpleLightsaber.PNG|link=Purple Lightsaber|[[File:Memberbadge.png|20px|Members only]]  '''[[Purple Lightsaber]]'''<br/>During The Rebel Journey.
File:Hera'sBlaster.PNG|link=Hera's Blaster|[[File:Memberbadge.png|20px|Members only]]  '''[[Hera's Blaster]]'''<br/>During The Rebel Journey.
File:DualLightsabers.PNG|link=Dual Lightsabers|[[File:Memberbadge.png|20px|Members only]]  '''[[Dual Lightsabers]]'''<br/>During The Rebel Journey.
File:StarshipIglooIcon.PNG|link=Starship Igloo|[[File:Memberbadge.png|20px|Members only]]  '''[[Starship Igloo]]'''<br/>During The Rebel Journey.
File:TheInquisitor.PNG|link=The Inquisitor|[[File:Memberbadge.png|20px|Members only]] '''[[The Inquisitor]]'''<br/>After defeating The Inquisitor
File:TheInquisitor'sOutfit.PNG|link=The Inquisitor's Outfit|[[File:Memberbadge.png|20px|Members only]] '''[[The Inquisitor's Outfit]]'''<br/>After defeating The Inquisitor
File:TheInquisitor'sLightsaber.PNG|link=The Inquisitor's Lightsaber|[[File:Memberbadge.png|20px|Members only]] '''[[The Inquisitor's Lightsaber]]'''<br/>After defeating The Inquisitor
File:TheSabine.PNG|link=The Sabine|[[File:Memberbadge.png|20px|Members only]]  '''[[The Sabine]]'''<br/>After completing Sabine's Task.
File:Sabine'sOutfit.PNG|link=Sabine's Outfit|[[File:Memberbadge.png|20px|Members only]]  '''[[Sabine's Outfit]]'''<br/>After completing Sabine's Task.
File:TheEzra.PNG|link=The Ezra|[[File:Memberbadge.png|20px|Members only]]  '''[[The Ezra]]'''<br/>After completing Ezra's Task.
File:Ezra'sOutfit.PNG|link=Ezra's Outfit|[[File:Memberbadge.png|20px|Members only]]  '''[[Ezra's Outfit]]'''<br/>After completing Ezra's Task.
File:TheZeb.PNG|link=The Zeb|[[File:Memberbadge.png|20px|Members only]]  '''[[The Zeb]]'''<br/>After completing Zeb's Task.
File:Zeb'sOutfit.PNG|link=Zeb's Outfit|[[File:Memberbadge.png|20px|Members only]] '''[[Zeb's Outfit]]'''<br/>After completing Zeb's Task.
File:TheHera.PNG|link=The Hera|[[File:Memberbadge.png|20px|Members only]] '''[[The Hera]]'''<br/>After completing Hera's Task.
File:Hera'sOutfit.PNG|link=Hera's Outfit|[[File:Memberbadge.png|20px|Members only]] '''[[Hera's Outfit]]'''<br/>After completing Hera's Task.
File:TheKanan.PNG|link=The Kanan|[[File:Memberbadge.png|20px|Members only]]  '''[[The Kanan]]'''<br/>After completing Kanan's Task.
File:Kanan'sOutfit.PNG|link=Kanan's Outfit|[[File:Memberbadge.png|20px|Members only]] '''[[Kanan's Outfit]]'''<br/>After completing Kanan's Task.
File:BlueLightsaber.PNG|link=Blue Lightsaber|[[File:Memberbadge.png|20px|Members only]]  '''[[Blue Lightsaber]]'''<br/>After completing Kanan's Task.
</gallery>

== Gallery ==
===Party Pictures===
<gallery widths="180">
File:StarWarsRebelsTakeoverBeach.PNG|The [[Beach]]
File:StarWarsRebelsTakeoverCove.PNG|The [[Cove]]
File:StarWarsRebelsTakeoverDock.PNG|The [[Dock]]
File:StarWarsRebelsTakeoverForest.PNG|The [[Forest]]
File:StarWarsRebelsTakeoverPlaza.PNG|The [[Plaza]]
File:StarWarsRebelsTakeoverForts.PNG|The [[Snow Forts]]
File:StarWarsRebelsTakeoverParty1.PNG|The [[Space (Star Wars Takeover)|Space]]
File:StarWarsRebelsTakeoverParty2.PNG|The [[The Inquisitor's Base|Inquisitor's Base]]
File:StarWarsRebelsTakeoverTown.PNG|The [[Town]]
</gallery>

===Sneak Peeks===
<gallery widths="180">
File:StarWarsRebelsSP1.JPG|A sneak peek of the Ghost freighter.
</gallery>

===Login Screens===
<gallery widths=180>
File:StarWarsRebelsLoginScreen1.PNG|The [[Login Screen]].
</gallery>

===Miscellaneous===
<gallery widths=180>
File:StarWarsRebelsHomepage1.JPG|[[Club Penguin (website)|Club Penguin's homepage]] (Slide 1).
File:StarWarsRebelsFacebookHeader.PNG|Club Penguin's Facebook header.
File:MapStarWarsRebelsTakeover.PNG|The [[map]] of the island during the party.
</gallery>

==Trivia==
*This was the first party to not have a mascot since the [[Frozen Party]].

==Videos==
<youtube>whbW3-TYRM8</youtube>
<youtube>sWigVsEboxM</youtube>

==Names in other languages==
{{OtherLanguage
|portuguese= Star Wars Rebels - A Invasão
|spanish= Star Wars Rebels - La Invasión
|french= Star Wars Rebels - La Fête
|german= Star Wars Rebels Party
|russian= Звёздные войны: Повстанцы
}}

==Sources and References==
{{reflist}}
{{SWFArchives}}

{{Party}}
[[Category:Parties of 2015]]
[[pt:Star Wars Rebels - A Invasão]]
